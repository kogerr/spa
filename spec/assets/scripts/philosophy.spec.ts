import { initPhilosophy } from '../../../src/assets/scripts/philosophy';

describe('Philosophy script', () => {
    it('should add div elements to main', () => {
        const appendChild = jasmine.createSpy('main.appendChild');
        const main = { appendChild } as unknown as HTMLElement;
        const getElementsByTagName = jasmine.createSpy('getElementsByTagName').and.returnValue([ main ]);
        const setAttribute = jasmine.createSpy('setAttribute');
        const createElement = jasmine.createSpy('document.createElement').and.returnValue({ setAttribute });
        (global as any).document = { createElement, getElementsByTagName };

        initPhilosophy();

        expect(getElementsByTagName).toHaveBeenCalledWith('main');
        expect(appendChild).toHaveBeenCalledWith({ setAttribute, id: 'div100' });
        expect(setAttribute).toHaveBeenCalledTimes(101);
        expect(createElement).toHaveBeenCalledWith('div');
    });
});
