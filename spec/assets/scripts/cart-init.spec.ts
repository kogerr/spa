import * as stateHandler from '../../../src/assets/scripts/state-handler';
import * as cartUpdate from '../../../src/assets/scripts/cart-update';
import { initializeCart } from '../../../src/assets/scripts/cart-init';

describe('Card init script', () => {
    it('should call other methods', () => {
        spyOn(stateHandler, 'initState');
        spyOn(cartUpdate, 'updateOnChange');

        initializeCart();

        expect(stateHandler.initState).toHaveBeenCalledWith();
        expect(cartUpdate.updateOnChange).toHaveBeenCalledWith();
    });
});
