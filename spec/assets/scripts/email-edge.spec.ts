describe('Email send page', () => {
    const event = { preventDefault: () => { } };
    const endpoint = '/admin/api/email';
    let addEventListener: jasmine.Spy;
    let reset: jasmine.Spy;
    let appendChild: jasmine.Spy;
    let removeChild: jasmine.Spy;
    let getElementsByTagName: jasmine.Spy;
    let getElementById: jasmine.Spy;
    let setAttribute: jasmine.Spy;
    let createElement: jasmine.Spy;

    beforeEach(() => {
        addEventListener = jasmine.createSpy('addEventListener').and.callFake((type, callback) => callback(event));
        reset = jasmine.createSpy('form.reset');
        appendChild = jasmine.createSpy('form.appendChild');
        removeChild = jasmine.createSpy('form.removeChild');
        const form = { addEventListener, reset, appendChild, removeChild };
        getElementsByTagName = jasmine.createSpy('getElementsByTagName').and.returnValue([ form ]);
        getElementById = jasmine.createSpy('getElementById').and.returnValues({ value: 'to' }, { value: 'name' });
        setAttribute = jasmine.createSpy('div.setAttribute');
        createElement = jasmine.createSpy('document.createElement').and.returnValue({ innerText: '', setAttribute });
        (global as any).document = { getElementById, getElementsByTagName, createElement };
        (global as any).window = { setTimeout: (handler: () => void, timeout: number) => handler() };
    });

    it('should display success message on success', (done) => {
        const json = jasmine.createSpy('response.json').and.returnValue(Promise.resolve({}));
        const fetchMock = jasmine.createSpy('fetch').and.returnValue(Promise.resolve({ json, status: 200 }));
        (global as any).fetch = fetchMock;

        require('../../../src/assets/scripts/email-edge');

        setTimeout(() => {
            expect(getElementsByTagName).toHaveBeenCalledWith('form');
            expect(getElementById).toHaveBeenCalledWith('to');
            expect(getElementById).toHaveBeenCalledWith('name');
            expect(fetchMock).toHaveBeenCalledWith(endpoint, { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: '{"to":"to","name":"name"}' });
            expect(reset).toHaveBeenCalledWith();
            expect(json).toHaveBeenCalledWith();
            expect(createElement).toHaveBeenCalledWith('div');
            expect(setAttribute).toHaveBeenCalledWith('class', 'success response');
            expect(appendChild).toHaveBeenCalledWith({ innerText: 'Success!', setAttribute });
            expect(removeChild).toHaveBeenCalled();
            done();
        }, 0);
    });

    it('should display error message on error', (done) => {
        const error = 'error';
        const json = jasmine.createSpy('response.json').and.returnValue(Promise.resolve({ error }));
        const fetchMock = jasmine.createSpy('fetch').and.returnValue(Promise.resolve({ json }));
        (global as any).fetch = fetchMock;

        require('../../../src/assets/scripts/email-edge');

        setTimeout(() => {
            expect(getElementsByTagName).toHaveBeenCalledWith('form');
            expect(getElementById).toHaveBeenCalledWith('to');
            expect(getElementById).toHaveBeenCalledWith('name');
            expect(fetchMock).toHaveBeenCalledWith(endpoint, { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: '{"to":"to","name":"name"}' });
            expect(reset).toHaveBeenCalledWith();
            expect(json).toHaveBeenCalledWith();
            expect(createElement).toHaveBeenCalledWith('div');
            expect(setAttribute).toHaveBeenCalledWith('class', 'error response');
            expect(appendChild).toHaveBeenCalledWith({ innerText: 'error', setAttribute });
            done();
        }, 0);
    });

    it('should not display any message when status is not 200 but there is no error message in payload', (done) => {
        const json = jasmine.createSpy('response.json').and.returnValue(Promise.resolve({}));
        const fetchMock = jasmine.createSpy('fetch').and.returnValue(Promise.resolve({ json }));
        (global as any).fetch = fetchMock;

        require('../../../src/assets/scripts/email-edge');

        setTimeout(() => {
            expect(getElementsByTagName).toHaveBeenCalledWith('form');
            expect(getElementById).toHaveBeenCalledWith('to');
            expect(getElementById).toHaveBeenCalledWith('name');
            expect(fetchMock).toHaveBeenCalledWith(endpoint, { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: '{"to":"to","name":"name"}' });
            expect(reset).toHaveBeenCalledWith();
            expect(json).toHaveBeenCalledWith();
            done();
        }, 0);
    });

    afterEach(() => {
        delete require.cache[require.resolve('../../../src/assets/scripts/email-edge')];
        delete (global as any).document;
        delete (global as any).window;
        delete (global as any).fetch;
    });
});
