import { initializeSwitch } from '../../../src/assets/scripts/light-switch';

describe('light switch', () => {
    it('should change body class on click', () => {
        const switchElement = { onclick: null };
        const body = { className: 'light' } as HTMLBodyElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(switchElement);
        (global as any).document = { getElementById, body };
        const getItem = jasmine.createSpy('localstorage.getItem').and.returnValue(null);
        const setItem = jasmine.createSpy('localstorage.setItem');
        (global as any).localStorage = { getItem, setItem };

        initializeSwitch();
        // @ts-ignore
        switchElement.onclick();

        expect(getElementById).toHaveBeenCalledWith('switch');
        expect(body.className).toBe('dark');
        expect(getItem).toHaveBeenCalledWith('switch');
        expect(setItem).toHaveBeenCalledWith('switch', 'dark');
    });

    it('should switch on light when it is dark', () => {
        const switchElement = { onclick: null };
        const body = { className: 'dark' } as HTMLBodyElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(switchElement);
        (global as any).document = { getElementById, body };
        const getItem = jasmine.createSpy('localstorage.getItem').and.returnValue('dark');
        const setItem = jasmine.createSpy('localstorage.setItem');
        (global as any).localStorage = { getItem, setItem };

        initializeSwitch();
        // @ts-ignore
        switchElement.onclick();

        expect(getElementById).toHaveBeenCalledWith('switch');
        expect(body.className).toBe('light');
        expect(getItem).toHaveBeenCalledWith('switch');
        expect(setItem).toHaveBeenCalledWith('switch', 'light');
    });

    it('should initialize dark when user preference is dark', () => {
        const switchElement = { onclick: null };
        const body = { className: 'light' } as HTMLBodyElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(switchElement);
        (global as any).document = { getElementById, body };
        const getItem = jasmine.createSpy('localstorage.getItem').and.returnValue(null);
        (global as any).localStorage = { getItem };
        const matchMedia = jasmine.createSpy('window.matchMedia').and.returnValue({ matches: true });
        (global as any).window = { matchMedia };

        initializeSwitch();

        expect(getElementById).toHaveBeenCalledWith('switch');
        expect(body.className).toBe('dark');
        expect(getItem).toHaveBeenCalledWith('switch');
    });
});
