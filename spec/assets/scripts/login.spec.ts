import { initLogin } from '../../../src/assets/scripts/login';

describe('Login page', () => {
    let appendChild: jasmine.Spy;
    let removeChild: jasmine.Spy;
    let getElementsByTagName: jasmine.Spy;
    let setAttribute: jasmine.Spy;
    let createElement: jasmine.Spy;
    let pushState: jasmine.Spy;

    beforeEach(() => {
        appendChild = jasmine.createSpy('form.appendChild');
        removeChild = jasmine.createSpy('form.removeChild');
        const form = { appendChild, removeChild };
        getElementsByTagName = jasmine.createSpy('getElementsByTagName').and.returnValue([ form ]);
        setAttribute = jasmine.createSpy('div.setAttribute');
        createElement = jasmine.createSpy('document.createElement').and.returnValue({ innerText: '', setAttribute });
        (global as any).document = { createElement, getElementsByTagName };
        const setTimeoutMock = (handler: () => void) => handler();
        pushState = jasmine.createSpy('window.history.pushState');
        const history = { pushState };
        (global as any).window = { setTimeout: setTimeoutMock, history };
    });

    it('should display error message on error', () => {
        (global as any).window.location = { href: 'error=error' };

        initLogin();

        expect(getElementsByTagName).toHaveBeenCalledWith('form');
        expect(createElement).toHaveBeenCalledWith('div');
        expect(setAttribute).toHaveBeenCalledWith('class', 'login-error');
        expect(appendChild).toHaveBeenCalledWith({ innerText: 'error', setAttribute });
        expect(pushState).toHaveBeenCalledWith('', 'Login', '');
        expect(removeChild).toHaveBeenCalled();
    });

    it('should not display error when there is no error in url parameter', () => {
        (global as any).window.location = { href: 'no-url-parameter' };

        initLogin();

        expect(getElementsByTagName).not.toHaveBeenCalled();
    });

    afterEach(() => {
        delete require.cache[require.resolve('../../../src/assets/scripts/login')];
        delete (global as any).document;
        delete (global as any).window;
    });
});
