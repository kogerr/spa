import { getSubPages } from '../../../src/assets/scripts/sub-page.factory';
import { landingInit } from '../../../src/assets/scripts/landing';
import { initPhilosophy } from '../../../src/assets/scripts/philosophy';
import createSpy = jasmine.createSpy;

describe('SubPage Factory', () => {
    it('should compliment stems', () => {
        const path = '/';
        const pages = [{ path, main: 'main', title: 'landing' }];
        const json = jasmine.createSpy('response.json').and.returnValue(Promise.resolve(pages));
        const fetchMock = jasmine.createSpy('fetch').and.returnValue(Promise.resolve({ json, status: 200 }));
        (global as any).fetch = fetchMock;
        const main = { innerHTML: '' } as unknown as HTMLElement;
        const getElementsByTagName = jasmine.createSpy('getElementsByTagName').and.returnValue([ main ]);
        (global as any).document = { getElementsByTagName };
        const stems = [
            { pathMatcher: /^\/$/, title: 'landing', init: landingInit },
            { pathMatcher: /^\/philosophy$/, title: 'Philosophy', init: initPhilosophy }
        ];

        getSubPages(stems).then((actual) => {
            const landing = actual[0];
            const scroll = createSpy();
            (global as any).scroll = scroll;
            expect(landing.path).toEqual(path);
            expect(landing.title).toEqual('landing');
            expect(landing.init).toEqual(landingInit);
            landing.setUp();
            expect(main.innerHTML).toEqual('main');

            const philosophy = actual[1];
            expect(philosophy.path).toEqual('');
            expect(philosophy.title).toEqual('Philosophy');
            expect(philosophy.init).toEqual(initPhilosophy);
            philosophy.setUp();
            expect(main.innerHTML).toEqual('');

            expect(fetchMock).toHaveBeenCalledWith('/api/pages');
            expect(json).toHaveBeenCalled();
            expect(getElementsByTagName).toHaveBeenCalledWith('main');
            expect(scroll).toHaveBeenCalledWith(0, 0);
        });
    });
});
