import { initSlides } from '../../../src/assets/scripts/carousel';

describe('Cart', () => {

    const slide = { index: 0, src: 'src' };

    it('should add unique items', () => {
        const append = jasmine.createSpy('append');
        const appendChild = jasmine.createSpy('appendChild');
        const removeChild = jasmine.createSpy('removeChild');
        const carouselElement = { innerHTML: '', children: [], append, removeChild, appendChild };
        const onclick = jasmine.createSpy('onclick');
        const button = { onclick } as unknown as HTMLDivElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValues(carouselElement, button, button);
        (global as any).document = { getElementById };
        const json = jasmine.createSpy('json').and.returnValue(Promise.resolve([ slide ]));
        (global as any).fetch = jasmine.createSpy('fetch').and.returnValue(Promise.resolve({ json }));

        initSlides('carouselId', (x) => ({ style: {} } as HTMLElement), [ slide ]);
    });
});
