import { initNewsFeed } from '../../../src/assets/scripts/newsfeed';

describe('Newsfeed', () => {

    it('should display newsfeed', () => {
        const addEventListener = jasmine.createSpy('addEventListener').and.callFake((type, callback) => {
            console.log();
        });
        const appendChild = jasmine.createSpy('form.appendChild');
        const element = { addEventListener, appendChild } as any as HTMLElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(element);
        const createElement = jasmine.createSpy('document.createElement').and.returnValue({ innerText: '' });
        (global as any).document = { getElementById, createElement };
        initNewsFeed();
    });
});
