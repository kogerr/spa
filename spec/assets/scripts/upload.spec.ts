describe('Upload script', () => {
    it('should add listeners', () => {
        const file = { name: 'filename.woff', size: 1 };
        const input = { onchange: (event: any) => {}, files: [ file ], innerText: '', src: '', className: '', alt: '' };
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(input);
        const appendChild = jasmine.createSpy('body.appendChild');
        const body = { appendChild };
        const json = () => Promise.resolve({ success: true, path: 'path' });
        const response = Promise.resolve({ json, status: 200 });
        (global as any).fetch = jasmine.createSpy('fetch').and.returnValue(response);
        const anchor = { href: '', target: '', innerText: '', src: ''};
        const createElement = jasmine.createSpy('createElement').and.returnValue(anchor);
        (global as any).document = { getElementById, createElement, body };

        require('../../../src/assets/scripts/upload');
        input.onchange({ target: input } as unknown as Event);
    });

    it('should add listeners when status is 409', () => {
        const file = { name: 'filename.woff', size: 1 };
        const input = { onchange: (event: any) => {}, files: [ file ], innerText: '', src: '', className: '', alt: '' };
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(input);
        const appendChild = jasmine.createSpy('body.appendChild');
        const body = { appendChild };
        const json = () => Promise.resolve({ success: true, path: 'path' });
        const response = Promise.resolve({ json, status: 409 });
        (global as any).fetch = jasmine.createSpy('fetch').and.returnValue(response);
        const anchor = { href: '', target: '', innerText: '', src: ''};
        const createElement = jasmine.createSpy('createElement').and.returnValue(anchor);
        (global as any).document = { getElementById, createElement, body };

        require('../../../src/assets/scripts/upload');
        input.onchange({ target: input } as unknown as Event);
    });

    it('should add listeners when status is 500', () => {
        const file = { name: 'filename.woff', size: 1 };
        const input = { onchange: (event: any) => {}, files: [ file ], innerText: '', src: '', className: '', alt: '' };
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(input);
        const appendChild = jasmine.createSpy('body.appendChild');
        const body = { appendChild };
        const json = () => Promise.resolve({ success: true, path: 'path' });
        const response = Promise.resolve({ json, status: 500 });
        (global as any).fetch = jasmine.createSpy('fetch').and.returnValue(response);
        const anchor = { href: '', target: '', innerText: '', src: ''};
        const createElement = jasmine.createSpy('createElement').and.returnValue(anchor);
        (global as any).document = { getElementById, createElement, body };

        require('../../../src/assets/scripts/upload');
        input.onchange({ target: input } as unknown as Event);
    });

    it('should add listeners when there are no files', () => {
        const input = { onchange: (event: any) => {}, files: [] };
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(input);
        const appendChild = jasmine.createSpy('body.appendChild');
        const body = { appendChild };
        const json = () => Promise.resolve({ success: true, path: 'path' });
        const response = Promise.resolve({ json, status: 500 });
        (global as any).fetch = jasmine.createSpy('fetch').and.returnValue(response);
        const anchor = { href: '', target: '', innerText: '', src: ''};
        const createElement = jasmine.createSpy('createElement').and.returnValue(anchor);
        (global as any).document = { getElementById, createElement, body };

        require('../../../src/assets/scripts/upload');
        input.onchange({} as unknown as Event);
    });
});
