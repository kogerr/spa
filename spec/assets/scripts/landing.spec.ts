import * as carousel from '../../../src/assets/scripts/carousel';
import { landingInit } from '../../../src/assets/scripts/landing';

describe('Landing page', () => {
    const slide = { index: 0, src: 'src', link: 'link', alt: 'alt' };
    const previewHover = 'previewHover';

    it('should add listeners to add buttons', (done) => {
        const insertBefore = jasmine.createSpy('insertBefore');
        const setAttribute = jasmine.createSpy('setAttribute');
        const appendChild = jasmine.createSpy('appendChild');
        const wrapper = { insertBefore, children: [ { setAttribute } ] };
        const hoverImage = { outerHTML: '', setAttribute, appendChild, alt: '', href: '', src: '' };
        const getElementsByClassName = jasmine.createSpy('getElementsByClassName').and.returnValue([ wrapper ]);
        const createElement = jasmine.createSpy('createElement').and.returnValue(hoverImage);
        (global as any).document = { getElementsByClassName, createElement };
        const hoverImages = [ { index: 0, previewHover } ];
        const json = jasmine.createSpy('response.json').and.returnValue(Promise.resolve(hoverImages));
        const fetchMock = jasmine.createSpy('fetch').and.returnValue(Promise.resolve({ json, status: 200 }));
        (global as any).fetch = fetchMock;
        const capture = (carouselId: string, convert: any, slides: any[]) => { convert(slide); return 0 as unknown as NodeJS.Timeout; };
        spyOn(carousel, 'initSlides').and.callFake(capture);

        landingInit().then(() => {
            expect(insertBefore).toHaveBeenCalledWith(hoverImage, undefined);
            expect(setAttribute).toHaveBeenCalledWith('class', 'hover-image');
            expect(setAttribute).toHaveBeenCalledWith('class', 'hover-image');
            expect(getElementsByClassName).toHaveBeenCalledWith('preview-wrapper');
            expect(createElement).toHaveBeenCalledWith('svg');
            expect(json).toHaveBeenCalledWith();
            expect(fetchMock).toHaveBeenCalledWith('/api/hover-images');
            expect(carousel.initSlides).toHaveBeenCalled();
            done();
        });
    });
});
