import { navigate, navigateOnPopState } from '../../../src/assets/scripts/navigate';
import { SubPage } from '../../../src/assets/scripts/subpage.interface';

describe('Navigate', () => {
    it('should change title, history and call callbacks', () => {
        const pathMatcher = /path/;
        const title = 'title';
        const path = 'path';
        const setUp = jasmine.createSpy('setUp');
        const init = jasmine.createSpy('init');
        const subPage: SubPage = { title, path, pathMatcher, setUp, init };
        const pushState = jasmine.createSpy('window.history.pushState');
        const history = { pushState };
        (global as any).window = { history };
        (global as any).document = { title: null };

        navigate(subPage);

        expect(setUp).toHaveBeenCalled();
        expect(document.title).toBe(title);
        expect(pushState).toHaveBeenCalledWith('', title, path);
        expect(init).toHaveBeenCalled();
    });

    it('should navigate on popstate event', () => {
        const title = 'title';
        const path = 'path';
        const pathMatcher = /path/;
        const setUp = jasmine.createSpy('setUp');
        const init = jasmine.createSpy('init');
        const subPage: SubPage = { title, path, pathMatcher, setUp, init };
        (global as any).window = { onpopstate: (event: PopStateEvent) => {} };
        const location = { pathname: 'path?param=1' };
        (global as any).document = { location, title: '' };

        const actual = navigateOnPopState([ subPage ]);
        (global as any).window.onpopstate({} as unknown as PopStateEvent);

        expect(actual).toEqual([ subPage ]);
        expect(setUp).toHaveBeenCalled();
        expect(document.title).toBe(title);
        expect(init).toHaveBeenCalled();
    });

    it('should not navigate on popstate event when current page is not found', () => {
        const title = 'title';
        const path = 'different path';
        const pathMatcher = /different path/;
        const setUp = jasmine.createSpy('setUp');
        const init = jasmine.createSpy('init');
        const subPage: SubPage = { title, path, pathMatcher, setUp, init };
        (global as any).window = { onpopstate: (event: PopStateEvent) => {} };
        const location = { pathname: 'path?param=1' };
        (global as any).document = { location, title: '' };

        const actual = navigateOnPopState([ subPage ]);
        (global as any).window.onpopstate({} as unknown as PopStateEvent);

        expect(actual).toEqual([ subPage ]);
        expect(setUp).not.toHaveBeenCalled();
        expect(document.title).toBe('');
        expect(init).not.toHaveBeenCalled();
    });
});
