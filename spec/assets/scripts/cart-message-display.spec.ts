import { displayCartMessage } from '../../../src/assets/scripts/cart-message-display';

describe('Cart Message Display', () => {
    it('should display message', () => {
        const message = 'something has been added';
        const cartMessageLine = { innerText: '', style: { display: '' } };
        const getElementById = jasmine.createSpy('getElementById').and.returnValue(cartMessageLine);
        (global as any).document = { getElementById };
        let listener = () => {};
        const st = global.setTimeout;
        (global as any).setTimeout = (handler: () => void, timeout: number) => listener = handler;

        displayCartMessage(message);
        expect(getElementById).toHaveBeenCalledWith('message-line');
        expect(cartMessageLine.innerText).toBe(message);
        expect(cartMessageLine.style.display).toBe('block');

        listener();
        expect(cartMessageLine.innerText).toBe('');
        expect(cartMessageLine.style.display).toBe('none');
        global.setTimeout = st;
    });
});
