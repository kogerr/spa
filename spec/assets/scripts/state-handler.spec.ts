import { addStateListener, addObject, clearObjects, getObjects,
    initState, removeObjects, hasObject, setField } from '../../../src/assets/scripts/state-handler';

describe('StateHandler', () => {
    const product = { bundle: 'full', family: 'family', license: 0, platform: 0, price: 1 };

    it('should add unique products', () => {
        const listener = jasmine.createSpy('listener');
        const setItem = jasmine.createSpy('localstorage.setItem');
        (global as any).localStorage = { setItem };

        addStateListener('products', listener);
        const actual1 = addObject('products', product);
        const actual2 = addObject('products', product);

        expect(setItem).toHaveBeenCalledWith('products', JSON.stringify([product]));
        expect(actual1).toBe(true);
        expect(actual2).toBe(false);
        expect(getObjects('products')).toEqual([ product ]);
        expect(listener).toHaveBeenCalledWith([ product ]);
    });

    it('should remove products', () => {
        const listener = jasmine.createSpy('listener');
        const setItem = jasmine.createSpy('localstorage.setItem');
        (global as any).localStorage = { setItem };
        expect(getObjects('products')).toEqual([ product ]);

        addStateListener('products', listener);
        const actual1 = removeObjects('products', product);
        const actual2 = removeObjects('products', { ...product, family: 'invalid family' });

        expect(actual1).toBe(true);
        expect(actual2).toBe(false);
        expect(getObjects('products')).toEqual([]);
        expect(setItem).toHaveBeenCalledWith('products', '[]');
        expect(listener).toHaveBeenCalledWith([]);
    });

    it('should initialize', () => {
        const listener = jasmine.createSpy('listener');
        addStateListener('products', listener);
        const setItem = jasmine.createSpy('localstorage.setItem');
        (global as any).localStorage = { setItem, products: JSON.stringify([ product ]) };

        initState();

        expect(getObjects('products')).toEqual([ product ]);
        expect(setItem).toHaveBeenCalledWith('items', '[]');
        expect(setItem).toHaveBeenCalledWith('license', '1');
        expect(listener).toHaveBeenCalledWith([ product ]);
    });

    it('should not load from localStorage if nothing is there', () => {
        const setItem = jasmine.createSpy('localstorage.setItem');
        (global as any).localStorage = { setItem };

        initState();

        expect(getObjects('products')).toEqual([ product ]);
    });

    it('should not load from localStorage if nothing is there', () => {
        const listener = jasmine.createSpy('listener');
        const setItem = jasmine.createSpy('localstorage.setItem');
        (global as any).localStorage = { setItem };
        addStateListener('products', listener);
        addObject('products', product);

        clearObjects('products');

        expect(listener).toHaveBeenCalledWith([]);
        expect(setItem).toHaveBeenCalledWith('products', '[]');
    });

    it('should tell if it has an object', () => {
        // WHEN state is empty

        const actual = hasObject('products', { license : 1 });

        expect(actual).toEqual(false);
    });

    it('setField should set the value of a field', () => {
        setField('license', 250);

        const actual = getObjects<number>('license');

        expect(actual).toEqual(250);
    });
});
