import { getSubpageStems } from '../../../src/assets/scripts/sub-page-stem.provider';
import { landingInit } from '../../../src/assets/scripts/landing';
import { initPhilosophy } from '../../../src/assets/scripts/philosophy';
import { initNewsFeed } from '../../../src/assets/scripts/newsfeed';
import { initLogin } from '../../../src/assets/scripts/login';
import { initCheckout } from '../../../src/assets/scripts/checkout';
import { initPaymentResult } from '../../../src/assets/scripts/payment-result';
import { initFontPage } from '../../../src/assets/scripts/init-font-page';
import { initCartPage } from '../../../src/assets/scripts/init-cart-page';

describe('SubPage stem provider', () => {
    it('should provide stems', () => {
        const actual = getSubpageStems();

        const expected = [
            { pathMatcher: /^\/$/, title: 'koger.io', init: landingInit },
            { pathMatcher: /^\/philosophy$/, title: 'Philosophy', init: initPhilosophy },
            { pathMatcher: /^\/newsfeed$/, title: 'Newsfeed', init: initNewsFeed },
            { pathMatcher: /^\/login.?/, title: 'Login', init: initLogin },
            { pathMatcher: /^\/registration$/, title: 'Registration', init: initLogin },
            { pathMatcher: /^\/checkout$/, title: 'Checkout', init: initCheckout },
            { pathMatcher: /^\/payment-result.?/, title: 'Payment Result', init: initPaymentResult },
            { pathMatcher: /^\/fonts\/.+/, title: 'font', init: initFontPage },
            { pathMatcher: /^\/cart$/, title: 'cart', init: initCartPage },
        ];

        expect(actual).toEqual(expected);
    });
});
