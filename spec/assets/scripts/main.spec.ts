import * as stemProvider from '../../../src/assets/scripts/sub-page-stem.provider';
import * as navigator from '../../../src/assets/scripts/navigate';
import * as subPageFactory from '../../../src/assets/scripts/sub-page.factory';
import * as menu from '../../../src/assets/scripts/menu';
import { landingInit } from '../../../src/assets/scripts/landing';
import * as cartInit from '../../../src/assets/scripts/cart-init';
import * as lightSwitch from '../../../src/assets/scripts/light-switch';
import { init } from '../../../src/assets/scripts/main';
import { SubPage } from '../../../src/assets/scripts/subpage.interface';

describe('Main', () => {

    const landing = { pathMatcher: /\//, title: 'landing', init: landingInit };

    it('should initialize current page and subpage downloads', (done) => {
        const subPages: SubPage[] = [ { pathMatcher: /\//, path: '/', title: 'landing', init: landingInit, setUp: landingInit } ];
        spyOn(stemProvider, 'getSubpageStems').and.returnValue([landing]);
        const location = { pathname: '/' };
        (global as any).document = { location, onload: null };
        (global as any).window = { onload: null };
        spyOn(navigator, 'initStem');
        spyOn(subPageFactory, 'getSubPages').and.returnValue(Promise.resolve(subPages));
        spyOn(navigator, 'navigateOnPopState').and.returnValue(subPages);
        spyOn(menu, 'initMenu');
        spyOn(cartInit, 'initializeCart');
        spyOn(lightSwitch, 'initializeSwitch');

        init().then(() => {
            expect(stemProvider.getSubpageStems).toHaveBeenCalled();
            expect(navigator.initStem).toHaveBeenCalledWith(landing);
            expect(subPageFactory.getSubPages).toHaveBeenCalledWith([landing]);
            expect(navigator.navigateOnPopState).toHaveBeenCalledWith(subPages);
            expect(menu.initMenu).toHaveBeenCalledWith(subPages);
            // @ts-ignore
            expect(cartInit.initializeCart).toHaveBeenCalledWith();
            expect(lightSwitch.initializeSwitch).toHaveBeenCalled();
            done();
        });
    });

    it('should not initialize current page when location does not match a page', (done) => {
        spyOn(stemProvider, 'getSubpageStems').and.returnValue([landing]);
        const location = { pathname: 'faulty' };
        (global as any).document = { location, onload: null };
        (global as any).window = { onload: null };
        spyOn(navigator, 'initStem');
        spyOn(subPageFactory, 'getSubPages').and.returnValue(Promise.resolve([]));
        spyOn(menu, 'initMenu');
        spyOn(cartInit, 'initializeCart');
        spyOn(lightSwitch, 'initializeSwitch');

        init().then(() => {
            expect(stemProvider.getSubpageStems).toHaveBeenCalled();
            expect(navigator.initStem).not.toHaveBeenCalled();
            expect(subPageFactory.getSubPages).toHaveBeenCalledWith([landing]);
            // @ts-ignore
            expect(cartInit.initializeCart).toHaveBeenCalledWith();
            expect(lightSwitch.initializeSwitch).toHaveBeenCalled();
            done();
        });
    });

});
