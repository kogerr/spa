describe('Preload script', () => {
    const imageName = 'image.png';
    const fontName = 'webfont.woff2';
    const rel = 'preload';

    it('should add link elements for assets', (done) => {
        const images = [ imageName ];
        const fonts = [ fontName ];
        const assets: { images: string[], fonts: string[] } = { images, fonts };
        const json = () => Promise.resolve(assets);
        const response = Promise.resolve({ json });
        const fetch = jasmine.createSpy('fetch').and.returnValue(response);
        (global as any).fetch = fetch;
        const imageLink = {};
        const fontLink = {};
        const createElement = jasmine.createSpy('document.createElement').and.returnValues(imageLink, fontLink);
        const appendChild = jasmine.createSpy('body.appendChild');
        (global as any).document = { createElement, body: { appendChild } };

        require('../../../src/assets/scripts/preload');

        const assert = () => {
            expect(fetch).toHaveBeenCalledWith('/api/preload');
            expect(createElement).toHaveBeenCalledWith('link');
            expect(appendChild).toHaveBeenCalledWith(imageLink);
            expect(createElement).toHaveBeenCalledWith('link');
            expect(appendChild).toHaveBeenCalledWith(fontLink);
            expect(imageLink).toEqual({ rel, href: imageName, as: 'image' });
            expect(fontLink).toEqual({ rel, href: fontName, as: 'font' });
            done();
        };
        setTimeout(assert, 10);
    });
});
