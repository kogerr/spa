import { updateOnChange } from '../../../src/assets/scripts/cart-update';
import * as stateHandler from '../../../src/assets/scripts/state-handler';

describe('Cart update', () => {
    const product = { bundle: 'full', family: 'family', license: 0, platform: 0, price: 1000 };

    it('should add event listeners', () => {
        const cartSum = { innerText: '' };
        const cartPreview = { style: { display: '' } } as HTMLSpanElement;
        const plusElement = { style: { display: '' } } as HTMLSpanElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValues(cartSum, cartPreview, plusElement);
        (global as any).document = { getElementById, location: { pathname: '/location' } };
        spyOn(stateHandler, 'addStateListener').and.callFake((field, listener) => listener([product]));
        spyOn(stateHandler, 'getObjects').and.returnValue([product]);

        updateOnChange();

        expect(getElementById).toHaveBeenCalledWith('cart-sum');
        expect(getElementById).toHaveBeenCalledWith('cart-preview');
        expect(getElementById).toHaveBeenCalledWith('cart-plus');
        expect(stateHandler.addStateListener).toHaveBeenCalled();
        expect(cartSum.innerText).toBe('1000 €');
        expect(cartPreview.style.display).toBe('block');
        expect(plusElement.style.display).toBe('inline');
    });

    it('should display 2 items when there are two items added', () => {
        const cartSum = { innerText: '' };
        const cartPreview = { style: { display: '' } } as HTMLSpanElement;
        const plusElement = { style: { display: '' } } as HTMLSpanElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValues(cartSum, cartPreview, plusElement);
        (global as any).document = { getElementById, location: { pathname: '/location' } };
        spyOn(stateHandler, 'addStateListener').and.callFake((field, listener) => listener([ product, product ]));
        spyOn(stateHandler, 'getObjects').and.returnValue([ product, product ]);

        updateOnChange();

        expect(getElementById).toHaveBeenCalledWith('cart-sum');
        expect(getElementById).toHaveBeenCalledWith('cart-preview');
        expect(getElementById).toHaveBeenCalledWith('cart-plus');
        expect(stateHandler.addStateListener).toHaveBeenCalled();
        expect(cartSum.innerText).toBe('2000 €');
        expect(cartPreview.style.display).toBe('block');
        expect(plusElement.style.display).toBe('inline');
    });

    it('should hide cart preview when there are no items', () => {
        const cartSum = { innerText: '' };
        const cartPreview = { style: { display: '' } } as HTMLSpanElement;
        const plusElement = { style: { display: '' } } as HTMLSpanElement;
        const getElementById = jasmine.createSpy('getElementById').and.returnValues(cartSum, cartPreview, plusElement);
        (global as any).document = { getElementById, location: { pathname: '/location' } };
        spyOn(stateHandler, 'addStateListener').and.callFake((field, listener) => listener([]));
        spyOn(stateHandler, 'getObjects').and.returnValue([]);

        updateOnChange();

        expect(getElementById).toHaveBeenCalledWith('cart-sum');
        expect(getElementById).toHaveBeenCalledWith('cart-preview');
        expect(getElementById).toHaveBeenCalledWith('cart-plus');
        expect(stateHandler.addStateListener).toHaveBeenCalled();
        expect(cartSum.innerText).toBe('0 €');
        expect(cartPreview.style.display).toBe('none');
        expect(plusElement.style.display).toBe('none');
    });
});
