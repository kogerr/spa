import { initPaymentResult } from '../../../src/assets/scripts/payment-result';
import * as stateHandler from '../../../src/assets/scripts/state-handler';

describe('Payment Result script', () => {
    it('should clear cart when result is success', () => {
        const search = '?r=eyJyIjowLCJ0Ijo1MDA3OTE0NzUsImUiOiJTVUNDRVNTIiwibSI6IlBVQkxJQ1RFU1RIVUYiLCJvIjoidHBycTJtcyJ9&s=s';
        (global as any).window = { location: { search } };
        (global as any).atob = (data: string) => Buffer.from(data, 'base64').toString('ascii');
        spyOn(stateHandler, 'clearObjects');

        initPaymentResult();

        expect(stateHandler.clearObjects).toHaveBeenCalledWith('products');
        expect(stateHandler.clearObjects).toHaveBeenCalledWith('items');
    });

    it('should not clear cart when result is failure', () => {
        const search = '?r=eyJyIjoyMDEzLCJ0Ijo1MDA3OTEzNTQsImUiOiJGQUlMIiwibSI6IlBVQkxJQ1RFU1RIVUYiLCJvIjoidHBvN2tjNSJ9&s=a6bwz21aV004lAO';
        (global as any).window = { location: { search } };
        (global as any).atob = (data: string) => Buffer.from(data, 'base64').toString('ascii');
        spyOn(stateHandler, 'clearObjects');

        initPaymentResult();

        expect(stateHandler.clearObjects).not.toHaveBeenCalled();
    });

    it('should not clear cart when there is no result url parameter', () => {
        const search = '?invalid=url&para=meters';
        (global as any).window = { location: { search } };
        spyOn(stateHandler, 'clearObjects');

        initPaymentResult();

        expect(stateHandler.clearObjects).not.toHaveBeenCalled();
    });
});
