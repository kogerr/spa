import { initFontPage } from '../../../src/assets/scripts/init-font-page';
import * as carousel from '../../../src/assets/scripts/carousel';
import * as testerFactory from '../../../src/assets/scripts/create-tester';
import * as showcase from '../../../src/assets/scripts/showcase';

describe('init font page', () => {

    const slide = { index: 0, src: 'src' };

    it('should init font page', (done) => {
        const subpageName = 'subpageName';
        (global as any).window = { location: { pathname: 'a/b/' + subpageName } };
        const json = jasmine.createSpy('json').and.returnValue(Promise.resolve({ fonts: [], texts: [], slides: [ slide ] }));
        (global as any).fetch = jasmine.createSpy('fetch').and.returnValue(Promise.resolve({ json }));
        const img = { alt: '', src: '' };
        const createElement = jasmine.createSpy('createElement').and.returnValue(img);
        (global as any).document = { createElement };
        spyOn(testerFactory, 'createTesters').and.returnValue(Promise.resolve([]))
        const capture = (carouselId: string, convert: any, slides: any[]) => { convert(slide); return 0 as unknown as NodeJS.Timeout; };
        spyOn(carousel, 'initSlides').and.callFake(capture);
        spyOn(showcase, 'initShowcase');

        initFontPage().then(() => {
            expect(testerFactory.createTesters).toHaveBeenCalledWith([], []);
            expect(carousel.initSlides).toHaveBeenCalled();
            expect(createElement).toHaveBeenCalledWith('img');
            expect(img).toEqual({ alt: 'alt', src: 'src' });
            expect(showcase.initShowcase).toHaveBeenCalledWith(subpageName, []);
            done();
        });
    });
});
