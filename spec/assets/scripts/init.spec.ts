import * as main from '../../../src/assets/scripts/main';

describe('init script', () => {
    it('should call init method from main', () => {
        spyOn(main, 'init');

        require('../../../src/assets/scripts/init');

        expect(main.init).toHaveBeenCalled();
    });
});
