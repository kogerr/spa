import { initMenu } from '../../../src/assets/scripts/menu';
import { SubPage } from '../../../src/assets/scripts/subpage.interface';
import * as navigation from '../../../src/assets/scripts/navigate';
import { landingInit } from '../../../src/assets/scripts/landing';

describe('Menu', () => {

    const landing = { pathMatcher: /\//, path: '/', title: 'landing', init: landingInit, setUp: landingInit };

    it('should add navigation events to anchors', () => {
        const subPages: SubPage[] = [ landing ];
        const anchors = [
            { onclick: null, pathname: '/' } as HTMLAnchorElement,
            { onclick: null, pathname: 'withoutBackslash' } as HTMLAnchorElement,
            { onclick: null, pathname: '/nonexistent' } as HTMLAnchorElement,
            { onclick: null, pathname: '' } as HTMLAnchorElement
        ];
        const getElementsByTagName = jasmine.createSpy('document.getElementsByTagName').and.returnValue(anchors);
        (global as any).document = { getElementsByTagName };
        spyOn(navigation, 'navigate');

        initMenu(subPages);
        expect(document.getElementsByTagName).toHaveBeenCalledWith('a');

        const preventDefault = jasmine.createSpy('a.preventDefault');
        const mouseEvent = {preventDefault} as unknown as MouseEvent;
        // @ts-ignore
        anchors[0].onclick(mouseEvent);
        expect(navigation.navigate).toHaveBeenCalledWith(landing);
        expect(preventDefault).toHaveBeenCalledWith();
    });
});
