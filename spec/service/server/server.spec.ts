import * as http from 'http';
import * as http2 from 'http2';
import { Http2SecureServer } from 'http2';
import { Credentials } from '../../../src/models/credentials.interface';
import * as credentialProvider from '../../../src/service/server/credential.provider';
import { httpsRedirect } from '../../../src/service/server/https-redirect.provider';
import * as routing from '../../../src/service/server/routing';
import * as prePushHandler from '../../../src/service/server/prepush.handler';
import { getCredentials, initServer } from '../../../src/service/server/server';

describe('server', () => {

    beforeEach((done) => {
        spyOn(console, 'log');
        setTimeout(() => done(), 100);
    });

    it('should create http server if credentials are undefined', async () => {
        const $credentials = Promise.resolve(undefined);
        const httpServer = { listen: (port: number, cb: any) => cb(), on: (event: string) => {} } as unknown as http.Server;
        spyOn(credentialProvider, 'readCredentials').and.returnValue($credentials);
        spyOn(http, 'createServer').and.returnValue(httpServer);
        spyOn(http2, 'createSecureServer');

        await initServer();

        expect(credentialProvider.readCredentials).toHaveBeenCalledWith();
        expect(http.createServer).toHaveBeenCalledWith({}, jasmine.any(Function));
        expect(console.log).toHaveBeenCalledWith('Listening on port 80');
        expect(http2.createSecureServer).not.toHaveBeenCalled();
        expect(getCredentials()).toEqual(undefined);
    });

    it('should create https server', async () => {
        const credentials = {key: 'key', ca: 'ca', cert: 'cert'} as Credentials;
        const listen = jasmine.createSpy().and.callFake((port, cb) => cb());
        const on = jasmine.createSpy('server::on');
        const httpServer = { listen, on } as unknown as http.Server;
        const http2Server = { listen, on } as unknown as Http2SecureServer;
        spyOn(credentialProvider, 'readCredentials').and.returnValue(Promise.resolve(credentials));
        spyOn(http, 'createServer').and.returnValue(httpServer);
        spyOn(http2, 'createSecureServer').and.returnValue(http2Server);
        spyOn(prePushHandler, 'prePushAssets');
        const route = spyOn(routing, 'route');

        await initServer();

        expect(credentialProvider.readCredentials).toHaveBeenCalled();
        expect(http.createServer).toHaveBeenCalledWith({}, httpsRedirect);
        expect(http2.createSecureServer).toHaveBeenCalledWith({ allowHTTP1: true, ...credentials }, route);
        expect(console.log).toHaveBeenCalledWith('Listening on port 80');
        expect(console.log).toHaveBeenCalledWith('Listening on port 443');
        expect(listen).toHaveBeenCalledWith(443, jasmine.any(Function));
        expect(listen).toHaveBeenCalledWith(80, jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('error', jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('error', jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('stream', prePushHandler.prePushAssets);
        expect(getCredentials()).toEqual(credentials);
    });
});
