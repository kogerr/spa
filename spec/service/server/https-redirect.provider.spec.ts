import { IncomingMessage, OutgoingHttpHeaders, ServerResponse } from 'http';
import { httpsRedirect } from '../../../src/service/server/https-redirect.provider';

describe('redirect provider', () => {
    const redirectStatusCode = 301;

    it('should redirect to https', () => {
        const url = 'url';
        const host = 'host';
        const request = { headers: { host }, url } as IncomingMessage;
        const end = jasmine.createSpy() as () => void;
        const writeHead = jasmine.createSpy() as (status: number, arg: OutgoingHttpHeaders) => void;
        const response = { writeHead, end } as ServerResponse;

        httpsRedirect(request, response);

        expect(writeHead).toHaveBeenCalledWith(redirectStatusCode, { Location: 'https://' + host + url });
        expect(end).toHaveBeenCalledWith();
    });
});
