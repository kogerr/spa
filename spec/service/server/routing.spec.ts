import { OutgoingHttpHeaders } from 'http';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { Asset } from '../../../src/models/asset.interface';
import { RequestHandler } from '../../../src/models/types';
import * as authHandler from '../../../src/service/server/atuh.handler';
import * as container from '../../../src/service/container';
import { controllerRoutes } from '../../../src/service/controllers/controller-mapping';
import { mimeTypes } from '../../../src/service/providers/mime-types';
import { route } from '../../../src/service/server/routing';
import * as fileReader from '../../../src/service/util/file-reader';

describe('routing', () => {
    let request: Http2ServerRequest;
    let response: Http2ServerResponse;
    let end: (arg?: string | Buffer, encoding?: string) => void;
    let setHeader: (name: string, value: string) => void;
    let writeHead: (status: number, arg: OutgoingHttpHeaders) => void;

    beforeEach(() => {
        end = jasmine.createSpy('response.end') as (arg?: string | Buffer, encoding?: string) => void;
        setHeader = jasmine.createSpy('response.setHeader') as (name: string, value: string) => void;
        writeHead = jasmine.createSpy('response.writeHead') as (status: number, arg: OutgoingHttpHeaders) => void;
        request = jasmine.createSpyObj<Http2ServerRequest>([ 'url' ]);
        response = { statusCode: 0, setHeader, writeHead, end } as Http2ServerResponse;
        spyOn(authHandler, 'redirectIfUnauthorized').and.returnValue((a: RequestHandler) => a(request, response));
    });

    it('should return 404 if it does not find page', () => {
        request = { url: 'asdf?a=b' } as Http2ServerRequest;
        const page = '<h1>Page not found</h1>';
        spyOn(container.fullPages, 'get').and.returnValue(page);

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(response.statusCode).toBe(404);
        expect(setHeader).toHaveBeenCalledWith('Content-Encoding', 'gzip');
        expect(setHeader).toHaveBeenCalledWith('Content-Type', 'text/html;charset=UTF-8');
        expect(end).toHaveBeenCalledWith(page, 'binary');
    });

    it('should return 404 if it does not find page, no url', () => {
        request = {} as Http2ServerRequest;
        const page = '<h1>Page not found</h1>';
        spyOn(container.fullPages, 'get').and.returnValue(page);

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(response.statusCode).toBe(404);
        expect(setHeader).toHaveBeenCalledWith('Content-Encoding', 'gzip');
        expect(setHeader).toHaveBeenCalledWith('Content-Type', 'text/html;charset=UTF-8');
        expect(end).toHaveBeenCalledWith(page, 'binary');
    });

    it('should delegate to controllers', () => {
        const url = 'url';
        const method = 'method';
        request = { url, method } as Http2ServerRequest;
        spyOn(controllerRoutes, 'has').and.returnValue(true);
        spyOn(controllerRoutes, 'handle');

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(controllerRoutes.has).toHaveBeenCalledWith(method, url);
        expect(controllerRoutes.handle).toHaveBeenCalledWith(method, url, request, response);
        expect(response.statusCode).toBe(200);
    });

    it('should send static jsons', () => {
        const json = '{}';
        const url = 'url';
        const method = 'method';
        request = { url, method } as Http2ServerRequest;
        spyOn(container.api, 'has').and.returnValue(true);
        spyOn(container.api, 'get').and.returnValue(json);

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(container.api.has).toHaveBeenCalledWith(url);
        expect(container.api.get).toHaveBeenCalledWith(url);
        expect(writeHead).toHaveBeenCalledWith(200, { 'Content-Encoding': 'gzip', 'Content-Type': 'application/json', 'Vary': 'Accept-Encoding' });
        expect(end).toHaveBeenCalledWith(json, 'binary');
        expect(response.statusCode).toBe(200);
    });

    it('should send page', () => {
        const page = 'page';
        const url = 'url';
        request = { url } as Http2ServerRequest;
        spyOn(container.fullPages, 'has').and.returnValue(true);
        spyOn(container.fullPages, 'get').and.returnValue(page);

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(container.fullPages.has).toHaveBeenCalledWith(url);
        expect(container.fullPages.get).toHaveBeenCalledWith(url);
        expect(setHeader).toHaveBeenCalledWith('Content-Encoding', 'gzip');
        expect(setHeader).toHaveBeenCalledWith('Content-Type', 'text/html;charset=UTF-8');
        expect(end).toHaveBeenCalledWith(page, 'binary');
        expect(response.statusCode).toBe(200);
    });

    it('should send asset', () => {
        const content = 'content';
        const eTag = 'eTag';
        const url = 'url';
        const contentType = 'contentType';
        const lastModified = new Date();
        request = { url, headers: {} } as Http2ServerRequest;
        spyOn(container.assets, 'has').and.returnValue(true);
        spyOn(container.assets, 'get').and.returnValue({ content, eTag, contentType, lastModified } as Asset);

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(container.assets.has).toHaveBeenCalledWith(url);
        expect(container.assets.get).toHaveBeenCalledWith(url);
        expect(setHeader).toHaveBeenCalledWith('Content-Encoding', 'gzip');
        expect(setHeader).toHaveBeenCalledWith('Vary', 'Accept-Encoding');
        expect(setHeader).toHaveBeenCalledWith('Cache-Control', 'max-age=31536000');
        expect(setHeader).toHaveBeenCalledWith('ETag', eTag);
        expect(response.statusCode).toBe(200);
        expect(end).toHaveBeenCalledWith(content, 'binary');
    });

    it('should send 304 if eTag matches', () => {
        const eTag = 'eTag';
        const url = 'url';
        request = { url, headers: { 'if-none-match': eTag } } as Http2ServerRequest;
        spyOn(container.assets, 'has').and.returnValue(true);
        spyOn(container.assets, 'get').and.returnValue({ eTag } as Asset);

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(container.assets.has).toHaveBeenCalledWith(url);
        expect(container.assets.get).toHaveBeenCalledWith(url);
        expect(response.statusCode).toBe(304);
        expect(end).toHaveBeenCalledWith();
    });

    it('should send 304 if lastModified is older', () => {
        const lastModified = new Date(2001, 0, 1);
        const ifModifiedSince = (new Date()).toUTCString();
        const url = 'url';
        request = { url, headers: { 'if-modified-since': ifModifiedSince } } as Http2ServerRequest;
        spyOn(container.assets, 'has').and.returnValue(true);
        spyOn(container.assets, 'get').and.returnValue({ lastModified } as Asset);

        route(request, response);

        expect(authHandler.redirectIfUnauthorized).toHaveBeenCalledWith(request, response);
        expect(container.assets.has).toHaveBeenCalledWith(url);
        expect(container.assets.get).toHaveBeenCalledWith(url);
        expect(response.statusCode).toBe(304);
        expect(end).toHaveBeenCalledWith();
    });

    it('should send files from the static directory', async () => {
        const url = '/url.html';
        const method = 'GET';
        request = { url, method } as Http2ServerRequest;
        const staticDirectory = 'static';
        const content = Buffer.from('asfd');
        const type = 'type';
        spyOn(mimeTypes, 'get').and.returnValue(type);
        spyOn(fileReader, 'fileExists').and.returnValue(true);
        spyOn(fileReader, 'readFileContent').and.returnValue(Promise.resolve(content));

        await route(request, response);

        expect(mimeTypes.get).toHaveBeenCalledWith('.html');
        expect(fileReader.fileExists).toHaveBeenCalledWith(staticDirectory + url);
        expect(fileReader.readFileContent).toHaveBeenCalledWith(staticDirectory + url);
        expect(setHeader).toHaveBeenCalledWith('Content-Type', type);
        expect(response.statusCode).toBe(200);
        expect(end).toHaveBeenCalledWith(content, 'binary');
    });

    it('should send 500 if reading files from the static directory throws error', async () => {
        const url = '/url';
        const method = 'GET';
        request = { url, method } as Http2ServerRequest;
        const staticDirectory = 'static';
        const error = 'error';
        spyOn(fileReader, 'fileExists').and.returnValue(true);
        spyOn(fileReader, 'readFileContent').and.returnValue(Promise.reject(error));

        await route(request, response);

        expect(fileReader.fileExists).toHaveBeenCalledWith(staticDirectory + url);
        expect(fileReader.readFileContent).toHaveBeenCalledWith(staticDirectory + url);
        expect(response.statusCode).toBe(500);
        expect(end).toHaveBeenCalledWith(JSON.stringify({ error }));
    });
});
