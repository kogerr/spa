import { readCredentials } from '../../../src/service/server/credential.provider';
import * as fileReader from '../../../src/service/util/file-reader';

describe('Credential Provider', () => {
    it('should provide credentials', async () => {
        const a = Buffer.from('a');
        const b = Buffer.from('b');
        const c = Buffer.from('c');
        spyOn(fileReader, 'readFileContent').and.returnValues(Promise.resolve(a), Promise.resolve(b), Promise.resolve(c));

        const actual = await readCredentials();

        expect(fileReader.readFileContent).toHaveBeenCalledWith('cert/privkey.pem');
        expect(fileReader.readFileContent).toHaveBeenCalledWith('cert/cert.pem');
        expect(fileReader.readFileContent).toHaveBeenCalledWith('cert/chain.pem');
        expect(actual).toEqual({ ca: 'c', cert: 'b', key: 'a' });
    });

    it('should return undefined on error', async () => {
        spyOn(fileReader, 'readFileContent').and.returnValue(Promise.reject(new Error('asdf')));
        const actual = await readCredentials();

        expect(fileReader.readFileContent).toHaveBeenCalledWith('cert/privkey.pem');
        expect(fileReader.readFileContent).toHaveBeenCalledWith('cert/cert.pem');
        expect(fileReader.readFileContent).toHaveBeenCalledWith('cert/chain.pem');
        expect(actual).toBe(undefined);
    });
});
