import { ServerHttp2Stream } from 'http2';
import { prePushAssets } from '../../../src/service/server/prepush.handler';
import { assets } from '../../../src/service/container';
import { Asset } from '../../../src/models/asset.interface';

const lastModified = new Date();

const asset: Asset = {
    filename: 'filename',
    content: 'content',
    contentType: 'contentType',
    lastModified,
    eTag: 'eTag',
    hidden: false,
};

describe('prePushHandler', () => {

    const hash = process.env.CI_COMMIT_SHORT_SHA ?? '';
    const scriptPath = '/scripts/script' + hash + '.js';
    const stylePath = '/styles/common' + hash + '.css';

    it('should pre push assets', () => {
        spyOn(assets, 'get').and.returnValue(asset);
        const respond = jasmine.createSpy('respond');
        const end = jasmine.createSpy('end');
        const pushStream = jasmine.createSpy('pushStream').and.callFake((headers, callback) => callback(null,  { respond, end }));
        const stream = { pushStream } as unknown as ServerHttp2Stream;

        prePushAssets(stream);

        expect(pushStream).toHaveBeenCalledWith({ ':path': stylePath }, jasmine.any(Function));
        expect(pushStream).toHaveBeenCalledWith({ ':path': scriptPath }, jasmine.any(Function));
        expect(respond).toHaveBeenCalledWith({ ':status': 200, 'Content-Encoding': 'gzip', 'Content-Type': 'contentType',
            'Last-Modified': lastModified.toUTCString(), ETag: 'eTag', 'Cache-Control': 'max-age=31536000' });
        expect(respond).toHaveBeenCalledWith({ ':status': 200, 'Content-Encoding': 'gzip', 'Content-Type': 'contentType',
            'Last-Modified': lastModified.toUTCString(), ETag: 'eTag', 'Cache-Control': 'max-age=31536000' });
        expect(end).toHaveBeenCalledWith('content', 'binary');
        expect(end).toHaveBeenCalledWith('content', 'binary');
    });

    it('should log error', () => {
        spyOn(console, 'error');
        const error: Error = { name: 'error name', message: 'error message'};
        spyOn(assets, 'get').and.returnValue(asset);
        const respond = jasmine.createSpy('respond');
        const end = jasmine.createSpy('end');
        const pushStream = jasmine.createSpy('pushStream').and.callFake((headers, callback) => callback(error,  { respond, end }));
        const stream = { pushStream } as unknown as ServerHttp2Stream;

        prePushAssets(stream);

        expect(console.error).toHaveBeenCalledWith(error);
        expect(console.error).toHaveBeenCalledWith(error);
        expect(pushStream).toHaveBeenCalledWith({ ':path': stylePath }, jasmine.any(Function));
        expect(pushStream).toHaveBeenCalledWith({ ':path': scriptPath }, jasmine.any(Function));
        expect(respond).toHaveBeenCalledWith({ ':status': 200, 'Content-Encoding': 'gzip', 'Content-Type': 'contentType',
            'Last-Modified': lastModified.toUTCString(), ETag: 'eTag', 'Cache-Control': 'max-age=31536000' });
        expect(respond).toHaveBeenCalledWith({ ':status': 200, 'Content-Encoding': 'gzip', 'Content-Type': 'contentType',
            'Last-Modified': lastModified.toUTCString(), ETag: 'eTag', 'Cache-Control': 'max-age=31536000' });
        expect(end).toHaveBeenCalledWith('content', 'binary');
        expect(end).toHaveBeenCalledWith('content', 'binary');
    });
});
