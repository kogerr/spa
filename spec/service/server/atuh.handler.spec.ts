import { IncomingMessage } from 'http';
import { Http2ServerResponse } from 'http2';
import { RequestHandler } from '../../../src/models/types';
import { redirectIfUnauthorized } from '../../../src/service/server/atuh.handler';
import * as jwtFactory from '../../../src/service/util/jwt.factory';
import * as parameterParser from '../../../src/service/util/parameter.parser';

describe('redirectIfUnauthorized', () => {
    let callback: RequestHandler;
    let response: Http2ServerResponse;

    beforeEach(() => {
        callback = jasmine.createSpy<RequestHandler>();
        response = jasmine.createSpyObj(['setHeader', 'end']);
    });

    it('should not redirect if there is no request.method', () => {
        const request: IncomingMessage = {} as IncomingMessage;

        redirectIfUnauthorized(request, response)(callback);

        expect(callback).toHaveBeenCalledWith(request, response);
    });

    it('should not redirect if there is a valid token', () => {
        const cookie = 'cookie';
        const request: IncomingMessage = { url: '/admin', headers: { cookie } } as IncomingMessage;
        const token = 'token';

        spyOn(parameterParser, 'parseParameters').and.returnValue(new Map([[ 'access_token', token ]]));
        spyOn(jwtFactory, 'validate').and.returnValue(true);

        redirectIfUnauthorized(request, response)(callback);

        expect(parameterParser.parseParameters).toHaveBeenCalledWith(cookie, ';');
        expect(jwtFactory.validate).toHaveBeenCalledWith(token);
        expect(callback).toHaveBeenCalledWith(request, response);
    });

    it('should redirect if request.url starts with admin and there is no token', () => {
        const url = '/admin';
        const request: IncomingMessage = { url, headers: {} } as IncomingMessage;

        redirectIfUnauthorized(request, response)(callback);

        expect(response.setHeader).toHaveBeenCalledWith('Set-Cookie', 'target=' + url + '; Path=/api/login; Secure; SameSite=strict');
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/login');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
        expect(callback).not.toHaveBeenCalled();
    });

});
