import * as newsDao from '../../../src/db/news.dao';
import { NewsPost } from '../../../src/models/news-post.interface';
import { getNewsPosts } from '../../../src/service/providers/newspost.provider';

describe('News post provider', () => {

    it('gets news posts', async () => {
        const text = 'text';
        const date = 0;
        const post = { date, text } as unknown as NewsPost;
        spyOn(newsDao, 'getNews').and.returnValue(Promise.resolve([post]));

        const actual = await getNewsPosts();

        expect(newsDao.getNews).toHaveBeenCalledWith();
        expect(actual).toEqual([ { date, template: text } ]);
    });
});
