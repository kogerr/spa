import { getCountryFromCode } from '../../../src/service/providers/country.provider';

describe('Country Provider', () => {
    it('should provide country', () => {
        const countryCode = 'HU';

        const actual = getCountryFromCode(countryCode);

        expect(actual).toEqual('Hungary');
    });

    it('should return country code if not found', () => {
        const countryCode = 'asdf';

        const actual = getCountryFromCode(countryCode);

        expect(actual).toEqual(countryCode);
    });
});
