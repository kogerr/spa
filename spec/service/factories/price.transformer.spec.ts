import { transform } from '../../../src/service/factories/price.transformer';
import { BasePrices } from '../../../src/models/base-prices.interface';
import { PriceBlock } from '../../../src/models/price-block.interface';

const expected: PriceBlock[] = [
    { platform:1, licenseSize:1, singleStyle:72, singlePair:108, family:360 },
    { platform:1, licenseSize:5, singleStyle:96, singlePair:144, family:480 },
    { platform:1, licenseSize:10, singleStyle:128, singlePair:192, family:640 },
    { platform:1, licenseSize:25, singleStyle:171, singlePair:256, family:853 },
    { platform:1, licenseSize:50, singleStyle:228, singlePair:341, family:1138 },
    { platform:1, licenseSize:100, singleStyle:303, singlePair:455, family:1517 },
    { platform:1, licenseSize:250, singleStyle:405, singlePair:607, family:2023 },
    { platform:1, licenseSize:500, singleStyle:539, singlePair:809, family:2697 },
    { platform:1, licenseSize:750, singleStyle:719, singlePair:1079, family:3596 },
    { platform:2, licenseSize:1, singleStyle:108, singlePair:162, family:540 },
    { platform:2, licenseSize:5, singleStyle:144, singlePair:216, family:720 },
    { platform:2, licenseSize:10, singleStyle:192, singlePair:288, family:960 },
    { platform:2, licenseSize:25, singleStyle:256, singlePair:384, family:1280 },
    { platform:2, licenseSize:50, singleStyle:341, singlePair:512, family:1707 },
    { platform:2, licenseSize:100, singleStyle:455, singlePair:683, family:2276 },
    { platform:2, licenseSize:250, singleStyle:607, singlePair:910, family:3034 },
    { platform:2, licenseSize:500, singleStyle:809, singlePair:1214, family:4045 },
    { platform:2, licenseSize:750, singleStyle:1079, singlePair:1618, family:5394 },
    { platform:4, licenseSize:1, singleStyle:144, singlePair:216, family:720 },
    { platform:4, licenseSize:5, singleStyle:192, singlePair:288, family:960 },
    { platform:4, licenseSize:10, singleStyle:256, singlePair:384, family:1280 },
    { platform:4, licenseSize:25, singleStyle:341, singlePair:512, family:1707 },
    { platform:4, licenseSize:50, singleStyle:455, singlePair:683, family:2276 },
    { platform:4, licenseSize:100, singleStyle:607, singlePair:910, family:3034 },
    { platform:4, licenseSize:250, singleStyle:809, singlePair:1214, family:4045 },
    { platform:4, licenseSize:500, singleStyle:1079, singlePair:1618, family:5394 },
    { platform:4, licenseSize:750, singleStyle:1438, singlePair:2158, family:7192 },
    { platform:3, licenseSize:1, singleStyle:135, singlePair:203, family:675 },
    { platform:3, licenseSize:5, singleStyle:180, singlePair:270, family:900 },
    { platform:3, licenseSize:10, singleStyle:240, singlePair:360, family:1200 },
    { platform:3, licenseSize:25, singleStyle:320, singlePair:480, family:1600 },
    { platform:3, licenseSize:50, singleStyle:427, singlePair:640, family:2133 },
    { platform:3, licenseSize:100, singleStyle:569, singlePair:853, family:2844 },
    { platform:3, licenseSize:250, singleStyle:759, singlePair:1138, family:3793 },
    { platform:3, licenseSize:500, singleStyle:1011, singlePair:1517, family:5057 },
    { platform:3, licenseSize:750, singleStyle:1348, singlePair:2023, family:6742 },
    { platform:5, licenseSize:1, singleStyle:162, singlePair:243, family:810 },
    { platform:5, licenseSize:5, singleStyle:216, singlePair:324, family:1080 },
    { platform:5, licenseSize:10, singleStyle:288, singlePair:432, family:1440 },
    { platform:5, licenseSize:25, singleStyle:384, singlePair:576, family:1920 },
    { platform:5, licenseSize:50, singleStyle:512, singlePair:768, family:2560 },
    { platform:5, licenseSize:100, singleStyle:683, singlePair:1024, family:3413 },
    { platform:5, licenseSize:250, singleStyle:910, singlePair:1365, family:4551 },
    { platform:5, licenseSize:500, singleStyle:1214, singlePair:1820, family:6068 },
    { platform:5, licenseSize:750, singleStyle:1618, singlePair:2427, family:8091 },
    { platform:6, licenseSize:1, singleStyle:189, singlePair:284, family:945 },
    { platform:6, licenseSize:5, singleStyle:252, singlePair:378, family:1260 },
    { platform:6, licenseSize:10, singleStyle:336, singlePair:504, family:1680 },
    { platform:6, licenseSize:25, singleStyle:448, singlePair:672, family:2240 },
    { platform:6, licenseSize:50, singleStyle:597, singlePair:896, family:2987 },
    { platform:6, licenseSize:100, singleStyle:796, singlePair:1195, family:3982 },
    { platform:6, licenseSize:250, singleStyle:1062, singlePair:1593, family:5310 },
    { platform:6, licenseSize:500, singleStyle:1416, singlePair:2124, family:7080 },
    { platform:6, licenseSize:750, singleStyle:1888, singlePair:2832, family:9439 },
    { platform:7, licenseSize:1, singleStyle:243, singlePair:365, family:1215 },
    { platform:7, licenseSize:5, singleStyle:324, singlePair:486, family:1620 },
    { platform:7, licenseSize:10, singleStyle:432, singlePair:648, family:2160 },
    { platform:7, licenseSize:25, singleStyle:576, singlePair:864, family:2880 },
    { platform:7, licenseSize:50, singleStyle:768, singlePair:1152, family:3840 },
    { platform:7, licenseSize:100, singleStyle:1024, singlePair:1536, family:5120 },
    { platform:7, licenseSize:250, singleStyle:1365, singlePair:2048, family:6827 },
    { platform:7, licenseSize:500, singleStyle:1820, singlePair:2731, family:9102 },
    { platform:7, licenseSize:750, singleStyle:2427, singlePair:3641, family:12136 }
];

describe('Price Transformer', () => {
    it('should transform BasePrices', () => {
        const basePrices: BasePrices = { family: 'family', desktop: 72, web: 108, app: 144 };

        const actual = transform(basePrices);

        expect(actual).toEqual(expected);
    });
});
