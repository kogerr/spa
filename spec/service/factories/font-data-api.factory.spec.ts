import { createFontFace } from '../../../src/service/factories/font-data-api.factory';
import { Font } from '../../../src/models/font.interface';

describe('FontDataApi Factory', () => {
    const index = 0;
    const data = 'data';

    it('should create font faces', () => {
        const webfont = { filename: 'filename.woff2', data };
        const font: Font = { family: 'family', weight: 400, italic: false, webfont, fullFonts: [] };
        const actual = createFontFace(index, font)
        const expected = { index:0, family:'family', source:'url(data:font/woff2;charset=utf-8;base64,data) format(\'woff2\')',
            descriptors:{ weight: 400, style: 'normal' } };
        expect(actual).toEqual(expected);
    });

    it('should create font faces when font is italic and filename without format', () => {
        const webfont = { filename: 'filename', data };
        const font: Font = { family: 'family', weight: 400, italic: true, webfont, fullFonts: [] };

        const actual = createFontFace(index, font)
        const expected = { index:0, family:'family', source:'url(data:font/woff;charset=utf-8;base64,data) format(\'woff\')',
            descriptors:{ weight: 400, style: 'italic' } };
        expect(actual).toEqual(expected);
    });
});
