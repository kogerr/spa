import { answerPaymentCallback } from '../../../src/service/controllers/payment-callback.controller';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';

describe('Payment Callback Controller', () => {
    it('should reply with 200', (done) => {
        const request = {} as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['writeHead', 'end']);

        answerPaymentCallback(request, response).then(() => {
            // @ts-ignore
            expect(response.writeHead).toHaveBeenCalledWith(200);
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith();
            done();
        });
    });
});
