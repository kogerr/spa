import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { UpdateWriteOpResult } from 'mongodb';
import * as projectFontDao from '../../../src/db/project-font.dao';
import * as fontDao from '../../../src/db/font.dao';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import * as jsonResponder from '../../../src/service/controllers/send-json-response';
import { handleFontFaceUpload, serveFontsByProjectUrl } from '../../../src/service/controllers/font-face.controller';
import * as incomingMessageResolver from '../../../src/service/util/incoming-message.resolver';
import * as containerService from '../../../src/service/loader/container.service';
import * as queryParameterResolver from '../../../src/service/controllers/get-query-parameter';

describe('FontFace controller', () => {

    const family = 'family';
    const weight = 400;
    const projectUrl = 'projectUrl';
    const request = { payload: 'payload' } as unknown as Http2ServerRequest;
    const data = 'source';
    const filename = 'filename.woff';
    const italic = false;
    const fontUpload = { projectUrl, index: 0, family, weight, italic, webfont: { filename, data }, fullFonts: [{ filename, data }] };
    const id = '0123456789abcdef01234567';
    const index = 0;

    it('handleFontFaceUpload should handle FontFace upload', (done) => {
        const setHeader = jasmine.createSpy('res.setHeader');
        const end = jasmine.createSpy('res.end');
        const response = { end, setHeader } as unknown as Http2ServerResponse;
        const updateResult = { result: { ok: 1, n: 1, nModified: 1 } } as UpdateWriteOpResult;
        spyOn(incomingMessageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(fontUpload));
        spyOn(fontDao, 'insertFont').and.returnValue(Promise.resolve(id));
        spyOn(projectFontDao, 'addFontToProject').and.returnValue(Promise.resolve(updateResult));
        spyOn(containerService, 'reloadPagesAndApi').and.returnValue(Promise.resolve());

        handleFontFaceUpload(request, response).then(() => {
            expect(incomingMessageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
            expect(fontDao.insertFont).toHaveBeenCalledWith({ family, weight, italic, webfont: { filename, data }, fullFonts: [{ filename, data }] });
            expect(projectFontDao.addFontToProject).toHaveBeenCalledWith(projectUrl, { index, id });
            expect(containerService.reloadPagesAndApi).toHaveBeenCalledWith();
            expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith(JSON.stringify(updateResult));
            done();
        });
    });

    it('handleFontFaceUpload should give error response if error occurs', async () => {
        const response = {} as unknown as Http2ServerResponse;
        const error = Error('error');
        spyOn(incomingMessageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(error));
        spyOn(errorResponder, 'sendErrorResponse');

        await handleFontFaceUpload(request, response);

        expect(incomingMessageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
        expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
    });

    it('serveFontsByProjectUrl should serve fonts by project url', (done) => {
        const setHeader = jasmine.createSpy('res.setHeader');
        const end = jasmine.createSpy('res.end');
        const response = { end, setHeader } as unknown as Http2ServerResponse;
        spyOn(queryParameterResolver, 'getQueryParameter').and.returnValue(projectUrl);
        spyOn(projectFontDao, 'getProjectFontsByUrl').and.returnValue(Promise.resolve([ { index, id } ]));
        spyOn(fontDao, 'getFontById').and
            .returnValue(Promise.resolve({ family, weight, italic, webfont: { filename, data }, fullFonts: [{ filename, data }] }));
        spyOn(jsonResponder, 'sendJsonResponse');

        serveFontsByProjectUrl(request, response).then(() => {
            expect(queryParameterResolver.getQueryParameter).toHaveBeenCalledWith(request, 'projectUrl');
            expect(projectFontDao.getProjectFontsByUrl).toHaveBeenCalledWith(projectUrl);
            expect(fontDao.getFontById).toHaveBeenCalledWith(id);
            expect(jsonResponder.sendJsonResponse).toHaveBeenCalledWith([{ index: 0, family: 'family', weight: 400, italic: false, id: '0123456789abcdef01234567' }], response);
            done();
        });
    });

    it('serveFontsByProjectUrl should give error response if error occurs', (done) => {
        const setHeader = jasmine.createSpy('res.setHeader');
        const end = jasmine.createSpy('res.end');
        const response = { end, setHeader } as unknown as Http2ServerResponse;
        const error = Error('error');
        spyOn(queryParameterResolver, 'getQueryParameter').and.throwError(error);
        spyOn(errorResponder, 'sendErrorResponse');

        serveFontsByProjectUrl(request, response).then(() => {
            expect(queryParameterResolver.getQueryParameter).toHaveBeenCalledWith(request, 'projectUrl');
            expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
            done();
        });
    });
});
