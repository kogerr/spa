import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import * as incomingMessageResolver from '../../../src/service/util/incoming-message.resolver';
import { startPayment } from '../../../src/service/controllers/payment-start.controller';
import * as startFacade from '../../../src/service/payment/start/barion/start-facade';
import * as validator from '../../../src/service/payment/validator';
import { ErrorStartResponse, StartResponse } from '../../../src/models/barion/start-response.interface';
import { CheckoutFormPayload } from '../../../src/models/checkout-form-payload.interface';

describe('Payment Start Controller', () => {
    const email = 'email@address.com';
    const total = '100';
    const GatewayUrl = 'paymentUrl';
    const name = 'name';
    const country = 'HU';
    const city = 'city';
    const zip = 'zip';
    const address = 'address';
    const phone = '3614148750';
    const family = 'family';
    const license = 1;
    const platform = 1;
    const bundle = 'family';
    const price = total;
    const weight = 400;
    const style = 'normal';
    const error = {
        Title: 'Model Validation Error',
        Description: 'The field Country must be a string or array type with a maximum length of \'2\'.',
        ErrorCode: 'ModelValidationError',
        HappenedAt: '2022-03-21T09:38:27.5907034Z',
        AuthData: 'kogerr@hotmail.com',
        EndPoint: 'https://api.test.barion.com/v2/Payment/Start'
    };
    const subtotal = '80';
    const vatValue = '20';

    it('should call payment start client', (done) => {
        const request = {} as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        const products = JSON.stringify([ { family, license, platform, bundle, price } ]);
        const items = JSON.stringify({ family, license: license.toString(), platform, weight, style, price });
        const payload: CheckoutFormPayload = { name, email, country, zip, city, address, phone, someoneElse: 'true',
            total: total.toString(), products, items, subtotal, vatValue };
        const startResponse = { GatewayUrl, Errors: [] } as StartResponse;
        spyOn(incomingMessageResolver, 'getRequestFormObject').and.returnValue(Promise.resolve(payload));
        spyOn(validator, 'validate').and.returnValue(Promise.resolve());
        spyOn(startFacade, 'initiateStart').and.returnValues(Promise.resolve(startResponse));

        startPayment(request, response).then(() => {
            expect(incomingMessageResolver.getRequestFormObject).toHaveBeenCalledWith(request);
            expect(validator.validate).toHaveBeenCalled();
            expect(startFacade.initiateStart).toHaveBeenCalled();
            expect(response.statusCode).toBe(302);
            expect(response.setHeader).toHaveBeenCalledWith('Location', GatewayUrl);
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith();
            done();
        })
    });

    it('should send error response on missing parameters', (done) => {
        const request = {} as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        const parameters = {};
        spyOn(incomingMessageResolver, 'getRequestFormObject').and.returnValue(Promise.resolve(parameters));

        startPayment(request, response).then(() => {
            expect(incomingMessageResolver.getRequestFormObject).toHaveBeenCalledWith(request);
            expect(response.statusCode).toBe(500);
            // @ts-ignore
            expect(response.end).toHaveBeenCalled();
            done();
        })
    });

    it('should send error message when downstream response has error code', (done) => {
        const request = {} as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        const products = JSON.stringify([ { family, license, platform, bundle, price } ]);
        const items = JSON.stringify({ family, license: license.toString(), platform, weight, style, price });
        const payload: CheckoutFormPayload = { name, email, country, zip, city, address, phone, someoneElse: 'true',
            total: total.toString(), products, items, subtotal, vatValue };
        const startResponse = { Errors: [ error ] } as ErrorStartResponse;
        spyOn(incomingMessageResolver, 'getRequestFormObject').and.returnValue(Promise.resolve(payload));
        spyOn(validator, 'validate').and.callFake((p, i, t) => Promise.resolve());
        spyOn(startFacade, 'initiateStart').and.returnValues(Promise.resolve(startResponse));

        startPayment(request, response).then(() => {
            expect(incomingMessageResolver.getRequestFormObject).toHaveBeenCalledWith(request);
            expect(startFacade.initiateStart).toHaveBeenCalled();
            expect(response.statusCode).toBe(500);
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith(JSON.stringify([ error ]));
            done();
        })
    });

    it('should call payment start client when invoice details are available', (done) => {
        const request = {} as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        const products = JSON.stringify([ { family, license, platform, bundle, price } ]);
        const items = JSON.stringify({ family, license: license.toString(), platform, weight, style, price });
        const payload: CheckoutFormPayload = { name, email, country, zip, city, address, phone, someoneElse: 'true',
            total: total.toString(), products, items, subtotal, vatValue };

        const startResponse = { GatewayUrl, Errors: [] } as StartResponse;
        spyOn(incomingMessageResolver, 'getRequestFormObject').and.returnValue(Promise.resolve(payload));
        spyOn(validator, 'validate').and.callFake((p, i, t) => Promise.resolve());
        spyOn(startFacade, 'initiateStart').and.returnValues(Promise.resolve(startResponse));

        startPayment(request, response).then(() => {
            expect(incomingMessageResolver.getRequestFormObject).toHaveBeenCalledWith(request);
            expect(startFacade.initiateStart).toHaveBeenCalled();
            expect(response.statusCode).toBe(302);
            expect(response.setHeader).toHaveBeenCalledWith('Location', GatewayUrl);
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith();
            done();
        })
    });

    it('should return error message if payment validation fails', (done) => {
        const request = {} as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        const products = JSON.stringify([ { family, license, platform, bundle, price } ]);
        const items = JSON.stringify({ family, license: license.toString(), platform, weight, style, price });
        const payload: CheckoutFormPayload = { name, email, country, zip, city, address, phone, someoneElse: 'true',
            total: total.toString(), products, items, subtotal, vatValue };
        spyOn(incomingMessageResolver, 'getRequestFormObject').and.returnValue(Promise.resolve(payload));
        spyOn(validator, 'validate').and.returnValue(Promise.reject(new Error('product prices and total do not match')));

        startPayment(request, response).then(() => {
            expect(incomingMessageResolver.getRequestFormObject).toHaveBeenCalledWith(request);
            expect(validator.validate).toHaveBeenCalled();
            expect(response.statusCode).toBe(500);
            // @ts-ignore
            expect(response.end).toHaveBeenCalled();
            done();
        })
    });
});
