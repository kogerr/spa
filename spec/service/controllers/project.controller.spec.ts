import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { handleProjectsQuery, handleProjectsAdminQuery, handleProjectsUpload, handleProjectRemoval,
    handleProjectUpdate, serveHoverImages } from '../../../src/service/controllers/project.controller';
import * as projectsDao from '../../../src/db/project.dao';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import * as messageResolver from '../../../src/service/util/incoming-message.resolver';
import * as fileUnlinker from '../../../src/service/util/file-unlinker';
import * as containerService from '../../../src/service/loader/container.service';
import { Project } from '../../../src/models/project/project.interface';
import { FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';

describe('Landing projects Controller', () => {

    const details = { description: 'desc', languages: 'lang', release: 'rel', version: 'ver', formats: 'formats', specimenPdf: 'pdf' };
    const project: Project = { index: 0, shortName: 'name', url: 'url', pageTitle: 'pageTitle', previewImage: 'previewImage',
        previewHover: 'previewHover', coverImage: 'coverImage', details, slides: [], fonts: [] };
    const id = '6064408a04cfcc0cf8de7b1a';
    const _id = new ObjectId(id);
    const projectsWithId = [ { _id, ...project } ] as WithId<Project>[];
    const errorMessage = 'error';

    it('handleProjectsQuery should return projects', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(projectsDao, 'getProjects').and.returnValue(Promise.resolve([ project ]));

        handleProjectsQuery(request, response)
            .then(() => {
                expect(projectsDao.getProjects).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([ project ]));
                done();
            });
    });
    it('handleProjectsQuery should send error when error happens', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(projectsDao, 'getProjects').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleProjectsQuery(request, response)
            .then(() => {
                expect(projectsDao.getProjects).toHaveBeenCalledWith();
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleProjectsAdminQuery should return projects with id', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(projectsDao, 'getProjectsWithId').and.returnValue(Promise.resolve(projectsWithId));

        handleProjectsAdminQuery(request, response)
            .then(() => {
                expect(projectsDao.getProjectsWithId).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json')
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(projectsWithId));
                done();
            });
    });

    it('handleProjectsAdminQuery should send error when error happens', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(projectsDao, 'getProjectsWithId').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleProjectsAdminQuery(request, response)
            .then(() => {
                expect(projectsDao.getProjectsWithId).toHaveBeenCalledWith();
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleProjectsUpload should add and send projects', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const addResult = {} as InsertOneWriteOpResult<WithId<Project>>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(project));
        spyOn(projectsDao, 'addProject').and.returnValue(Promise.resolve(addResult));
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(projectsDao, 'getProjectsWithId').and.returnValue(Promise.resolve(projectsWithId));

        handleProjectsUpload(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(projectsDao.addProject).toHaveBeenCalledWith(project);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(projectsDao.getProjectsWithId).toHaveBeenCalled();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(projectsWithId));
                done();
            });
    });

    it('handleProjectsUpload should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleProjectsUpload(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleProjectRemoval should remove and send projects', (done) => {
        const request = {} as Http2ServerRequest;
        const payload = { id, src: [ project.previewImage ] };
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const addResult = {} as InsertOneWriteOpResult<WithId<Project>>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(payload));
        spyOn(projectsDao, 'removeProject').and.returnValue(Promise.resolve(addResult));
        spyOn(fileUnlinker, 'unlinkFile').and.returnValue(Promise.resolve());
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(projectsDao, 'getProjectsWithId').and.returnValue(Promise.resolve(projectsWithId));

        handleProjectRemoval(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(projectsDao.removeProject).toHaveBeenCalledWith(id);
                expect(fileUnlinker.unlinkFile).toHaveBeenCalledWith(project.previewImage);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(projectsDao.getProjectsWithId).toHaveBeenCalled();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(projectsWithId));
                done();
            });
    });

    it('handleProjectRemoval should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleProjectRemoval(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleProjectUpdate should update and send projects', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const result = {} as FindAndModifyWriteOpResultObject<Project>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(projectsWithId));
        spyOn(projectsDao, 'updateProjects').and.returnValue(Promise.resolve([ result ]));
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(projectsDao, 'getProjectsWithId').and.returnValue(Promise.resolve(projectsWithId));

        handleProjectUpdate(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(projectsDao.updateProjects).toHaveBeenCalledWith(projectsWithId);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(projectsDao.getProjectsWithId).toHaveBeenCalled();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(projectsWithId));
                done();
            });
    });

    it('handleProjectUpdate should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleProjectUpdate(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });


    it('serveHoverImages should return hover images', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(projectsDao, 'getHoverImages').and.returnValue(Promise.resolve([ project ]));

        serveHoverImages(request, response)
            .then(() => {
                expect(projectsDao.getHoverImages).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([ project ]));
                done();
            });
    });

    it('serveHoverImages should return error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(projectsDao, 'getHoverImages').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        serveHoverImages(request, response)
            .then(() => {
                expect(projectsDao.getHoverImages).toHaveBeenCalledWith();
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });
});
