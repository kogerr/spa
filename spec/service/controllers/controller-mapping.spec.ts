import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { controllerRoutes } from '../../../src/service/controllers/controller-mapping';

describe('controllerRoutes', () => {

    it('has returns false if it does not have route', () => {
        const actual = controllerRoutes.has('GET', 'asdf');

        expect(actual).toBe(false);
    });

    it('handle delegates request to listener', () => {
        const request = jasmine.createSpyObj<Http2ServerRequest>(['url']);
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'writeHead', 'end']);

        controllerRoutes.handle('GET', '/api/preload', request, response);
    });

    it('handle throws exception if there is no handler', () => {
        const request = jasmine.createSpyObj<Http2ServerRequest>(['url']);
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'writeHead', 'end']);

        try {
            controllerRoutes.handle('GET', 'asdf', request, response);
        } catch (error) {
            expect(error).toEqual(new Error('No controller found for the request.'));
        }
    });
});
