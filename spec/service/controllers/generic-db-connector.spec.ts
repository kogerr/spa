import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import * as messageResolver from '../../../src/service/util/incoming-message.resolver';
import * as fileUnlinker from '../../../src/service/util/file-unlinker';
import * as containerService from '../../../src/service/loader/container.service';
import { LandingSlide } from '../../../src/models/landing-slide.interface';
import { Collection, Cursor, DeleteWriteOpResultObject, FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';
import { handleAddition, handleAdminQuery, handlePublicQuery, handleRemoval, handleUpdate } from '../../../src/service/controllers/generic-db-connector';
import { db } from '../../../src/db/mongo';

describe('Landing Slides Controller', () => {

    const slide: LandingSlide = {index: 0, src: '/image.png', link: '/link', alt: 'alt'};
    const slides = [ slide ] as LandingSlide[];
    const id = '6064408a04cfcc0cf8de7b1a';
    const _id = new ObjectId(id);
    const slidesWithId = [{_id, ...slide}] as WithId<LandingSlide>[];
    const errorMessage = 'error';
    const collection = 'landingSlides';

    it('handlePublicQuery should return items', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const slides = [ slide ] as LandingSlide[];
        const toArray = () => Promise.resolve(slides);
        const cursor = {toArray} as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        handlePublicQuery(collection)(request, response)
            .then(() => {
                expect(find).toHaveBeenCalledWith({}, {projection: {_id: false}});
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json')
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([slide]));
                done();
            });
    });

    it('handlePublicQuery should return error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        const toArray = () => Promise.reject(errorMessage)
        const cursor = {toArray} as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);
        spyOn(errorResponder, 'sendErrorResponse');

        handlePublicQuery(collection)(request, response)
            .then(() => {
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleAdminQuery should return items', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const toArray = () => Promise.resolve(slides);
        const cursor = {toArray} as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        handleAdminQuery(collection)(request, response)
            .then(() => {
                expect(find).toHaveBeenCalledWith({});
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json')
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([slide]));
                done();
            });
    });

    it('handleAdminQuery should return error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        const toArray = () => Promise.reject(errorMessage);
        const cursor = {toArray} as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);
        spyOn(errorResponder, 'sendErrorResponse');

        handleAdminQuery(collection)(request, response)
            .then(() => {
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleAddition should add and send items', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const addResult = {} as InsertOneWriteOpResult<WithId<LandingSlide>>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(slide));
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve(addResult));
        const toArray = () => Promise.resolve(slidesWithId);
        const cursor = {toArray} as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({ insertOne, find } as unknown as Collection);
        spyOn(containerService, 'reloadPagesAndApi');

        handleAddition(collection)(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(insertOne).toHaveBeenCalledWith(slide);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(find).toHaveBeenCalledWith({});
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(slidesWithId));
                done();
            });
    });

    it('handleAddition should send error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(slide));
        const insertOne = jasmine.createSpy().and.returnValue(Promise.reject(errorMessage));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);
        spyOn(errorResponder, 'sendErrorResponse');

        handleAddition(collection)(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(insertOne).toHaveBeenCalledWith(slide);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleRemoval should remove and send items', (done) => {
        const request = {} as Http2ServerRequest;
        const payload = { id, src: [ slide.src ] };
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(payload));
        const deleteResult = {} as DeleteWriteOpResultObject;
        const deleteOne = jasmine.createSpy().and.returnValue(Promise.resolve(deleteResult));
        const toArray = () => Promise.resolve(slidesWithId);
        const cursor = {toArray} as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({ deleteOne, find } as unknown as Collection);
        spyOn(fileUnlinker, 'unlinkFile').and.returnValue(Promise.resolve());
        spyOn(containerService, 'reloadPagesAndApi');

        handleRemoval(collection)(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(deleteOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
                expect(fileUnlinker.unlinkFile).toHaveBeenCalledWith(slide.src);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(find).toHaveBeenCalledWith({});
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(slidesWithId));
                done();
            });
    });

    it('handleRemoval should send error', (done) => {
        const request = {} as Http2ServerRequest;
        const payload = { id, src: [ slide.src ] };
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(payload));
        const deleteOne = jasmine.createSpy().and.returnValue(Promise.reject(errorMessage));
        db.collection = jasmine.createSpy().and.returnValue({ deleteOne } as unknown as Collection);
        spyOn(fileUnlinker, 'unlinkFile').and.returnValue(Promise.resolve());
        spyOn(errorResponder, 'sendErrorResponse');

        handleRemoval(collection)(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(deleteOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
                expect(fileUnlinker.unlinkFile).toHaveBeenCalledWith(slide.src);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleUpdate should update and send slides', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(slidesWithId));
        const result = { ok: 1 } as FindAndModifyWriteOpResultObject<LandingSlide>;
        const findOneAndUpdate = jasmine.createSpy().and.returnValue(result);
        const toArray = () => Promise.resolve(slidesWithId);
        const cursor = {toArray} as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find, findOneAndUpdate } as unknown as Collection);
        spyOn(containerService, 'reloadPagesAndApi');

        handleUpdate(collection)(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(findOneAndUpdate).toHaveBeenCalledWith({ _id }, { $set:{ src: '/image.png', index: 0, alt: 'alt', link: '/link' }});
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(find).toHaveBeenCalledWith({});
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(slidesWithId));
                done();
            });
    });

    it('handleUpdate should send error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleUpdate(collection)(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });
});
