import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { Asset } from '../../../src/models/asset.interface';
import * as container from '../../../src/service/container';
import { servePreloadPaths } from '../../../src/service/controllers/preload.controller';

describe('Preload Controller', () => {

    it('should return images and fonts', () => {
        const request = {} as Http2ServerRequest;
        const end = jasmine.createSpy('end');
        const writeHead = jasmine.createSpy('writeHead');
        const response = { writeHead, end } as unknown as Http2ServerResponse;
        const imagePath = 'image/path';
        const fontPath = 'font/path';
        container.assets.clear();
        container.assets.set(imagePath, { contentType: 'image'} as Asset);
        container.assets.set(fontPath, { contentType: 'font'} as Asset);

        servePreloadPaths(request, response);

        expect(writeHead).toHaveBeenCalledWith(200, { 'Content-Type': 'application/json', 'Content-Length': 26 });
        expect(end).toHaveBeenCalledWith(JSON.stringify([imagePath, fontPath]));
    });

});
