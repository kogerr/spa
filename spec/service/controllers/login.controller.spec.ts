import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import * as userDao from '../../../src/db/user.dao';
import { handleLogin } from '../../../src/service/controllers/login.controller';
import * as incomingMessageResolver from '../../../src/service/util/incoming-message.resolver';
import * as jwtFactory from '../../../src/service/util/jwt.factory';
import * as parameterParser from '../../../src/service/util/parameter.parser';

const errorMessage = 'Could not validate credentials.';

describe('Login Controller', () => {

    it('handleLogin responds with token', async () => {
        const email = 'email';
        const password = 'password';
        const token = 'token';
        const request = { headers: { cookie: 'cookie' } } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        spyOn(incomingMessageResolver, 'getRequestFormObject')
            .and.returnValue(Promise.resolve({ email, password }));
        spyOn(parameterParser, 'parseParameters').and.returnValue(new Map([['target', 'target']]));
        spyOn(userDao, 'checkUser').and.returnValue(Promise.resolve(true));
        spyOn(jwtFactory, 'createToken').and.returnValue(token);

        await handleLogin(request, response);

        expect(jwtFactory.createToken).toHaveBeenCalledWith(email);
        expect(response.setHeader).toHaveBeenCalledWith('Set-Cookie', 'access_token=' + token + '; Path=/; Secure');
        expect(response.setHeader).toHaveBeenCalledWith('Location', 'target');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
    });

    it('handleLogin responds with token when there is no target location', async () => {
        const email = 'email';
        const password = 'password';
        const token = 'token';
        const request = { headers: {} } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        spyOn(incomingMessageResolver, 'getRequestFormObject')
            .and.returnValue(Promise.resolve({ email, password }));
        spyOn(parameterParser, 'parseParameters').and.returnValue(new Map([['target', 'target']]));
        spyOn(userDao, 'checkUser').and.returnValue(Promise.resolve(true));
        spyOn(jwtFactory, 'createToken').and.returnValue(token);

        await handleLogin(request, response);

        expect(jwtFactory.createToken).toHaveBeenCalledWith(email);
        expect(response.setHeader).toHaveBeenCalledWith('Set-Cookie', 'access_token=' + token + '; Path=/; Secure');
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
    });

    it('handleLogin redirects when user already exists', async () => {
        const email = 'email';
        const password = 'password';
        const request = jasmine.createSpyObj<Http2ServerRequest>(['url']);
        const setHeader = jasmine.createSpy();
        const end = jasmine.createSpy('end');
        const response = { setHeader, end, statusCode: 0 } as unknown as Http2ServerResponse;
        spyOn(incomingMessageResolver, 'getRequestFormObject')
            .and.returnValue(Promise.resolve({ email, password }));
        spyOn(userDao, 'checkUser').and.returnValue(Promise.resolve(false));

        await handleLogin(request, response);

        expect(response.setHeader).toHaveBeenCalledWith('error', errorMessage);
        expect(end).toHaveBeenCalledWith();
        expect(response.statusCode).toBe(302);
    });

    it('handleLogin redirects when credentials are empty', async () => {
        const request = jasmine.createSpyObj<Http2ServerRequest>(['url']);
        const setHeader = jasmine.createSpy();
        const end = jasmine.createSpy('end');
        const response = { setHeader, end, statusCode: 0 } as unknown as Http2ServerResponse;
        spyOn(incomingMessageResolver, 'getRequestFormObject').and.returnValue(Promise.resolve({}));

        await handleLogin(request, response);

        expect(response.setHeader).toHaveBeenCalledWith('error', 'Cannot parse credentials.');
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/login?error=Cannot%20parse%20credentials.');
        expect(end).toHaveBeenCalledWith();
        expect(response.statusCode).toBe(302);
    });
});
