import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { handleBasePricesQuery, handleBasePricesAdminQuery, handleBasePricesRemoval,
    handleBasePricesUpdate, handlePricesQuery } from '../../../src/service/controllers/base-prices.controller';
import * as fontDao from '../../../src/db/font.dao';
import * as basePricesDao from '../../../src/db/base-prices.dao';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import * as messageResolver from '../../../src/service/util/incoming-message.resolver';
import * as containerService from '../../../src/service/loader/container.service';
import { BasePrices } from '../../../src/models/base-prices.interface';
import { FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';
import * as queryParameterResolver from '../../../src/service/controllers/get-query-parameter';
import * as responder from '../../../src/service/controllers/send-json-response';
import * as priceTransformer from '../../../src/service/factories/price.transformer';

describe('BasePrices Controller', () => {

    const basePrices: BasePrices = { family: 'family', desktop: 72, web: 108, app: 144 };
    const basePricesWithId = [ { _id: new ObjectId('6064408a04cfcc0cf8de7b1a'), ...basePrices } ] as WithId<BasePrices>[];
    const errorMessage = 'error';

    it('handleBasePricesQuery should return basePrices', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(basePricesDao, 'getBasePrices').and.returnValue(Promise.resolve([ basePrices ]));
        spyOn(fontDao, 'getFontFamilies').and.returnValue(Promise.resolve([ 'family', 'family2' ]));

        handleBasePricesQuery(request, response)
            .then(() => {
                expect(basePricesDao.getBasePrices).toHaveBeenCalledWith();
                expect(fontDao.getFontFamilies).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([ basePrices, { family: 'family2' } ]));
                done();
            });
    });

    it('handleBasePricesQuery should send error when error happens', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(basePricesDao, 'getBasePrices').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleBasePricesQuery(request, response)
            .then(() => {
                expect(basePricesDao.getBasePrices).toHaveBeenCalledWith();
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleBasePricesAdminQuery should return basePrices with id', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(basePricesDao, 'getBasePricesWithId').and.returnValue(Promise.resolve(basePricesWithId));

        handleBasePricesAdminQuery(request, response)
            .then(() => {
                expect(basePricesDao.getBasePricesWithId).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json')
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(basePricesWithId));
                done();
            });
    });

    it('handleBasePricesAdminQuery should send error when error happens', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(basePricesDao, 'getBasePricesWithId').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleBasePricesAdminQuery(request, response)
            .then(() => {
                expect(basePricesDao.getBasePricesWithId).toHaveBeenCalledWith();
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleBasePricesRemoval should remove and send basePrices', (done) => {
        const family = 'family';
        const request = {} as Http2ServerRequest;
        const payload = { family };
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const addResult = {} as InsertOneWriteOpResult<WithId<BasePrices>>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(payload));
        spyOn(basePricesDao, 'removeBasePrices').and.returnValue(Promise.resolve(addResult));
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(basePricesDao, 'getBasePrices').and.returnValue(Promise.resolve([ basePrices ]));
        spyOn(fontDao, 'getFontFamilies').and.returnValue(Promise.resolve([ family ]));

        handleBasePricesRemoval(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(basePricesDao.removeBasePrices).toHaveBeenCalledWith(family);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(basePricesDao.getBasePrices).toHaveBeenCalledWith();
                expect(fontDao.getFontFamilies).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([ basePrices ]));
                done();
            });
    });

    it('handleBasePricesRemoval should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleBasePricesRemoval(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleBasePricesUpdate should update and send basePrices', (done) => {
        const family = 'family';
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const result = {} as FindAndModifyWriteOpResultObject<BasePrices>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(basePrices));
        spyOn(basePricesDao, 'upsertBasePrices').and.returnValue(Promise.resolve(result));
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(basePricesDao, 'getBasePrices').and.returnValue(Promise.resolve([ basePrices ]));
        spyOn(fontDao, 'getFontFamilies').and.returnValue(Promise.resolve([ family ]));

        handleBasePricesUpdate(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(basePricesDao.upsertBasePrices).toHaveBeenCalledWith(basePrices);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(basePricesDao.getBasePrices).toHaveBeenCalledWith();
                expect(fontDao.getFontFamilies).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([ basePrices ]));
                done();
            });
    });

    it('handleBasePricesUpdate should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleBasePricesUpdate(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handlePricesQuery should send transformed prices', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        const priceBlocks = [{ platform: 1, licenseSize: 1, singleStyle: 2, singlePair: 3, family: 10 }];
        spyOn(queryParameterResolver, 'getQueryParameter').and.returnValue(basePrices.family);
        spyOn(basePricesDao, 'getBasePricesByFamily').and.returnValue(Promise.resolve(basePrices));
        spyOn(priceTransformer, 'transform').and.returnValue(priceBlocks);
        spyOn(responder, 'sendJsonResponse');

        handlePricesQuery(request, response)
            .then(() => {
                expect(queryParameterResolver.getQueryParameter).toHaveBeenCalledWith(request, 'family');
                expect(basePricesDao.getBasePricesByFamily).toHaveBeenCalledWith(basePrices.family);
                expect(priceTransformer.transform).toHaveBeenCalledWith(basePrices);
                expect(responder.sendJsonResponse).toHaveBeenCalledWith(priceBlocks, response);
                done();
            });
    });

    it('handlePricesQuery should send empty array when there are no prices in db', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(queryParameterResolver, 'getQueryParameter').and.returnValue(basePrices.family);
        spyOn(basePricesDao, 'getBasePricesByFamily').and.returnValue(Promise.resolve(null));
        spyOn(responder, 'sendJsonResponse');

        handlePricesQuery(request, response)
            .then(() => {
                expect(queryParameterResolver.getQueryParameter).toHaveBeenCalledWith(request, 'family');
                expect(basePricesDao.getBasePricesByFamily).toHaveBeenCalledWith(basePrices.family);
                expect(responder.sendJsonResponse).toHaveBeenCalledWith([], response);
                done();
            });
    });

    it('handlePricesQuery should send error message when an error happens', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        const error = new Error(errorMessage);
        spyOn(queryParameterResolver, 'getQueryParameter').and.throwError(error);
        spyOn(errorResponder, 'sendErrorResponse');

        handlePricesQuery(request, response)
            .then(() => {
                expect(queryParameterResolver.getQueryParameter).toHaveBeenCalledWith(request, 'family');
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
                done();
            });
    });

});
