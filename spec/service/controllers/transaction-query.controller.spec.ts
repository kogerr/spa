import * as urlUtil from 'url';
import * as queryFacade from '../../../src/service/payment/query/transaction-query.facade';
import { getTransactionDetails } from '../../../src/service/controllers/transaction-query.controller';
import { IncomingMessage, ServerResponse } from 'http';

describe('Transaction Query Controller', () => {

    const id = 'id';
    const url = 'url';
    const request = { url } as IncomingMessage;
    const query = { id };
    const end = jasmine.createSpy('res.end');
    const response = { end } as unknown as ServerResponse;

    it('should return transaction query result', (done) => {
        const result = 'result';
        spyOn(urlUtil, 'parse').and.returnValue({ query } as unknown as urlUtil.UrlWithParsedQuery);
        spyOn(queryFacade, 'queryTransactionDetails').and.returnValue(Promise.resolve(result));


        getTransactionDetails(request, response).then(() => {
            expect(urlUtil.parse).toHaveBeenCalledWith(url, true);
            expect(queryFacade.queryTransactionDetails).toHaveBeenCalledWith(id);
            expect(end).toHaveBeenCalledWith(result);
            done();
        })
    });

    it('should return error when there is an error', (done) => {
        const error = 'error';
        spyOn(urlUtil, 'parse').and.returnValue({ query } as unknown as urlUtil.UrlWithParsedQuery)
        spyOn(queryFacade, 'queryTransactionDetails').and.returnValue(Promise.reject(error));


        getTransactionDetails(request, response).then(() => {
            expect(urlUtil.parse).toHaveBeenCalledWith(url, true);
            expect(queryFacade.queryTransactionDetails).toHaveBeenCalledWith(id);
            expect(end).toHaveBeenCalledWith(error);
            done();
        })
    });
});
