import { handleProjectSlidesUpdate, serveProjectSlides, serveProjectSlidesByUrl } from '../../../src/service/controllers/project-slides.controller';
import * as urlUtil from 'url';
import * as projectsDao from '../../../src/db/project.dao';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import * as messageResolver from '../../../src/service/util/incoming-message.resolver';
import * as unlinker from '../../../src/service/util/file-unlinker';
import { IncomingMessage } from 'http';
import { Http2ServerResponse } from 'http2';
import { FindAndModifyWriteOpResultObject } from 'mongodb';
import { Project } from '../../../src/models/project/project.interface';

describe('Project Slides Controller', () => {

    const id = 'id';
    const url = 'url';
    const request = { url } as IncomingMessage;

    it('serveProjectSlides should return slides based on id', (done) => {
        const query = { id };
        const slides = [ { index: 0, src: 'src' } ];
        spyOn(urlUtil, 'parse').and.returnValue({ query } as unknown as urlUtil.UrlWithParsedQuery);
        spyOn(projectsDao, 'getProjectSlides').and.returnValue(Promise.resolve(slides));
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);

        serveProjectSlides(request, response).then(() => {
            expect(urlUtil.parse).toHaveBeenCalledWith(url, true);
            expect(projectsDao.getProjectSlides).toHaveBeenCalledWith(id);
            expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith(JSON.stringify(slides));
            done();
        })
    });

    it('serveProjectSlides should return error when there is an error', (done) => {
        const query = { id };
        const error = 'error';
        spyOn(urlUtil, 'parse').and.returnValue({ query } as unknown as urlUtil.UrlWithParsedQuery);
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(projectsDao, 'getProjectSlides').and.returnValue(Promise.reject(error));
        spyOn(errorResponder, 'sendErrorResponse');

        serveProjectSlides(request, response).then(() => {
            expect(urlUtil.parse).toHaveBeenCalledWith(url, true);
            expect(projectsDao.getProjectSlides).toHaveBeenCalledWith(id);
            expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
            done();
        })
    });

    it('serveProjectSlidesByUrl should return slides based on url', (done) => {
        const query = { url: 'url' };
        const slides = [ { index: 0, src: 'src' } ];
        spyOn(urlUtil, 'parse').and.returnValue({ query } as unknown as urlUtil.UrlWithParsedQuery);
        spyOn(projectsDao, 'getProjectSlidesByUrl').and.returnValue(Promise.resolve(slides));
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);

        serveProjectSlidesByUrl(request, response).then(() => {
            expect(urlUtil.parse).toHaveBeenCalledWith(url, true);
            expect(projectsDao.getProjectSlidesByUrl).toHaveBeenCalledWith('url');
            expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith(JSON.stringify(slides));
            done();
        })
    });

    it('serveProjectSlidesByUrl should return error when there is an error', (done) => {
        const query = { url: 'url' };
        const error = 'error';
        spyOn(urlUtil, 'parse').and.returnValue({ query } as unknown as urlUtil.UrlWithParsedQuery);
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(projectsDao, 'getProjectSlidesByUrl').and.returnValue(Promise.reject(error));
        spyOn(errorResponder, 'sendErrorResponse');

        serveProjectSlidesByUrl(request, response).then(() => {
            expect(urlUtil.parse).toHaveBeenCalledWith(url, true);
            expect(projectsDao.getProjectSlidesByUrl).toHaveBeenCalledWith('url');
            expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
            done();
        })
    });

    it('handleProjectSlidesUpdate should update and serve slides', (done) => {
        const slides = [ { index: 0, src: 'src' } ];
        const remove = 'remove';
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve({ id, slides, remove }));
        const result = {} as FindAndModifyWriteOpResultObject<Project>;
        spyOn(projectsDao, 'updateProjectSlides').and.returnValue(Promise.resolve(result));
        spyOn(unlinker, 'unlinkFile').and.returnValue(Promise.resolve());
        spyOn(projectsDao, 'getProjectSlides').and.returnValue(Promise.resolve(slides));
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);

        handleProjectSlidesUpdate(request, response).then(() => {
            expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
            expect(projectsDao.updateProjectSlides).toHaveBeenCalledWith(id, slides);
            expect(unlinker.unlinkFile).toHaveBeenCalledWith(remove);
            expect(projectsDao.getProjectSlides).toHaveBeenCalledWith(id);
            expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith(JSON.stringify(slides));
            done();
        })
    });

    it('should return error when there is an error', (done) => {
        const error = 'error';
        const slides = [ { index: 0, src: 'src' } ];
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve({ id, slides }));
        spyOn(projectsDao, 'updateProjectSlides').and.returnValue(Promise.reject(error));
        spyOn(errorResponder, 'sendErrorResponse');
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);

        handleProjectSlidesUpdate(request, response).then(() => {
            expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
            expect(projectsDao.updateProjectSlides).toHaveBeenCalledWith(id, slides);
            expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
            done();
        })
    });
});
