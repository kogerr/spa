import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { handleLandingSlidesQuery, handleLandingSlidesAdminQuery, handleLandingSlidesUpload, handleLandingSlideRemoval,
    handleLandingSlideUpdate } from '../../../src/service/controllers/landing-slide.controller';
import * as slidesDao from '../../../src/db/landing-slide.dao';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import * as messageResolver from '../../../src/service/util/incoming-message.resolver';
import * as fileUnlinker from '../../../src/service/util/file-unlinker';
import * as containerService from '../../../src/service/loader/container.service';
import { LandingSlide } from '../../../src/models/landing-slide.interface';
import { FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';

describe('Landing Slides Controller', () => {

    const slide: LandingSlide = {index: 0, src: '/image.png', link: '/link', alt: 'alt'};
    const id = '6064408a04cfcc0cf8de7b1a';
    const _id = new ObjectId(id);
    const slidesWithId = [ { _id, ...slide } ] as WithId<LandingSlide>[];
    const errorMessage = 'error';

    it('handleLandingSlidesQuery should return slides', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(slidesDao, 'getLandingSlides').and.returnValue(Promise.resolve([ slide ]));

        handleLandingSlidesQuery(request, response)
            .then(() => {
                expect(slidesDao.getLandingSlides).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json')
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify([ slide ]));
                done();
            });
    });

    it('handleLandingSlidesQuery should send error when error happens', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(slidesDao, 'getLandingSlides').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleLandingSlidesQuery(request, response)
            .then(() => {
                expect(slidesDao.getLandingSlides).toHaveBeenCalledWith();
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleLandingSlidesAdminQuery should return slides with id', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        spyOn(slidesDao, 'getLandingSlidesWithId').and.returnValue(Promise.resolve(slidesWithId));

        handleLandingSlidesAdminQuery(request, response)
            .then(() => {
                expect(slidesDao.getLandingSlidesWithId).toHaveBeenCalledWith();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json')
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(slidesWithId));
                done();
            });
    });

    it('handleLandingSlidesAdminQuery should send error when error happens', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(slidesDao, 'getLandingSlidesWithId').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleLandingSlidesAdminQuery(request, response)
            .then(() => {
                expect(slidesDao.getLandingSlidesWithId).toHaveBeenCalledWith();
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleLandingSlidesUpload should add and send slides', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const addResult = {} as InsertOneWriteOpResult<WithId<LandingSlide>>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(slide));
        spyOn(slidesDao, 'addLandingSlide').and.returnValue(Promise.resolve(addResult));
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(slidesDao, 'getLandingSlidesWithId').and.returnValue(Promise.resolve(slidesWithId));

        handleLandingSlidesUpload(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(slidesDao.addLandingSlide).toHaveBeenCalledWith(slide);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(slidesDao.getLandingSlidesWithId).toHaveBeenCalled();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(slidesWithId));
                done();
            });
    });

    it('handleLandingSlidesUpload should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleLandingSlidesUpload(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleLandingSlideRemoval should remove and send slides', (done) => {
        const request = {} as Http2ServerRequest;
        const payload = { id, src: slide.src };
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const addResult = {} as InsertOneWriteOpResult<WithId<LandingSlide>>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(payload));
        spyOn(slidesDao, 'removeLandingSlide').and.returnValue(Promise.resolve(addResult));
        spyOn(fileUnlinker, 'unlinkFile').and.returnValue(Promise.resolve());
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(slidesDao, 'getLandingSlidesWithId').and.returnValue(Promise.resolve(slidesWithId));

        handleLandingSlideRemoval(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(slidesDao.removeLandingSlide).toHaveBeenCalledWith(id);
                expect(fileUnlinker.unlinkFile).toHaveBeenCalledWith(slide.src);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(slidesDao.getLandingSlidesWithId).toHaveBeenCalled();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(slidesWithId));
                done();
            });
    });

    it('handleLandingSlideRemoval should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleLandingSlideRemoval(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });

    it('handleLandingSlideUpdate should update and send slides', (done) => {
        const request = {} as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'end']);
        const result = {} as FindAndModifyWriteOpResultObject<LandingSlide>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(slidesWithId));
        spyOn(slidesDao, 'updateLandingSlides').and.returnValue(Promise.resolve([ result ]));
        spyOn(containerService, 'reloadPagesAndApi');
        spyOn(slidesDao, 'getLandingSlidesWithId').and.returnValue(Promise.resolve(slidesWithId));

        handleLandingSlideUpdate(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(slidesDao.updateLandingSlides).toHaveBeenCalledWith(slidesWithId);
                expect(containerService.reloadPagesAndApi).toHaveBeenCalled();
                expect(slidesDao.getLandingSlidesWithId).toHaveBeenCalled();
                expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
                // @ts-ignore
                expect(response.end).toHaveBeenCalledWith(JSON.stringify(slidesWithId));
                done();
            });
    });

    it('handleLandingSlideUpdate should send error when there is an error', (done) => {
        const request = {} as Http2ServerRequest;
        const response = {} as Http2ServerResponse;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(errorMessage));
        spyOn(errorResponder, 'sendErrorResponse');

        handleLandingSlideUpdate(request, response)
            .then(() => {
                expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
                expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, errorMessage);
                done();
            });
    });
});
