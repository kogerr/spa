import { IncomingMessage, ServerResponse } from 'http';
import * as smtpDao from '../../../src/db/smtp.dao';
import { handleSmtpUpdate } from '../../../src/service/controllers/smtp-update.controller';
import * as incomingMessageResolver from '../../../src/service/util/incoming-message.resolver';

const req = {} as unknown as IncomingMessage;
const host = 'host';
const from = 'from';
const password = 'password';
const payload = { host, from, password };

describe('SMTP properties update controller', () => {
    let writeHead: jasmine.Spy;
    let end: jasmine.Spy;
    let res: ServerResponse;

    beforeEach(() => {
        writeHead = jasmine.createSpy('res.writeHead');
        end = jasmine.createSpy('res.end');
        res = { writeHead, end } as unknown as ServerResponse;
        spyOn(incomingMessageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(payload));
    });

    it('should handle SMTP properties update', async () => {
        spyOn(smtpDao, 'updateSmtpProperties').and.returnValue(Promise.resolve({ ok: 1 }));

        await handleSmtpUpdate(req, res);

        expect(incomingMessageResolver.getRequestDataJson).toHaveBeenCalledWith(req);
        expect(smtpDao.updateSmtpProperties).toHaveBeenCalledWith(host, from, password);
        expect(writeHead).toHaveBeenCalledWith(200, { 'Content-Type': 'application/json' });
        expect(end).toHaveBeenCalledWith(JSON.stringify({ success: true }));
    });

    it('should send error response on error', async () => {
        spyOn(smtpDao, 'updateSmtpProperties').and.returnValue(Promise.resolve({ ok: 0 }));

        await handleSmtpUpdate(req, res);

        expect(incomingMessageResolver.getRequestDataJson).toHaveBeenCalledWith(req);
        expect(smtpDao.updateSmtpProperties).toHaveBeenCalledWith(host, from, password);
        expect(writeHead).toHaveBeenCalledWith(400, { 'Content-Type': 'application/json' });
        expect(end).toHaveBeenCalledWith(JSON.stringify({ success: false, error: new Error('Could not update smtp properties') }));
    });
});
