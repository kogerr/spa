import { IncomingMessage, ServerResponse } from 'http';
import { handleFontUpdate } from '../../../src/service/controllers/font-update.controller';
import * as containerService from '../../../src/service/loader/container.service';
import * as messageResolver from '../../../src/service/util/incoming-message.resolver';
import * as projectFontDao from '../../../src/db/project-font.dao';
import * as jsonResponder from '../../../src/service/controllers/send-json-response';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import { FindAndModifyWriteOpResultObject } from 'mongodb';
import { Project } from 'src/models/project/project.interface';

describe('FontUpdate controller', () => {
    const url = 'url';
    const request = { url } as IncomingMessage;
    const end = jasmine.createSpy('res.end');
    const setHeader = jasmine.createSpy('res.setHeader');
    const response = { end, setHeader } as unknown as ServerResponse;

    it('handleFontUpdate should handle fonts update', (done) => {
        const projectUrl = 'projectUrl';
        const fonts = [ { index: 0,  id: 'id' } ];
        const message = { projectUrl, fonts };
        const findAndModifyResult = { ok: 1 } as FindAndModifyWriteOpResultObject<Project>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(message));
        spyOn(projectFontDao, 'updateProjectFonts').and.returnValue(Promise.resolve(findAndModifyResult));
        spyOn(jsonResponder, 'sendJsonResponse');
        spyOn(containerService, 'reloadPagesAndApi').and.returnValue(Promise.resolve());

        handleFontUpdate(request, response).then(() => {
            expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
            expect(projectFontDao.updateProjectFonts).toHaveBeenCalledWith(projectUrl, fonts);
            expect(jsonResponder.sendJsonResponse).toHaveBeenCalledWith(findAndModifyResult, response);
            expect(containerService.reloadPagesAndApi).toHaveBeenCalledWith();
            done();
        })
    });

    it('handleFontUpdate should send error', (done) => {
        const error = new Error('error');
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(error));
        spyOn(errorResponder, 'sendErrorResponse');

        handleFontUpdate(request, response).then(() => {
            expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
            expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
            done();
        })
    });

});
