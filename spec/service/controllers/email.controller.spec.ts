import { IncomingMessage, ServerResponse } from 'http';
import * as exampleEmail from '../../../src/components/example-email/example-email';
import * as smtpDao from '../../../src/db/smtp.dao';
import { handleEmailRequest } from '../../../src/service/controllers/email.controller';
import * as smtpClient from '../../../src/service/email/smtp.client';
import * as incomingMessageResolver from '../../../src/service/util/incoming-message.resolver';

const from = 'from';
const to = 'to';
const name = 'name';
const htmlContent = 'htmlContent';
const smtpProperties = { from, credentials: 'credentials', host: 'host' };
const req = {} as unknown as IncomingMessage;
const payload = { to, name };

describe('email controller', () => {
    let writeHead: jasmine.Spy;
    let end: jasmine.Spy;
    let res: ServerResponse;

    beforeEach(() => {
        writeHead = jasmine.createSpy('res.writeHead');
        end = jasmine.createSpy('res.end');
        res = { writeHead, end } as unknown as ServerResponse;
        spyOn(incomingMessageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(payload));
        spyOn(exampleEmail, 'renderExampleEmail').and.returnValue(htmlContent);
    });

    it('should handle email requests', async () => {
        spyOn(smtpDao, 'getSmtpProperties').and.returnValue(Promise.resolve(smtpProperties));
        spyOn(smtpClient, 'sendEmail');

        await handleEmailRequest(req, res);

        expect(incomingMessageResolver.getRequestDataJson).toHaveBeenCalledWith(req);
        expect(exampleEmail.renderExampleEmail).toHaveBeenCalledWith(name);
        expect(smtpDao.getSmtpProperties).toHaveBeenCalledWith();
        expect(smtpClient.sendEmail).toHaveBeenCalledWith(to, htmlContent, smtpProperties);
        expect(writeHead).toHaveBeenCalledWith(200, { 'Content-Type': 'application/json' });
        expect(end).toHaveBeenCalledWith(JSON.stringify({ success: true }));
    });

    it('should send error response on error', async () => {
        spyOn(smtpDao, 'getSmtpProperties').and.returnValue(Promise.resolve(smtpProperties));
        spyOn(smtpClient, 'sendEmail').and.throwError('error');

        await handleEmailRequest(req, res);

        expect(incomingMessageResolver.getRequestDataJson).toHaveBeenCalledWith(req);
        expect(exampleEmail.renderExampleEmail).toHaveBeenCalledWith(name);
        expect(smtpDao.getSmtpProperties).toHaveBeenCalledWith();
        expect(smtpClient.sendEmail).toHaveBeenCalledWith(to, htmlContent, smtpProperties);
        expect(writeHead).toHaveBeenCalledWith(400, { 'Content-Type': 'application/json' });
        expect(end).toHaveBeenCalledWith(JSON.stringify({ success: false, error: {} }));
    });

    it('should send error response when credentials cannot be read', async () => {
        spyOn(smtpDao, 'getSmtpProperties').and.returnValue(Promise.resolve(null));

        await handleEmailRequest(req, res);

        expect(incomingMessageResolver.getRequestDataJson).toHaveBeenCalledWith(req);
        expect(exampleEmail.renderExampleEmail).toHaveBeenCalledWith(name);
        expect(smtpDao.getSmtpProperties).toHaveBeenCalledWith();
        expect(writeHead).toHaveBeenCalledWith(400, { 'Content-Type': 'application/json' });
        expect(end).toHaveBeenCalledWith(JSON.stringify({ success: false, error: {} }));
    });
});
