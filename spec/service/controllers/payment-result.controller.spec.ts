import { handlePaymentResult } from '../../../src/service/controllers/payment-result.controller';
import { Http2ServerRequest } from 'http2';
import { ServerResponse } from 'http';
import * as parameterParser from '../../../src/service/util/parameter.parser';
import * as resultFacade from '../../../src/service/payment/back/barion/result-facade';
import * as responder from '../../../src/service/controllers/send-json-response';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import { Order } from '../../../src/models/order.interface';
import { Customer } from '../../../src/models/customer.interface';
import { StatusResponse } from '../../../src/models/barion/status-response.interface';

describe('Payment Result Controller', () => {
    const total = 100;
    const paymentId = 'paymentId';
    const orderId = 'orderId';
    const customer = { email: 'email' } as Customer;
    const order: Order = { paymentId, status: 'succeeded', orderId, total, customer, updated: Date.now(), items: [], products: [] };
    const status = {} as StatusResponse;

    it('should render payment result page',  (done) => {
        const url = 'asdf?qwer';
        const request = { url } as unknown as Http2ServerRequest;
        const end = jasmine.createSpy('res.end');
        const setHeader = jasmine.createSpy('res.setHeader');
        const response = { end, setHeader, statusCode: 0 } as unknown as ServerResponse;
        const parameters = new Map<string, string>([[ 'orderId', orderId ]]);
        spyOn(parameterParser, 'parseParameters').and.returnValue(parameters);
        spyOn(resultFacade, 'queryStatus').and.returnValue(Promise.resolve({ order, status }));
        spyOn(responder, 'sendJsonResponse');

        handlePaymentResult(request, response).then(() => {
            expect(parameterParser.parseParameters).toHaveBeenCalledWith('qwer');
            expect(resultFacade.queryStatus).toHaveBeenCalledWith(orderId);
            expect(responder.sendJsonResponse).toHaveBeenCalledWith({ order, status }, response);
            done();
        });
    });

    it('should throw error if there is no url',  (done) => {
        const request = {} as unknown as Http2ServerRequest;
        const response = { } as unknown as ServerResponse;
        spyOn(errorResponder, 'sendErrorResponse');

        handlePaymentResult(request, response).then(() => {
            expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, new TypeError('Cannot read properties of undefined (reading \'split\')'));
            done();
        });
    });

});
