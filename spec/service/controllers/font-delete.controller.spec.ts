import { IncomingMessage, ServerResponse } from 'http';
import { handleFontDelete } from '../../../src/service/controllers/font-delete.controller';
import * as containerService from '../../../src/service/loader/container.service';
import * as messageResolver from '../../../src/service/util/incoming-message.resolver';
import * as projectFontDao from '../../../src/db/project-font.dao';
import * as fontDao from '../../../src/db/font.dao';
import * as jsonResponder from '../../../src/service/controllers/send-json-response';
import * as errorResponder from '../../../src/service/controllers/send-error-response';
import { FindAndModifyWriteOpResultObject } from 'mongodb';
import { Project } from '../../../src/models/project/project.interface';

describe('Font delete controller', () => {
    const projectUrl = 'projectUrl';
    const id = 'id';
    const query = { projectUrl, id };
    const request = { url: 'url' } as IncomingMessage;
    const response = { end: () => {} } as unknown as ServerResponse;

    it('handleFontDelete should handle FontFace deletion', (done) => {
        const deleteResponse = { result: {} };
        const findAndModifyResult = { ok: 1 } as FindAndModifyWriteOpResultObject<Project>;
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.resolve(query));
        spyOn(fontDao, 'removeFontById').and.returnValue(Promise.resolve(deleteResponse));
        spyOn(projectFontDao, 'removeProjectFonts').and.returnValue(Promise.resolve(findAndModifyResult));
        spyOn(containerService, 'reloadPagesAndApi').and.returnValue(Promise.resolve());
        spyOn(jsonResponder, 'sendJsonResponse');

        handleFontDelete(request, response).then(() => {
            expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
            expect(fontDao.removeFontById).toHaveBeenCalledWith(id);
            expect(projectFontDao.removeProjectFonts).toHaveBeenCalledWith(projectUrl, id);
            expect(containerService.reloadPagesAndApi).toHaveBeenCalledWith();
            expect(jsonResponder.sendJsonResponse).toHaveBeenCalledWith(findAndModifyResult, response)
            done();
        })
    });

    it('handleFontDelete should send error', (done) => {
        const error = new Error('error');
        spyOn(messageResolver, 'getRequestDataJson').and.returnValue(Promise.reject(error));
        spyOn(errorResponder, 'sendErrorResponse');

        handleFontDelete(request, response).then(() => {
            expect(messageResolver.getRequestDataJson).toHaveBeenCalledWith(request);
            expect(errorResponder.sendErrorResponse).toHaveBeenCalledWith(response, error);
            done();
        })
    });

});
