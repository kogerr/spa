import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import * as userDao from '../../../src/db/user.dao';
import { handleRegister } from '../../../src/service/controllers/register.controller';
import * as incomingMessageResolver from '../../../src/service/util/incoming-message.resolver';
import * as jwtFactory from '../../../src/service/util/jwt.factory';

describe('Register Controller', () => {

    process.env.REGISTRATION = 'true';

    const email = 'email';
    const password = 'password';

    it('handleRegister responds with token', async () => {
        const token = 'token';
        const request = { headers: { cookie: 'cookie' } } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        spyOn(incomingMessageResolver, 'getRequestParameters')
            .and.returnValue(Promise.resolve(new Map([['email', email], [ 'password', password]])));
        spyOn(userDao, 'checkEmail').and.returnValue(Promise.resolve(false));
        spyOn(userDao, 'registerUser').and.returnValue(Promise.resolve({ email, password }));
        spyOn(jwtFactory, 'createToken').and.returnValue(token);

        await handleRegister(request, response);

        expect(incomingMessageResolver.getRequestParameters).toHaveBeenCalledWith(request);
        expect(userDao.checkEmail).toHaveBeenCalledWith(email);
        expect(userDao.registerUser).toHaveBeenCalledWith(email, jasmine.any(String));
        expect(jwtFactory.createToken).toHaveBeenCalledWith(email);
        expect(response.setHeader).toHaveBeenCalledWith('Set-Cookie', 'access_token=' + token + '; Path=/; Secure');
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
    });

    it('handleLogin redirects on error when user already exists', async () => {
        const request = { headers: { cookie: 'cookie' } } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        spyOn(incomingMessageResolver, 'getRequestParameters')
            .and.returnValue(Promise.resolve(new Map([['email', email], [ 'password', password]])));
        spyOn(userDao, 'checkEmail').and.returnValue(Promise.resolve(true));

        await handleRegister(request, response);

        expect(incomingMessageResolver.getRequestParameters).toHaveBeenCalledWith(request);
        expect(userDao.checkEmail).toHaveBeenCalledWith(email);
        expect(response.setHeader).toHaveBeenCalledWith('error', 'User already exists.');
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/registration?error=User%20already%20exists.');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
    });

    it('handleLogin redirects on error when there are no credentials', async () => {
        const request = {} as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        spyOn(incomingMessageResolver, 'getRequestParameters').and.returnValue(Promise.resolve(new Map([])));

        await handleRegister(request, response);

        expect(incomingMessageResolver.getRequestParameters).toHaveBeenCalledWith(request);
        expect(response.setHeader).toHaveBeenCalledWith('error', 'Cannot parse credentials.');
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/registration?error=Cannot%20parse%20credentials.');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
    });

    it('handleLogin redirects on error when unexpected error occurs', async () => {
        const errorMessage = 'Unexpected error';
        const request = { headers: { cookie: 'cookie' } } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        spyOn(incomingMessageResolver, 'getRequestParameters').and.returnValue(Promise.reject(new Error(errorMessage)));

        await handleRegister(request, response);

        expect(incomingMessageResolver.getRequestParameters).toHaveBeenCalledWith(request);
        expect(response.setHeader).toHaveBeenCalledWith('error', errorMessage);
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/registration?error=Unexpected%20error');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
    });

    it('handleLogin redirects on error when registration is closed', async () => {
        delete(process.env.REGISTRATION);
        const request = { headers: { cookie: 'cookie' } } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>([ 'setHeader', 'writeHead', 'end']);
        spyOn(incomingMessageResolver, 'getRequestParameters')
            .and.returnValue(Promise.resolve(new Map([['email', email], [ 'password', password]])));
        spyOn(userDao, 'checkEmail').and.returnValue(Promise.resolve(false));

        await handleRegister(request, response);

        expect(incomingMessageResolver.getRequestParameters).toHaveBeenCalledWith(request);
        expect(userDao.checkEmail).toHaveBeenCalledWith(email);
        expect(response.setHeader).toHaveBeenCalledWith('error', 'Not open for registration.');
        expect(response.setHeader).toHaveBeenCalledWith('Location', '/registration?error=Not%20open%20for%20registration.');
        expect(response.end).toHaveBeenCalled();
        expect(response.statusCode).toBe(302);
    });
});
