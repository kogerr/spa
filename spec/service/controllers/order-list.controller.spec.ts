import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { Order } from '../../../src/models/order.interface';
import * as orderDao from '../../../src/db/order.dao';
import { getOrderListPage } from '../../../src/service/controllers/order-list.controller';
import * as orderListComponent from '../../../src/components/admin/order-list/order-list';
import { Page } from '../../../src/models/page.interface';
import * as mainframe from '../../../src/components/mainframe/mainframe';
import { Customer } from '../../../src/models/customer.interface';

describe('Order List Controller', () => {

    const customer = { email: 'email' } as Customer;
    const orderRef = '0123';
    const paymentId = '1234';
    const updated = 5678;
    const order: Order = { orderId: orderRef, total: 100, status: 'pending', paymentId, customer, updated, items: [], products: [] };

    it('should render order list component', (done) => {
        const request = { headers: { cookie: 'cookie' } } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'writeHead', 'end']);
        const page = { main: 'main' } as Page;
        const content = 'content';
        spyOn(orderDao, 'getOrders').and.returnValue(Promise.resolve([order]));
        spyOn(orderListComponent, 'renderOrderList').and.returnValue(page);
        spyOn(mainframe, 'renderMainframe').and.returnValue({ content, path: '' });

        getOrderListPage(request, response).then(() => {
            expect(orderDao.getOrders).toHaveBeenCalledWith();
            expect(orderListComponent.renderOrderList).toHaveBeenCalledWith([order]);
            expect(mainframe.renderMainframe).toHaveBeenCalledWith(page);
            expect(response.statusCode).toBe(200);
            expect(response.setHeader).toHaveBeenCalledWith('Content-Type', 'text/html;charset=UTF-8');
            // @ts-ignore
            expect(response.end).toHaveBeenCalledWith(content);
            done();
        });
    });

    it('should render order list component', (done) => {
        const request = { headers: { cookie: 'cookie' } } as unknown as Http2ServerRequest;
        const response = jasmine.createSpyObj<Http2ServerResponse>(['setHeader', 'writeHead', 'end']);
        const error = 'error';
        spyOn(orderDao, 'getOrders').and.returnValue(Promise.reject(error));
        spyOn(console, error);

        getOrderListPage(request, response).then(() => {
            expect(console.error).toHaveBeenCalledWith(error);
            done();
        });
    });
});
