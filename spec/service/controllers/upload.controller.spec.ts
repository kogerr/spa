import * as fs from 'fs';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import * as containerService from '../../../src/service/loader/container.service';
import { handleUpload } from '../../../src/service/controllers/upload.controller';
import * as incomingMessageResolver from '../../../src/service/util/incoming-message.resolver';

describe('Upload Controller', () => {

    const filename = 'filename';
    const request = { headers: { filename }} as unknown as Http2ServerRequest;
    let setHeader: (...arg: string[]) => void;
    let end: (arg: string) => void;
    let response: Http2ServerResponse;

    beforeEach(() => {
        setHeader = jasmine.createSpy();
        end = jasmine.createSpy('end');
        response = { setHeader, end, statusCode: 0 } as unknown as Http2ServerResponse;
    });

    it('should return error if file already exists', async () => {
        const statMock = (path: fs.PathLike, callback: any) => callback(null, { isFile: () => true } as fs.Stats);
        spyOn(fs, 'stat').and.callFake(statMock as typeof fs.stat);

        await handleUpload(request, response);

        // @ts-ignore
        expect(fs.stat).toHaveBeenCalledWith('src/assets/media/filename', jasmine.any(Function));
        expect(response.statusCode).toBe(409);
        expect(setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
        expect(end).toHaveBeenCalledWith('{"error":"A file with that name already exists.","filename":"filename"}');
    });

    it('should upload file', async () => {
        const statMock = (path: fs.PathLike, callback: any) => callback(null, { isFile: () => false } as fs.Stats);
        statMock.__promisify__ = () => Promise.resolve({} as fs.Stats);
        // @ts-ignore
        spyOn(fs, 'stat').and.callFake(statMock);
        const content = Buffer.from([]);
        spyOn(incomingMessageResolver, 'getRequestDataBuffer').and.returnValue(Promise.resolve(content));
        const writeFileMock = (path: any, data: any, options: any, callback: any) => callback(null);
        spyOn(fs, 'writeFile').and.callFake(writeFileMock as typeof fs.writeFile);
        spyOn(containerService, 'renderAssets').and.returnValue(Promise.resolve());
        spyOn(containerService, 'reloadPagesAndApi').and.returnValue(Promise.resolve());

        await handleUpload(request, response);

        // @ts-ignore
        expect(fs.stat).toHaveBeenCalledWith('src/assets/media/filename', jasmine.any(Function));
        expect(incomingMessageResolver.getRequestDataBuffer).toHaveBeenCalledWith(request);
        expect(fs.writeFile).toHaveBeenCalled();
        // @ts-ignore
        expect(containerService.renderAssets).toHaveBeenCalledWith(undefined);
        // @ts-ignore
        expect(containerService.reloadPagesAndApi).toHaveBeenCalledWith(undefined);
        expect(response.statusCode).toBe(200);
        expect(setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
        expect(end).toHaveBeenCalledWith('{"success":true,"path":"/media/filename"}');
    });

    it('should upload file with overwrite header', async () => {
        request.headers.overwrite = 'true';
        request.headers.base64 = 'true';
        const statMock = (path: fs.PathLike, callback: any) => callback(null, { isFile: () => true } as fs.Stats);
        statMock.__promisify__ = () => Promise.resolve({} as fs.Stats);
        // @ts-ignore
        spyOn(fs, 'stat').and.callFake(statMock);
        const content = Buffer.from([]);
        spyOn(incomingMessageResolver, 'getRequestDataBuffer').and.returnValue(Promise.resolve(content));
        const writeFileMock = (path: any, data: any, options: any, callback: any) => callback(null);
        spyOn(fs, 'writeFile').and.callFake(writeFileMock as typeof fs.writeFile);
        spyOn(containerService, 'renderAssets').and.returnValue(Promise.resolve());
        spyOn(containerService, 'reloadPagesAndApi').and.returnValue(Promise.resolve());

        await handleUpload(request, response);

        // @ts-ignore
        expect(fs.stat).toHaveBeenCalledWith('src/assets/media/filename.b64', jasmine.any(Function));
        expect(incomingMessageResolver.getRequestDataBuffer).toHaveBeenCalledWith(request);
        expect(fs.writeFile).toHaveBeenCalled();
        // @ts-ignore
        expect(containerService.renderAssets).toHaveBeenCalledWith(undefined);
        // @ts-ignore
        expect(containerService.reloadPagesAndApi).toHaveBeenCalledWith(undefined);
        expect(response.statusCode).toBe(200);
        expect(setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
        expect(end).toHaveBeenCalledWith('{"success":true,"path":"/media/filename.b64"}');
    });

    it('should return error if file cannot be written', async () => {
        const statMock = (path: fs.PathLike, callback: any) => callback(null, { isFile: () => false } as fs.Stats);
        statMock.__promisify__ = () => Promise.resolve({} as fs.Stats);
        // @ts-ignore
        spyOn(fs, 'stat').and.callFake(statMock);
        const content = Buffer.from([]);
        spyOn(incomingMessageResolver, 'getRequestDataBuffer').and.returnValue(Promise.resolve(content));
        const writeFileMock = (path: any, data: any, options: any, callback: any) => callback(new Error('asdf'));
        spyOn(fs, 'writeFile').and.callFake(writeFileMock as typeof fs.writeFile);

        await handleUpload(request, response);

        // @ts-ignore
        expect(fs.stat).toHaveBeenCalledWith('src/assets/media/filename.b64', jasmine.any(Function));
        expect(incomingMessageResolver.getRequestDataBuffer).toHaveBeenCalledWith(request);
        expect(fs.writeFile).toHaveBeenCalled();
        expect(response.statusCode).toBe(500);
        expect(setHeader).toHaveBeenCalledWith('Content-Type', 'application/json');
        expect(end).toHaveBeenCalledWith('{"error":"asdf"}');
    });
});
