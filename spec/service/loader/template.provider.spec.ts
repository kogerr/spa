import { NamedFile } from '../../../src/models/named-file.interface';
import { readTemplates } from '../../../src/service/loader/template.provider';
import * as fileReader from '../../../src/service/util/file-reader';

describe('Template Provider', () => {

    const templatesDirectory = './src/components';

    it('should read templates', async () => {
        const htmlFile = { name: 'asdf.html', path: 'path', content: Buffer.from('html content')} as NamedFile;
        const txtFile = { name: 'qwer.txt', path: 'path', content: Buffer.from('text content')} as NamedFile;
        spyOn(fileReader, 'readFromPath').and.returnValue(Promise.resolve([ htmlFile, txtFile ]));

        const actual = await readTemplates();

        expect(fileReader.readFromPath).toHaveBeenCalledWith(templatesDirectory);
        expect(actual).toEqual([{ name: htmlFile.name, path: htmlFile.path, content: '`html content`'}]);
    });
});
