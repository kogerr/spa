import * as zlib from 'zlib';
import { compress } from '../../../src/service/loader/compressor';

describe('Compressor', () => {
    it('should compress to gzip', async () => {
        const data = 'data';
        const compressed = 'compressed';
        const result = Buffer.from(compressed);
        const gzipMock = (buf: zlib.InputType, callback: zlib.CompressCallback) => { callback(null, result); };
        spyOn(zlib, 'gzip').and.callFake(gzipMock as typeof zlib.gzip);

        const actual = await compress(data);

        expect(zlib.gzip).toHaveBeenCalled();
        expect(actual).toEqual(compressed);
    });

    it('should propagate error', (done) => {
        const data = 'data';
        const compressed = 'compressed';
        const result = Buffer.from(compressed);
        const error = new Error('asdf');
        const gzipMock = (buf: zlib.InputType, callback: zlib.CompressCallback) => { callback(error, result); };
        spyOn(zlib, 'gzip').and.callFake(gzipMock as typeof zlib.gzip);

        compress(data)
            .catch((actual) => {
                expect(zlib.gzip).toHaveBeenCalled();
                expect(actual).toEqual(error);
                done();
            });
    });
});
