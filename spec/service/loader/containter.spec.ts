import * as mainframe from '../../../src/components/mainframe/mainframe';
import { Page } from '../../../src/models/page.interface';
import { api, assets, fullPages, hiddenAssets } from '../../../src/service/container';
import { initContainer, reloadPagesAndApi, renderAssets } from '../../../src/service/loader/container.service';
import * as rawPagesFactory from '../../../src/service/loader/raw-pages.factory';
import * as staticApiFactory from '../../../src/service/loader/static-api.factory';
import * as assetProvider from '../../../src/service/loader/asset.provider';
import * as templateProvider from '../../../src/service/loader/template.provider';
import * as compressor from '../../../src/service/loader/compressor';

describe('container', () => {
    it('init should call providers and factories', async () => {
        const template = { path: 'path', name: 'name', content: 'content' };
        const page = createPage();
        const asset = createAsset(false);
        const hiddenAsset = createAsset(true);
        const compressedContent = 'compressed content';

        spyOn(templateProvider, 'readTemplates').and.returnValue(Promise.resolve([ template ]));
        spyOn(rawPagesFactory, 'createRawPages').and.returnValue(Promise.resolve({ rawPages: [ page ], rawAdminPages: [] as Page[]}));
        spyOn(staticApiFactory, 'createStaticApi').and.returnValue(Promise.resolve([['a', 'b']]));
        spyOn(compressor, 'compress').and.returnValue(Promise.resolve(compressedContent));
        spyOn(mainframe, 'renderMainframe').and.returnValue({ path: 'path', content: 'content'});
        spyOn(assetProvider, 'readAssets').and.returnValue(Promise.resolve([ { urlPath: '', asset }, { urlPath: '', asset: hiddenAsset }]));

        await initContainer();

        expect(templateProvider.readTemplates).toHaveBeenCalledWith();
        expect(rawPagesFactory.createRawPages).toHaveBeenCalled();
        expect(staticApiFactory.createStaticApi).toHaveBeenCalled();
        expect(compressor.compress).toHaveBeenCalledWith('b');
        expect(mainframe.renderMainframe).toHaveBeenCalledWith(page);
        expect(compressor.compress).toHaveBeenCalledWith('content');
        expect(assetProvider.readAssets).toHaveBeenCalledWith();

        expect(api.get('a')).toEqual(compressedContent);
        expect(assets.get('')).toBe(asset);
        expect(hiddenAssets.get('')).toBe(hiddenAsset);
        expect(fullPages).toEqual(new Map([[ 'path', compressedContent ]]));
    });

    it('reloadPagesAndApi should call providers and factories', async () => {
        const page = createPage();
        spyOn(rawPagesFactory, 'createRawPages').and.returnValue(Promise.resolve({ rawPages: [ page ], rawAdminPages: [] as Page[]}));
        spyOn(staticApiFactory, 'createStaticApi').and.returnValue(Promise.resolve([['a', 'b']]));
        spyOn(mainframe, 'renderMainframe').and.returnValue({ path: 'path', content: 'content'});

        await reloadPagesAndApi();

        expect(rawPagesFactory.createRawPages).toHaveBeenCalled();
        expect(staticApiFactory.createStaticApi).toHaveBeenCalled();
        expect(mainframe.renderMainframe).toHaveBeenCalledWith(page);
    });

    it('reloadPagesAndApi should log error', async () => {
        const errorMessage = 'asdf';
        spyOn(rawPagesFactory, 'createRawPages').and.returnValue(Promise.reject(errorMessage));
        spyOn(console, 'error');

        await reloadPagesAndApi();

        expect(rawPagesFactory.createRawPages).toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledWith(errorMessage);
    });

    it('renderAssets should call readAssets', async () => {
        const asset = createAsset(false);
        const hiddenAsset = createAsset(true);
        spyOn(assetProvider, 'readAssets').and.returnValue(Promise.resolve([ { urlPath: '', asset }, { urlPath: '', asset: hiddenAsset }]));

        await renderAssets();

        expect(assetProvider.readAssets).toHaveBeenCalledWith();
        expect(assets.get('')).toBe(asset);
        expect(hiddenAssets.get('')).toBe(hiddenAsset);
    });

    it('renderAssets should delegate error', (done) => {
        const errorMessage = 'asdf';
        spyOn(assetProvider, 'readAssets').and.returnValue(Promise.reject(errorMessage));

        renderAssets()
            .catch((error) => {
                expect(error).toBe(errorMessage);
                done();
            });
    });
});

const createPage = () => ({
    main: 'main',
    path: 'path',
    scripts: [],
    styles: [],
    title: 'title',
});

const createAsset = (hidden: boolean) => ({
    content: 'content',
    contentType: 'contentType',
    eTag: 'eTag',
    filename: 'filename',
    hidden,
    lastModified: new Date(),
});
