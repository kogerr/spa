import { Page } from '../../../src/models/page.interface';
import { createStaticApi } from '../../../src/service/loader/static-api.factory';
import * as projectExtrasApiFactory from '../../../src/service/factories/project-extras-factory';

describe('Static Api Factory', () => {

    it('should create mapping', async () => {
        const rawPages = [] as Page[];
        const projectExtrasEndpoints: [string, string][] = [['/a', 'b']];
        spyOn(projectExtrasApiFactory, 'createProjectExtrasEndpoints').and.returnValue(Promise.resolve(projectExtrasEndpoints));

        const actual = await createStaticApi(rawPages);

        expect(projectExtrasApiFactory.createProjectExtrasEndpoints).toHaveBeenCalledWith();
        expect(actual).toEqual([[ '/api/pages', '[]' ],
            [ '/a', 'b' ]]);
    });
});
