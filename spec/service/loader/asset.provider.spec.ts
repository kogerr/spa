import * as crypto from 'crypto';
import * as fs from 'fs';
import { NamedFile } from '../../../src/models/named-file.interface';
import { readAssets } from '../../../src/service/loader/asset.provider';
import { mimeTypes } from '../../../src/service/providers/mime-types';
import * as compressor from '../../../src/service/loader/compressor';
import * as fileReader from '../../../src/service/util/file-reader';

describe('Asset Provider', () => {

    it('should read assets', async () => {
        const content = Buffer.from('content');
        const filename = 'filename';
        const file = { name: filename, path: 'path', content } as NamedFile;
        const base64file = { name: filename + '.b64', path: 'path', content } as NamedFile;
        const mtime = new Date(1986, 10, 9);
        const compressedFile = 'asdf';
        const contentType = 'contentType';
        spyOn(fileReader, 'readFromPath').and.returnValue(Promise.resolve([file, base64file]));
        spyOn(console, 'log');
        const statMock = (path: fs.PathLike, callback: any) => callback(null, { mtime, isFile: () => true } as fs.Stats);
        spyOn(mimeTypes, 'get').and.returnValue(contentType);
        spyOn(fs, 'stat').and.callFake(statMock as typeof fs.stat);
        spyOn(compressor, 'compress').and.returnValue(Promise.resolve(compressedFile));
        const digest = jasmine.createSpy().and.returnValue('eTag');
        const update = jasmine.createSpy('Hash::update').and.returnValue({ digest });
        spyOn(crypto, 'createHash').and.returnValue({ update } as unknown as crypto.Hash);

        const actual = await readAssets();

        expect(fileReader.readFromPath).toHaveBeenCalledWith(assetsDirectory);
        expect(console.log).toHaveBeenCalledWith('Loaded asset path/filename');
        expect(mimeTypes.get).toHaveBeenCalledWith('e');
        // @ts-ignore
        expect(fs.stat).toHaveBeenCalledWith('path/filename', jasmine.any(Function));
        expect(compressor.compress).toHaveBeenCalledWith(content);
        expect(crypto.createHash).toHaveBeenCalledWith('md5');
        expect(update).toHaveBeenCalledWith(content);
        expect(digest).toHaveBeenCalledWith('hex');
        const expectedAsset = { content: 'asdf', contentType: 'contentType', lastModified: mtime, eTag: 'eTag', filename, hidden: false };
        const expectedAsset2 = { content: 'content', contentType: 'contentType',
            lastModified: mtime, eTag: 'eTag', filename: filename + '.b64', hidden: false };
        expect(actual).toEqual([ { urlPath: 'path/filename', asset: expectedAsset },
            { urlPath: 'path/filename.b64', asset: expectedAsset2 } ]);
    });

    const assetsDirectory = './src/assets';

    it('should read assets when no mime type available', async () => {
        const content = Buffer.from('content');
        const filename = 'filename';
        const file = { name: filename, path: 'path', content } as NamedFile;
        const mtime = new Date(1986, 10, 9);
        const compressedFile = 'asdf';
        spyOn(fileReader, 'readFromPath').and.returnValue(Promise.resolve([file]));
        spyOn(console, 'log');
        spyOn(mimeTypes, 'get').and.returnValue(undefined);
        const statMock = (path: fs.PathLike, callback: any) => callback(null, { mtime, isFile: () => true } as fs.Stats);
        spyOn(fs, 'stat').and.callFake(statMock as typeof fs.stat);
        spyOn(compressor, 'compress').and.returnValue(Promise.resolve(compressedFile));

        const actual = await readAssets();

        expect(fileReader.readFromPath).toHaveBeenCalledWith(assetsDirectory);
        expect(console.log).toHaveBeenCalledWith('Loaded asset path/filename');
        expect(mimeTypes.get).toHaveBeenCalledWith('e');
        // @ts-ignore
        expect(fs.stat).toHaveBeenCalledWith('path/filename', jasmine.any(Function));
        expect(compressor.compress).toHaveBeenCalledWith(content);
        const expectedAsset = { content: 'asdf', contentType: '', lastModified: mtime,
            eTag: '9a0364b9e99bb480dd25e1f0284c8555', filename, hidden: false };
        expect(actual).toEqual([ { urlPath: 'path/filename', asset: expectedAsset } ]);
    });

    it('should read assets when type is hidden', async () => {
        const content = Buffer.from('content');
        const filename = 'filename.ttf';
        const file = { name: filename, path: 'path', content } as NamedFile;
        const mtime = new Date(1986, 10, 9);
        const compressedFile = 'asdf';
        spyOn(fileReader, 'readFromPath').and.returnValue(Promise.resolve([file]));
        spyOn(console, 'log');
        spyOn(mimeTypes, 'get').and.returnValue(undefined);
        const statMock = (path: fs.PathLike, callback: any) => callback(null, { mtime, isFile: () => true } as fs.Stats);
        spyOn(fs, 'stat').and.callFake(statMock as typeof fs.stat);
        spyOn(compressor, 'compress').and.returnValue(Promise.resolve(compressedFile));

        const actual = await readAssets();

        expect(fileReader.readFromPath).toHaveBeenCalledWith(assetsDirectory);
        expect(console.log).toHaveBeenCalledWith('Loaded asset path/filename.ttf');
        expect(mimeTypes.get).toHaveBeenCalledWith('.ttf');
        // @ts-ignore
        expect(fs.stat).toHaveBeenCalledWith('path/filename.ttf', jasmine.any(Function));
        expect(compressor.compress).toHaveBeenCalledWith(content);
        const expectedAsset = { content: 'asdf', contentType: '', lastModified: mtime,
            eTag: '9a0364b9e99bb480dd25e1f0284c8555', filename, hidden: true };
        expect(actual).toEqual([ { urlPath: 'path/filename.ttf', asset: expectedAsset } ]);
    });

    it('should delegate error', (done) => {
        const content = Buffer.from('content');
        const filename = 'filename.ttf';
        const file = { name: filename, path: 'path', content } as NamedFile;
        spyOn(fileReader, 'readFromPath').and.returnValue(Promise.resolve([file]));
        spyOn(console, 'log');
        spyOn(mimeTypes, 'get').and.returnValue(undefined);
        const statError = new Error('stat error');
        const statMock = (path: fs.PathLike, callback: any) => callback(statError, {} as fs.Stats);
        spyOn(fs, 'stat').and.callFake(statMock as typeof fs.stat);

        readAssets()
            .catch((error) => {
                expect(fileReader.readFromPath).toHaveBeenCalledWith(assetsDirectory);
                expect(console.log).toHaveBeenCalledWith('Loaded asset path/filename.ttf');
                expect(mimeTypes.get).toHaveBeenCalledWith('.ttf');
                // @ts-ignore
                expect(fs.stat).toHaveBeenCalledWith('path/filename.ttf', jasmine.any(Function));
                expect(error).toBe(statError);
                done();
            });
    });
});
