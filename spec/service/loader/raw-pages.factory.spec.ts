import * as emailEdge from '../../../src/components/admin/email-edge/email-edge';
import * as fontFaceUpload from '../../../src/components/admin/font-face-upload/font-face-upload';
import * as uploadPage from '../../../src/components/admin/upload-page/upload-page';
import * as landing from '../../../src/components/landing/landing';
import * as login from '../../../src/components/login/login';
import * as newsfeed from '../../../src/components/newsfeed/newsfeed';
import * as philosophy from '../../../src/components/philosophy/philosophy';
import * as registration from '../../../src/components/registration/registration';
import * as checkout from '../../../src/components/checkout/checkout';
import * as adminMenu from '../../../src/components/admin/admin-menu/admin-menu';
import * as pageNotFound from '../../../src/components/404/404';
import * as landingSlideManager from '../../../src/components/admin/landing-slide-manager/landing-slide-manager';
import * as projectEditor from '../../../src/components/admin/project-editor/project-editor';
import * as projectManager from '../../../src/components/admin/project-manager/project-manager';
import * as projectPage from '../../../src/components/project/project-page';
import * as projectDao from '../../../src/db/project.dao';
import * as cartPage from '../../../src/components/cart/cart';
import * as projectSlidesPage from '../../../src/components/admin/project-slides-page/project-slides-page';
import * as fontManagerPage from '../../../src/components/admin/font-manager/font-manager';
import * as textManagerPage from '../../../src/components/admin/text-manager/text-manager';
import * as sandboxPage from '../../../src/components/admin/sandbox/sandbox';
import * as priceDetailsPage from '../../../src/components/admin/price-details/price-details';
import * as pricesComponent from '../../../src/components/admin/prices/prices.component';
import { createRawPages } from '../../../src/service/loader/raw-pages.factory';
import { Page } from '../../../src/models/page.interface';
import { Project } from '../../../src/models/project/project.interface';

describe('Raw Pages Factory', () => {

    const details = { description: 'desc', languages: 'lang', release: 'rel', version: 'ver', formats: 'formats', specimenPdf: 'pdf' };
    const project: Project = { index: 0, shortName: 'name', url: 'url', pageTitle: 'pageTitle', previewImage: 'previewImage',
        previewHover: 'previewHover', coverImage: 'coverImage', details, slides: [], fonts: [] };

    it('should call all components', async () => {
        const page = createPage();
        spyOn(landing, 'renderLanding').and.returnValue(Promise.resolve(page));
        spyOn(philosophy, 'renderPhilosophy').and.returnValue(page);
        spyOn(newsfeed, 'renderNewsFeed').and.returnValue(Promise.resolve(page));
        spyOn(login, 'renderLogin').and.returnValue(page);
        spyOn(registration, 'renderRegistration').and.returnValue(page);
        spyOn(uploadPage, 'renderUploadPage').and.returnValue(page);
        spyOn(emailEdge, 'renderEmailTestPage').and.returnValue(page);
        spyOn(fontFaceUpload, 'renderFontFaceUploadPage').and.returnValue(page);
        spyOn(checkout, 'renderCheckout').and.returnValue(page);
        spyOn(adminMenu, 'renderAdminMenu').and.returnValue(page);
        spyOn(pageNotFound, 'render404').and.returnValue(page);
        spyOn(landingSlideManager, 'renderLandingSlideManager').and.returnValue(page);
        spyOn(projectEditor, 'renderProjectEditor').and.returnValue(page);
        spyOn(projectManager, 'renderProjectManager').and.returnValue(page);
        spyOn(projectDao, 'getProjects').and.returnValue(Promise.resolve([ project ]));
        spyOn(projectPage, 'renderProjectPage').and.returnValue(page);
        spyOn(projectSlidesPage, 'renderProjectSlidesPage').and.returnValue(page);
        spyOn(fontManagerPage, 'renderFontManagerPage').and.returnValue(page);
        spyOn(textManagerPage, 'renderTextManagerPage').and.returnValue(page);
        spyOn(sandboxPage, 'renderSandboxPage').and.returnValue(page);
        spyOn(priceDetailsPage, 'renderPriceDetails').and.returnValue(page);
        spyOn(pricesComponent, 'renderPricesComponent').and.returnValue(page);
        spyOn(cartPage, 'renderCart').and.returnValue(page);

        const actual = await createRawPages();

        expect(landing.renderLanding).toHaveBeenCalledWith();
        expect(philosophy.renderPhilosophy).toHaveBeenCalledWith();
        expect(newsfeed.renderNewsFeed).toHaveBeenCalledWith();
        expect(login.renderLogin).toHaveBeenCalledWith();
        expect(registration.renderRegistration).toHaveBeenCalledWith();
        expect(uploadPage.renderUploadPage).toHaveBeenCalledWith();
        expect(emailEdge.renderEmailTestPage).toHaveBeenCalledWith();
        expect(fontFaceUpload.renderFontFaceUploadPage).toHaveBeenCalledWith();
        expect(checkout.renderCheckout).toHaveBeenCalledWith();
        expect(adminMenu.renderAdminMenu).toHaveBeenCalledWith();
        expect(pageNotFound.render404).toHaveBeenCalledWith();
        expect(landingSlideManager.renderLandingSlideManager).toHaveBeenCalledWith();
        expect(projectEditor.renderProjectEditor).toHaveBeenCalledWith();
        expect(projectManager.renderProjectManager).toHaveBeenCalledWith();
        expect(projectDao.getProjects).toHaveBeenCalledWith();
        expect(projectPage.renderProjectPage).toHaveBeenCalledWith(project);
        expect(projectSlidesPage.renderProjectSlidesPage).toHaveBeenCalledWith();
        expect(fontManagerPage.renderFontManagerPage).toHaveBeenCalledWith();
        expect(textManagerPage.renderTextManagerPage).toHaveBeenCalledWith();
        expect(sandboxPage.renderSandboxPage).toHaveBeenCalledWith();
        expect(priceDetailsPage.renderPriceDetails).toHaveBeenCalledWith();
        expect(pricesComponent.renderPricesComponent).toHaveBeenCalledWith();
        expect(cartPage.renderCart).toHaveBeenCalledWith();
        expect(actual).toEqual({ rawPages: [ page, page, page, page, page, page, page, page, page, page ],
            rawAdminPages: [ page, page, page, page, page, page, page, page, page, page, page, page ]});
    });
});

const createPage = () => ({
    main: 'main',
    path: 'path',
    scripts: [],
    styles: [],
    title: 'title',
}) as Page;
