import * as orderDao from '../../../../../src/db/order.dao';
import * as statusClient from '../../../../../src/service/payment/back/barion/status-client';
import * as server from '../../../../../src/service/server/server';
import * as invoiceFacade from '../../../../../src/service/payment/szamlazz/invoice-facade';
import * as orderServeService from '../../../../../src/service/payment/back/order-serve.service';
import { StatusResponse } from '../../../../../src/models/barion/status-response.interface';
import { queryStatus } from '../../../../../src/service/payment/back/barion/result-facade';
import { Order } from '../../../../../src/models/order.interface';
import { Credentials } from '../../../../../src/models/credentials.interface';

describe('Result Facade', () => {
    const orderId = 'orderId';
    const paymentId = 'paymentId';
    const order = { orderId, paymentId } as Order;
    const status = { Status: 'Succeeded', CompletedAt: '2022-03-22T11:50:06.189Z' } as any as StatusResponse;
    const credentials = {} as Credentials;
    const pdf = 'pdf';

    // it('should return order and status', (done) => {
    //     spyOn(orderDao, 'getOrderByOrderId').and.returnValue(Promise.resolve(order));
    //     spyOn(statusClient, 'getStatus').and.returnValue(Promise.resolve(status));
    //     spyOn(server, 'getCredentials').and.returnValue(credentials);
    //     spyOn(orderDao, 'updateOrder').and.returnValue(Promise.resolve({ value: order }));
    //     spyOn(invoiceFacade, 'initiateInvoice').and.returnValue(Promise.resolve(pdf));
    //     spyOn(orderServeService, 'serveOrder');
    //
    //     queryStatus(orderId).then((actual) => {
    //         expect(actual).toEqual({ order, status });
    //         expect(orderDao.getOrderByOrderId).toHaveBeenCalledWith(orderId);
    //         expect(statusClient.getStatus).toHaveBeenCalledWith(paymentId, credentials);
    //         expect(server.getCredentials).toHaveBeenCalledWith();
    //         expect(orderDao.updateOrder).toHaveBeenCalled();
    //         expect(invoiceFacade.initiateInvoice).toHaveBeenCalled();
    //         expect(orderServeService.serveOrder).toHaveBeenCalled();
    //         done();
    //     });
    // });

    it('should return error message when order cannot be found', (done) => {
        spyOn(orderDao, 'getOrderByOrderId').and.returnValue(Promise.resolve(null));

        queryStatus(orderId).catch((actual) => {
            expect(actual).toEqual(new Error('Order orderId not found'));
            expect(orderDao.getOrderByOrderId).toHaveBeenCalledWith(orderId);
            done();
        });
    });
});
