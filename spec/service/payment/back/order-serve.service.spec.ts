import { Order } from '../../../../src/models/order.interface';
import { serveOrder } from '../../../../src/service/payment/back/order-serve.service';
import * as smtpDao from '../../../../src/db/smtp.dao';
import * as smtpClient from '../../../../src/service/email/smtp.client';
import * as confirmationMailComponent from '../../../../src/components/confirmation-mail/confirmation-mail';
import * as fontDao from '../../../../src/db/font.dao';
import { Item } from '../../../../src/models/cart-item.interface';
import { Customer } from '../../../../src/models/customer.interface';
import { Font } from '../../../../src/models/font.interface';

const errorMessage = 'Could not get Smtp properties';

describe('Order serve service', () => {
    const total = 100;
    const paymentId = 'paymentId';
    const status = 'SUCCESS';
    const orderId = 'orderId';
    const customer = { email: 'customer@email.com' } as Customer;
    const credentials = 'credentials';
    const from = 'from@address.com';
    const host = 'smtp.host';
    const family = 'family';
    const weight = 400;
    const style = 'normal';
    const items: Item[] = [ { family, weight, style } as Item ];
    const content = 'content';
    const italic = false;
    const data = 'data';
    const webfont = { filename: 'familynormal400.woff2', data };
    const font: Font = { family, weight, italic, webfont, fullFonts: [] };
    const pdf = 'pdf';

    it('should serve order', (done) => {
        const order: Order = { paymentId, status, orderId, total, customer, updated: Date.now(), items, products: [] };
        spyOn(confirmationMailComponent, 'renderConfirmationMail').and.returnValue(content);
        const properties = { credentials, from, host };
        spyOn(fontDao, 'findFont').and.returnValue(Promise.resolve(font));
        spyOn(smtpDao, 'getSmtpProperties').and.returnValue(Promise.resolve(properties));
        spyOn(smtpClient, 'sendEmail').and.returnValue(Promise.resolve());

        serveOrder(order, pdf).then(() => {
            expect(confirmationMailComponent.renderConfirmationMail).toHaveBeenCalledWith(order);
            expect(fontDao.findFont).toHaveBeenCalledWith(family, weight, italic);
            expect(smtpDao.getSmtpProperties).toHaveBeenCalledWith();
            const fontAttachment = { content: 'data', contentType: 'font/woff2', filename: 'familynormal400.woff2' };
            const pdfAttachment = { content: 'pdf', contentType: 'application/pdf', filename: 'invoice.pdf' };
            expect(smtpClient.sendEmail).toHaveBeenCalledWith(customer.email, content, properties, fontAttachment, pdfAttachment);
            done();
        });
    });

    it('should log error and order when credentials are not found', (done) => {
        const order: Order = { paymentId, status, orderId, total, customer, updated: Date.now(), items, products: [] };
        spyOn(confirmationMailComponent, 'renderConfirmationMail').and.returnValue(content);
        spyOn(fontDao, 'findFont').and.returnValue(Promise.resolve(font));
        spyOn(smtpDao, 'getSmtpProperties').and.returnValue(Promise.resolve(null));
        spyOn(console, 'error');

        serveOrder(order).then(() => {
            expect(confirmationMailComponent.renderConfirmationMail).toHaveBeenCalledWith(order);
            expect(smtpDao.getSmtpProperties).toHaveBeenCalledWith();
            expect(console.error).toHaveBeenCalledWith(errorMessage, JSON.stringify(order));
            done();
        });
    });
});
