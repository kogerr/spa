import { transformResult } from '../../../../src/service/payment/back/payment-result.transformer';
import { RawPaymentResult } from '../../../../src/models/payment-result.interface';

describe('Payment result transformer', () => {
    it('should transform', () => {
        const input: RawPaymentResult = { r: 0, t: 500778925, e: 'SUCCESS', m: 'PUBLICTESTHUF', o: '0000020201227074709491' };

        const actual = transformResult(input);

        const expected = { result: 0, transactionId: 500778925, event: 'SUCCESS', merchant: 'PUBLICTESTHUF', orderId: '0000020201227074709491' };
        expect(actual).toEqual(expected);
    });
});
