import { PaymentRequest } from '../../../../../src/service/payment/start/payment-request.interface';
import { StartRequest } from '../../../../../src/models/barion/start-request.interface';
import { StartResponse } from '../../../../../src/models/barion/start-response.interface';
import Timeout = NodeJS.Timeout;
import { initiateStart } from '../../../../../src/service/payment/start/barion/start-facade';
import * as startRequestFactory from '../../../../../src/service/payment/start/barion/start-request.factory';
import * as server from '../../../../../src/service/server/server';
import * as startClient from '../../../../../src/service/payment/start/barion/start-client';
import * as orderSave from '../../../../../src/service/payment/start/order-save';
import * as orderRefProvider from '../../../../../src/service/payment/start/order-ref.provider';
import * as uuidGenerator from '../../../../../src/service/payment/start/uuid-generator';

describe('Start facade', () => {
    const orderId = 'orderId';
    const uuid = 'uuid';
    const customerEmail = 'email@address.com';
    const total = '100';
    const currency = 'HUF';
    const language = 'HU';
    const ca = 'ca';
    const cert = 'cert';
    const key = 'key';
    const GatewayUrl = 'GatewayUrl';

    it('should call factory, client and db', (done) => {
        const paymentRequest = { customerEmail, total, currency, language } as unknown as PaymentRequest;
        const startRequest = {} as StartRequest;
        const credentials = { ca, cert, key };
        const startResponse = { GatewayUrl } as StartResponse;
        spyOn(orderRefProvider, 'createOrderRef').and.returnValue(orderId);
        spyOn(uuidGenerator, 'generateUUID').and.returnValue(uuid);
        spyOn(startRequestFactory, 'createBody').and.returnValue(startRequest);
        spyOn(server, 'getCredentials').and.returnValue(credentials);
        spyOn(startClient, 'callStart').and.returnValues(Promise.resolve(startResponse));
        // @ts-ignore
        spyOn(global, 'setTimeout').and.callFake(setTimeoutMock);
        spyOn(orderSave, 'saveResult').and.returnValue(Promise.resolve());

        initiateStart(paymentRequest).then((actual) => {
            expect(actual).toEqual(startResponse);
            expect(orderRefProvider.createOrderRef).toHaveBeenCalledWith();
            expect(uuidGenerator.generateUUID).toHaveBeenCalledWith();
            expect(startRequestFactory.createBody).toHaveBeenCalledWith(paymentRequest, orderId, uuid);
            expect(server.getCredentials).toHaveBeenCalledWith();
            expect(startClient.callStart).toHaveBeenCalledWith(startRequest, credentials);
            expect(global.setTimeout).toHaveBeenCalled();
            expect(orderSave.saveResult).toHaveBeenCalledWith(startRequest, startResponse, paymentRequest);
            done();
        })
    });

    const setTimeoutMock = (cb: (...args: any[]) => void, ms: number): Timeout => {
        cb();
        return ms as unknown as Timeout;
    }
});
