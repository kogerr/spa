import { createBody } from '../../../../../src/service/payment/start/barion/start-request.factory';
import { PaymentRequest } from '../../../../../src/service/payment/start/payment-request.interface';
import { Customer } from '../../../../../src/models/customer.interface';
import { Item } from '../../../../../src/models/cart-item.interface';
import { FundingSource, StartRequest } from '../../../../../src/models/barion/start-request.interface';

describe('StartRequest factory', () => {
    const uuid = 'uuid';
    const orderId = 'orderId';
    const email = 'kogerr@hotmail.com';
    const total = 12000;
    const name = 'name';
    const country = 'HU';
    const city = 'city';
    const zip = 'zip';
    const address = 'address';
    const phone = '3614148750';
    const items: Item[] = [];
    const price = total;
    const title = 'font product title';

    const POSKey = '41a8c935220e42d096b923911b0f685f';
    const PaymentType = 'Immediate';
    const GuestCheckOut = true;
    const FundingSources: FundingSource[] = [ 'BankCard', 'GooglePay' ];
    const RedirectUrl = 'https://koger.io/payment-result?orderId=' + orderId;
    const CallbackUrl = 'https://koger.io/api/payment-callback';
    const Locale = 'en-US';
    const Currency = 'EUR';
    const PayerAccountInformation = {};
    const PurchaseInformation = {};
    const ChallengePreference = 0;
    const Payee = 'kogerr@hotmail.com';
    const Quantity = 1;
    const Unit = 'instance';

    it('should create start request body', () => {
        const customer: Customer = { name, email, country, city, address, zip, phone };
        const product = { title, price };
        const paymentRequest: PaymentRequest = { customer, products: [ product ], items, total };

        const actual = createBody(paymentRequest, orderId, uuid) ;

        const expected: StartRequest = {
            POSKey, PaymentType, GuestCheckOut, FundingSources, PaymentRequestId: uuid, PayerHint: email, CardHolderNameHint: name,
            RedirectUrl, CallbackUrl, Transactions: [ { POSTransactionId: uuid, Payee, Total: total, Items: [ { Name: title,
                    Description: title, Quantity, Unit, UnitPrice: total, ItemTotal: total } ] } ], OrderNumber: orderId,
            ShippingAddress: { Country: country, City: city, Zip: zip, Street: address, FullName: name }, Locale,
            Currency, PayerPhoneNumber: phone, PayerWorkPhoneNumber: phone, PayerHomeNumber: phone, BillingAddress:
                { Country: country, City: city, Zip: zip, Street: address, FullName: name }, PayerAccountInformation,
            PurchaseInformation, ChallengePreference
        };
        expect(actual).toEqual(expected);
    });

});
