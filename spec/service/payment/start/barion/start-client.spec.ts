import { callStart } from '../../../../../src/service/payment/start/barion/start-client';
import * as https from 'https';
import { ClientRequest, IncomingMessage } from 'http';
import { RequestOptions } from 'https';
import { StartRequest } from '../../../../../src/models/barion/start-request.interface';

describe('Payment start client', () => {
    const ca = 'ca';
    const cert = 'cert';
    const key = 'key';

    it('should call payment start downstream endpoint', (done) => {
        const startRequest = { POSKey: 'POSKey' } as StartRequest;
        const credentials = { ca, cert, key };
        // @ts-ignore
        spyOn(https, 'request').and.callFake(requestStub);

        callStart(startRequest, credentials).then((actual) => {
            // @ts-ignore
            expect(actual).toEqual({});
            done();
        });
    });

    const requestStub = (options: RequestOptions, cb: (res: IncomingMessage) => void) => {
        const on = (event: string, callback: (data?: string) => void) => {
            if (event === 'end') {
                callback();
            } else if(event === 'data') {
                callback('{}');
            }
        };
        const res = { statusCode: 200, on } as unknown as IncomingMessage;
        cb(res);
        const write = jasmine.createSpy('write');
        const end = jasmine.createSpy('end');
        return { write, end } as unknown as ClientRequest;
    };

    it('should reject on non-200 response', (done) => {
        const startRequest = { POSKey: 'POSKey' } as StartRequest;
        // @ts-ignore
        spyOn(https, 'request').and.callFake(badRequestStub);

        callStart(startRequest, undefined).catch((actual) => {
            // @ts-ignore
            expect(actual).toEqual({});
            done();
        });
    });

    const badRequestStub = (options: RequestOptions, cb: (res: IncomingMessage) => void) => {
        const on = (event: string, callback: (data?: string) => void) => {
            if (event === 'end') {
                callback();
            } else if(event === 'data') {
                callback('{}');
            }
        };
        const res = { statusCode: 404, headers: {}, on } as unknown as IncomingMessage;
        cb(res);
        const write = jasmine.createSpy('write');
        const end = jasmine.createSpy('end');
        return { write, end } as unknown as ClientRequest;
    };
});
