import { createBody } from '../../../../src/service/payment/start/start-request.factory';
import { PaymentRequest } from '../../../../src/service/payment/start/payment-request.interface';
import * as orderRefProvider from '../../../../src/service/payment/start/order-ref.provider';
import * as timeoutProvider from '../../../../src/service/payment/start/timeout.provider';
import * as saltProvider from '../../../../src/service/payment/random-salt.provider';
import { Customer } from '../../../../src/models/customer.interface';
import { Item } from '../../../../src/models/cart-item.interface';
import { StartRequest } from '../../../../src/service/payment/start/start-request.interface';

describe('StartRequest factory', () => {
    const customerEmail = 'kogerr@hotmail.com';
    const currency = 'HUF';
    const language = 'EN';
    const total = 12000;
    const name = 'name';
    const country = 'country';
    const city = 'city';
    const zip = 'zip';
    const address = 'address';
    const phone = '3614148750';
    const items: Item[] = [];
    const price = total;
    const timeout = 'timeout';
    const url = 'https://koger.io/payment-result';
    const amount = '1';
    const title = 'font product title';

    it('should create start request body', () => {
        const customer: Customer = { name, email: customerEmail, country, city, address, zip, phone };
        const product = { title, price };
        const paymentRequest: PaymentRequest = { customer, products: [ product ], items, total };
        spyOn(saltProvider, 'createSalt').and.returnValue('salt');
        spyOn(orderRefProvider, 'createOrderRef').and.returnValue('orderRef');
        spyOn(timeoutProvider, 'createTimeout').and.returnValue(timeout);

        const actual = createBody(paymentRequest) ;
        const expected: StartRequest = { salt: 'salt', merchant: 'PUBLICTESTHUF', orderRef: 'orderRef',
            sdkVersion: 'SimplePayV2.1_Payment_PHP_SDK_2.0.7_190701:dd236896400d7463677a82a47f53e36e', methods: ['CARD'], timeout,
            url, customerEmail, currency, language, total: total.toString(),
            invoice: { name, country, city, address, zip, phone: '3614148750', threeDSReqAuthMethod: '01' },
            items: [{ title, description: title, amount, price: total.toString() }]
        };
        expect(actual).toEqual(expected);
    });

});
