import { createSalt } from '../../../../src/service/payment/random-salt.provider';

describe('Salt provider', () => {
    it('should provide salt with date part', () => {
        const datePart = (Date.now()).toString().substring(0, 9);

        const actual = createSalt();

        expect(actual.includes(datePart)).toBe(true);
    });
});
