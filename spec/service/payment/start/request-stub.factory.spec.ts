import { createRequest } from '../../../../src/service/payment/start/request-stub.factory';
import * as signatureFactory from '../../../../src/service/payment/signature.factory';
import { StartRequest } from '../../../../src/service/payment/start/start-request.interface';

describe('RequestStub Factory', () => {
    it('should create signature header', () => {
        const customerEmail = 'kogerr@hotmail.com';
        const currency = 'HUF';
        const language = 'HU';
        const total = '12000';
        const requestBody = { customerEmail, currency, language, total } as StartRequest;
        spyOn(signatureFactory, 'create').and.returnValue('signature');

        const actual = createRequest(requestBody);

        const expected = {
            body: '{"customerEmail":"kogerr@hotmail.com","currency":"HUF","language":"HU","total":"12000"}',
            headers: { 'Content-Type':'application/json', Signature: 'signature'},
            method: 'POST'
        };
        expect(actual).toEqual(expected);
    });

});
