import { callStart } from '../../../../src/service/payment/start/start-client';
import { RequestStub } from '../../../../src/service/payment/start/request-stub.factory';
import * as https from 'https';
import { ClientRequest, IncomingMessage } from 'http';
import { RequestOptions } from 'https';

describe('Payment start client', () => {
    const ca = 'ca';
    const cert = 'cert';
    const key = 'key';

    it('should call payment start downstream endpoint', (done) => {
        const headers = {};
        const body = '';
        const startRequest = { headers, body } as RequestStub;
        const credentials = { ca, cert, key };
        // @ts-ignore
        spyOn(https, 'request').and.callFake(requestStub);

        callStart(startRequest, credentials).then((actual) => {
            // @ts-ignore
            expect(actual).toEqual({});
            done();
        });
    });

    const requestStub = (options: RequestOptions, cb: (res: IncomingMessage) => void) => {
        const on = (event: string, callback: (data?: string) => void) => {
            if (event === 'end') {
                callback();
            } else if(event === 'data') {
                callback('{}');
            }
        };
        const res = { statusCode: 200, on } as unknown as IncomingMessage;
        cb(res);
        const write = jasmine.createSpy('write');
        const end = jasmine.createSpy('end');
        return { write, end } as unknown as ClientRequest;
    };

    it('should reject on non-200 response', (done) => {
        const headers = {};
        const body = '';
        const startRequest = { headers, body } as RequestStub;
        // @ts-ignore
        spyOn(https, 'request').and.callFake(badRequestStub);

        callStart(startRequest, undefined).catch((actual) => {
            // @ts-ignore
            expect(actual).toEqual({});
            done();
        });
    });

    const badRequestStub = (options: RequestOptions, cb: (res: IncomingMessage) => void) => {
        const res = { statusCode: 404, headers: {} } as unknown as IncomingMessage;
        cb(res);
        const write = jasmine.createSpy('write');
        const end = jasmine.createSpy('end');
        return { write, end } as unknown as ClientRequest;
    };
});
