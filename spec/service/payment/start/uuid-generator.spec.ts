import { generateUUID } from '../../../../src/service/payment/start/uuid-generator';

describe('UUID Generator', () => {
    it('should generate a proper UUID', () => {
        const actual = generateUUID();
        expect(actual).toMatch(/[\da-f]{8}-[\da-f]{4}-[\da-f]{4}-[\da-f]{4}-[\da-f]{12}/);
    });
});
