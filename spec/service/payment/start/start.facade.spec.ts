import * as requestStubFactory from '../../../../src/service/payment/start/request-stub.factory';
import * as startRequestFactory from '../../../../src/service/payment/start/start-request.factory';
import * as server from '../../../../src/service/server/server';
import { PaymentRequest } from '../../../../src/service/payment/start/payment-request.interface';
import { initiateStart } from '../../../../src/service/payment/start/start.facade';
import { StartResponse } from '../../../../src/service/payment/start/start-response.interface';
import * as startClient from '../../../../src/service/payment/start/start-client';
import { StartRequest } from '../../../../src/service/payment/start/start-request.interface';

describe('Start facade', () => {
    const customerEmail = 'email@address.com';
    const total = '100';
    const currency = 'HUF';
    const language = 'HU';
    const ca = 'ca';
    const cert = 'cert';
    const key = 'key';
    const paymentUrl = 'paymentUrl';
    const body = '';

    it('should call factory, client and db', (done) => {
        const paymentRequest = { customerEmail, total, currency, language } as unknown as PaymentRequest;
        const headers = {};
        const requestStub = { headers, body } as requestStubFactory.RequestStub;
        const startRequest = { customerEmail, language } as StartRequest;
        const credentials = { ca, cert, key };
        const startResponse = { paymentUrl } as StartResponse;
        spyOn(startRequestFactory, 'createBody').and.returnValue(startRequest);
        spyOn(requestStubFactory, 'createRequest').and.returnValue(requestStub);
        spyOn(server, 'getCredentials').and.returnValue(credentials);
        spyOn(startClient, 'callStart').and.returnValues(Promise.resolve(startResponse));

        initiateStart(paymentRequest).then((actual) => {
            expect(actual).toEqual(startResponse);
            expect(startRequestFactory.createBody).toHaveBeenCalledWith(paymentRequest);
            expect(requestStubFactory.createRequest).toHaveBeenCalledWith(startRequest);
            expect(server.getCredentials).toHaveBeenCalledWith();
            expect(startClient.callStart).toHaveBeenCalledWith(requestStub, credentials);
            done();
        })
    });

});
