import { createTimeout } from '../../../../src/service/payment/start/timeout.provider';

describe('Timeout provider', () => {
    it('should provide a timeout date with today\'s date', () => {
        const datePart = (new Date(Date.now() + 3600000)).toISOString().substring(0, 12);

        const actual = createTimeout();

        expect(actual.startsWith(datePart)).toBe(true);
    });
});
