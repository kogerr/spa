import { saveResult } from '../../../../src/service/payment/start/order-save';
import { Order } from '../../../../src/models/order.interface';
import { StartRequest } from '../../../../src/models/barion/start-request.interface';
import { ErrorStartResponse, SuccessStartResponse } from '../../../../src/models/barion/start-response.interface';
import * as orderTransformer from '../../../../src/service/payment/start/order.transformer';
import * as orderDao from '../../../../src/db/order.dao';
import { InsertOneWriteOpResult, WithId } from 'mongodb';
import { createOrder } from '../../../../src/service/payment/start/order.transformer';
import * as startErrorDao from '../../../../src/db/start-error.dao';
import { StartError } from '../../../../src/models/start-error.interface';
import { Customer } from '../../../../src/models/customer.interface';

describe('Order save service', () => {
    const customerEmail = 'email@address.com';
    const customer = { email: customerEmail } as Customer;
    const GatewayUrl = 'GatewayUrl';
    const orderRef = '0123';
    const PaymentId = '1234';
    const startRequest = { OrderNumber: orderRef } as StartRequest;
    const order = { orderId: orderRef, total: 100, status: 'pending', paymentId: PaymentId, customer, updated: Date.now(), items: [], products: [] };
    const paymentRequest = { customer, items: [], products: [], total: 100 };
    const startError = {
        Title: 'Model Validation Error',
        Description: 'The field Country must be a string or array type with a maximum length of \'2\'.',
        ErrorCode: 'ModelValidationError',
        HappenedAt: '2022-03-21T09:38:27.5907034Z',
        AuthData: 'kogerr@hotmail.com',
        EndPoint: 'https://api.test.barion.com/v2/Payment/Start'
    };

    it('should transform and save orders', (done) => {
        const startResponse = { GatewayUrl, PaymentId } as SuccessStartResponse;
        spyOn(orderTransformer, 'createOrder').and.returnValue(order);
        const insertOneResult = { insertedCount: 1, result: { ok: 1 } } as InsertOneWriteOpResult<WithId<Order>>;
        spyOn(orderDao, 'addOrder').and.returnValue(Promise.resolve(insertOneResult));

        saveResult(startRequest, startResponse, paymentRequest)
            .then(() => {
                expect(orderTransformer.createOrder).toHaveBeenCalledWith(orderRef, PaymentId, paymentRequest);
                expect(orderDao.addOrder).toHaveBeenCalledWith(order);
                done();
            })
    });

    it('should log error', (done) => {
        const startResponse = { GatewayUrl, PaymentId } as SuccessStartResponse;
        spyOn(orderTransformer, 'createOrder').and.returnValue(order);
        const error = new Error('error');
        spyOn(orderDao, 'addOrder').and.returnValue(Promise.reject(error));
        spyOn(console, 'error');

        saveResult(startRequest, startResponse, paymentRequest)
            .then(() => {
                expect(console.error).toHaveBeenCalledWith(error);
                expect(orderTransformer.createOrder).toHaveBeenCalledWith(orderRef, PaymentId, paymentRequest);
                expect(orderDao.addOrder).toHaveBeenCalledWith(order);
                done();
            })
    });

    it('should transform order when response is Error', (done) => {
        const startResponse: ErrorStartResponse = { Errors: [ startError ] };
        const insertOneResult = { insertedCount: 1, result: { ok: 1 } } as InsertOneWriteOpResult<WithId<StartError>>;
        spyOn(startErrorDao, 'addError').and.returnValue(Promise.resolve(insertOneResult));

        saveResult(startRequest, startResponse, paymentRequest).then(() => {
            expect(startErrorDao.addError).toHaveBeenCalledWith(startRequest, startResponse);
            done();
        });
    });
});
