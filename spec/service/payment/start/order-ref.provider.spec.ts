import { createOrderRef } from '../../../../src/service/payment/start/order-ref.provider';

describe('OrderRef provider', () => {
    const base = 1640991600000;

    it('should provide orderRef with date part', () => {

        const actual = createOrderRef();

        const expected = Date.now() - base;
        expect(parseInt(actual, 32)).toBeLessThanOrEqual(expected);
    });
});
