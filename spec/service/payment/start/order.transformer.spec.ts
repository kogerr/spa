import { createOrder } from '../../../../src/service/payment/start/order.transformer';
import { Customer } from '../../../../src/models/customer.interface';

describe('Order transformer', () => {
    const customerEmail = 'email@address.com';
    const totalNumber = 100;
    const orderRef = '0123';
    const paymentId = 'paymentId';
    const customer = { email: customerEmail } as Customer;
    const paymentRequest = { customer, items: [], products: [], total: 100 };

    it('should transform order', () => {


        const actual = createOrder(orderRef, paymentId, paymentRequest);

        expect(actual.orderId).toEqual(orderRef);
        expect(actual.paymentId).toEqual(paymentId);
        expect(actual.customer).toEqual(customer);
        expect(actual.items).toEqual([]);
        expect(actual.products).toEqual([]);
        expect(actual.total).toEqual(totalNumber);
        expect(actual.status).toEqual('pending');
    });

});
