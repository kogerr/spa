import { create } from '../../../src/service/payment/signature.factory';

describe('Signature Factory', () => {
    it('should create proper signature', () => {
        const body = {'salt':'126dac8a12693a6475c7c24143024ef8','merchant':'PUBLICTESTHUF','orderRef':'101010515680292482600','currency':'HUF','customerEmail':'sdk_test@otpmobil.com','language':'HU','sdkVersion':'SimplePayV2.1_Payment_PHP_SDK_2.0.7_190701:dd236896400d7463677a82a47f53e36e','methods':['CARD'],'total':'25','timeout':'2019-09-11T19:14:08+00:00','url':'https://sdk.simplepay.hu/back.php','invoice':{'name':'SimplePay V2 Tester','company':'','country':'hu','state':'Budapest','city':'Budapest','zip':'1111','address':'Address 1','address2':'Address 2','phone':'06203164978'}};

        const actual = create(JSON.stringify(body));

        const expected = 'GyBTSjRGZi++mMO2AIPR7p92ac08/VWI8R4R3VMogVhw5FmlmYJ1andf0Ngb9Rg9';
        expect(actual).toEqual(expected);
    });
});
