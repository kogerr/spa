import * as saltProvider from '../../../../src/service/payment/random-salt.provider';
import { createDetailsRequest } from '../../../../src/service/payment/query/transaction-query-request.factory';

describe('Transaction query request factory', () => {
    const merchant = 'PUBLICTESTHUF';
    const detailed = true;
    const sdkVersion = 'SimplePayV2.1_Payment_PHP_SDK_2.0.7_190701:dd236896400d7463677a82a47f53e36e';
    const salt = 'salt';
    const transactionIds = [ 'transactionId' ];

    it('should create request', () => {
        spyOn(saltProvider, 'createSalt').and.returnValue(salt);

        const actual = createDetailsRequest(transactionIds);

        expect(actual).toEqual({ merchant, detailed, transactionIds, salt, sdkVersion });
        expect(saltProvider.createSalt).toHaveBeenCalledWith();
    });
});
