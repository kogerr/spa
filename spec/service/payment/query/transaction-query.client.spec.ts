import * as https from 'https';
import { ClientRequest, IncomingMessage } from 'http';
import { RequestOptions } from 'https';
import { callTransactionQuery } from '../../../../src/service/payment/query/transaction-query.client';

describe('Transaction query client', () => {
    const ca = 'ca';
    const cert = 'cert';
    const key = 'key';
    const httpResponse = '{}';

    it('should call query downstream endpoint', (done) => {
        const headers = {};
        const body = '';
        const credentials = { ca, cert, key };
        // @ts-ignore
        spyOn(https, 'request').and.callFake(requestStub);

        callTransactionQuery(body, headers, credentials).then((actual) => {
            expect(actual).toEqual(httpResponse);
            done();
        });
    });

    const requestStub = (options: RequestOptions, cb: (res: IncomingMessage) => void) => {
        const on = (event: string, callback: (data?: string) => void) => {
            if (event === 'end') {
                callback();
            } else if(event === 'data') {
                callback(httpResponse);
            }
        };
        const res = { statusCode: 200, on } as unknown as IncomingMessage;
        cb(res);
        const write = jasmine.createSpy('write');
        const end = jasmine.createSpy('end');
        return { write, end } as unknown as ClientRequest;
    };

    it('should reject on non-200 response', (done) => {
        const headers = {};
        const body = '';
        // @ts-ignore
        spyOn(https, 'request').and.callFake(badRequestStub);

        callTransactionQuery(body, headers, undefined).catch((actual) => {
            // @ts-ignore
            expect(actual).toEqual({});
            done();
        });
    });

    const badRequestStub = (options: RequestOptions, cb: (res: IncomingMessage) => void) => {
        const res = { statusCode: 404, headers: {} } as unknown as IncomingMessage;
        cb(res);
        const write = jasmine.createSpy('write');
        const end = jasmine.createSpy('end');
        return { write, end } as unknown as ClientRequest;
    };
});
