import * as requestFactory from '../../../../src/service/payment/query/transaction-query-request.factory';
import * as signatureFactory from '../../../../src/service/payment/signature.factory';
import * as server from '../../../../src/service/server/server';
import * as client from '../../../../src/service/payment/query/transaction-query.client';
import { TransactionRequest } from '../../../../src/service/payment/query/transaction-request.interface';
import { queryTransactionDetails } from '../../../../src/service/payment/query/transaction-query.facade';

describe('Transaction Query Facade', () => {

    const transactionId = 'transactionId';
    const transactionIds = [ transactionId ];
    const response = 'response';

    it('should return query response', (done) => {
        const body = { transactionIds } as TransactionRequest;
        spyOn(requestFactory, 'createDetailsRequest').and.returnValue(body)
        spyOn(signatureFactory, 'create').and.returnValue('signature')
        const credentials = { key: 'key', cert: 'cert', ca: 'ca' };
        spyOn(server, 'getCredentials').and.returnValue(credentials)
        spyOn(client, 'callTransactionQuery').and.returnValue(Promise.resolve(response))

        queryTransactionDetails(transactionId).then(actual => {
            expect(actual).toEqual(response);
            expect(requestFactory.createDetailsRequest).toHaveBeenCalledWith(transactionIds);
            expect(signatureFactory.create).toHaveBeenCalledWith(JSON.stringify(body));
            expect(server.getCredentials).toHaveBeenCalledWith();
            expect(client.callTransactionQuery)
                .toHaveBeenCalledWith(JSON.stringify(body), { 'Content-Type': 'application/json', 'Signature': 'signature' }, credentials);
            done();
        })
    });
});
