import { encrypt } from '../../../src/service/util/encryptor';

describe('Encryptor', () => {
    it('should encrypt', () => {
        const data = 'data';

        const actual = encrypt(data);

        const expected = 'GóÝ/\x9CßÓN\x04n­\x8A>\x9CÖ\n¶¯J\x0BªÅÝ«AÜ­TV8ê¹';
        expect(actual).toEqual(expected);
    });
});
