import { parseParameterObject, parseParameters } from '../../../src/service/util/parameter.parser';

describe('Parameter Parser', () => {
    it('should parse parameters', () => {
        const actual = parseParameters('a=b&c=d');
        expect(actual).toEqual(new Map([['a', 'b'], [ 'c', 'd']]));
    });

    it('should parse parameters to objects', () => {
        const actual = parseParameterObject<{ a: string, c: string }>('a=b&c=d');
        expect(actual).toEqual({ a: 'b', c: 'd' });
    });
});
