import * as fs from 'fs';
import { unlinkFile } from '../../../src/service/util/file-unlinker';
import { NoParamCallback, PathLike } from 'fs';

describe('File Unlinker', () => {
    const assetsDirectory = 'src/assets';

    it('should unlink file', (done) => {
        const path = '/media/picture.png';
        let pathCapture = '';

        // @ts-ignore
        const mockFsUnlink: typeof fs.unlink = (pathLike: PathLike, callback: NoParamCallback) => {
            callback(null);
            pathCapture = pathLike.toString();
        };
        spyOn(fs, 'unlink').and.callFake(mockFsUnlink);

        unlinkFile(path).then(() => {
            expect(fs.unlink).toHaveBeenCalled();
            expect(pathCapture).toEqual(assetsDirectory + path);
            done();
        });
    });

    it('should log error and reject', (done) => {
        const path = '/media/picture.png';
        let pathCapture = '';
        const error = new Error('error');

        // @ts-ignore
        const mockFsUnlink: typeof fs.unlink = (pathLike: PathLike, callback: NoParamCallback) => {
            callback(error);
            pathCapture = pathLike.toString();
        };
        spyOn(fs, 'unlink').and.callFake(mockFsUnlink);
        spyOn(console, 'error');

        unlinkFile(path).catch((err) => {
            expect(fs.unlink).toHaveBeenCalled();
            expect(console.error).toHaveBeenCalledWith(error);
            expect(pathCapture).toEqual(assetsDirectory + path);
            expect(err).toEqual(error);
            done();
        });
    });
});
