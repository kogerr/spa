import { Http2ServerRequest } from 'http2';
import {
    getRequestParameters,
    getRequestDataBuffer,
    getRequestDataJson,
    getRequestFormObject
} from '../../../src/service/util/incoming-message.resolver';
import * as parameterParser from '../../../src/service/util/parameter.parser';

describe('IncomingMessage Resolver', () => {
    it('getRequestParameters should get request parameters', async () => {
        const on = (event: string, cb: (content?: string) => void) => {
            if (event === 'end') {
                cb();
            } else if (event === 'data') {
                cb('data');
            }
        };
        const request = { on } as unknown as Http2ServerRequest;
        const parameters = new Map([['email', 'email']]);
        spyOn(parameterParser, 'parseParameters').and.returnValue(parameters);

        const actual = await getRequestParameters(request);

        expect(actual).toEqual(parameters);
    });

    it('getRequestFormObject should get request parameters as an object', async () => {
        const on = (event: string, cb: (content?: string) => void) => {
            if (event === 'end') {
                cb();
            } else if (event === 'data') {
                cb('data');
            }
        };
        const request = { on } as unknown as Http2ServerRequest;
        const parameters =  { email: 'email' };
        spyOn(parameterParser, 'parseParameterObject').and.returnValue(parameters);

        const actual = await getRequestFormObject(request);

        expect(actual).toEqual(parameters);
    });

    it('getRequestData should propagate error', (done) => {
        const error = new Error('error');
        const on = (event: string, cb: (content: any) => void) => cb(error);
        const request = { on } as unknown as Http2ServerRequest;

        getRequestParameters(request).catch((actual) => {
            expect(actual).toEqual(error);
            done();
        });
    });

    it('getRequestDataBuffer should get request data', async () => {
        const on = (event: string, cb: (content?: Uint8Array) => void) => {
            if (event === 'end') {
                cb();
            } else if (event === 'data') {
                cb(new Uint8Array([1234]));
            }
        };
        const request = { on } as unknown as Http2ServerRequest;

        const actual = await getRequestDataBuffer(request);

        expect(actual).toEqual(Buffer.from([1234]));
    });

    it('getRequestDataBuffer should propagate error', (done) => {
        const error = new Error('error');
        const on = (event: string, cb: (content: any) => void) => cb(error);
        const request = { on } as unknown as Http2ServerRequest;

        getRequestDataBuffer(request).catch((actual) => {
            expect(actual).toEqual(error);
            done();
        });
    });

    it('getRequestDataJson should get request data', async () => {
        const on = (event: string, cb: (content?: string) => void) => {
            if (event === 'end') {
                cb();
            } else if (event === 'data') {
                cb('{ "data": "data" }');
            }
        };
        const request = { on } as unknown as Http2ServerRequest;

        const actual = await getRequestDataJson(request);

        expect(actual).toEqual({ data: 'data' });
    });
});
