import { base64decode } from '../../../src/service/util/decoder';

describe('Decoder', () => {
    it('should decode base64', () => {
        const input = 'ZXhwZWN0ZWQ=';

        const actual = base64decode(input);

        expect(actual).toBe('expected');
    });
});
