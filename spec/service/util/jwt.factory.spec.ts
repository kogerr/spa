import * as crypto from 'crypto';
import { createToken, validate } from '../../../src/service/util/jwt.factory';

describe('JWT Factory', () => {
    const sub = 'sub';

    it('createToken should create token', () => {
        spyOn(Date, 'now').and.returnValue(999);
        const digest = jasmine.createSpy().and.returnValue('signature');
        const update = jasmine.createSpy('Hmac::update').and.returnValue({ digest });
        spyOn(crypto, 'createHmac').and.returnValue({ update } as unknown as crypto.Hmac);

        const actual = createToken(sub);

        expect(actual).toEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJzdWIiLCJpYXQiOjB9.signature');
    });

    it('createToken should create token', () => {
        const digest = jasmine.createSpy().and.returnValue('signature');
        const update = jasmine.createSpy('Hmac::update').and.returnValue({ digest });
        spyOn(crypto, 'createHmac').and.returnValue({ update } as unknown as crypto.Hmac);

        const actual = validate('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJzdWIiLCJpYXQiOjB9.signature');

        expect(actual).toEqual(true);
    });
});
