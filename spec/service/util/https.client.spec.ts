import { IncomingMessage } from 'http';
import * as http2 from 'http2';
import * as https from 'https';
import { Credentials } from '../../../src/models/credentials.interface';
import * as server from '../../../src/service/server/server';
import { download, http2download } from '../../../src/service/util/https.client';

describe('Https Client', () => {

    const url = 'https://url.com/path';

    it('download should download with credentials', async () => {
        const credentials = {key: 'key', ca: 'ca', cert: 'cert'} as Credentials;
        spyOn(server, 'getCredentials').and.returnValue(credentials);
        const onMock = (event: string, cb: (content?: string) => void) => {
            if (event === 'end') {
                cb();
            } else if (event === 'data') {
                cb('data');
            }
        };
        const on = jasmine.createSpy('IncomingMessage::on').and.callFake(onMock);
        const response = { statusCode: 200, on } as unknown as IncomingMessage;
        spyOn(https, 'get').and.callFake(((u: string, o: unknown, cb: (res: IncomingMessage) => void) => cb(response)) as typeof https.get);

        const actual = await download(url);

        expect(https.get).toHaveBeenCalledWith(url, { rejectUnauthorized: false, ca: 'ca', cert: 'cert', key: 'key' }, jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('error', jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('data', jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('end', jasmine.any(Function));
        expect(actual).toEqual('data');
    });

    it('download should return headers on error', (done) => {
        spyOn(server, 'getCredentials').and.returnValue(undefined);
        const headers = { warning: 'warning' };
        const response = { statusCode: 404, headers } as unknown as IncomingMessage;
        spyOn(https, 'get').and.callFake(((u: string, o: unknown, cb: (res: IncomingMessage) => void) => cb(response)) as typeof https.get);

        download(url)
            .catch((actual) => {
                expect(https.get).toHaveBeenCalledWith(url, { rejectUnauthorized: false }, jasmine.any(Function));
                expect(actual).toEqual(headers);
                done();
            });
    });

    it('http2download should download with credentials', async () => {
        const credentials = {key: 'key', ca: 'ca', cert: 'cert'} as Credentials;
        spyOn(server, 'getCredentials').and.returnValue(credentials);
        const onMock = (event: string, cb: (content?: string | any) => void) => {
            if (event === 'end') {
                cb();
            } else if (event === 'data') {
                cb('data');
            } else if (event === 'response') {
                cb({':status': 200});
            }
        };
        const on = jasmine.createSpy('IncomingMessage::on').and.callFake(onMock);
        const request = { setEncoding: (a: string) => {}, on };
        const clientRequest = jasmine.createSpy('client.request').and.returnValue(request);
        const client = { on: (a: string, b: unknown) => {}, request: clientRequest, close: () => {}} as unknown as http2.ClientHttp2Session;
        spyOn(http2, 'connect').and.returnValue(client);

        const actual = await http2download(url);

        expect(http2.connect).toHaveBeenCalledWith('https://url.com', { rejectUnauthorized: false, ca: 'ca', cert: 'cert', key: 'key' });
        expect(on).toHaveBeenCalledWith('response', jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('data', jasmine.any(Function));
        expect(on).toHaveBeenCalledWith('end', jasmine.any(Function));
        expect(actual).toEqual('data');
    });

    it('http2download should return headers on error', (done) => {
        spyOn(server, 'getCredentials').and.returnValue(undefined);
        const headers = { ':status': 404, 'warning': 'warning' };
        const onMock = (event: string, cb: (content: any) => void) => cb(headers);
        const on = jasmine.createSpy('IncomingMessage::on').and.callFake(onMock);
        const request = { setEncoding: (a: string) => {}, on };
        const clientRequest = jasmine.createSpy('client.request').and.returnValue(request);
        const clientOn = jasmine.createSpy('client::on');
        const close = jasmine.createSpy('client::close');
        const client = { on: clientOn, request: clientRequest, close } as unknown as http2.ClientHttp2Session;
        spyOn(http2, 'connect').and.returnValue(client);

        http2download(url).catch((actual) => {
            expect(http2.connect).toHaveBeenCalledWith('https://url.com', { rejectUnauthorized: false });
            expect(on).toHaveBeenCalledWith('response', jasmine.any(Function));
            expect(clientOn).toHaveBeenCalledWith('error', jasmine.any(Function));
            expect(close).toHaveBeenCalledWith();
            expect(actual).toEqual(headers);
            done();
        });
    });

    it('http2download should return error on client error', (done) => {
        spyOn(server, 'getCredentials').and.returnValue(undefined);
        const error = new Error('message');
        const onMock = (event: string, cb: (error: any) => void) => cb(error);
        const clientRequest = jasmine.createSpy('client.request');
        const clientOn = jasmine.createSpy('client::on').and.callFake(onMock);
        const close = jasmine.createSpy('client::close');
        const client = { on: clientOn, request: clientRequest, close} as unknown as http2.ClientHttp2Session;
        spyOn(http2, 'connect').and.returnValue(client);

        http2download(url).catch((actual) => {
            expect(http2.connect).toHaveBeenCalledWith('https://url.com', { rejectUnauthorized: false });
            expect(clientOn).toHaveBeenCalledWith('error', jasmine.any(Function));
            expect(close).toHaveBeenCalledWith();
            expect(actual).toEqual(error);
            done();
        });
    });

    it('http2download should reject faulty url', (done) => {
        http2download('zxcvadsf').catch((actual) => {
            expect(actual).toEqual(new Error('Cannot parse host'));
            done();
        });
    });
});
