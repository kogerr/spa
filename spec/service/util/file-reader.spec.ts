import * as fs from 'fs';
import { NamedFile } from '../../../src/models/named-file.interface';
import { fileExists, readFromPath } from '../../../src/service/util/file-reader';

describe('File Reader', () => {
    it('reads from path', async () => {
        const path = 'path';
        const directory = { isDirectory: () => true } as fs.Dirent;
        const file = { isFile: () => true, isDirectory: () => false, name: 'name' } as fs.Dirent;
        const x = { isDirectory: () => false, isFile: () => false } as fs.Dirent;
        const content = Buffer.from('content');
        spyOn(fs, 'readdir').and.callFake(mockReaddir([file, directory, x]));
        spyOn(fs, 'readFile').and.callFake(mockReadFile(content));

        const actual = await readFromPath('path');

        expect(fs.readdir).toHaveBeenCalledWith(path, { encoding: 'utf8', withFileTypes: true }, jasmine.any(Function));
        expect(fs.readFile).toHaveBeenCalled();
        expect(actual).toEqual([{ path: 'path', name: 'name', content } as NamedFile]);
    });

    let first = true;

    const mockReaddir = (answer: fs.Dirent[]) =>
        ((p: any, o: any, cb: any) => {
            if (first) {
                first = false;
                cb(null, answer);
            } else {
                cb(null, []);
            }
        }) as typeof fs.readdir;

    const mockReadFile = (answer: Buffer | string) =>
        ((p: any, o: any, cb: any) => cb(null, answer)) as typeof fs.readFile;

    it('propagates error from readdir', (done) => {
        const path = 'path';
        const error = new Error('asdf');
        spyOn(fs, 'readdir').and.callFake(((p: any, o: any, cb: any) => cb(error, 'qwer')) as typeof fs.readdir);

        readFromPath('path')
            .catch((actual) => {
                expect(fs.readdir).toHaveBeenCalledWith(path, { encoding: 'utf8', withFileTypes: true }, jasmine.any(Function));
                expect(actual).toEqual(error);
                done();
            });
    });

    it('propagates error from readFile', (done) => {
        const path = 'path';
        const file = { isFile: () => true, isDirectory: () => false, name: 'name' } as fs.Dirent;
        const error = new Error('asdf');
        spyOn(fs, 'readdir').and.callFake(((p: any, o: any, cb: any) => cb(null, [file])) as typeof fs.readdir);
        spyOn(fs, 'readFile').and.callFake(((p: any, o: any, cb: any) => cb(error, 'qwer')) as typeof fs.readFile);

        readFromPath('path')
            .catch((actual) => {
                expect(fs.readdir).toHaveBeenCalledWith(path, { encoding: 'utf8', withFileTypes: true }, jasmine.any(Function));
                expect(fs.readFile).toHaveBeenCalled();
                expect(actual).toEqual(error);
                done();
            });
    });

    it('fileExists checks if file exists', () => {
        const path = 'app.ts';
        spyOn(fs, 'statSync').and.returnValue({ isFile: () => true } as unknown as fs.Stats);

        const actual = fileExists(path);

        expect(fs.statSync).toHaveBeenCalledWith(path);
        expect(actual).toBe(true);
    });
});
