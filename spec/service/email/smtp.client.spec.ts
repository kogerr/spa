import * as net from 'net';
import { BufferAttachment } from '../../../src/models/attachment.interface';
import { sendEmail, sendEmailBufferAttachment } from '../../../src/service/email/smtp.client';

const from = 'from';
const message = 'message';
const htmlContent = 'htmlContent';
const credentials = 'credentials';
const host = 'host';
const smtpProperties = { from, credentials, host };

describe('emailService', () => {
    it('should send email', async () => {
        const attachedFile = createAttachment();
        const setEncoding = jasmine.createSpy('socket.setEncoding');
        const on = jasmine.createSpy('socket.on').and.callFake(onFake(false));
        const write = jasmine.createSpy('socket.write');
        const socket = { on, setEncoding, write } as unknown as net.Socket;
        spyOn(net, 'connect').and.returnValue(socket);
        spyOn(console, 'log');
        spyOn(console, 'error');

        await sendEmailBufferAttachment(from, htmlContent, smtpProperties, attachedFile);

        expect(net.connect).toHaveBeenCalled();
        expect(on).toHaveBeenCalled();
        expect(setEncoding).toHaveBeenCalledWith('utf8');
        expect(write).toHaveBeenCalled();
        expect(console.log).toHaveBeenCalled();
    });

    it('should reject on error event', async () => {
        const setEncoding = jasmine.createSpy('socket.setEncoding');
        const on = jasmine.createSpy('socket.on').and.callFake(onFake(true));
        const write = jasmine.createSpy('socket.write')
            .and.callFake((event: string, callback: (message: string) => void) => callback(message));
        const socket = { on, setEncoding, write } as unknown as net.Socket;
        spyOn(net, 'connect').and.returnValue(socket);
        spyOn(console, 'log');
        spyOn(console, 'error');

        sendEmail(from, htmlContent, smtpProperties).catch((actual: Error) => {
            expect(net.connect).toHaveBeenCalled();
            expect(on).toHaveBeenCalled();
            expect(setEncoding).toHaveBeenCalledWith('utf8');
            expect(write).toHaveBeenCalled();
            expect(console.log).toHaveBeenCalled();
            expect(console.error).toHaveBeenCalledWith(actual);
        });
    });

    it('should reject on error message', async () => {
        const setEncoding = jasmine.createSpy('socket.setEncoding');
        const fake = (event: string, callback: (message?: string | Error) => void) => {
            callback('Error');
        };
        const on = jasmine.createSpy('socket.on').and.callFake(fake);
        const write = jasmine.createSpy('socket.write')
            .and.callFake((event: string, callback: (message: string) => void) => callback(message));
        const socket = { on, setEncoding, write } as unknown as net.Socket;
        spyOn(net, 'connect').and.returnValue(socket);
        spyOn(console, 'log');
        spyOn(console, 'error');

        sendEmail(from, htmlContent, smtpProperties).catch((actual: Error) => {
            expect(net.connect).toHaveBeenCalled();
            expect(on).toHaveBeenCalled();
            expect(setEncoding).toHaveBeenCalledWith('utf8');
            expect(write).toHaveBeenCalled();
            expect(console.log).toHaveBeenCalled();
            expect(console.error).toHaveBeenCalledWith(actual);
        });
    });

    it('should send email when attachment is empty', async () => {
        const attachedFile = { content: '', contentType: 'contentType', filename: 'filename', }
        const setEncoding = jasmine.createSpy('socket.setEncoding');
        const on = jasmine.createSpy('socket.on').and.callFake(onFake(false));
        const write = jasmine.createSpy('socket.write');
        const socket = { on, setEncoding, write } as unknown as net.Socket;
        spyOn(net, 'connect').and.returnValue(socket);
        spyOn(console, 'log');
        spyOn(console, 'error');

        await sendEmail(from, htmlContent, smtpProperties, attachedFile);

        expect(net.connect).toHaveBeenCalled();
        expect(on).toHaveBeenCalled();
        expect(setEncoding).toHaveBeenCalledWith('utf8');
        expect(write).toHaveBeenCalled();
        expect(console.log).toHaveBeenCalled();
    });

    const onFake = (erroneous = false) => (event: string, callback: (message?: string | Error) => void) => {
        if (event === 'data') {
            for (let i = 0; i < 14; i++) {
                callback(message);
            }
            callback('queued');
        } else if (event === 'error' && erroneous) {
            callback(new Error(message));
        }
    };
});

const createAttachment = (): BufferAttachment => ({
    content: Buffer.from('contentcontentcontentcontentcontentcontentcontentcontentcontentcontentcontentcontentcontentcontentcontent'),
    contentType: 'contentType',
    filename: 'filename',
});
