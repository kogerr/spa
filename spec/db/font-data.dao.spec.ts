import { Collection, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';
import { db } from '../../src/db/mongo';
import { getFontDataById, insertFontData } from '../../src/db/font-data.dao';

describe('Font Data DAO', () => {

    const data = 'font data';
    const id = '0123456789abcdef01234567';

    it('insertFontData should return id', async () => {
        const result = { insertedId: new ObjectId(id) } as unknown as InsertOneWriteOpResult<WithId<{ data: string }>>;
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve(result));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);

        const actual = await insertFontData(data);

        expect(insertOne).toHaveBeenCalledWith({ data });
        expect(actual).toEqual(id);
    });

    it('getFontDataById should return font data', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve({ data }));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getFontDataById(id);

        expect(findOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
        expect(actual).toEqual(data);
    });

    it('getFontDataById should null when font data is not found', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(null));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getFontDataById(id);

        expect(findOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
        expect(actual).toBeUndefined();
    });
});
