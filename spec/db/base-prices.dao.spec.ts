import { Collection, Cursor, DeleteWriteOpResultObject, FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';
import { addBasePrices, getBasePrices, getBasePricesWithId, removeBasePrices, upsertBasePrices, getBasePricesByFamily } from '../../src/db/base-prices.dao';
import { db } from '../../src/db/mongo';
import { BasePrices } from '../../src/models/base-prices.interface';

describe('BasePrices DAO', () => {
    const family = 'family';
    const basePrices: BasePrices = { family, desktop: 72, web: 108, app: 144 };
    const id = '6064408a04cfcc0cf8de7b1a';

    it('getBasePrices should return basePrices', async () => {
        const basePricess = [ basePrices ] as BasePrices[];
        const toArray = () => Promise.resolve(basePricess);
        const cursor = { toArray } as unknown as Cursor<BasePrices>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getBasePrices();

        expect(find).toHaveBeenCalledWith({}, { projection: { _id: false }});
        expect(actual).toEqual(basePricess);
    });

    it('getBasePricesByFamily should return basePrices for a font family', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(basePrices));
        db.collection = jasmine.createSpy().and.returnValue({findOne} as unknown as Collection);

        const actual = await getBasePricesByFamily(family);

        expect(findOne).toHaveBeenCalledWith({ family });
        expect(actual).toEqual(basePrices);
    });

    it('getBasePricesWithId should return basePrices with ids', async () => {
        const _id = new ObjectId(id);
        const basePricesList = [ { _id, ...basePrices } ] as WithId<BasePrices>[];
        const toArray = () => Promise.resolve(basePricesList);
        const cursor = { toArray } as unknown as Cursor<BasePrices>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getBasePricesWithId();

        expect(find).toHaveBeenCalledWith({});
        expect(actual).toEqual(basePricesList);
    });

    it('addBasePrices should add basePrices to collection', async () => {
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve({ insertedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);

        const actual = await addBasePrices(basePrices);

        expect(insertOne).toHaveBeenCalledWith(basePrices);
        expect(actual).toEqual({ insertedCount: 1, result: { ok: 1 } } as InsertOneWriteOpResult<any>);
    });

    it('removeBasePrices should call deleteOne on collection', async () => {
        const deleteOne = jasmine.createSpy().and.returnValue(Promise.resolve({ deletedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ deleteOne } as unknown as Collection);

        const actual = await removeBasePrices(family);

        expect(deleteOne).toHaveBeenCalledWith({ family });
        expect(actual).toEqual({ deletedCount: 1, result: { ok: 1 } } as DeleteWriteOpResultObject);
    });

    it('upsertBasePrices should update basePrices', async () => {
        const result = { ok: 1 } as FindAndModifyWriteOpResultObject<BasePrices>;

        const findOneAndUpdate = jasmine.createSpy().and.returnValue(result);
        db.collection = jasmine.createSpy().and.returnValue({ findOneAndUpdate } as unknown as Collection);

        const actual = await upsertBasePrices(basePrices);

        const update = { $set: { desktop: basePrices.desktop, web: basePrices.web, app: basePrices.app } };
        expect(findOneAndUpdate).toHaveBeenCalledWith({ family }, update, { upsert: true });
        expect(actual).toEqual(result);
    });

});
