import { Collection, Cursor, FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, WithId } from 'mongodb';
import { db } from '../../src/db/mongo';
import { addOrder, getOrderByOrderId, getOrders, updateOrder } from '../../src/db/order.dao';
import { Order } from '../../src/models/order.interface';

describe('Order DAO', () => {
    const orderId = 'orderId';
    const total = 1;
    const order = { orderId, total } as Order;

    it('getOrders should return orders', async () => {
        const orders = [ order ];
        const toArray = () => Promise.resolve(orders);
        const cursor = { toArray } as unknown as Cursor<Order>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({ find } as unknown as Collection);

        const actual = await getOrders();

        expect(find).toHaveBeenCalledWith();
        expect(actual).toEqual(orders);
    });

    it('getOrderByOrderId should return an order', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(order));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getOrderByOrderId(orderId);

        expect(actual).toEqual(order);
    });

    it('addOrder should add order', async () => {
        const insertOneResult = { insertedCount: 1, result: { ok: 1 } } as InsertOneWriteOpResult<WithId<Order>>;
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve(insertOneResult));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);

        const actual = await addOrder(order);

        expect(insertOne).toHaveBeenCalledWith(order);
        expect(actual).toEqual(insertOneResult);
    });

    it('updateOrder should update order', async () => {
        const status = 'succeeded';
        const updated = 1234;
        const result = { insertedCount: 1, result: { ok: 1 } } as FindAndModifyWriteOpResultObject<Order>;
        const findOneAndUpdate = jasmine.createSpy().and.returnValue(Promise.resolve(result));
        db.collection = jasmine.createSpy().and.returnValue({ findOneAndUpdate } as unknown as Collection);

        const actual = await updateOrder(orderId, status, updated);

        expect(findOneAndUpdate).toHaveBeenCalledWith({ orderId }, { $set: { status, updated } });
        expect(actual).toEqual(result);
    });
});
