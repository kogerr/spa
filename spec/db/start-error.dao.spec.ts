import { Collection, InsertOneWriteOpResult } from 'mongodb';
import { db } from '../../src/db/mongo';
import { StartRequest } from '../../src/models/barion/start-request.interface';
import { ErrorStartResponse } from '../../src/models/barion/start-response.interface';
import { addError } from '../../src/db/start-error.dao';

describe('Start Error DAO', () => {
    const startRequest = { } as StartRequest;
    const error = {
        Title: 'Model Validation Error',
        Description: 'The field Country must be a string or array type with a maximum length of \'2\'.',
        ErrorCode: 'ModelValidationError',
        HappenedAt: '2022-03-21T09:38:27.5907034Z',
        AuthData: 'kogerr@hotmail.com',
        EndPoint: 'https://api.test.barion.com/v2/Payment/Start'
    };
    const startResponse: ErrorStartResponse = { Errors: [ error ] };

    it('addError should add start error', async () => {
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve({ insertedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);

        const actual = await addError(startRequest, startResponse);

        expect(insertOne).toHaveBeenCalledWith({ startRequest, startResponse });
        expect(actual).toEqual({ insertedCount: 1, result: { ok: 1 } } as InsertOneWriteOpResult<any>);
    });
});
