import { Collection, Cursor } from 'mongodb';
import { db } from '../../src/db/mongo';
import { getNews } from '../../src/db/news.dao';
import { NewsPost } from '../../src/models/news-post.interface';

describe('News DAO', () => {

    it('getNews should return news', async () => {
        const $news = [] as NewsPost[];
        const toArray = () => Promise.resolve($news);
        const cursor = { toArray } as unknown as Cursor<NewsPost>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({ find } as unknown as Collection);

        const actual = await getNews();

        expect(actual).toEqual($news);
    });
});
