import { Collection, Cursor, DeleteWriteOpResultObject, FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';
import { addLandingSlide, getLandingSlides, getLandingSlidesWithId, removeLandingSlide, updateLandingSlides } from '../../src/db/landing-slide.dao';
import { db } from '../../src/db/mongo';
import { LandingSlide } from '../../src/models/landing-slide.interface';

describe('LandingSlide DAO', () => {
    const slide: LandingSlide = { index: 0, src: '/image.png', link: '/link', alt: 'alt' };
    const id = '6064408a04cfcc0cf8de7b1a';

    it('getLandingSlides should return landing slides', async () => {
        const slides = [ slide ] as LandingSlide[];
        const toArray = () => Promise.resolve(slides);
        const cursor = { toArray } as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getLandingSlides();

        expect(find).toHaveBeenCalledWith({}, { projection: { _id: false }});
        expect(actual).toEqual(slides);
    });

    it('getLandingSlidesWithId should return landing slides with ids', async () => {
        const _id = new ObjectId(id);
        const slides = [ { _id, ...slide } ] as WithId<LandingSlide>[];
        const toArray = () => Promise.resolve(slides);
        const cursor = { toArray } as unknown as Cursor<LandingSlide>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getLandingSlidesWithId();

        expect(find).toHaveBeenCalledWith({});
        expect(actual).toEqual(slides);
    });

    it('addLandingSlide should add slide to collection', async () => {
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve({ insertedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);

        const actual = await addLandingSlide(slide);

        expect(insertOne).toHaveBeenCalledWith(slide);
        expect(actual).toEqual({ insertedCount: 1, result: { ok: 1 } } as InsertOneWriteOpResult<any>);
    });

    it('removeLandingSlide should call deleteOne on collection', async () => {
        const deleteOne = jasmine.createSpy().and.returnValue(Promise.resolve({ deletedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ deleteOne } as unknown as Collection);

        const actual = await removeLandingSlide(id);

        expect(deleteOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
        expect(actual).toEqual({ deletedCount: 1, result: { ok: 1 } } as DeleteWriteOpResultObject);
    });

    it('updateLandingSlides should update landing slides', async () => {
        const _id = new ObjectId(id);
        const slides = [ { _id, ...slide } ] as WithId<LandingSlide>[];
        const result = { ok: 1 } as FindAndModifyWriteOpResultObject<LandingSlide>;
        const findOneAndUpdate = jasmine.createSpy().and.returnValue(result);
        db.collection = jasmine.createSpy().and.returnValue({ findOneAndUpdate } as unknown as Collection);

        const actual = await updateLandingSlides(slides);

        expect(findOneAndUpdate).toHaveBeenCalledWith({ _id }, { $set: { src: '/image.png', index: 0, alt: 'alt', link: '/link' } });
        expect(actual).toEqual([ result ]);
    });
});
