import { db } from '../../src/db/mongo';
import { Font } from '../../src/models/font.interface';
import { Collection, Cursor, DeleteWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';
import { insertFont, getFontFamilies, getFontById, removeFontById, findFont } from '../../src/db/font.dao';
import { Project } from '../../src/models/project/project.interface';

describe('Font Dao', () => {

    const id = '0123456789abcdef01234567';
    const data = 'font data';
    const webfont = { filename: 'filename', data };
    const font: Font = { family: 'family', weight: 400, italic: false, webfont, fullFonts: [] };

    it('getFontFamilies should get all the fonts', async () => {
        const family = [ 'family' ];
        const toArray = () => Promise.resolve(family);
        const cursor = { toArray } as unknown as Cursor<Project>;
        cursor.map = jasmine.createSpy().and.returnValue(cursor);
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({ find } as unknown as Collection);

        const actual = await getFontFamilies();

        expect(find).toHaveBeenCalledWith();
        expect(cursor.map).toHaveBeenCalled();
        expect(db.collection).toHaveBeenCalled();
        expect(actual).toEqual(family);
    });

    it('insertFont should return id', async () => {
        const result = { insertedId: new ObjectId(id) } as unknown as InsertOneWriteOpResult<WithId<{ data: string }>>;
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve(result));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);

        const actual = await insertFont(font);

        expect(insertOne).toHaveBeenCalledWith(font);
        expect(actual).toEqual(id);
    });

    it('getFontById should return font data', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(font));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getFontById(id);

        expect(findOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
        expect(actual).toEqual(font);
    });

    it('getFontById should return null when font data is not found', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(null));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getFontById(id);

        expect(findOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
        expect(actual).toBeNull();
    });

    it('removeFontById should remove font', async () => {
        const resultObject = { result: { ok: 1, n: 1 } } as DeleteWriteOpResultObject;
        const deleteOne = jasmine.createSpy().and.returnValue(Promise.resolve(resultObject));
        db.collection = jasmine.createSpy().and.returnValue({ deleteOne } as unknown as Collection);

        const actual = await removeFontById(id);

        expect(deleteOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
        expect(actual).toEqual(resultObject);
    });

    it('findFont should return font', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(font));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await findFont('family', 400, false);

        expect(findOne).toHaveBeenCalledWith({ family: 'family', weight: 400, italic: false });
        expect(actual).toEqual(font);
    });
});
