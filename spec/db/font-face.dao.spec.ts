import { Collection, Cursor } from 'mongodb';
import { getFontFaces } from '../../src/db/font-face.dao';
import { db } from '../../src/db/mongo';
import { FontFace } from '../../src/models/font-face.interface';

describe('FontFace DAO', () => {
    const data = 'base64data';

    it('getFontFaces should return font faces', async () => {
        const fontFace: FontFace = { family: 'family', style: 'normal', weight: 400, data, id: 'id', public: false, format: 'woff2' };
        const fontFaces = [ fontFace ] as FontFace[];
        const toArray = () => Promise.resolve(fontFaces);
        const cursor = { toArray } as unknown as Cursor<FontFace>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({ find } as unknown as Collection);

        const actual = await getFontFaces();

        expect(find).toHaveBeenCalledWith({}, { projection: { _id: false }});
        expect(actual).toEqual(fontFaces);
    });

});
