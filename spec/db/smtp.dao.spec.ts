import { Collection, FindAndModifyWriteOpResultObject } from 'mongodb';
import { db } from '../../src/db/mongo';
import { getSmtpProperties, updateSmtpProperties } from '../../src/db/smtp.dao';
import { SmtpProperties } from '../../src/models/smtp-properties.interface';

const host = 'host';
const from = 'username@domain.com';
const credentials = 'AHVzZXJuYW1lQGRvbWFpbi5jb20AcGFzc3dvcmQ=';

describe('Smtp properties DAO', () => {
    it('should read smtp properties', async () => {
        const smtpProperties = { host, from, credentials };
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(smtpProperties));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getSmtpProperties();

        expect(db.collection).toHaveBeenCalled();
        expect(findOne).toHaveBeenCalledWith({});
        expect(actual).toBe(smtpProperties);
    });

    it('should encode and save smtp properties', async () => {
        const password = 'password';
        const smtpProperties = { host, from, credentials } as SmtpProperties;
        const result: FindAndModifyWriteOpResultObject<SmtpProperties> = {};
        const findOneAndReplace = jasmine.createSpy().and.returnValue(Promise.resolve(result));
        db.collection = jasmine.createSpy().and.returnValue({ findOneAndReplace } as unknown as Collection);

        const actual = await updateSmtpProperties(host, from, password);

        expect(db.collection).toHaveBeenCalled();
        expect(findOneAndReplace).toHaveBeenCalledWith({}, smtpProperties);
        expect(actual).toBe(result);
    });
});
