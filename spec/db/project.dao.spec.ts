import { Collection, Cursor, DeleteWriteOpResultObject, FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId, WithId } from 'mongodb';
import { addProject, getHoverImages, getPreviewImages, getProjects, getProjectSlides, getProjectSlidesByUrl, getProjectsWithId,
    removeProject, updateProjects, updateProjectSlides } from '../../src/db/project.dao';
import { db } from '../../src/db/mongo';
import { Project } from '../../src/models/project/project.interface';

describe('Project DAO', () => {
    const details = { description: 'desc', languages: 'lang', release: 'rel', version: 'ver', formats: 'formats', specimenPdf: 'pdf' };
    const slide = { src: 'src', index: 0 };
    const project: Project = { index: 0, shortName: 'name', url: 'url', pageTitle: 'pageTitle', previewImage: 'previewImage',
        previewHover: 'previewHover', coverImage: 'coverImage', details, slides: [ slide ], fonts: [] };
    const id = '6064408a04cfcc0cf8de7b1a';

    it('getProjects should return projects', async () => {
        const projects = [ project ] as Project[];
        const toArray = () => Promise.resolve(projects);
        const cursor = { toArray } as unknown as Cursor<Project>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getProjects();

        expect(find).toHaveBeenCalledWith({}, { projection: { _id: false }});
        expect(actual).toEqual(projects);
    });

    it('getProjectsWithId should return projects with ids', async () => {
        const _id = new ObjectId(id);
        const projects = [ { _id, ...project } ] as WithId<Project>[];
        const toArray = () => Promise.resolve(projects);
        const cursor = { toArray } as unknown as Cursor<Project>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getProjectsWithId();

        expect(find).toHaveBeenCalledWith({});
        expect(actual).toEqual(projects);
    });

    it('getPreviewImages should return preview images', async () => {
        const projects = [ { previewImage: project.previewImage, index: project.index } ] as any[];
        const toArray = () => Promise.resolve(projects);
        const cursor = { toArray } as unknown as Cursor<Project>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getPreviewImages();

        expect(find).toHaveBeenCalledWith({}, { fields: { previewImage: true, _id: false, index: true, url: true } });
        expect(actual).toEqual(projects);
    });

    it('getHoverImages should return preview hover images', async () => {
        const projects = [ { previewImage: project.previewImage, index: project.index } ] as any[];
        const toArray = () => Promise.resolve(projects);
        const cursor = { toArray } as unknown as Cursor<Project>;
        const find = jasmine.createSpy().and.returnValue(cursor);
        db.collection = jasmine.createSpy().and.returnValue({find} as unknown as Collection);

        const actual = await getHoverImages();

        expect(find).toHaveBeenCalledWith({}, { fields: { previewHover : true, _id: false, index: true } });
        expect(actual).toEqual(projects);
    });

    it('addProject should add project to collection', async () => {
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve({ insertedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ insertOne } as unknown as Collection);

        const actual = await addProject(project);

        expect(insertOne).toHaveBeenCalledWith(project);
        expect(actual).toEqual({ insertedCount: 1, result: { ok: 1 } } as InsertOneWriteOpResult<any>);
    });

    it('removeProject should call deleteOne on collection', async () => {
        const deleteOne = jasmine.createSpy().and.returnValue(Promise.resolve({ deletedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ deleteOne } as unknown as Collection);

        const actual = await removeProject(id);

        expect(deleteOne).toHaveBeenCalledWith({ _id: new ObjectId(id) });
        expect(actual).toEqual({ deletedCount: 1, result: { ok: 1 } } as DeleteWriteOpResultObject);
    });

    it('updateProjects should update projects', async () => {
        const _id = new ObjectId(id);
        const projects = [ { _id, ...project } ] as WithId<Project>[];
        const result = { ok: 1 } as FindAndModifyWriteOpResultObject<Project>;
        const findOneAndUpdate = jasmine.createSpy().and.returnValue(result);
        db.collection = jasmine.createSpy().and.returnValue({ findOneAndUpdate } as unknown as Collection);

        const actual = await updateProjects(projects);

        expect(findOneAndUpdate).toHaveBeenCalledWith({ _id }, { $set: project });
        expect(actual).toEqual([ result ]);
    });

    it('getProjectSlides should return the slides of a project by id', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(project));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getProjectSlides(id);

        expect(findOne).toHaveBeenCalledWith({ _id: new ObjectId(id) }, { fields: { slides: true } });
        expect(actual).toEqual([ slide ]);
    });

    it('getProjectSlides should return undefined when db returns null', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(null));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getProjectSlides(id);

        expect(findOne).toHaveBeenCalledWith({ _id: new ObjectId(id) }, { fields: { slides: true } });
        expect(actual).toEqual(undefined);
    });

    it('getProjectSlidesByUrl should return the slides of a project by url', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(project));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getProjectSlidesByUrl(project.url);

        expect(findOne).toHaveBeenCalledWith({ url: project.url }, { fields: { slides: true } });
        expect(actual).toEqual([ slide ]);
    });

    it('getProjectSlidesByUrl should return undefined when db returns null', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(null));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await getProjectSlidesByUrl(project.url);

        expect(findOne).toHaveBeenCalledWith({ url: project.url }, { fields: { slides: true } });
        expect(actual).toEqual(undefined);
    });

    it('updateProjectSlides should update the slides of a project', async () => {
        const result = { ok: 1 } as FindAndModifyWriteOpResultObject<Project>;
        const findOneAndUpdate = jasmine.createSpy().and.returnValue(Promise.resolve(result));
        db.collection = jasmine.createSpy().and.returnValue({ findOneAndUpdate } as unknown as Collection);

        const actual = await updateProjectSlides(id, [ slide ]);

        expect(findOneAndUpdate).toHaveBeenCalledWith({ _id: new ObjectId(id) }, { $set: { slides: [ slide ] } });
        expect(actual).toEqual(result);
    });
});
