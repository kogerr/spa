import { provideMongoClient } from '../../src/db/mongo-client.provider';

describe('MongoClient provider', () => {

    it('provides mongoClient', () => {
        const actual = provideMongoClient();

        expect(actual.isConnected()).toBe(false);
    });
});
