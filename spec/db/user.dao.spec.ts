import { Collection } from 'mongodb';
import { db } from '../../src/db/mongo';
import { checkEmail, checkUser, registerUser } from '../../src/db/user.dao';

const email = 'email';
const password = 'password';

describe('User DAO', () => {

    it('checks user', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve({ email, password }));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await checkUser(email, password);

        expect(db.collection).toHaveBeenCalled();
        expect(findOne).toHaveBeenCalledWith({ email, password });
        expect(actual).toBe(true);
    });

    it('registerUser registers user', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(null));
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve({ insertedCount: 1, result: { ok: 1 } }));
        db.collection = jasmine.createSpy().and.returnValue({ findOne, insertOne } as unknown as Collection);

        const actual = await registerUser(email, password);

        expect(db.collection).toHaveBeenCalled();
        expect(findOne).toHaveBeenCalledWith({ email, password });
        expect(insertOne).toHaveBeenCalledWith({ email, password });
        expect(actual).toEqual({ email, password });
    });

    it('registerUser throws error if they exist', (done) => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve({ email, password }));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        registerUser(email, password)
            .catch((error) => {
                expect(error).toEqual(new Error('User already exists'));
                done();
            });

        expect(db.collection).toHaveBeenCalled();
        expect(findOne).toHaveBeenCalledWith({ email, password });
    });

    it('registerUser throws error if insert fails', (done) => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve(null));
        const insertFailed = { insertedCount: 0, result: { ok: 0 } };
        const insertOne = jasmine.createSpy().and.returnValue(Promise.resolve(insertFailed));
        db.collection = jasmine.createSpy().and.returnValue({ findOne, insertOne } as unknown as Collection);

        registerUser(email, password)
            .catch((error) => {
                expect(error).toEqual(new Error(JSON.stringify(insertFailed)));
                done();
            });

        expect(db.collection).toHaveBeenCalled();
        expect(findOne).toHaveBeenCalledWith({ email, password });
    });

    it('checkEmail checks email', async () => {
        const findOne = jasmine.createSpy().and.returnValue(Promise.resolve({ email, password }));
        db.collection = jasmine.createSpy().and.returnValue({ findOne } as unknown as Collection);

        const actual = await checkEmail(email);

        expect(db.collection).toHaveBeenCalled();
        expect(findOne).toHaveBeenCalledWith({ email });
        expect(actual).toBe(true);
    });
});
