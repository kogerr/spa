import { Collection, Db, MongoCallback, MongoClient, MongoError, MongoNetworkError } from 'mongodb';
import { initDb } from '../../src/db/mongo';
import * as mongoClientProvider from '../../src/db/mongo-client.provider';

const dbName = 'spa';
const retries = 3;

describe('mongo', () => {

    let collection: Collection;
    let collectionFunction: (arg: string) => Collection;
    let db: Db;
    let dbFunction: (arg: string) => Db;

    beforeEach(() => {
        collection = {} as Collection;
        collectionFunction = jasmine.createSpy('Db::collection').and.returnValue(collection);
        db = { collection: collectionFunction } as unknown as Db;
        dbFunction = jasmine.createSpy('MongoClient::db').and.returnValue(db) as (arg: string) => Db;
        spyOn(console, 'log');
        spyOn(console, 'error');
    });

    it('connects to db', async () => {
        const nullError = null as unknown as MongoError;
        const connect = jasmine.createSpy('MongoClient::connect')
            .and.callFake((callback: MongoCallback<MongoClient>) => callback(nullError, { db: dbFunction } as MongoClient));
        const client = { connect } as unknown as MongoClient;

        spyOn(mongoClientProvider, 'provideMongoClient').and.returnValue(client);

        await initDb(0);

        expect(mongoClientProvider.provideMongoClient).toHaveBeenCalledWith();
        expect(connect).toHaveBeenCalled();
        expect(dbFunction).toHaveBeenCalledWith(dbName);
        expect(collectionFunction).not.toHaveBeenCalled();
        expect(console.log).toHaveBeenCalledWith('Connected to MongoDB');
    });

    it('retries connection', async () => {
        const mongoError = new MongoError('mongo error');
        const nullError = null as unknown as MongoError;
        let firstTry = true;
        const connect = jasmine.createSpy('MongoClient::connect')
            .and.callFake((callback: MongoCallback<MongoClient>) => {
                if (firstTry) {
                    firstTry = false;
                    callback(mongoError, {} as MongoClient);
                } else {
                    callback(nullError, { db: dbFunction } as MongoClient);
                }
            });
        const client = { connect } as unknown as MongoClient;

        spyOn(mongoClientProvider, 'provideMongoClient').and.returnValue(client);

        await initDb(0);

        expect(mongoClientProvider.provideMongoClient).toHaveBeenCalledWith();
        expect(connect).toHaveBeenCalled();
        expect(dbFunction).toHaveBeenCalledWith(dbName);
        expect(collectionFunction).not.toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledWith(mongoError);
        expect(console.log).toHaveBeenCalledWith('Connected to MongoDB');
    });

    it('retries overstep retry limit', async () => {
        const mongoError = new MongoError('mongo error');
        const nullError = null as unknown as MongoError;
        let retryCount = 0;
        const connect = jasmine.createSpy('MongoClient::connect')
            .and.callFake((callback: MongoCallback<MongoClient>) => {
                if (retryCount <= retries) {
                    retryCount++;
                    callback(mongoError, {} as MongoClient);
                } else {
                    callback(nullError, { db: dbFunction } as MongoClient);
                }
            });
        const client = { connect } as unknown as MongoClient;

        spyOn(mongoClientProvider, 'provideMongoClient').and.returnValue(client);

        await initDb(0)
            .catch((error) => {
                expect(error).toBe(mongoError);
            });

        expect(mongoClientProvider.provideMongoClient).toHaveBeenCalledWith();
        expect(connect).toHaveBeenCalled();
        expect(dbFunction).not.toHaveBeenCalled();
        expect(collectionFunction).not.toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledWith(mongoError);
    });

    it('can be called with default interval', (done) => {
        setTimeout(() => {
            done();
        }, 500);
        initDb().catch(() => {});
    });
});
