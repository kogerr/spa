import { renderLogin } from '../../../src/components/login/login';
import { templates } from '../../../src/service/container';

describe('Login page', () => {
    it('should render login page', () => {
        const fakeTemplates = new Map([['login.main.html', '`main`'], ['login.metadata.html', '`metadata`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderLogin();

        expect(actual).toEqual({ main: 'main', metadata: 'metadata', path: '/login', title: 'Login', scripts: [], styles: [] });
    });
});
