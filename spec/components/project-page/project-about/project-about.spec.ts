import { renderProjectAbout } from '../../../../src/components/project/project-about/project-about';
import { templates } from '../../../../src/service/container';

const templateKey = 'project-about.html';

describe('Project about subcomponent', () => {

    const details = { description: 'desc', languages: 'lang', release: 'rel', version: 'ver', formats: 'formats', specimenPdf: 'pdf' };
    const shortName = 'shortName';

    it('should render about part for project page', () => {
        const fakeTemplates = new Map([[ templateKey, 'JSON.stringify(projectDetails) + shortName' ]]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderProjectAbout(shortName, details);

        const expected = '{"description":"desc","languages":"lang","release":"rel","version":"ver","formats":"formats",' +
            '"specimenPdf":"pdf"}' + shortName;
        expect(actual).toEqual(expected);
    });
});
