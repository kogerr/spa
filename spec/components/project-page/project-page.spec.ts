import { renderProjectPage } from '../../../src/components/project/project-page';
import * as projectAbout from '../../../src/components/project/project-about/project-about';
import { templates } from '../../../src/service/container';
import { Project } from '../../../src/models/project/project.interface';

const mainTemplateKey = 'project.main.html';
const metadataTemplateKey = 'project.metadata.html';
const scripts: string[] = [];
const styles: string[] = [];

describe('Project page component', () => {

    const slide = { index: 0, src: 'src' };
    const details = { description: 'desc', languages: 'lang', release: 'rel', version: 'ver', formats: 'formats', specimenPdf: 'pdf' };
    const project: Project = { index: 0, shortName: 'name', url: 'url', pageTitle: 'pageTitle', previewImage: 'previewImage',
        previewHover: 'previewHover', coverImage: 'coverImage', details, slides: [ slide ], fonts: [] };
    const about = 'about';

    it('should render project page', () => {
        spyOn(projectAbout, 'renderProjectAbout').and.returnValue(about);
        const mainTemplate = 'JSON.stringify(project) + about + getImage(project.slides[0])';
        const fakeTemplates = new Map([[ mainTemplateKey, mainTemplate ], [ metadataTemplateKey, 'url' ]]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderProjectPage(project);

        expect(projectAbout.renderProjectAbout).toHaveBeenCalledWith(project.shortName, details);
        const main = JSON.stringify(project) + about + '<img style="left: 0" src="src" alt="alt" />';
        const metadata = 'https://koger.io/url';
        expect(actual).toEqual({ main, metadata, path: '/fonts/url', title: project.pageTitle, scripts, styles });
    });
});
