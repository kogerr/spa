import { renderEmailTestPage } from '../../../../src/components/admin/email-edge/email-edge';
import { templates } from '../../../../src/service/container';

const main = 'main';
const metadata = '';
const path = '/admin/email';
const title = 'email testing page';
const scripts = [ '/scripts/email-edge.js' ];
const styles = [ '/styles/email-edge.css' ];

describe('Email testing page', () => {
    it('should render email testing page', () => {
        const fakeTemplates = new Map([['email-edge.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderEmailTestPage();

        expect(actual).toEqual({ main, metadata, path, title, scripts, styles });
    });
});
