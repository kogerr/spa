import { renderUploadPage } from '../../../../src/components/admin/upload-page/upload-page';
import { templates } from '../../../../src/service/container';

describe('Upload page', () => {
    it('should render upload page', () => {
        const fakeTemplates = new Map([['upload-page.main.html', '`main`'], ['upload-page.metadata.html', '`metadata`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderUploadPage();

        expect(actual).toEqual({ main: 'main', metadata: 'metadata', path: '/admin/upload', title: 'Upload Page', scripts: [ '/scripts/upload.js' ], styles: [] });
    });
});
