import { templates } from '../../../../src/service/container';
import { renderSandboxPage } from '../../../../src/components/admin/sandbox/sandbox';

describe('Text Manager page', () => {

    const path = '/admin/sandbox';
    const title = 'Sandbox';
    const scripts: string[] = [ '/scripts/admin/sandbox.js' ];
    const styles: string[] = [ '/styles/sandbox.css' ];
    const metadata = '';
    const mainTemplateKey = 'sandbox.main.html';

    it('should render Text Manager page', () => {
        spyOn(templates, 'get').and.returnValue('`main`');

        const actual = renderSandboxPage();

        expect(actual).toEqual({ main: 'main', metadata, path, title, scripts, styles });
        expect(templates.get).toHaveBeenCalledWith(mainTemplateKey);
    });
});
