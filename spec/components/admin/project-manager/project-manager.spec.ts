import { renderProjectManager } from '../../../../src/components/admin/project-manager/project-manager';
import { templates } from '../../../../src/service/container';

const main = 'main';
const path = '/admin/projects';
const title = 'Projects';
const scripts: string[] = [ '/scripts/admin/project-manager.js' ];
const styles: string[] = [ '/styles/project-manager.css' ];
const metadata = '';
const mainTemplateKey = 'project-manager.main.html';

describe('Project manager component', () => {
    it('should render project manager page', () => {
        const fakeTemplates = new Map([[ mainTemplateKey, '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderProjectManager();

        expect(actual).toEqual({ main, metadata, path, title, scripts, styles });
    });
});
