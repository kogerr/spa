import { renderPriceDetails } from '../../../../src/components/admin/price-details/price-details';
import { templates } from '../../../../src/service/container';

const path = '/admin/price-details';
const title = 'Price Details';
const scripts: string[] = [ '/scripts/admin/price-details.js' ];
const styles: string[] = [ '/styles/price-details.css' ];
const metadata = '';
const main = 'main';

describe('Price details component', () => {
    it('should render project editor page', () => {
        const fakeTemplates = new Map([['price-details.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderPriceDetails();

        expect(actual).toEqual({ main, metadata, path, title, scripts, styles });
    });
});
