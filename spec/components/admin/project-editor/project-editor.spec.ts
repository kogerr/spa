import { renderProjectEditor } from '../../../../src/components/admin/project-editor/project-editor';
import { templates } from '../../../../src/service/container';

const path = '/admin/project-editor';
const title = 'Project Editor';
const scripts: string[] = [ '/scripts/admin/project-editor.js' ];
const styles: string[] = [ '/styles/project-editor.css' ];
const metadata = '';
const main = 'main';

describe('Project editor component', () => {
    it('should render project editor page', () => {
        const fakeTemplates = new Map([['project-editor.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderProjectEditor();

        expect(actual).toEqual({ main, metadata, path, title, scripts, styles });
    });
});
