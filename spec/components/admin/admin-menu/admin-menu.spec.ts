import { templates } from '../../../../src/service/container';
import { renderAdminMenu } from '../../../../src/components/admin/admin-menu/admin-menu';

describe('Admin Menu page', () => {
    const path = '/admin';
    const title = 'Admin Platform';
    const scripts: string[] = [];
    const styles: string[] = [ '/styles/admin-menu.css' ];
    const mainTemplateKey = 'admin-menu.main.html';
    const metadata = '';

    it('should render Admin Menu page', () => {
        const fakeTemplates = new Map([[mainTemplateKey, '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderAdminMenu();

        expect(actual).toEqual({ main: 'main', metadata, path, title, scripts, styles });
    });
});
