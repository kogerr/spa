import { templates } from '../../../../src/service/container';
import { renderFontManagerPage } from '../../../../src/components/admin/font-manager/font-manager';

describe('Font Manager page', () => {

    const path = '/admin/font-manager';
    const title = 'Font Manager';
    const scripts: string[] = [ '/scripts/admin/font-manager.js' ];
    const styles: string[] = [ '/styles/font-manager.css' ];
    const metadata = '';
    const mainTemplateKey = 'font-manager.main.html';

    it('should render Font Manager page', () => {
        spyOn(templates, 'get').and.returnValue('`main`');

        const actual = renderFontManagerPage();

        expect(actual).toEqual({ main: 'main', metadata, path, title, scripts, styles });
        expect(templates.get).toHaveBeenCalledWith(mainTemplateKey);
    });
});
