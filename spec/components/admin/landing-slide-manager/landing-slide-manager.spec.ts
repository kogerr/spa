import { renderLandingSlideManager } from '../../../../src/components/admin/landing-slide-manager/landing-slide-manager';
import { templates } from '../../../../src/service/container';

const main = 'main';
const path = '/admin/main-slides';
const title = 'Main Page Slides';
const scripts: string[] = [ '/scripts/admin/landing-slide-manager.js' ];
const styles: string[] = [ '/styles/landing-slide-manager.css' ];
const metadata = '';

describe('Landing slide manager component', () => {
    it('should render landing slide manager page', () => {
        const fakeTemplates = new Map([['landing-slide-manager.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderLandingSlideManager();

        expect(actual).toEqual({ main, metadata, path, title, scripts, styles });
    });
});
