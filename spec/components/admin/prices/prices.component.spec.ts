import { renderPricesComponent } from '../../../../src/components/admin/prices/prices.component';
import { templates } from '../../../../src/service/container';

const path = '/admin/prices';
const title = 'Prices';
const scripts: string[] = [ '/scripts/admin/prices.js' ];
const styles: string[] = [ '/styles/prices.css' ];
const metadata = '';
const main = 'main';

describe('Prices component', () => {
    it('should render project editor page', () => {
        const fakeTemplates = new Map([['prices.component.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderPricesComponent();

        expect(actual).toEqual({ main, metadata, path, title, scripts, styles });
    });
});
