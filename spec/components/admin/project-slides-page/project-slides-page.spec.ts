import { renderProjectSlidesPage } from '../../../../src/components/admin/project-slides-page/project-slides-page';
import { templates } from '../../../../src/service/container';

const main = 'main';
const path = '/admin/project-slides';
const title = 'Project Slides';
const scripts: string[] = [ '/scripts/admin/project-slides-manager.js' ];
const styles: string[] = [ '/styles/landing-slide-manager.css' ];
const metadata = '';
const mainTemplateKey = 'project-slides-page.main.html';

describe('Project Slides page', () => {
    it('should render project slides page', () => {
        const fakeTemplates = new Map([[ mainTemplateKey, '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderProjectSlidesPage();

        expect(actual).toEqual({ main, metadata, path, title, scripts, styles });
    });
});
