import { renderFontFaceUploadPage } from '../../../../src/components/admin/font-face-upload/font-face-upload';
import { templates } from '../../../../src/service/container';

const path = '/admin/font-face-upload';
const title = 'font face upload page';
const scripts: string[] = [ '/scripts/admin/font-face-upload.js' ];
const styles: string[] = [ '/styles/font-face-upload.css' ];
const metadata = '';

describe('FontFace Upload page', () => {
    it('should render FontFace upload page', () => {
        spyOn(templates, 'get').and.returnValue('`main`');

        const actual = renderFontFaceUploadPage();

        expect(actual).toEqual({ main: 'main', metadata, path, title, scripts, styles });
    });
});
