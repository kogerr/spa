import { templates } from '../../../../src/service/container';
import { renderTextManagerPage } from '../../../../src/components/admin/text-manager/text-manager';

describe('Text Manager page', () => {

    const path = '/admin/text-manager';
    const title = 'Placeholder Texts';
    const scripts: string[] = [ '/scripts/admin/text-manager.js' ];
    const styles: string[] = [ '/styles/text-manager.css' ];
    const metadata = '';
    const mainTemplateKey = 'text-manager.main.html';

    it('should render Text Manager page', () => {
        spyOn(templates, 'get').and.returnValue('`main`');

        const actual = renderTextManagerPage();

        expect(actual).toEqual({ main: 'main', metadata, path, title, scripts, styles });
        expect(templates.get).toHaveBeenCalledWith(mainTemplateKey);
    });
});
