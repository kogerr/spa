import { templates } from '../../../../src/service/container';
import { Invoice } from '../../../../src/service/payment/start/start-request.interface';
import { renderInvoice } from '../../../../src/components/admin/order-list/invoice';

describe('Invoice Component', () => {
    const name = 'name';
    const company = 'company';
    const country = 'country';
    const address = 'address';
    const city = 'city';
    const zip = 'zip';
    const invoice: Invoice = { name, company, country, address, city, zip };

    it('should render invoice component from template', () => {
        const fakeTemplates = new Map([['invoice.template.html', '`${invoice.name} ${address} ${invoice.company}`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderInvoice(invoice);

        const expected = 'name country zip city address company';
        expect(actual).toEqual(expected);
    });
});
