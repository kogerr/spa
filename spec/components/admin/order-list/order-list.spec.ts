import { templates } from '../../../../src/service/container';
import { renderOrderList } from '../../../../src/components/admin/order-list/order-list';
import { Order } from '../../../../src/models/order.interface';
import * as orderComponent from '../../../../src/components/admin/order-list/order';
import { Customer } from '../../../../src/models/customer.interface';

describe('Order list page', () => {
    const customer = { email: 'email@address.com' } as Customer;
    const orderRef = '0123';
    const paymentId = '1234';
    const updated = 5678;
    const order: Order = { orderId: orderRef, total: 100, status: 'pending', paymentId, customer, updated, items: [], products: [] };

    const path = '/admin/order-list';
    const title = 'Order List';
    const scripts: string[] = [ '/scripts/order-list.js' ];
    const styles: string[] = [ '/styles/order-list.css' ];
    const metadata = '';

    it('should render order list page', () => {
        const fakeTemplates = new Map([['order-list.main.html', '`${orderList}`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));
        spyOn(orderComponent, 'renderOrder').and.returnValue('order');

        const actual = renderOrderList([order]);

        expect(actual).toEqual({ main: 'order', metadata, path, title, scripts, styles });
        expect(orderComponent.renderOrder).toHaveBeenCalled();
    });
});
