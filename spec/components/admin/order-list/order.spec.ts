import { templates } from '../../../../src/service/container';
import { Order } from '../../../../src/models/order.interface';
import { renderOrder } from '../../../../src/components/admin/order-list/order';
import { Customer } from '../../../../src/models/customer.interface';

describe('Order Component', () => {
    const customer = { email: 'email@address.com' } as Customer;
    const orderRef = '0123';
    const paymentId = '1234';
    const updated = 5678;

    it('should render order component from template', () => {
        const order: Order = { orderId: orderRef, total: 100, status: 'pending', paymentId, customer, updated, items: [], products: [] };
        const fakeTemplates = new Map([['order.template.html', '`${date} ${JSON.stringify(order)} ${statusClass}`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderOrder(order);

        const expected = '1970-01-01 00:00 {"orderId":"0123","total":100,"status":"pending","paymentId":"1234","customer":' +
            '{"email":"email@address.com"},"updated":5678,"items":[],"products":[]} pending';
        expect(actual).toEqual(expected);
    });

});
