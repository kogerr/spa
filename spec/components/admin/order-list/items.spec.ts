import { Item } from '../../../../src/service/payment/start/start-request.interface';
import { renderItems } from '../../../../src/components/admin/order-list/items';
import { templates } from '../../../../src/service/container';

describe('Items Component', () => {
    it('should render items table', () => {
        const items = [ { ref: 'ref', title: 'title', price: '1000' }, { ref: 'ref', title: 'title', price: '2000' } ] as Item[];
        const fakeTemplates = new Map([['items.template.html', '`${itemLines} ${total}`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderItems(items);

        const expected = '<tr><td>ref</td><td>title</td><td>1000</td></tr><tr><td>ref</td><td>title</td><td>2000</td></tr> 3000';
        expect(actual).toBe(expected);
    });
});
