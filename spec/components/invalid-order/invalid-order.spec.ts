import { templates } from '../../../src/service/container';
import { PaymentResult } from '../../../src/models/payment-result.interface';
import { renderInvalidOrder } from '../../../src/components/invalid-order/invalid-order';

describe('Payment Result page', () => {
    const result = 0;
    const transactionId = 1;
    const event = 'SUCCESS';
    const merchant = 'merchant';
    const orderId = 'orderId';
    const title = 'Invalid Order';
    const path = 'invalid';

    it('should render Payment Result page', () => {
        const paymentResult: PaymentResult = { result, transactionId, event, merchant, orderId };
        const fakeTemplates = new Map([['invalid-order.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderInvalidOrder(paymentResult);

        expect(actual).toEqual({ main: 'main', metadata: '', path, title, scripts: [], styles: [] });
    });
});
