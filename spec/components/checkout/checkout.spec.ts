import { renderCheckout } from '../../../src/components/checkout/checkout';
import { templates } from '../../../src/service/container';

describe('Checkout page', () => {
    it('should render checkout page', () => {
        const fakeTemplates = new Map([['checkout.main.html', '`main`'], ['checkout.metadata.html', '`metadata`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderCheckout();

        expect(actual).toEqual({ main: 'main', metadata: 'metadata', path: '/checkout', title: 'Checkout', scripts: [], styles: [] });
    });
});
