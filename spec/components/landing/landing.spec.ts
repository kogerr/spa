import { renderLanding } from '../../../src/components/landing/landing';
import * as projectPreview from '../../../src/components/landing/project-preview/project-preview';
import { templates } from '../../../src/service/container';
import * as landingSlideDao from '../../../src/db/landing-slide.dao';
import * as projectDao from '../../../src/db/project.dao';
import { LandingSlide } from '../../../src/models/landing-slide.interface';

const scripts: string[] = [];
const styles: string[] = [];
const path = '/';
const title = 'koger.io';

describe('Landing page', () => {

    const slide: LandingSlide = { index: 0, src: 'src', link: 'link', alt: 'alt' };
    const preview = { previewImage: 'previewImage', index: 0, url: 'url' };
    const renderedPreview = 'renderedPreview';

    it('should render landing page', async () => {
        const fakeTemplates = new Map([
            ['landing.main.html', 'getImages(slides) + previewWrappers'],
            ['landing.metadata.html', '`metadata`'],
        ]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));
        spyOn(landingSlideDao, 'getLandingSlides').and.returnValue(Promise.resolve([ slide ]));
        spyOn(projectDao, 'getPreviewImages').and.returnValue(Promise.resolve([ preview ]));
        spyOn(projectPreview, 'renderLandingProjectPreview').and.returnValue(renderedPreview);

        const actual = await renderLanding();

        const main = '<a href="link" style="left: 0%"><img src="src" alt="alt" /></a>' + renderedPreview;
        expect(landingSlideDao.getLandingSlides).toHaveBeenCalledWith();
        expect(projectDao.getPreviewImages).toHaveBeenCalledWith();
        expect(projectPreview.renderLandingProjectPreview).toHaveBeenCalledWith(preview);
        const expected = { main, metadata: 'metadata', path, title, scripts, styles };
        expect(actual).toEqual(expected);
    });

});
