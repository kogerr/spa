import { templates } from '../../../../src/service/container';
import { renderLandingProjectPreview } from '../../../../src/components/landing/project-preview/project-preview';

describe('Project preview subcomponent', () => {

    const preview = { previewImage: 'previewImage', index: 0, url: 'url' };
    const templateKey = 'project-preview.main.html';

    it('should render project preview for landing page', async () => {
        const fakeTemplates = new Map([[templateKey, 'JSON.stringify(preview)']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = await renderLandingProjectPreview(preview);

        expect(templates.get).toHaveBeenCalledWith(templateKey);
        expect(actual).toEqual(JSON.stringify(preview));
    });
});
