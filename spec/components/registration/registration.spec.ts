import { renderRegistration } from '../../../src/components/registration/registration';
import { templates } from '../../../src/service/container';

describe('Registration page', () => {
    it('should render registration page', () => {
        const fakeTemplates = new Map([['registration.main.html', '`main`'], ['registration.metadata.html', '`metadata`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderRegistration();

        expect(actual).toEqual({ main: 'main', metadata: 'metadata', path: '/registration', title: 'Registration', scripts: [ '/scripts/login.js' ], styles: [] });
    });
});
