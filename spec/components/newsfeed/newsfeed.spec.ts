import { renderNewsFeed } from '../../../src/components/newsfeed/newsfeed';
import { DatedTemplate } from '../../../src/models/dated-template.interface';
import { templates } from '../../../src/service/container';
import * as newspostProvider from '../../../src/service/providers/newspost.provider';

describe('Newsfeed', () => {

    const path = '/newsfeed';
    const title = 'newsfeed';
    const scripts: string[] = [];
    const styles: string[] = [];

    it('should render newsfeed', async () => {
        const newsPost = { date: 2, template: 'news' } as DatedTemplate;
        const newsPost2 = { date: 3, template: 'othernews' } as DatedTemplate;
        const fakeTemplates = new Map([['newsfeed.main.html', 'posts'], ['newsfeed.metadata.html', '`metadata`']]);
        spyOn(newspostProvider, 'getNewsPosts').and.returnValue(Promise.resolve([ newsPost, newsPost2 ]));
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = await renderNewsFeed();

        expect(newspostProvider.getNewsPosts).toHaveBeenCalledWith();
        expect(templates.get).toHaveBeenCalledWith('newsfeed.main.html');
        expect(templates.get).toHaveBeenCalledWith('newsfeed.metadata.html');
        expect(actual).toEqual({ main: 'othernewsnews', metadata: 'metadata', path, title, scripts, styles });
    });
});
