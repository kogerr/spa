import { renderPhilosophy } from '../../../src/components/philosophy/philosophy';
import { templates } from '../../../src/service/container';

describe('Philosophy page', () => {
    it('should render philosophy page', () => {
        const fakeTemplates = new Map([['philosophy.main.html', '`main`'], ['philosophy.metadata.html', '`metadata`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderPhilosophy();

        expect(actual).toEqual({ main: 'main', metadata: 'metadata', path: '/philosophy', title: 'philosophy', scripts: [], styles: [] });
    });
});
