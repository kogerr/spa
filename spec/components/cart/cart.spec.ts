import { renderCart } from '../../../src/components/cart/cart';
import { templates } from '../../../src/service/container';

describe('Cart page', () => {
    it('should render cart page', () => {
        const fakeTemplates = new Map([['cart.main.html', '`main`'], ['cart.metadata.html', '`metadata`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderCart();

        expect(actual).toEqual({ main: 'main', metadata: 'metadata', path: '/cart', title: 'Cart', scripts: [], styles: [] });
    });
});
