import { renderExampleEmail } from '../../../src/components/example-email/example-email';
import { templates } from '../../../src/service/container';

describe('Example email', () => {
    it('should render an email template', () => {
        const name = 'Theodosius';
        spyOn(templates, 'get').and.callFake((key: string) => '`${name}`');

        const actual = renderExampleEmail(name);

        expect(actual).toEqual(name);
    });
});
