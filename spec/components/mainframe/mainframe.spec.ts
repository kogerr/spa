import { renderMainframe } from '../../../src/components/mainframe/mainframe';
import { templates } from '../../../src/service/container';

describe('Mainframe', () => {
    it('should render mainframe', () => {
        const fakeTemplates = new Map([['mainframe.html', '[main, metadata, path, scripts, getStyleTags(styles), title, hash].join(",")']]);
        const page = { main: 'main', metadata: 'metadata', path: 'path', scripts: [ 'script' ], styles: [ 'style' ], title: 'title' };
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));
        const hash = process.env.CI_COMMIT_SHORT_SHA ?? '';

        const actual = renderMainframe(page);

        expect(actual).toEqual({ path: 'path', content: 'main,metadata,path,script,<link href="style" rel="stylesheet">,title,' + hash });
    });
});
