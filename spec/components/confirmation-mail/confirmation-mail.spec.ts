import { templates } from '../../../src/service/container';
import { Order } from '../../../src/models/order.interface';
import { renderConfirmationMail } from '../../../src/components/confirmation-mail/confirmation-mail';
import { Customer } from '../../../src/models/customer.interface';

describe('Confirmation mail page', () => {
    const customer = { name: 'name', email: 'email' } as Customer;
    const product = { title: 'some kind of font product', price: 6000 };
    const order: Order = { orderId: 'tja00qq', customer, paymentId: '500781334',
        status: 'SUCCESS', total : 6000, updated : 1609619742262, items : [], products: [ product ] };

    it('should render full confirmation email string', () => {
        const path = 'confirmation-mail.full.html';
        const template = '`${getProducts(order.products)} ${order.customer.name} ${getDate(order.updated)}`';
        const fakeTemplates = new Map([[path, template]]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderConfirmationMail(order);

        const expected = '<tr><td style="padding: 16px">some kind of font product</td><td style="text-align: right; padding: 16px">' +
            '6000 €</td></tr> name Sat Jan 02 2021';
        expect(actual).toEqual(expected);
    });
});
