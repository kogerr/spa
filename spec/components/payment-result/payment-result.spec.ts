import { renderPaymentResult } from '../../../src/components/payment-result/payment-result';
import { templates } from '../../../src/service/container';
import { Order } from '../../../src/models/order.interface';
import { Customer } from '../../../src/models/customer.interface';

describe('Payment Result page', () => {
    const total = 100;
    const paymentId = 'paymentId';
    const status = 'SUCCESS';
    const orderId = 'orderId';
    const customer = { email: 'email' } as unknown as Customer;

    it('should render Payment Result page', () => {
        const order: Order = { paymentId, status, orderId, total, customer, updated: Date.now(), items: [], products:[] };
        const fakeTemplates = new Map([['payment-result.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = renderPaymentResult(order, status);

        expect(actual).toEqual({ main: 'main', metadata: '', path: 'invalid', title: 'Payment SUCCESS', scripts: [], styles: [] });
    });
});
