import { render404 } from '../../../src/components/404/404';
import { templates } from '../../../src/service/container';

describe('404 page', () => {
    it('should render 404 page', () => {
        const fakeTemplates = new Map([['404.main.html', '`main`']]);
        spyOn(templates, 'get').and.callFake((key: string) => fakeTemplates.get(key));

        const actual = render404();

        expect(actual).toEqual({ main: 'main', metadata: '', path: '404', title: '404 Page Not Found', scripts: [], styles: [] });
    });
});
