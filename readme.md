# Single Page Application Experiment

The goal is to build a page that would work without javascript as a traditional static page, but when the javascript files are run it turns into a single page applications. No frameworks used, performance is crucial.

## Build

To build the app, run `tsc` and `node build`.

## Watch

To continuously build on changes, run both `tsc --watch` and `node watch`.

## TODO

* [ ] Products price validation
* [ ] Invoice downstream service integration
* [ ] VAT matching and verifications (partly dependent on downstream)
* [ ] Handle invoice properties in database
* [ ] Reorganize DB tables
* [x] 25% tags in the showcase
* [ ] info page (waiting for input?)
* [ ] Payment Start error display (page maybe)
* [ ] Payment result pages (input) ?
* [ ] Reimplement email (input)
* [ ] Check how to assign fonts to project pages
* [x] Add bundle version generation (script done, css left)
* [x] Env vars in pipeline
* [ ] GooglePay
* [ ] logout
* [ ] metrics of purchases
* [ ] design for mobile devices (input)
* [ ] make project work without slides
* [ ] make playground work without prices
