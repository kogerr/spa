import { Dirent, mkdir, readdir, readFile, writeFile } from 'fs';

const sourceDir = 'src';
const targetDir = 'build';

const makeParentDir = (path: string, content: Buffer | string) => {
    const directory = path.slice(0, path.lastIndexOf('/'));
    mkdir(directory, { recursive: true }, (error: NodeJS.ErrnoException | null) => {
        if (error) {
            console.error(error);
        } else {
            copyFile(path, content);
        }
    });
};

const copyFile = (path: string, content: Buffer | string) => {
    writeFile(path, content, {}, (error: NodeJS.ErrnoException | null) => {
        if (error && error.path) {
            makeParentDir(error.path, content);
        } else {
            console.log('Copied', path);
        }
    });
};

const convertFile = (path: string) => {
    if (path.endsWith('.css')) {
        readFile(path, { encoding: 'utf8' }, (err, content) => {
            const minifiedContent = content.replace(/\n\s*/g, '');
            copyFile(targetDir + '/' + path, minifiedContent);
        });
    } else {
        readFile(path, null, (err, content) => {
            copyFile(targetDir + '/' + path, content);
        });
    }
};

const readDirectory = (path: string) => {
    readdir(path, {encoding: 'utf8', withFileTypes: true}, (err, files) => {
        if (files) {
            files.forEach((entry: Dirent) => {
                if (entry.isDirectory()) {
                    readDirectory(path + '/' + entry.name);
                } else if (entry.isFile() && !entry.name.endsWith('.ts')) {
                    convertFile(path + '/' + entry.name);
                }
            });
        }
    });
};

readDirectory(sourceDir);
