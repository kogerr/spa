FROM node:14 as builder

WORKDIR /usr/src/app

RUN apt-get update

ARG CI_COMMIT_SHORT_SHA
ENV CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}

COPY app.ts ./
COPY build.ts ./
COPY tsconfig* ./
COPY package.json ./
COPY package-lock.json ./
COPY webpack.config.js ./
RUN mkdir src
COPY src/. ./src

RUN mv src/assets/styles/common.css src/assets/styles/common${CI_COMMIT_SHORT_SHA}.css

RUN npm install
RUN npm run build

FROM node:14

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/build/ ./
COPY --from=builder /usr/src/app/package*.json ./

RUN npm ci --production

EXPOSE 80
EXPOSE 443

ENTRYPOINT [ "node", "app.js" ]
