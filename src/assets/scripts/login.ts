export const initLogin = () => {
    const error = getErrorParameter();
    if (error) {
        displayError(error);
        removeErrorParameter();
    }
};

const getErrorParameter = () => {
    const matches = window.location.href.match(/error=(.+)/);

    return matches && matches[1] ? decodeURIComponent(matches[1]) : undefined;
};

const displayError = (error: string) => {
    const div = document.createElement('div');
    div.setAttribute('class', 'login-error');
    div.innerText = error;
    const form = document.getElementsByTagName('form')[0] as HTMLFormElement;
    form.appendChild(div);
    window.setTimeout(() => form.removeChild(div), 3000);
};

const removeErrorParameter = () => window.history.pushState('', 'Login', window.location.href.replace(/\??error=(.+)/, ''));
