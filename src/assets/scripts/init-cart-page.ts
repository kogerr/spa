import { addObject, clearObjects, getObjects, Item, Product, removeObjects, setField } from './state-handler';
import { equals } from './equals';

type ItemWithAnchor = { anchor: HTMLElement, item: Item };
type PriceBlock = { platform: number, licenseSize: number, singleStyle: number, singlePair: number, family: number };

const pricesEndpoint = '/api/prices?family=';

const platforms = [ 'desktop', 'web', 'app' ];

// tslint:disable-next-line:no-bitwise
const getPlatforms = (n: number) => (n >>> 0).toString(2)
    .split('').reverse()
    .map((d, i) => d === '1' ? platforms[i] : null)
    .filter(a => a);

const handleLicenseChange = (allPriceBlocks: Map<string, PriceBlock[]>, license: number) => {
    const products = [ ...getObjects<Product[]>('products') ];
    const items = [ ...getObjects<Item[]>('items') ];
    clearObjects('items');
    clearObjects('products');

    products.forEach(product => {
        const priceBlock = (allPriceBlocks.get(product.family) as PriceBlock[])
            .find(pb => equals({ licenseSize: license, platform: product.platform }, pb)) as PriceBlock;
        // @ts-ignore
        const price = priceBlock[product.bundle]
        addObject('products', {...product, license, price});
    });
    items.forEach(item => {
        const priceBlock = (allPriceBlocks.get(item.family) as PriceBlock[])
            .find(pb => equals({ licenseSize: license, platform: item.platform }, pb)) as PriceBlock;
        const price = priceBlock.singleStyle;
        addObject('items', {...item, license, price});
    });

    setField('license', license);

    setUpCart(getObjects<Item[]>('items'), getObjects<Product[]>('products'))
        .map(item => listenToX(item, getObjects<Product[]>('products'), (allPriceBlocks.get(item.item.family) as PriceBlock[])));
};

const setSizeSelector = () => {
    const license = getObjects<number>('license');
    const sizeSelect = document.getElementById('size') as HTMLSelectElement;
    sizeSelect.value = license.toString(10);

    return sizeSelect;
};

const listenToX = (itemWithAnchor: ItemWithAnchor, products: Product[], priceBlocks: PriceBlock[]) => {
    itemWithAnchor.anchor.onclick = (event) => {
        event.preventDefault();
        removeObjects('items', itemWithAnchor.item);
        const { license, platform, family, weight, style } = itemWithAnchor.item;
        const single = products.find(p => equals({ license, platform, family, weight, style, bundle: 'singleStyle' }, p));
        const pair = products.find(p => equals({ license, platform, family, weight, bundle: 'singlePair' }, p));
        const familyProduct = products.find(p => equals({ license, platform, family, bundle: 'family' }, p));
        const priceBlock = priceBlocks.find(pb => equals({ licenseSize: license, platform }, pb)) as PriceBlock;

        if (single) {
            removeObjects('products', single);
        } else if (pair) {
            removeObjects('products', pair);
            addObject('products', { ...pair, style: style === 'italic' ? 'normal' : 'italic', bundle: 'singleStyle', price: priceBlock.singleStyle });
        } else if (familyProduct) {
            removeObjects('products', familyProduct);
            getObjects<Item[]>('items')
                .filter(item => equals({ family, license, platform }, item))
                .forEach((item, index, all) => {
                    const counterpart = all.find(n => equals({ ...item, style: item.style === 'italic' ? 'normal' : 'italic' }, n));
                    if (counterpart) {
                        addObject('products', { family, license, platform,
                            weight: item.weight, bundle: 'singlePair', price: priceBlock.singlePair });
                    } else {
                        addObject('products', { family, license, platform,
                            weight: item.weight, style: item.style, bundle: 'singleStyle', price: priceBlock.singleStyle });
                    }
            });
        }

        setUpCart(getObjects<Item[]>('items'), getObjects<Product[]>('products')).map(item => listenToX(item, products, priceBlocks));
    };
};


const fetchPrices = (family: string) =>
    fetch(pricesEndpoint + family)
        .then(res => res.json() as Promise<PriceBlock[]>)

const displayItem = (item: Item, itemsElement: HTMLDivElement) => {
    const div = document.createElement('div');
    div.className = 'cart-line cart-item';
    div.innerHTML = `<span>${item.family} ${item.weight} ${item.style}</span>`
        + `<div class="t2 link">${getPlatforms(item.platform).join('<span> + </span>')}</div>`
        + `<span class="cart-item-price">${item.price} €</span>`;
    const x = document.createElement('span');
    x.className = 'remove-link link';
    x.innerText = '✕';
    div.appendChild(x);
    itemsElement.appendChild(div);

    return x;
};

const setUpCart = (items: Item[], products: Product[]) => {
    const itemsElement = document.getElementById('cart-items') as HTMLDivElement;
    itemsElement.innerHTML = '';
    items.sort((a, b) => (a.license - b.license) !== 0 ? (a.license - b.license)
        : (a.platform - b.platform) !== 0 ? (a.platform - b.platform)
        : (a.family.localeCompare(b.family) !== 0 ? (a.family.localeCompare(b.family))
        : (a.weight - b.weight)));

    let itemsPrice = 0;
    const itemsWithAnchors: ItemWithAnchor[] = [];
    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        const anchor = displayItem(item, itemsElement);
        itemsWithAnchors.push({ anchor, item });
        itemsPrice += item.price;
    }

    const productsPrice = products.map(p => p.price).reduce((a, b) => a + b, 0);
    (document.getElementById('cart-discount') as HTMLSpanElement).innerText = '- ' + (itemsPrice - productsPrice) + ' €';
    (document.getElementById('cart-total') as HTMLHeadingElement).innerText = productsPrice + ' €';

    if (itemsWithAnchors.length === 0) {
        (document.getElementById('cart-emptiness') as HTMLDivElement).style.display = 'grid';
        (document.getElementById('cart-content') as HTMLDivElement).style.display = 'none';
    }

    return itemsWithAnchors;
};

export const initCartPage = () => {
    (document.getElementById('cart-preview') as HTMLDivElement).style.display = 'none';
    const items = getObjects<Item[]>('items');
    const products = getObjects<Product[]>('products');

    if (items.length > 0 && products.length > 0) {
        (document.getElementById('cart-emptiness') as HTMLDivElement).style.display = 'none';
        (document.getElementById('cart-content') as HTMLDivElement).style.display = 'grid';
        const itemsWithAnchors = setUpCart(items, products);
        const families: string[] = [];
        products.forEach(p => { if (!families.includes(p.family)) { families.push(p.family); } });
        const allPriceBlocks = new Map<string, PriceBlock[]>();
        const promises = families.map(family => fetchPrices(family).then(priceBlock => {
            itemsWithAnchors.filter(iwa => iwa.item.family === family).map(iwa => listenToX(iwa, products, priceBlock));
            allPriceBlocks.set(family, priceBlock);
        }));
        const sizeSelect = setSizeSelector();

        return Promise.all(promises)
            .then(() => { sizeSelect.onchange = (event) => handleLicenseChange(allPriceBlocks, parseInt(sizeSelect.value, 10)); });
    }
};
