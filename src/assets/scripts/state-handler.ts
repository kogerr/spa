import { equals } from './equals';

export type Product = { family: string, license: number, platform: number, bundle: string, weight?: number, style?: string, price: number };
export type Item = { family: string, license: number, platform: number, weight: number, style: string, price: number };
type State = { products: Product[], items: Item[], license: number };
type Listener = ((u: (Product[]) | (Item[]) | number) => any);

const state : State = { products: [], items: [], license: 1 };
const listeners: { products: Listener[], items: Listener[], license: Listener[] } = { products: [], items: [], license: [] };

const updateListeners = (field: keyof State) =>
    listeners[field].map((listener) => listener(state[field]));

export const getObjects = <T>(field: keyof State) => state[field] as unknown as T;

export const removeObjects = (field: 'products' | 'items', filter: Partial<Item | Product>) => {
    const matched = (state[field] as unknown[]).filter(item => equals(filter, item));
    const found = matched.length > 0;
    if (found) {
        matched.forEach(p => state[field].splice(state[field].findIndex((q: Item | Product) => equals(p, q)), 1));
        updateListeners(field);
        localStorage.setItem(field, JSON.stringify(state[field]));
    }

    return found;
};

export const addObject = (field: 'items' | 'products', item: Item | Product) => {
    const canBeAdded = !(state[field] as unknown[]).some(p => equals(item, p));

    if (canBeAdded) {
        (state[field] as unknown[]).push(item);
        updateListeners(field);
        localStorage.setItem(field, JSON.stringify(state[field]));
    }

    return canBeAdded;
};

export const hasObject = (field: 'items' | 'products', item: Partial<Item | Product>) =>
    (state[field] as unknown[]).findIndex(item2 => equals(item, item2)) > -1;

export const addStateListener = <T>(field: keyof State, listener: Listener) => {
    listeners[field].push(listener);
    listener(state[field]);
};

export const clearObjects = (field: 'items' | 'products') => {
    state[field].splice(0);
    localStorage.setItem(field, '[]');
    updateListeners(field);
};

export const setField = (field: keyof State, value: any) => {
    state[field] = value;
    localStorage[field] = JSON.stringify(value);
};

export const initState = () => {
    (Object.getOwnPropertyNames(state) as (keyof State)[]).forEach((field) => {
        if (localStorage[field]) {
            state[field] = JSON.parse(localStorage[field]);
        } else {
            localStorage.setItem(field, JSON.stringify(state[field]));
        }
        updateListeners(field);
    })
};
