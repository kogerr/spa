import { addStateListener, Product } from './state-handler';

export const updateOnChange = () => {
    const cartSum = document.getElementById('cart-sum') as HTMLDivElement;
    const cartPreview = document.getElementById('cart-preview') as HTMLDivElement;
    const plusElement = document.getElementById('cart-plus') as HTMLSpanElement;
    addStateListener('products', (products) => renderDropdown(products, cartSum, cartPreview, plusElement));
};

const renderDropdown = (products: any, cartSum: HTMLDivElement, cartPreview: HTMLDivElement, plusElement: HTMLSpanElement) => {
    cartPreview.style.display = products.length
        && !document.location.pathname.startsWith('/cart')
        && !document.location.pathname.startsWith('/checkout')
        ? 'block' : 'none';
    cartSum.innerText = (products as Product[]).reduce((a, b) => a + b.price, 0).toString() + ' €';
    plusElement.style.display = products.length ? 'inline' : 'none';
};
