namespace Preload {
    fetch('/api/preload')
        .then((response: Response) => response.json())
        .then((content: { images: string[], fonts: string[] }) => {
            content.images.forEach((href) => addLinkElement(href, 'image'));
            content.fonts.forEach((href) => addLinkElement(href, 'font'));
        });

    const addLinkElement = (href: string, as: string) => {
        const link = document.createElement('link');
        link.rel = 'preload';
        link.href = href;
        link.as = as;
        document.body.appendChild(link);
    };
}
