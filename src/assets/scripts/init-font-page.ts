import { createTesters } from './create-tester';
import { initSlides } from './carousel';
import { initShowcase } from './showcase';

const convert = (slide: any): HTMLImageElement => {
    const htmlImageElement = document.createElement('img');
    htmlImageElement.alt = 'alt';
    htmlImageElement.src = slide.src;

    return htmlImageElement;
};

export const initFontPage = () => {
    const pathParts = window.location.pathname.split('/');
    const url = pathParts[pathParts.length - 1];
    return fetch('/api/extras/' + url)
        .then(res => res.json())
        .then((extras) => createTesters(extras.fonts, extras.texts).then(() => extras))
        .then((extras) => { if (extras.slides.length) { initSlides('project-carousel', convert, extras.slides) }; return extras.fonts })
        .then(fonts => initShowcase(url, fonts));
};
