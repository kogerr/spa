import { setUpBehaviour } from './showcase-behaviour';
import { getObjects, removeObjects, addObject, Product, Item, hasObject, setField } from './state-handler';

type PriceBlock = { platform: number, licenseSize: number, singleStyle: number, singlePair: number, family: number };
type Font = { index: number, family: string, source: string, descriptors: { weight: number, style: string } };
type Elements = { showcaseWrapper :HTMLDivElement, platformForm :HTMLFormElement, sizeSelect :HTMLSelectElement, productForm :HTMLFormElement };
type Styles = [ keyof CSSStyleDeclaration, string ][];

const pricesEndpoint = '/api/prices?family=';

const handleLicenseChange = (family: string, priceBlocks: PriceBlock[], platformForm: HTMLFormElement, license: number) => {
    const platform = getPlatform(platformForm);
    if (platform) {
        const products = getObjects<Product[]>('products').filter(p => p.family === family);
        const items = getObjects<Item[]>('items').filter(p => p.family === family);
        const priceBlock = priceBlocks.find(pb => pb.licenseSize === license && pb.platform === platform) as PriceBlock;
        removeObjects('products', { family });
        products.forEach(product => {
            // @ts-ignore
            const price = priceBlock[product.bundle];
            addObject('products', { ...product, license, price });
        });
        items.forEach(item => removeObjects('items', item));
        items.forEach(item => {
            const price = priceBlock.singleStyle;
            addObject('items', { ...item, license, price });
        });
    }
};

const createLine = (id: string, description: string, price: number, style: Styles, productForm: HTMLFormElement, checked: boolean) => {
    const input = document.createElement('input');
    input.type = 'checkbox';
    input.id = id;
    input.name = id;
    input.value = id;
    input.checked = checked;
    const label = document.createElement('label') as HTMLLabelElement;
    label.htmlFor = id;
    const nameSpan = document.createElement('span');
    nameSpan.innerText = description;
    // @ts-ignore
    style.forEach(p => { nameSpan.style[p[0]] = p[1]; });
    const discountSpan = document.createElement('span');
    discountSpan.className = 't2';
    discountSpan.innerText = 'save 25%';
    discountSpan.style.opacity = '0';
    const priceSpan = document.createElement('span');
    priceSpan.innerText = price.toString();
    label.append(nameSpan, discountSpan, priceSpan)

    productForm.append(input, label);
    return { input, discountSpan, priceSpan };
};

const createFontLine = (font: Font, price: number, productForm: HTMLFormElement, checked: boolean) => {
    const description = font.family + ' ' + font.descriptors.weight + (font.descriptors.style === 'italic' ? ' Italic' : '');
    const id = description.replace(/s/g, '-');
    const style: [ keyof CSSStyleDeclaration, string ][] =
        [ ['fontFamily', font.family ],
        ['fontStyle', font.descriptors.style ],
        ['fontWeight', font.descriptors.weight.toString() ] ];

    return createLine(id, description, price, style, productForm, checked);
};

const getPlatform = (platformForm: HTMLFormElement) =>
    ([ platformForm.elements[0], platformForm.elements[1], platformForm.elements[2] ] as HTMLInputElement[])
        .filter(input => input.checked)
        .map(input => parseInt(input.value, 10))
        .reduce((a, b) => a + b, 0);

const createHandle = (font: Font, priceBlock: PriceBlock, productForm: HTMLFormElement, allChecked: boolean) => {
    const family = font.family;
    const weight = font.descriptors.weight;
    const style = font.descriptors.style;
    const price = priceBlock.singleStyle;
    const checked = allChecked
        || hasObject('products', { family, weight, style, bundle: 'singleStyle', platform: priceBlock.platform, license: priceBlock.licenseSize })
        || hasObject('products', { family, weight, bundle: 'singlePair', platform: priceBlock.platform, license: priceBlock.licenseSize });

    return { family, weight, style, ...createFontLine(font, price, productForm, checked) };
};

const createShowcase = (family: string, fonts: Font[], priceBlocks: PriceBlock[], elements: Elements) => {
    elements.showcaseWrapper.style.display = 'block';
    elements.productForm.innerHTML = '';

    const platform = getPlatform(elements.platformForm);
    if (platform) {
        const license = getObjects<number>('license');
        const priceBlock = priceBlocks.find(block => block.licenseSize === license && block.platform === platform) as PriceBlock;
        const allChecked = hasObject('products', { family, bundle: 'family', platform, license });
        const allHandle = createLine('full-family', family + ' full family', priceBlock.family, [], elements.productForm, allChecked);
        const fontHandles = fonts.map(font => createHandle(font, priceBlock, elements.productForm, allChecked));
        setUpBehaviour(family, allHandle, fontHandles, priceBlock);
    }
};

const setUpProducts = (family: string, fonts: Font[], priceBlocks: PriceBlock[]) => {
    const showcaseWrapper = document.getElementById('showcase-wrapper') as HTMLDivElement;
    const platformForm = document.getElementById('platform-form') as HTMLFormElement;
    const platformDiscounts = document.getElementById('platform-discounts') as HTMLDivElement;
    const sizeSelect = document.getElementById('size') as HTMLSelectElement;
    const productForm = document.getElementById('product-form') as HTMLFormElement;
    const elements: Elements = { showcaseWrapper, platformForm, sizeSelect, productForm };

    createShowcase(family, fonts, priceBlocks, elements);
    platformForm.onchange = (event) => createShowcase(family, fonts, priceBlocks, elements);
    sizeSelect.value = getObjects<number>('license').toString(10);
    sizeSelect.onchange = (event) => {
        const newLicense = parseInt(sizeSelect.value, 10);
        setField('license', newLicense);
        handleLicenseChange(family, priceBlocks, platformForm, newLicense);
        createShowcase(family, fonts, priceBlocks, elements);
    };

    for (let i = 0; i < 3; i++) {
        const checkbox = (platformForm.elements[i] as HTMLInputElement);
        (platformDiscounts.children[i] as HTMLSpanElement).style.opacity = checkbox.checked ? '0' : '1';
        checkbox.onchange = () =>
            (platformDiscounts.children[i] as HTMLSpanElement).style.opacity = checkbox.checked ? '0' : '1';
    }
};

export const initShowcase = (family: string, fonts: Font[]) =>
    fetch(pricesEndpoint + family)
        .then(res => res.json() as Promise<PriceBlock[]>)
        .then((priceBlocks) => { if (priceBlocks.length > 0) setUpProducts(family, fonts, priceBlocks) });
