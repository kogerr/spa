export interface SubPageStem {
    pathMatcher: RegExp,
    title: string,
    init: () => void
}
