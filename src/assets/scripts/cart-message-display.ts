export const displayCartMessage = (message: string) => {
    const cartMessageLine = document.getElementById('message-line') as HTMLDivElement;
    cartMessageLine.style.display = 'block';
    cartMessageLine.innerText = message;
    setTimeout(() => {
        cartMessageLine.style.display = 'none';
        cartMessageLine.innerText = '';
    }, 3000);
};
