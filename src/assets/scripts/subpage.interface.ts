import { SubPageStem } from './sub-page-stem.interface';

export interface SubPage extends SubPageStem {
    setUp: () => void,
    path: string
}
