type Slide = { index: number, src: string };
type convert = (slide: Slide) => HTMLElement;
type CarouselState = { carousel: HTMLDivElement, slides: HTMLElement[], index: number, lock: boolean };

const setUpCarousel = (state: CarouselState) => {
    const previous = state.slides[state.slides.length - 1];
    const current = state.slides[0];
    const next = state.slides[1];
    previous.style.left = '-100%';
    current.style.left = '0%';
    next.style.left = '100%';
    state.carousel.innerHTML = '';
    state.carousel.append(previous, current, next);
};

const shiftSlides = (state: CarouselState, direction: 1 | -1) => {
    if (!state.lock) {
        state.lock = true;
        const children = state.carousel.children;
        for (let i = 0; i < children.length; i++) {
            (children[i] as HTMLElement).style.left = ((i + direction - 1) * 100) + '%';
        }
        state.index -= direction;
        if (state.index < 0) {
            state.index = state.slides.length - 1;
        } else if (state.index >= state.slides.length) {
            state.index = 0;
        }
        setTimeout(() => {
            if (direction === -1) {
                state.carousel.removeChild(children[0]);
                const next = state.index < state.slides.length - 1 ? state.slides[state.index + 1] : state.slides[0];
                next.style.left = '100%';
                state.carousel.appendChild(next);
            } else {
                state.carousel.removeChild(children[children.length - 1]);
                const previous = state.index > 0 ? state.slides[state.index - 1] : state.slides[state.slides.length - 1];
                previous.style.left = '-100%';
                state.carousel.prepend(previous);
            }
            state.lock = false
        }, 1000);
    }

    return state;
};

const listenToButtons = (state: CarouselState) => {
    const leftButton = document.getElementById('slide-left') as HTMLDivElement;
    const rightButton = document.getElementById('slide-right') as HTMLDivElement;
    leftButton.onclick = () => shiftSlides(state, 1);
    rightButton.onclick = () => shiftSlides(state, -1);

    return state;
};

const startRotation = (state: CarouselState) => {
    return setInterval(() => shiftSlides(state, -1), 2000);
};

const fill = (slideStubs: Slide[]) => {
    let newSlideArray: Slide[];
    if (slideStubs.length === 1) {
        newSlideArray = [ slideStubs[0], slideStubs[0], slideStubs[0] ];
    } else if (slideStubs.length === 2) {
        newSlideArray = [ slideStubs[0], slideStubs[1], slideStubs[0], slideStubs[1] ];
    } else {
        newSlideArray = slideStubs;
    }

    return newSlideArray;
};

export const initSlides = (carouselId: string, cb: convert, slideStubs: Slide[]) => {
    const carousel = document.getElementById(carouselId) as HTMLDivElement
    const slides: HTMLElement[] = fill(slideStubs.sort((a, b) => a.index - b.index))
        .map((slide) => cb(slide));

    const state = { carousel, slides, index: 0, lock: false };

    setUpCarousel(state);
    listenToButtons(state);
    return startRotation(state);
};
