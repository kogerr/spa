import { SubPage } from './subpage.interface';
import { SubPageStem } from './sub-page-stem.interface';

export const navigate = (subPage: SubPage) => {
    subPage.setUp();
    window.history.pushState('', subPage.title, subPage.path);
    initStem(subPage);
};

export const initStem = (subPage: SubPageStem) => {
    document.title = subPage.title;
    subPage.init();
};

export const navigateOnPopState = (subPages: SubPage[]) => {
    window.onpopstate = (event: PopStateEvent) => {
        const path = document.location.pathname.split('?')[0];
        const currentPage = subPages.find(subPage => subPage.path === path);
        if (currentPage) {
            currentPage.setUp();
            initStem(currentPage);
        } else {
            document.location.href = path;
        }
    };

    return subPages;
};
