import { getSubpageStems } from './sub-page-stem.provider';
import { initStem, navigateOnPopState } from './navigate';
import { getSubPages } from './sub-page.factory';
import { initMenu } from './menu';
import { initializeCart } from './cart-init';
import { initializeSwitch } from './light-switch';

export const init = () => {
    initializeCart();
    const subPagesStems = getSubpageStems();
    const currentPage = subPagesStems.find(subPage => subPage.pathMatcher.test(document.location.pathname));
    if (currentPage) {
        initStem(currentPage);
    }

    return Promise.resolve(initializeSwitch())
        .then(() => getSubPages(subPagesStems))
        .then(navigateOnPopState)
        .then(initMenu)
        .catch(console.error);
};
