const addDiv = (index: number, main: HTMLElement) => {
    const div = document.createElement('div');
    const colourCode = (index * 167772).toString(16).padStart(6, '0');
    div.setAttribute('style', 'height: 1vh;background-color: #' + colourCode);
    div.id = 'div' + index;
    main.appendChild(div);
    if (index < 100) {
        addDiv(index + 1, main);
    }
};

export const initPhilosophy = () => {
    const main: HTMLElement = document.getElementsByTagName('main')[0] as HTMLElement;
    addDiv(0, main);
};
