export const equals = (a: any, b: any) => !Object.entries(a).map(entry => b[entry[0]] === entry[1]).some(x => !x);
