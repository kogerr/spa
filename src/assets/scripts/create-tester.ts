type Font = { index: number, family: string, source: string, descriptors: { weight: number, style: string } };
type PlaceholderText = { index: number, text: string };

const defaultSize = 160;

const setDefaults = (playground: HTMLDivElement, slider: HTMLInputElement, size: HTMLLabelElement, font: Font, text: string) => {
    const defaultSizeString = defaultSize.toString();
    playground.style.fontFamily = font.family;
    playground.style.fontWeight = font.descriptors.weight.toString(10);
    playground.style.fontStyle = font.descriptors.style;
    playground.style.fontSize = defaultSizeString + 'px';
    playground.style.columnCount = '1';
    playground.innerText = text;
    slider.value = defaultSizeString;
    size.innerText = defaultSizeString + 'px';

    return playground;
};

const createToggle = (name: string, fontFamily: string) => {
    const id = fontFamily + '-' + name;
    const input = document.createElement('input');
    input.type = 'checkbox';
    input.id = id;
    const label = document.createElement('label');
    label.className = 'toggle-24';
    label.innerText = name;
    label.setAttribute('for', id);

    return [ input, label ];
};

const addTester = (font: Font, index: number, fonts: Font[], wrapper: HTMLDivElement, text: string) => {
    const tester = document.createElement('div');
    tester.className = 'tester';
    const header = document.createElement('div');
    header.className = 'tester-header t2';
    const left = document.createElement('div');
    left.className = 'header-left';
    const select = document.createElement('select');
    fonts.forEach((f, j) => {
        const option = document.createElement('option');
        option.innerText = `${f.family} ${f.descriptors.weight} ${f.descriptors.style}`;
        option.value = `${f.family} ${f.descriptors.weight} ${f.descriptors.style}`;
        if (j === index) {
            option.selected = true;
        }
        select.append(option)
    });
    const slider = document.createElement('input');
    slider.className = 'tester-slider';
    slider.type = 'range';
    slider.min = '12';
    slider.max = '240';
    const size = document.createElement('label');
    const centre = document.createElement('div');
    centre.className = 'header-center';
    const refresh = document.createElement('button');
    const right = document.createElement('div');
    right.className = 'header-right';
    const toggles = document.createElement('div');
    toggles.className = 'toggles';
    const inputsAndLabels = [ ...createToggle('alt', font.family), ...createToggle('lig', font.family),
        ...createToggle('num', font.family) ];
    const buy = document.createElement('button');
    buy.className = 'toggle-24';
    buy.innerText = 'buy';
    const playground = document.createElement('div');
    playground.className = 'tester-playground';
    playground.contentEditable = 'true';
    setDefaults(playground, slider, size, font, text);

    tester.appendChild(header);
    header.appendChild(left);
    left.appendChild(select);
    left.appendChild(slider);
    left.appendChild(size);
    header.appendChild(centre);
    centre.appendChild(refresh);
    header.appendChild(right);
    right.appendChild(toggles);
    inputsAndLabels.forEach(node => toggles.appendChild(node));
    right.appendChild(buy);
    tester.appendChild(playground);
    wrapper.appendChild(tester);

    select.onchange = () => {
        const selected = fonts[select.selectedIndex];
        playground.style.fontFamily = selected.family;
        playground.style.fontWeight = selected.descriptors.weight.toString();
        playground.style.fontStyle = selected.descriptors.style.toString();
    };
    slider.oninput = () => {
        const fontSize = slider.value + 'px';
        size.innerText = fontSize;
        playground.style.fontSize = fontSize;
        const value = parseInt(slider.value, 10);
        if (value < 23) {
            playground.style.columnCount = '3';
        } else if (value < 33) {
            playground.style.columnCount = '2';
        } else {
            playground.style.columnCount = '1'
        }
    };
    refresh.onclick = () => setDefaults(playground, slider, size, font, text);
    buy.onclick = () => document.getElementById('showcase')?.scrollIntoView({ behavior: 'smooth' });
    playground.onpaste = (event) => {
        event.preventDefault();
        const pastedText = event.clipboardData?.getData('text/plain');
        document.execCommand('insertHTML', false, pastedText);
    };
};

const createFontFace = (fontData: Font): Promise<unknown> => {
    // @ts-ignore
    const font = new FontFace(fontData.family, fontData.source, fontData.descriptors);
    return font.load().then(() => (document as any).fonts.add(font));
};

const processFonts = (fonts: Font[], wrapper: HTMLDivElement, texts: PlaceholderText[]) =>
    fonts.map((font, index) => createFontFace(font).then(() => addTester(font, index, fonts, wrapper, texts[index % texts.length].text)));

export const createTesters = (fonts: Font[], texts: PlaceholderText[]) => {
    fonts.sort((a, b) => a.index - b.index);
    texts.sort((a, b) => a.index - b.index);
    const wrapper = document.getElementById('testers-wrap') as HTMLDivElement;
    return Promise.all(processFonts(fonts, wrapper, texts));
};
