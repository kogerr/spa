import { initSlides } from './carousel';

const convert = (slide: any): HTMLAnchorElement => {
    const htmlImageElement = document.createElement('img');
    htmlImageElement.alt = slide.alt;
    htmlImageElement.src = slide.src;
    const anchor = document.createElement('a');
    anchor.href = slide.link;
    anchor.appendChild(htmlImageElement);

    return anchor;
};

const insertPreviewHoverSvgs = () => {
    const wrappers = document.getElementsByClassName('preview-wrapper');
    return fetch('/api/hover-images')
        .then(res => res.json() as Promise<{ index: number, previewHover: string }[]>)
        .then(images => images.sort((a, b) => a.index - b.index))
        .then(previews => previews.map(p => p.previewHover))
        .then(images => images.forEach((image, i) => {
            const hoverImage = document.createElement('svg');
            wrappers[i].insertBefore(hoverImage, wrappers[i].firstChild);
            hoverImage.outerHTML = image;
            hoverImage.setAttribute('class', 'hover-image');
            wrappers[i].children[0].setAttribute('class', 'hover-image');
        }));
};

export const landingInit = () => {
    return Promise.all([
        fetch('/api/landing-slides').then(res => res.json()).then(slides => initSlides('carousel', convert, slides)),
        insertPreviewHoverSvgs()
    ]);
};
