import { navigate } from './navigate';
import { SubPage } from './subpage.interface';

export const initMenu = (pages: SubPage[]) => {
    const anchors: HTMLCollectionOf<HTMLAnchorElement> = document.getElementsByTagName('a');

    for(let i = 0; i < anchors.length; i++) {
        const pathname = anchors[i].pathname.split('?')[0];
        // if (pathname) {
            const page = pages.find(s => s.path === pathname);
            if (page) {
                anchors[i].onclick = (event: MouseEvent) => {
                    event.preventDefault();
                    navigate(page);
                    initMenu(pages);
                };
            }
        }
    // }
};
