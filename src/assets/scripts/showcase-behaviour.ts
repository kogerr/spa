import { addObject, removeObjects } from './state-handler';
import { displayCartMessage } from './cart-message-display';

type AllHandle = { input: HTMLInputElement, discountSpan: HTMLSpanElement };
type FontHandle = { input: HTMLInputElement, weight: number, style: string, discountSpan: HTMLSpanElement, family: string, priceSpan: HTMLSpanElement };
type PriceBlock = { platform: number, licenseSize: number, singleStyle: number, singlePair: number, family: number };

const platforms = [ 'desktop', 'web', 'app' ];

// tslint:disable-next-line:no-bitwise
const getPlatform = (n: number) => (n >>> 0).toString(2)
    .split('').reverse()
    .map((d, i) => d === '1' ? platforms[i] : null)
    .filter(a => a).join('+');

const getText = (fontHandle: FontHandle, priceBlock: PriceBlock, change: string) =>
    fontHandle.family + ' ' + fontHandle.weight + ' ' + fontHandle.style + ' '
    + getPlatform(priceBlock.platform) + ' ' + priceBlock.licenseSize + ' ' + change;

const createProduct = (fontHandle: FontHandle, priceBlock: PriceBlock, bundle: keyof PriceBlock) =>
    ({ family: fontHandle.family, bundle, price: priceBlock[bundle], license: priceBlock.licenseSize,
        platform: priceBlock.platform, weight: fontHandle.weight, style: fontHandle.style });

const createItem = (fontHandle: FontHandle, priceBlock: PriceBlock) =>
    ({ family: fontHandle.family, weight: fontHandle.weight, style: fontHandle.style,
        platform: priceBlock.platform, license: priceBlock.licenseSize, price: priceBlock.singleStyle });

const isPair = (handle1: FontHandle, handle2: FontHandle) =>
    handle2.family === handle1.family && handle2.weight === handle1.weight && handle2.style !== handle1.style;

const setUpAllHandle = (family: string, allHandle: AllHandle, fontHandles: FontHandle[], priceBlock: PriceBlock) => {
    allHandle.input.onchange = (event) => {
        removeObjects('products', { family, platform: priceBlock.platform, license: priceBlock.licenseSize });
        fontHandles.forEach(fh => fh.priceSpan.innerText = priceBlock.singleStyle.toString());
        if (allHandle.input.checked) {
            displayCartMessage(family + ' family ' + getPlatform(priceBlock.platform) + ' ' + priceBlock.licenseSize + ' added to cart');
            fontHandles.forEach(fh => { fh.input.checked = true; fh.discountSpan.style.opacity = '0'; addObject('items', createItem(fh, priceBlock)) });
            addObject('products', { family, bundle: 'family', platform: priceBlock.platform, license: priceBlock.licenseSize, price: priceBlock.family });
        } else {
            displayCartMessage(family + ' family ' + getPlatform(priceBlock.platform) + ' ' + priceBlock.licenseSize + ' removed from cart');
            fontHandles.forEach(fh => { fh.input.checked = false; removeObjects('items', createItem(fh, priceBlock)); });
        }
    };
};

const setUpCheckedBehaviour = (fontHandle: FontHandle, allHandle: AllHandle, priceBlock: PriceBlock, allInputs: HTMLInputElement[], counterpart?: FontHandle) => {
    displayCartMessage(getText(fontHandle, priceBlock, 'added to cart'));
    fontHandle.discountSpan.style.opacity = '0';
    addObject('items', createItem(fontHandle, priceBlock));
    if (!allInputs.some(i => !i.checked)) {
        allHandle.input.checked = true;
        removeObjects('products', { family: fontHandle.family, platform: priceBlock.platform, license: priceBlock.licenseSize });
        addObject('products', { family: fontHandle.family, bundle: 'family', platform: priceBlock.platform, license: priceBlock.licenseSize, price: priceBlock.family });
    } else if (counterpart && counterpart.input.checked) {
        fontHandle.discountSpan.style.opacity = '0';
        removeObjects('products', { family: counterpart.family, bundle: 'singleStyle', style: counterpart.style, weight: counterpart.weight, license: priceBlock.licenseSize, platform: priceBlock.platform });
        addObject('products', { family: fontHandle.family, bundle: 'singlePair', weight: fontHandle.weight, license: priceBlock.licenseSize, platform: priceBlock.platform, price: priceBlock.singlePair });
    } else {
        if (counterpart) {
            counterpart.discountSpan.style.opacity = '1';
            counterpart.priceSpan.innerText = ((2 * priceBlock.singleStyle) - priceBlock.singlePair).toString();
        }
        addObject('products', createProduct(fontHandle, priceBlock, 'singleStyle'));
    }
};

const setUpUncheckedBehaviour = (fontHandle: FontHandle, allHandle: AllHandle, priceBlock: PriceBlock, allHandles: FontHandle[], counterpart?: FontHandle) => {
    displayCartMessage(getText(fontHandle, priceBlock, 'removed from cart'));
    removeObjects('items', createItem(fontHandle, priceBlock));
    if (counterpart) {
        counterpart.priceSpan.innerText = priceBlock.singleStyle.toString();
    }
    if (allHandles.filter(h => h.input.checked).length === allHandles.length - 1) {
        allHandle.discountSpan.style.opacity = '1';
        allHandle.input.checked = false;
        removeObjects('products', { family: fontHandle.family, platform: priceBlock.platform, license: priceBlock.licenseSize, bundle: 'family' });
        fontHandle.priceSpan.innerText = ((2 * priceBlock.singleStyle) - priceBlock.singlePair).toString();
        fontHandle.discountSpan.style.opacity = '1';
        const checkedHandles = allHandles.filter(h => h.input.checked);
        checkedHandles.forEach(handle => {
            const bundle: keyof PriceBlock = checkedHandles.some(h => isPair(h, handle)) ? 'singlePair' : 'singleStyle';
            const product = { family: handle.family, bundle, weight: handle.weight, license: priceBlock.licenseSize, platform: priceBlock.platform, price: priceBlock[bundle] };
            if (bundle === 'singleStyle') {
                // @ts-ignore
                product.style = handle.style;
            }
            addObject('products', product);
        });
    } else if (counterpart && counterpart.input.checked) {
        fontHandle.discountSpan.style.opacity = '1';
        fontHandle.priceSpan.innerText = ((2 * priceBlock.singleStyle) - priceBlock.singlePair).toString();
        removeObjects('products', { family: fontHandle.family, bundle: 'singlePair', weight: fontHandle.weight, license: priceBlock.licenseSize, platform: priceBlock.platform });
        addObject('products', createProduct(counterpart, priceBlock,'singleStyle'));
    } else {
        if (counterpart) {
            counterpart.discountSpan.style.opacity = '0';
        }
        removeObjects('products', { family: fontHandle.family, bundle: 'singleStyle', style: fontHandle.style, weight: fontHandle.weight, license: priceBlock.licenseSize, platform: priceBlock.platform });
    }
};

const setUpHandles = (fontHandle: FontHandle, allHandle: AllHandle, priceBlock: PriceBlock, allHandles: FontHandle[], counterpart?: FontHandle) => {
    fontHandle.input.onchange = (event) => {
        if (fontHandle.input.checked) {
            setUpCheckedBehaviour(fontHandle, allHandle, priceBlock, allHandles.map(h => h.input), counterpart);
        } else {
            setUpUncheckedBehaviour(fontHandle, allHandle, priceBlock, allHandles, counterpart);
        }
    };
};

export const setUpBehaviour = (family: string, allHandle: AllHandle, fontHandles: FontHandle[], priceBlock: PriceBlock) => {
    fontHandles.forEach(fontHandle => {
        const counterpart = fontHandles.find(handle => isPair(handle, fontHandle));
        setUpHandles(fontHandle, allHandle, priceBlock, fontHandles, counterpart);
    });
    setUpAllHandle(family, allHandle, fontHandles, priceBlock);
};
