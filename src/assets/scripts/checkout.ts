import { getObjects } from './state-handler';
import { Product } from './state-handler';

export const initCheckout = () => {
    const total = document.getElementById('total') as HTMLInputElement;
    const productsInput = document.getElementById('products') as HTMLInputElement;
    const itemsInput = document.getElementById('items') as HTMLInputElement;
    const form = document.forms[0] as HTMLFormElement;

    const products = getObjects<Product[]>('products');
    productsInput.value = JSON.stringify(products);
    total.value = (products as Product[]).reduce((a, b) => a + b.price, 0).toString();
    itemsInput.value = JSON.stringify(getObjects('items'));

    form['submit-button'].onclick = (event: PointerEvent) => form.classList.add('touched');
    form.oninvalid = console.log;

    const someoneElseButton = document.getElementById('someone-else') as HTMLInputElement;
    const licenseForm = document.getElementById('license-form') as HTMLFieldSetElement;

    someoneElseButton.onchange = (event) => {
        licenseForm.className = someoneElseButton.checked ? 'shown' : 'hidden';
    };
};
