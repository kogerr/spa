export const initializeSwitch = () => {
    const switchElement = document.getElementById('switch') as HTMLElement;
    switchElement.onclick = changeMode;
    const currentState = localStorage.getItem('switch');
    if (currentState) {
        document.body.className = currentState;
    } else if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        document.body.className = 'dark';
    }
};

const changeMode = () => {
    const newState = document.body.className === 'light' ? 'dark' : 'light';
    document.body.className = newState;
    localStorage.setItem('switch', newState)
};
