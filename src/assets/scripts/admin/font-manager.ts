namespace FontManager {
    type Font = { index: number; family: string; weight: number; italic: boolean; id: string };
    const fontsEndpoint = '/admin/api/fonts';
    const wrapper = document.getElementById('fonts-wrapper') as HTMLDivElement;
    const newFontButton = document.getElementById('new-font') as HTMLAnchorElement;
    const projectUrl = new URLSearchParams(window.location.search).get('project');
    let fonts: Font[] = [];

    const removeFont = (font: Font, index: number) => {
        fonts.splice(index, 1);
        for (let i = 0; i < fonts.length; i++) {
            fonts[i].index = i;
        }
        const body = JSON.stringify({ projectUrl, id: font.id });
        const method = 'DELETE';
        const headers = { 'Content-Type': 'application/json' };

        fetch(fontsEndpoint, { body, method, headers })
            .then(response => response.json())
            .then(() => init())
            .catch(console.error);
    };

    const swapFontWithNext = (i: number) => {
        if (i >= 0 && i < fonts.length - 1) {
            const firstFontIndex = fonts[i].index;
            fonts[i].index = fonts[i + 1].index;
            fonts[i + 1].index = firstFontIndex;
            fonts.sort((a, b) => a.index - b.index);
            const body = JSON.stringify({ projectUrl, fonts: fonts.map(({ index, id }) => ({ index, id })) });
            const method = 'PATCH';
            const headers = { 'Content-Type': 'application/json' };

            return fetch(fontsEndpoint, { body, method, headers })
                .then(response => response.json())
                .then(() => init())
                .catch(console.error);
        }
    };

    const displayFonts = () => {
        wrapper.innerHTML = '';
        fonts.forEach((font, index) => {
            const nameElement = document.createElement('div');
            nameElement.innerText = font.family + ' ' + font.weight + (font.italic ? ' italic' : '');
            nameElement.className = 'name t1';
            const arrowsWrapperElement = document.createElement('div');
            arrowsWrapperElement.className = 'arrows-wrapper';
            const upArrow = document.createElement('div');
            upArrow.innerText = '↑';
            upArrow.className = 'clickable link t1';
            upArrow.onclick = () => swapFontWithNext(index - 1);
            const downArrow = document.createElement('div');
            downArrow.innerText = '↓';
            downArrow.className = 'clickable link t1';
            downArrow.onclick = () => swapFontWithNext(index);
            arrowsWrapperElement.appendChild(upArrow);
            arrowsWrapperElement.appendChild(downArrow);
            const removeElement = document.createElement('div');
            removeElement.innerText = '✕';
            removeElement.className = 'remove clickable link t1';
            removeElement.onclick = () => removeFont(font, index);
            const fontElement = document.createElement('div');
            fontElement.className = 'font-line';
            fontElement.appendChild(nameElement);
            fontElement.appendChild(arrowsWrapperElement);
            fontElement.appendChild(removeElement);
            wrapper.appendChild(fontElement);
        });
    };

    const setUpNewFontButton = () => newFontButton.href = '/admin/font-face-upload?project=' + projectUrl +
        '&index=' + fonts.length;

    const init = () => fetch(fontsEndpoint + '?projectUrl=' + projectUrl)
        .then(response => response.json())
        .then((response: Font[]) => { fonts = response.sort((a, b) => a.index - b.index); })
        .then(displayFonts)
        .then(setUpNewFontButton)
        .catch(console.error);

    init();
}
