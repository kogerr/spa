namespace TextManager {
    type PlaceholderText = { _id: string, index: number, text: string };
    const endpoint = '/admin/api/texts';
    const wrapper = document.getElementById('texts-wrapper') as HTMLDivElement;
    const headers = { 'Content-Type': 'application/json' };

    const removeText = (i: number, texts: PlaceholderText[]) => {
        const removeInit = { body: JSON.stringify({ id: texts[i]._id, src: [] }), method: 'DELETE', headers };
        texts.splice(i, 1);
        const reindexed = texts.map(({text, _id}, index) => ({ index, text, _id }));
        const updateInit = { body: JSON.stringify([ ...reindexed ]), method: 'PATCH', headers };

        fetch(endpoint, removeInit).then(() => fetch(endpoint, updateInit))
            .then(res => res.json())
            .then((ts: PlaceholderText[]) => ts.sort((a, b) => a.index - b.index))
            .then(displayTexts)
            .catch(console.error);
    };

    const addText = (index: number, text: string) => {
        if (text && text.length) {
            const body = JSON.stringify({ index, text });
            const method = 'POST';
            fetch(endpoint, { body, method, headers })
                .then(res => res.json())
                .then((texts: PlaceholderText[]) => texts.sort((a, b) => a.index - b.index))
                .then(displayTexts)
                .catch(console.error);
        }
    };

    const addNewTextPart = (index: number) => {
        const newTextLine = document.createElement('div');
        newTextLine.className = 'new-text-line';
        const textArea = document.createElement('textarea');
        const plus = document.createElement('button');
        plus.innerText = '+';
        newTextLine.appendChild(textArea);
        newTextLine.appendChild(plus);
        plus.onclick = () => addText(index, textArea.value);
        wrapper.appendChild(newTextLine);
    };

    const swapTexts = (i: number, texts: PlaceholderText[]) => {
        const j = i + 1;
        if(i >= 0 && j < texts.length) {
            const payload = [ { _id: texts[i]._id, index: texts[j].index }, { _id: texts[j]._id, index: texts[i].index } ];
            const body = JSON.stringify(payload);
            const method = 'PATCH';
            fetch(endpoint, { body, method, headers })
                .then(res => res.json())
                .then((ts: PlaceholderText[]) => ts.sort((a, b) => a.index - b.index))
                .then(displayTexts)
                .catch(console.error);
        }
    };

    const addTextLine = (text: any, index: any, texts: any) => {
        const textLine = document.createElement('div');
        textLine.className = 'text-line';
        const textArea = document.createElement('p');
        textArea.innerText = text.text;
        const arrows = document.createElement('div');
        arrows.className = 'arrows-wrapper';
        const up = document.createElement('button');
        up.innerText = '↑';
        const down = document.createElement('button');
        down.innerText = '↓';
        const remove = document.createElement('button');
        remove.innerText = '✕';
        textLine.appendChild(textArea);
        textLine.appendChild(arrows);
        arrows.appendChild(up);
        arrows.appendChild(down);
        textLine.appendChild(remove);
        wrapper.appendChild(textLine);

        up.onclick = () => swapTexts(index - 1, texts);
        down.onclick = () => swapTexts(index, texts);
        remove.onclick = () => removeText(index, texts);
    };

    const displayTexts = (texts: PlaceholderText[]) => {
        wrapper.innerHTML = '';
        texts.forEach((text, index, all) => addTextLine(text, index, all));
        addNewTextPart(texts.length);
    };

    const init = () => fetch(endpoint)
        .then(res => res.json())
        .then((texts: PlaceholderText[]) => texts.sort((a, b) => a.index - b.index))
        .then(displayTexts)
        .catch(console.error);

    init();
}
