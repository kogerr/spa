const projectForm = document.getElementsByTagName('form')[0] as HTMLFormElement;
const shortNameElement = document.getElementById('shortName') as HTMLInputElement;
const urlElement = document.getElementById('url') as HTMLInputElement;
// @ts-ignore
const projectEndpoint = '/admin/api/projects';

let project: any = {
    languages: 'albanian, basque, bemba, bosnian, breton, catalan, croatian, czech, danish, dutch, english, esperanto, estonian, faroese, filipino, finnish, french, galician, german, hungarian, icelandic, indonesian, irish, italian, latvian, lithuanian, malay, maltese, maori, norwegian, polish, portuguese, romanian, serbian, slovak, slovenian, somali, spanish, swahili, swedish, zulu',
    release: 'gestalten, 2013',
    version: '1.1',
    formats: 'otf, ttf, woff, woff2',
    slides: []
};
let allProjects: any[] = [];
const idParameter = new URLSearchParams(window.location.search).get('id');

const svgMapping: [RegExp, string][] = [
    [/black/g, 'var(--blak)'],
    [/#DDDDDD/g, 'var(--neutral)'],
    [/#5450FA/g, 'var(--ink)']
];

const uploadFile = (file: File, fieldId: string, overwrite = false) =>
    postFileData(file, overwrite)
        .then((response) => handleUpldResponse(response, file, fieldId))
        .catch(console.error);

const postFileData = (file: File, overwrite = false): Promise<Response> => {
    const headers = [
        [ 'filename', file.name ],
        [ 'Content-Length', file.size.toString(10) ],
        [ 'base64', false ],
        [ 'overwrite', overwrite.toString() ]
    ];
    const init = { method: 'POST', headers, body: file } as RequestInit;

    return fetch('/admin/api/upload', init);
};

const handleUpldResponse = (response: Response, file: File, fieldId: string): undefined | Promise<unknown> => {
    if (response.status === 409) {
        if (confirm('File already exists. Do you wish to overwrite?')) {
            return uploadFile(file, fieldId, true).then(console.log).catch(console.error);
        }
    } else if (response.status === 200) {
        return response.json().then(responseBody => showPrvw(responseBody, fieldId, file.name));
    } else {
        return response.json().then(console.error);
    }
};

const showPrvw = (responseBody: { success: boolean, path: string }, fieldId: string, fileName: string) => {
    project[fieldId] = responseBody.path;
    const fileDisplay = document.getElementById(fieldId + '-display') as HTMLAnchorElement;
    const pngPreview = document.getElementById(fieldId + '-preview') as HTMLImageElement;
    if (fileDisplay) {
        fileDisplay.style.display = 'block';
        fileDisplay.innerText = fileName;
        fileDisplay.href = responseBody.path;
    } else if (pngPreview) {
        pngPreview.style.display = 'block';
        pngPreview.src = responseBody.path;
    }
};

const processSvg = (file: File, fieldId: string): Promise<void> => {
    return file.text()
        .then(svgContent => {
            svgMapping.forEach(r => {
                svgContent = svgContent.replace(r[0], r[1]);
            });
            project[fieldId] = svgContent;
            const svgPreview = document.getElementById(fieldId + '-wrapper') as HTMLDivElement;
            svgPreview.innerHTML = svgContent;
            svgPreview.style.display = 'block';
        })
        .catch(console.error);
};

for (let i = 0; i < projectForm.elements.length; i++) {
    const element = projectForm.elements[i] as HTMLInputElement;
    if (element.type === 'text' || element.tagName.toLowerCase() === 'textarea') {
        element.addEventListener('change', () => project[element.id] = element.value);
    } else if (element.type === 'file') {
        if (element.accept && element.accept.includes('svg')) {
            // @ts-ignore
            element.addEventListener('change', (event) => processSvg(event.target.files[0], element.id));
        } else {
            // @ts-ignore
            element.addEventListener('change', (event) => uploadFile(event.target.files[0], element.id, false));
        }
    }
}

shortNameElement.addEventListener('change', () => {
    const urlValue = encodeURI(shortNameElement.value.replace(/\s/g, '-'));
    urlElement.value = urlValue;
    project.url = urlValue;
});

const fillForm = (projectResponse: any) => {
    project = projectResponse;
    Object.entries(projectResponse).forEach(entry => {
        const inputElement = projectForm[entry[0]];
        const value = entry[1] as string;
        if (inputElement && (inputElement.type === 'text' || inputElement.tagName.toLowerCase() === 'textarea')) {
            inputElement.value = value;
        } else if (inputElement && inputElement.type === 'file') {
            inputElement.required = false;
            const svgPreview = document.getElementById(entry[0] + '-wrapper') as HTMLDivElement;
            const fileDisplay = document.getElementById(entry[0] + '-display') as HTMLAnchorElement;
            const pngPreview = document.getElementById(entry[0] + '-preview') as HTMLImageElement;
            if (svgPreview) {
                svgPreview.style.display = 'block';
                svgPreview.innerHTML = (entry[1] as string);
            } else if (fileDisplay) {
                fileDisplay.style.display = 'block';
                fileDisplay.innerText = value;
                fileDisplay.href = value;
            } else if (pngPreview) {
                pngPreview.style.display = 'block';
                pngPreview.src = entry[1] as string;
            }
        }
    });
};

const transform = (flatProject: { languages: string, release: string, version: string, formats: string, description: string, specimenPdf: string }) => {
    const { description, languages, release, version, formats, specimenPdf, ...base } = flatProject;
    const update = { ...base, details: { description, languages, release, version, formats, specimenPdf } };
    console.log(flatProject);
    console.log(update);
    return update;
};

const flatten = (structuredProject: { details: any }): any => {
    const { details, ...base } = structuredProject;
    return { ...base, ...details };
};

const initProjectEditor = () => {
    if (idParameter) {
        fillForm(flatten(allProjects.find((p: any) => p._id === idParameter)));
        projectForm.onsubmit = (event) => {
            event.preventDefault();
            fetch(projectEndpoint, { body: JSON.stringify([transform(project)]), method: 'PATCH' })
                .then((response) => response.json() as Promise<LandingSlide[]>)
                // .then(() => window.location.href = '/admin/projects')
                .catch(console.error);
        };
    } else {
        projectForm.onsubmit = (event) => {
            event.preventDefault();
            project.index = allProjects.length;
            fetch(projectEndpoint, { body: JSON.stringify(transform(project)), method: 'POST' })
                .then((response) => response.json() as Promise<LandingSlide[]>)
                .then(() => window.location.href = '/admin/projects')
                .catch(console.error);
        };
    }
};

fetch(projectEndpoint, { method: 'GET' })
    .then(response => response.json())
    .then(response => allProjects = response)
    .then(initProjectEditor)
    .catch(console.error);
