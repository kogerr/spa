// @ts-ignore
const uploadEndpoint = '/admin/api/upload';
const slidesEndpoint = '/admin/api/landing-slides';
const newSlideWrapper = document.getElementById('new-slide-wrapper') as HTMLDivElement;
const slidesContainer = document.getElementById('slides-container') as HTMLDivElement;
const newSlideButton = document.getElementById('new-slide') as HTMLButtonElement;
const slideUploadForm = document.getElementById('new-slide-form') as HTMLFormElement;
const preview = document.getElementById('preview') as HTMLImageElement;
const uploadCancelButton = document.getElementById('cancel-button') as HTMLImageElement;
const slideErrorPanel = document.getElementById('error-panel') as HTMLDivElement;
const slideMessagePanel = document.getElementById('message-panel') as HTMLDivElement;
let slides: LandingSlide[] = [];
let currentUploadSrc: string | undefined;

type LandingSlide = { _id: string, index: number; src: string; link: string; alt?: string; };

const getSlides = (): Promise<LandingSlide[]> => fetch(slidesEndpoint)
    .then(response => response.json())
    .catch(console.error);

const removeSlide = (slide: LandingSlide) => fetch(slidesEndpoint, { body: JSON.stringify({ id: slide._id, src: [ slide.src ] }), method: 'DELETE' })
    .then((response) => response.json() as Promise<LandingSlide[]>)
    .then(response => { response.sort((a, b) => a.index - b.index); slides = response; displaySlides(); })
    .catch(error => { slideErrorPanel.style.display = 'block'; slideErrorPanel.innerText = error });

// @ts-ignore
const displayMessage = (message: string) => {
    slideMessagePanel.innerText = message;
    slideMessagePanel.style.display = 'block';
    setTimeout(() => slideMessagePanel.style.display = 'none', 2000);
};

const displaySlides = () => {
    slidesContainer.innerHTML = '';
    slides.forEach((slide, i) => {
        const imageElement = document.createElement('img');
        imageElement.src = slide.src;
        imageElement.alt = slide.alt || '';
        const div = document.createElement('div');
        div.className = 'slidePreview';
        const linkLine = document.createElement('div');
        linkLine.className = 'input-line';
        const linkLabel = document.createElement('label');
        linkLabel.innerText = 'link';
        const linkInput = document.createElement('input');
        linkInput.value = slide.link;
        linkInput.onchange = () => {
            slide.link = linkInput.value;
            sendUpdate([ slide ]);
        };
        linkLine.appendChild(linkLabel);
        linkLine.appendChild(linkInput);
        const altLine = document.createElement('div');
        altLine.className = 'input-line';
        const altLabel = document.createElement('altLabel');
        altLabel.innerText = 'alt';
        const altInput = document.createElement('input');
        altInput.value = slide.alt || '';
        altInput.onchange = () => {
            slide.alt = altInput.value;
            sendUpdate([ slide ]);
        };
        altLine.appendChild(altLabel);
        altLine.appendChild(altInput);
        const removeButton = document.createElement('button');
        removeButton.innerText = 'Remove';
        removeButton.className = 'button-40';
        removeButton.onclick = () => removeSlide(slide);
        const arrowsRow = document.createElement('div');
        arrowsRow.className = 'arrows-row link t1';
        const leftArrow = document.createElement('span');
        leftArrow.innerText = '←';
        const rightArrow = document.createElement('span');
        rightArrow.innerText = '→';
        arrowsRow.appendChild(leftArrow);
        arrowsRow.appendChild(rightArrow);
        div.appendChild(imageElement);
        div.appendChild(linkLine);
        div.appendChild(altLine);
        div.appendChild(arrowsRow);
        div.appendChild(removeButton);
        slidesContainer.appendChild(div);

        rightArrow.onclick = () => swapSlides(i, i + 1);
        leftArrow.onclick = () => swapSlides(i - 1, i);
    });
};

const swapSlides = (i: number, j: number) => {
    const first = slides[i];
    const second = slides[j];
    if (first && second) {
        first.index = j;
        second.index = i;
        slides.sort((a, b) => a.index - b.index);
        sendUpdate(slides);
    }
};

const sendUpdate = (updatedSlides: LandingSlide[]) =>
    fetch(slidesEndpoint, { body: JSON.stringify(updatedSlides), method: 'PATCH' })
        .then((response) => response.json() as Promise<LandingSlide[]>)
        .then(response => { response.sort((a, b) => a.index - b.index); slides = response; displaySlides(); })
        .then(() => displayMessage('Updated'))
        .catch(error => { slideErrorPanel.style.display = 'block'; slideErrorPanel.innerText = error });

const uploadImageOnChange = (event: Event) => {
    const element = (event.target as HTMLInputElement);
    if (element && element.files && element.files.length) {
        return uploadImage(element.files[0]);
    } else {
        return Promise.resolve();
    }
};

const uploadImage = (file: File, overwrite = false) =>
    postImage(file, overwrite)
        .then((response) => handleUploadResponse(response, file))
        .catch(console.error);

const postImage = (file: File, overwrite = false): Promise<Response> => {
    const method = 'POST';
    const base64 = file.name.endsWith('.woff') || file.name.endsWith('.woff2') ? 'true' : 'false';
    const headers = {
        'filename': file.name,
        'Content-Length': file.size.toString(10),
        base64,
        'overwrite': overwrite.toString()
    };
    const init = { method, headers, body: file };

    return fetch(uploadEndpoint, init);
};

const handleUploadResponse = (response: Response, file: File): undefined | Promise<unknown> => {
    if (response.status === 409) {
        if (confirm('File already exists. Do you wish to overwrite?')) {
            return uploadImage(file, true).then(console.log).catch(console.error);
        }
    } else if (response.status === 200) {
        return response.json().then(showPreview);
    } else {
        return response.json().then(console.error);
    }
};

const showPreview = (responseBody: { success: boolean, path: string }) => {
    currentUploadSrc = responseBody.path;
    preview.style.display = 'block';
    preview.src = responseBody.path;
};

slideUploadForm.image.onchange = uploadImageOnChange;

getSlides()
    .then(response => { response.sort((a, b) => a.index - b.index); slides = response; displaySlides(); });

newSlideButton.onclick = () => {
    newSlideWrapper.style.display = 'block';
};

slideUploadForm.onsubmit = (event) => {
    event.preventDefault();
    const alt = slideUploadForm.alt.value;
    const src = currentUploadSrc;
    const link = slideUploadForm.link.value;
    const index = slides.length;
    fetch(slidesEndpoint, { body: JSON.stringify({ alt, src, link, index }), method: 'POST' })
        .then((response) => response.json() as Promise<LandingSlide[]>)
        .then(response => { response.sort((a, b) => a.index - b.index); slides = response; displaySlides(); })
        .then(() => {
            newSlideWrapper.style.display = 'none';
            slideUploadForm.reset();
            preview.style.display = 'none';
        })
        .catch(error => { slideErrorPanel.style.display = 'block'; slideErrorPanel.innerText = error });
};

uploadCancelButton.onclick = () => newSlideWrapper.style.display = 'none';
