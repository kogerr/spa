const fontUpdateEndpoint = '/admin/api/font-update';
const keys: string[] = [ 'family', 'style', 'weight', 'public', 'price', 'woff-file' ];
const form = document.getElementsByTagName('form')[0];
const update: any = {};

keys.forEach(key => {
    // @ts-ignore
    form.elements[key].onchange = (event: Event) => {
        // @ts-ignore
        update[key] = event.target.value;
    };
});

const readFile = (file: File): Promise<{ data: string, fileName: string }> => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    return new Promise(resolve => {
        reader.onload = () => {
            const data = (reader.result as string).replace(/data:.+?;base64,/, '');
            const fileName = file.name;
            resolve({ data, fileName });
        };
    });
};

const displayResponse = (text: string) => {
    const div = document.createElement('div');
    div.innerText = text;
    div.style.padding = '16px';
    div.style.backgroundColor = '#dfe';
    div.style.opacity = '1';
    div.style.transition = 'opacity 1s';
    form.appendChild(div);
};

const redirect = () => setTimeout(() => document.location.href = '/admin/fonts', 1000);

const handleUpdateResponse = (response: any) => {
    if (response.updatedExisting) {
        displayResponse('Successful update');
        redirect();
    } else {
        console.error(response);
        displayResponse(response);
    }
};

const send = (fullUpdate: unknown) => {
    // @ts-ignore
    const body = JSON.stringify({ id: form.elements.id.value, update: fullUpdate });
    const method = 'POST';
    const headers = { 'Content-Type': 'application/json' };
    const credentials = 'include';

    fetch(fontUpdateEndpoint, { body, credentials, headers, method })
        .then(res => res.json())
        .then(handleUpdateResponse)
        .catch(handleUpdateResponse);
};

form.onsubmit = (event) => {
    event.preventDefault();

    if (update['woff-file']) {
        // @ts-ignore
        readFile(form.elements['woff-file'].files[0]).then((result) => {
            delete update['woff-file'];
            // @ts-ignore
            const format = result.fileName.match(/\.([^.]+?$)/)[1];
            send({ ...update, data: result.data, format });
        })
    } else if(Object.getOwnPropertyNames(update).length > 0) {
        send(update);
    }
};
