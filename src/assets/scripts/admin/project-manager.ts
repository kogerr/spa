type Project = { index: number, shortName: string, url: string, pageTitle: string, previewImage: string,
    coverImage: string, _id?: string, details: { specimenPdf: string } };

// @ts-ignore
const projectEndpoint = '/admin/api/projects';
let projects: Project[] = [];
const projectsContainer = document.getElementById('projects-container') as HTMLDivElement;
const projectMessagePanel = document.getElementById('message-panel') as HTMLDivElement;
const projectErrorPanel = document.getElementById('error-panel') as HTMLDivElement;

// @ts-ignore
const displayMessage = (message: string) => {
    projectMessagePanel.innerText = message;
    projectMessagePanel.style.display = 'block';
    setTimeout(() => projectMessagePanel.style.display = 'none', 2000);
};

const sendProjectUpdate = (updatedProjects: Project[]) =>
    fetch(projectEndpoint, { body: JSON.stringify(updatedProjects), method: 'PATCH' })
        .then((response) => response.json() as Promise<Project[]>)
        .then(response => { response.sort((a, b) => a.index - b.index); projects = response; displayProjectPreviews(); })
        .then(() => displayMessage('Updated'))
        .catch(error => { projectErrorPanel.style.display = 'block'; projectErrorPanel.innerText = error });

const swapProjects = (i: number, j: number) => {
    const first = projects[i];
    const second = projects[j];
    if (first && second) {
        first.index = j;
        second.index = i;
        projects.sort((a, b) => a.index - b.index);
        sendProjectUpdate(projects);
    }
};

const removeProject = (project: Project) =>
    fetch(projectEndpoint, {body: JSON.stringify({id: project._id, src: [ project.details.specimenPdf ]}), method: 'DELETE'})
        .then((response) => response.json() as Promise<Project[]>)
        .then(response => { response.sort((a, b) => a.index - b.index); projects = response; displayProjectPreviews(); })
        .catch(error => { projectErrorPanel.style.display = 'block'; projectErrorPanel.innerText = error });

const displayProjectPreviews = () => {
    projectsContainer.innerHTML = '';
    projects.forEach((project, i) => {
        const imageWrapper = document.createElement('div');
        imageWrapper.innerHTML = project.previewImage;
        imageWrapper.className = 'image-wrapper';
        const div = document.createElement('div');
        div.className = 'project-preview';
        const nameLine = document.createElement('div');
        nameLine.innerText = project.shortName;
        const editButton = document.createElement('button');
        editButton.innerText = 'Edit';
        editButton.className = 'link t1';
        editButton.onclick = () => window.location.href = '/admin/project-editor?id=' + project._id;
        const slidesButton = document.createElement('button');
        slidesButton.innerText = 'Slides';
        slidesButton.className = 'link t1';
        slidesButton.onclick = () => window.location.href = '/admin/project-slides?id=' + project._id;
        const fontsButton = document.createElement('button');
        fontsButton.innerText = 'Fonts';
        fontsButton.className = 'link t1';
        fontsButton.onclick = () => window.location.href = '/admin/font-manager?project=' + project.url;
        const removeButton = document.createElement('button');
        removeButton.innerText = 'Remove';
        removeButton.className = 'link t1';
        removeButton.onclick = () => removeProject(project);
        const arrowsRow = document.createElement('div');
        arrowsRow.className = 'arrows-row link h1';
        const leftArrow = document.createElement('span');
        leftArrow.innerText = '←';
        const rightArrow = document.createElement('span');
        rightArrow.innerText = '→';
        arrowsRow.appendChild(leftArrow);
        arrowsRow.appendChild(rightArrow);
        div.appendChild(imageWrapper);
        div.appendChild(nameLine);
        div.appendChild(arrowsRow);
        div.appendChild(editButton);
        div.appendChild(slidesButton);
        div.appendChild(fontsButton);
        div.appendChild(removeButton);
        projectsContainer.appendChild(div);

        rightArrow.onclick = () => swapProjects(i, i + 1);
        leftArrow.onclick = () => swapProjects(i - 1, i);
    })
};

fetch(projectEndpoint, { method: 'GET' })
    .then(response => response.json())
    .then(response => projects = response)
    .then(displayProjectPreviews)
    .catch(console.error);
