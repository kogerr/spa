namespace Sandbox {
    const playground = document.getElementById('tester-playground') as HTMLDivElement;
    const slider = document.getElementById('tester-slider') as HTMLInputElement;
    const resetButton = document.getElementById('reset-button') as HTMLButtonElement;
    const sizeLabel = document.getElementById('size-label') as HTMLLabelElement;
    const fileInput = document.getElementById('file-input') as HTMLInputElement;
    const noLigInput = document.getElementById('no-lig') as HTMLInputElement;
    const alternatesLine = document.getElementById('alternates') as HTMLDivElement;
    const ligaturesLine = document.getElementById('ligatures') as HTMLDivElement;
    const numericLine = document.getElementById('numeric') as HTMLDivElement;
    const otherLine = document.getElementById('other') as HTMLDivElement;
    const defaultSize = 160;
    const alternates: string[] = [ '"aalt"', '"hist"', '"salt" 1', '"salt" 2', '"salt" 3', '"salt" 4', '"ss01"', '"ss02"', '"ss03"', '"cv01"', '"cv02"' ];
    const ligatures: string[] = [ '"liga"', '"clig"', '"dlig"', '"hlig"', '"calt"' ];
    const numeric: string[] = [ '"ordn"', '"zero"', '"lnum"', '"onum"', '"pnum"', '"tnum"', '"frac"', '"afrc"' ];
    const other: string[] = [ '"numr"', '"case"', '"dnom"', '"sinf"', '"sups"' ];
    let currentFeatures: string[] = [];

    slider.oninput = () => {
        const fontSize = slider.value + 'px';
        sizeLabel.innerText = fontSize;
        playground.style.fontSize = fontSize;
        const value = parseInt(slider.value, 10);
        if (value < 23) {
            playground.style.columnCount = '3';
        } else if (value < 33) {
            playground.style.columnCount = '2';
        } else {
            playground.style.columnCount = '1'
        }
    };

    resetButton.onclick = () => {
        const fontSize = defaultSize.toString() + 'px';
        sizeLabel.innerText = fontSize;
        playground.style.fontSize = fontSize;
        playground.style.columnCount = '1';
    };

    const createFontFace = (source: string): Promise<unknown> => {
        const family = 'testfont';
        const descriptors = { style: 'normal', weight: 400 };
        // @ts-ignore
        const font = new FontFace(family, source, descriptors);
        return font.load()
            .then(() => console.log('this still runs'))
            .then(() => (document as any).fonts.add(font));
    };

    const onChange = (event: Event) => {
        event.preventDefault();
        // @ts-ignore
        const webfontFile = fileInput.files[0];
        if (webfontFile) {
            return readFile(webfontFile)
                .then(createFontFace)
                .catch(console.error);
        } else {
            return Promise.reject('no file');
        }
    };

    const readFile = (file: File): Promise<string> => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        return new Promise(resolve => {
            reader.onload = () => {
                const source = 'url(' + (reader.result as string) + ')';
                console.log(source);
                resolve(source);
            };
        });
    };

    fileInput.onchange = onChange;

    noLigInput.onchange = (event) => {
        if (noLigInput.checked) {
            playground.style.fontVariantLigatures = 'none';
        } else {
            playground.style.removeProperty('font-variant-ligatures');
        }
    };

    playground.onpaste = (event) => {
        event.preventDefault();
        const pastedText = event.clipboardData?.getData('text/plain');
        document.execCommand('insertHTML', false, pastedText);
    };

    const addFeatureSetting = (feature: string) => {
        currentFeatures.push(feature);
        playground.style.fontFeatureSettings = currentFeatures.join(', ');
    };

    const removeFeatureSetting = (feature: string) => {
        currentFeatures = currentFeatures.filter(f => f !== feature);
        playground.style.fontFeatureSettings = currentFeatures.join(', ');
    };

    const createFeaturesLine = (features: string[], line: HTMLDivElement) => {
        features.forEach((a) => {
            const input = document.createElement('input');
            input.type = 'checkbox';
            input.id = a;
            const label = document.createElement('label');
            label.innerText = a;
            input.setAttribute('for', a);
            const span = document.createElement('span');
            span.appendChild(input);
            span.appendChild(label);
            line.appendChild(span);
            input.onchange = () => {
                if (input.checked) {
                    addFeatureSetting(a);
                } else {
                    removeFeatureSetting(a);
                }
            };
        });
    }

    const setUp = () => {
        createFeaturesLine(alternates, alternatesLine);
        createFeaturesLine(ligatures, ligaturesLine);
        createFeaturesLine(numeric, numericLine);
        createFeaturesLine(other, otherLine);
    };

    setUp();
}
