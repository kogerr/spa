namespace PriceDetails {
    const familyParameter = new URLSearchParams(window.location.search).get('family');
    let variables = { family: familyParameter, desktop: 0, web: 0, app: 0 };
    const table = document.getElementById('price-table') as HTMLDivElement;
    const saveButton = document.getElementById('save-prices') as HTMLAnchorElement;
    const messagePanel = document.getElementById('message-panel') as HTMLDivElement;
    const errorPanel = document.getElementById('error-panel') as HTMLDivElement;
    const pricesEndpoint = '/admin/api/prices';
    type ResponsePrices = { family: string, desktop?: number, web?: number, app?: number };

    enum Platform {
        desktop = 1,
        web = 2,
        'desktop+web' = 3,
        app = 4,
        'desktop+app' = 5,
        'web+app' = 6,
        'desktop+web+app' = 7
    }

    interface BasePrices {
        desktop: number
        web: number
        app: number
    }

    type Styles = 'singleStyle' | 'singlePair' | 'family';
    type LicenseSize = 1 | 5 | 10 | 25 | 50 | 100 | 250 | 500 | 750;

    interface PriceElement {
        platform: Platform
        licenseSize: LicenseSize
        styles: Styles
        price: number
    }

    interface BasePrices {
        desktop: number
        web: number
        app: number
    }

    const licenseToPower: { size: LicenseSize, power: number }[] = [
        { size: 1, power: 0 },
        { size: 5, power: 1 },
        { size: 10, power: 2 },
        { size: 25, power: 3 },
        { size: 50, power: 4 },
        { size: 100, power: 5 },
        { size: 250, power: 6 },
        { size: 500, power: 7 },
        { size: 750, power: 8 }
    ];

    const displayMessage = (panel: HTMLDivElement, message: string) => {
        panel.style.display = '';
        panel.innerText = message;
        setTimeout(() => panel.style.display = 'none', 2000);
    };

    // tslint:disable:max-line-length
    const calculateOneLicensePrices = (basePrices: BasePrices): PriceElement[] => [
        {platform: Platform.desktop, licenseSize: 1, styles: 'singleStyle', price: basePrices.desktop},
        {platform: Platform.web, licenseSize: 1, styles: 'singleStyle', price: basePrices.web},
        {platform: Platform.app, licenseSize: 1, styles: 'singleStyle', price: basePrices.app},

        {platform: Platform['desktop+web'], licenseSize: 1, styles: 'singleStyle', price: (basePrices.desktop + basePrices.web) * 0.75},
        {platform: Platform['desktop+app'], licenseSize: 1, styles: 'singleStyle', price: (basePrices.desktop + basePrices.app) * 0.75},
        {platform: Platform['web+app'], licenseSize: 1, styles: 'singleStyle', price: (basePrices.web + basePrices.app) * 0.75},
        {platform: Platform['desktop+web+app'], licenseSize: 1, styles: 'singleStyle', price: (basePrices.desktop + basePrices.web + basePrices.app) * 0.75 },
    ];

    const addStyleVariants = (priceElement: PriceElement): PriceElement[] => [
        priceElement,
        { ...priceElement, styles: 'singlePair', price: priceElement.price * 1.5},
        { ...priceElement, styles: 'family', price: priceElement.price * 5}
    ];

    const addAllLicences = (priceElement: PriceElement) =>
        licenseToPower.map(({ size, power }) => scaleToLicence(priceElement, size, power));

    const scaleToLicence = (priceElement: PriceElement, licenseSize: LicenseSize, power: number) => {
        const price = calculateByLicenseSize(priceElement.price, power);

        return {...priceElement, licenseSize, price}
    };

    const calculateByLicenseSize = (price: number, power: number) => Math.round(price * Math.pow(4, power) / Math.pow(3, power));

    const createPriceDiv = (priceElement: PriceElement) => {
        const div = document.createElement('div');
        div.innerText = priceElement.price.toString(10);
        div.title = `${Platform[priceElement.platform]} ${priceElement.styles} license:${priceElement.licenseSize}`;

        return div;
    };

    const createLine = (priceElements: PriceElement[]) => {
        const line = document.createElement('div');
        line.className = 'price-line';
        priceElements.map(pe => createPriceDiv(pe)).forEach(div => line.appendChild(div));

        return line;
    };

    const createLineWithInput = (priceElements: PriceElement[], inputId: string) => {
        const line = document.createElement('div');
        line.className = 'price-line';
        const inputPriceElement = priceElements.splice(0, 1)[0];
        const input = document.createElement('input');
        input.value = inputPriceElement.price.toString(10);
        input.type = 'number';
        input.id = inputId;
        line.appendChild(input);
        priceElements.map(pe => createPriceDiv(pe)).forEach(div => line.appendChild(div));

        return {line, input};
    };

    const changeTable = (basePrices: BasePrices) => {
        const oneLicensePrices = calculateOneLicensePrices(basePrices).flatMap(addStyleVariants);
        for (let i = 0; i < oneLicensePrices.length; i++) {
            const priceWithLicenses = addAllLicences(oneLicensePrices[i]).map(pe => ({...pe, price: Math.round(pe.price)}));
            const line = table.children[i];
            for (let j = 0; j < line.children.length; j++) {
                const element = line.children[j];
                if (element.tagName === 'DIV') {
                    (element as HTMLDivElement).innerText = priceWithLicenses[j].price.toString(10);
                }
            }
        }
    };

    const createTable = (basePrices: BasePrices) => {
        const oneLicensePrices = calculateOneLicensePrices(basePrices).flatMap(addStyleVariants);

        for (let i = 0; i < oneLicensePrices.length; i++) {
            const priceWithLicenses = addAllLicences(oneLicensePrices[i]).map(pe => ({...pe, price: Math.round(pe.price)}));

            if (i === 0 || i === 3 || i === 6) {
                const platform = Platform[oneLicensePrices[i].platform];
                const {line, input} = createLineWithInput(priceWithLicenses, platform);
                input.oninput = (event) => {
                    if (input.value.length > 0) {
                        // @ts-ignore
                        variables[platform] = parseInt(input.value, 10);
                        changeTable(variables);
                    }
                };
                table.appendChild(line);
            } else {
                table.appendChild(createLine(priceWithLicenses));
            }
        }
    };

    const findPrices = (prices: ResponsePrices[]) => {
        if (familyParameter) {
            const values = prices.find(price => price.family === familyParameter);
            if (values && values.desktop && values.web && values.app) {
                // @ts-ignore
                variables = values;
            }
        }

        return variables;
    };

    const getPrices = () => fetch(pricesEndpoint, { method: 'GET' })
        .then(response => (response.json() as Promise<ResponsePrices[]>))
        .then(findPrices)
        .then(createTable)
        .catch((error) => displayMessage(errorPanel, error));

    getPrices();

    saveButton.onclick = (event) => fetch(pricesEndpoint, { method: 'PATCH', body: JSON.stringify(variables) })
        .then(response => (response.json() as Promise<ResponsePrices[]>))
        .then(findPrices)
        .then(changeTable)
        .then(() => displayMessage(messagePanel, 'save successful'))
        .catch((error) => displayMessage(errorPanel, error));

}
