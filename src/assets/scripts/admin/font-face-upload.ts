namespace Upload {
    type UploadResponse = { modifiedCount?: number, result: { ok: number } };
    const projectUrl = new URLSearchParams(window.location.search).get('project');
    const index = new URLSearchParams(window.location.search).get('index') as string;
    const targetEndpoint = '/admin/api/fonts';
    const form = document.getElementsByTagName('form')[0] as HTMLFormElement;

    const displaySuccess = (response: UploadResponse) => {
        if (response.result.ok && response.modifiedCount === 1) {
            const div = document.createElement('div');
            div.innerText = 'Successful Upload';
            div.style.padding = '16px';
            div.style.backgroundColor = '#dfe';
            div.style.opacity = '1';
            div.style.transition = 'opacity 1s';
            form.appendChild(div);
            setTimeout(() => document.location.href = '/admin/font-manager?project=' + projectUrl, 1000);
        } else {
            console.error(response);
        }
    };

    const onSubmit = (event: Event) => {
        event.preventDefault();
        const webfontFile = form['woff-file'].files[0];
        const fullFontsInput = form['full-fonts'].files;
        const fullFontFiles: File[] = [];
        for (let i = 0; i < fullFontsInput.length; i++) {
            fullFontFiles.push(fullFontsInput[i]);
        }

        if (webfontFile) {
            return readFile(webfontFile)
                .then(woffFile => Promise.all(fullFontFiles.map(file => readFile(file))).then(fullFiles => ({ woffFile, fullFiles })))
                .then(files => createInit(files.woffFile, files.fullFiles))
                .then(init => fetch(targetEndpoint, init))
                .then(response => response.json() as unknown as UploadResponse)
                .then(displaySuccess)
                .then(() => form.reset())
                .catch(console.error);
        } else {
            return Promise.reject('no file');
        }
    };

    const readFile = (file: File): Promise<{ data: string, filename: string }> => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        return new Promise(resolve => {
            reader.onload = () => {
                const data = (reader.result as string).replace(/data:.+?;base64,/, '');
                const filename = file.name;
                resolve({ data, filename });
            };
        });
    };

    const createInit = (webfont: { data: string, filename: string }, fullFonts: { data: string, filename: string }[]) => {
        const family = form.family.value;
        const italic = (form.italic as any).checked;
        const weight = parseInt(form.weight.value, 10);
        const fontFace = { projectUrl, index: parseInt(index, 10), family, weight, italic, webfont, fullFonts };
        const body = JSON.stringify(fontFace);
        const method = 'POST';
        const headers = { 'Content-Type': 'application/json' };
        return { method, headers, body };
    };

    form.onsubmit = onSubmit;
}
