namespace Prices {
    type BasePrice = { family: string, desktop?: number, web?: number, app?: number };
    const pricesEndpoint = '/admin/api/prices';
    const table = document.getElementById('prices-table') as HTMLDivElement;
    const errorPanel = document.getElementById('error-panel') as HTMLDivElement;

    const displayMessage = (error: any) => {
        errorPanel.style.display = '';
        errorPanel.innerText = error;
        setTimeout(() => errorPanel.style.display = 'none', 2000);
    };

    const switchButton = (button: HTMLButtonElement, lineData: BasePrice) => {
        if (lineData.app && lineData.web && lineData.app) {
            button.style.display = '';
        } else {
            button.style.display = 'none';
        }
    };

    const createLine = (price: BasePrice) => {
        const lineData: BasePrice = { ...price };
        const line = document.createElement('div');
        line.className = 'line';
        const name = document.createElement('div');
        name.innerText = price.family;
        name.className = 'price-name t1';
        const desktopInput = document.createElement('input');
        desktopInput.type = 'number';
        desktopInput.id = price.family + '-desktop';
        const webInput = document.createElement('input');
        webInput.type = 'number';
        webInput.id = price.family + '-web';
        const appInput = document.createElement('input');
        appInput.type = 'number';
        appInput.id = price.family + '-app';
        const detailsButton = document.createElement('button');
        detailsButton.innerText = 'details';
        detailsButton.onclick = () => window.location.href = '/admin/price-details?family=' + lineData.family;
        detailsButton.className = 'link t1';
        const removeButton = document.createElement('button');
        removeButton.innerText = 'remove';
        removeButton.className = 'link t1';
        const updateButton = document.createElement('button');
        updateButton.innerText = 'update';
        updateButton.className = 'link t1';
        updateButton.style.display = 'none';
        switchButton(removeButton, lineData);

        if (price.web && price.app && price.desktop) {
            desktopInput.value = price.desktop.toString();
            webInput.value = price.web.toString();
            appInput.value = price.app.toString();
        }

        desktopInput.oninput = (event) => {
            if (desktopInput.value.length > 0) {
                lineData.desktop = parseInt(desktopInput.value, 10);
            } else {
                delete lineData.desktop;
            }
            switchButton(updateButton, lineData);
        };
        webInput.oninput = (event) => {
            if (desktopInput.value.length > 0) {
                lineData.web = parseInt(webInput.value, 10);
            } else {
                delete lineData.web;
            }
            switchButton(updateButton, lineData);
        };
        appInput.oninput = (event) => {
            if (desktopInput.value.length > 0) {
                lineData.app = parseInt(appInput.value, 10);
            } else {
                delete lineData.app;
            }
            switchButton(updateButton, lineData);
        };

        removeButton.onclick = (event) => {
            const body = JSON.stringify({ family: price.family });
            const method = 'DELETE';
            const headers = { 'Content-Type': 'application/json' };
            fetch(pricesEndpoint, { body, method, headers }).then(processPrices).catch(displayMessage);
        };

        updateButton.onclick = (event) => {
            const body = JSON.stringify(lineData);
            const method = 'PATCH';
            const headers = { 'Content-Type': 'application/json' };
            fetch(pricesEndpoint, { body, method, headers }).then(processPrices).catch(displayMessage);
        };

        line.append(name, desktopInput, webInput, appInput, detailsButton, removeButton, updateButton);

        return line;
    };

    const fillTable = (prices: BasePrice[]) => {
        table.innerHTML = '';
        prices.map(price => createLine(price)).forEach(line => table.appendChild(line));
    };

    const processPrices = (response: Response) => (response.json() as Promise<BasePrice[]>)
        .then(fillTable);

    const getPrices = () => fetch(pricesEndpoint, { method: 'GET' })
        .then(processPrices);

    getPrices()
        .catch(displayMessage);
}
