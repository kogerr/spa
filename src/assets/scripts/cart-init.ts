import { updateOnChange } from './cart-update';
import { initState } from './state-handler';

export const initializeCart = () => {
    initState();
    updateOnChange();
};
