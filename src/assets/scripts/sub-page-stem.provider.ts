import { landingInit } from './landing';
import { SubPageStem } from './sub-page-stem.interface';
import { initPhilosophy } from './philosophy';
import { initNewsFeed } from './newsfeed';
import { initLogin } from './login';
import { initCheckout } from './checkout';
import { initPaymentResult } from './payment-result';
import { initFontPage } from './init-font-page';
import { initCartPage } from './init-cart-page';

export const getSubpageStems = (): SubPageStem[] => [
    { pathMatcher: /^\/$/, title: 'koger.io', init: landingInit },
    { pathMatcher: /^\/philosophy$/, title: 'Philosophy', init: initPhilosophy },
    { pathMatcher: /^\/newsfeed$/, title: 'Newsfeed', init: initNewsFeed },
    { pathMatcher: /^\/login.?/, title: 'Login', init: initLogin },
    { pathMatcher: /^\/registration$/, title: 'Registration', init: initLogin },
    { pathMatcher: /^\/checkout$/, title: 'Checkout', init: initCheckout },
    { pathMatcher: /^\/payment-result.?/, title: 'Payment Result', init: initPaymentResult },
    { pathMatcher: /^\/fonts\/.+/, title: 'font', init: initFontPage },
    { pathMatcher: /^\/cart$/, title: 'cart', init: initCartPage },
];
