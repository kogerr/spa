namespace EmailEdge {
    const form = document.getElementsByTagName('form')[0] as HTMLFormElement;
    const toField = document.getElementById('to') as HTMLInputElement;
    const nameField = document.getElementById('name') as HTMLInputElement;
    const targetEndpoint = '/admin/api/email';

    const displayError = (error: string, cssClass: string) => {
        const div = document.createElement('div');
        div.innerText = error;
        div.setAttribute('class', cssClass);
        form.appendChild(div);
        window.setTimeout(() => {
            form.removeChild(div);
        }, 2000);
    };

    const responseHandler = (response: Response) => {
        response.json().then((payload: { success: boolean, error?: string}) => {
            if (response.status === 200) {
                displayError('Success!', 'success response');
            } else if (payload.error) {
                displayError(payload.error, 'error response');
            }
        });
    };

    form.addEventListener('submit', (event: Event) => {
        event.preventDefault();
        const body = JSON.stringify({ to: toField.value, name: nameField.value });
        const method = 'POST';
        const headers = { 'Content-Type': 'application/json' };
        const init = { method, headers, body };
        fetch(targetEndpoint, init)
            .then(responseHandler, console.error)
            .then(() => form.reset());
    });
}
