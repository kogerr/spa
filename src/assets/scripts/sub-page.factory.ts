import { SubPage } from './subpage.interface';
import { SubPageStem } from './sub-page-stem.interface';

interface Page {
    main: string;
    metadata?: string;
    path: string;
    scripts: string[];
    title: string;
    styles: string[];
}

export const getSubPages: (subPagesStems: SubPageStem[]) => Promise<SubPage[]> = (subPageStems: SubPageStem[]) => {
    const main = document.getElementsByTagName('main')[0];

    return fetch('/api/pages')
        .then(response => response.json())
        .then(response => subPageStems.map(stem => complimentSubPage(stem, response, main)));
};

const complimentSubPage = (stem: SubPageStem, response: Page[], main: HTMLElement) => {
    const correspondingPage = response.find(page => stem.pathMatcher.test(page.path));

    const setUp = correspondingPage ? () => { main.innerHTML = correspondingPage.main; scroll(0, 0); } : () => main.innerHTML = '';
    const path = correspondingPage ? correspondingPage.path : '';
    const title = correspondingPage ? correspondingPage.title : stem.title;

    return { pathMatcher: stem.pathMatcher, path, title, init: stem.init, setUp };
};
