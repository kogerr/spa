import { clearObjects } from './state-handler';

export const initPaymentResult = () => {
    const match = window.location.search.match(/r=(.+?)&/);
    if (match && match[1] && JSON.parse(atob(decodeURIComponent(match[1]))).r === 0) {
        clearObjects('products');
        clearObjects('items');
    }
};
