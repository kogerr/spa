const targetEndpoint = '/admin/api/upload';
const firstFileInput = document.getElementById('first-file') as HTMLInputElement;
const secondFileInput = document.getElementById('second-file') as HTMLInputElement;

const uploadOnChange = (event: Event) => {
    const element = (event.target as HTMLInputElement);
    if (element && element.files && element.files.length) {
        return upload(element.files[0]);
    } else {
        return Promise.resolve();
    }
};

const upload = (file: File, overwrite = false) =>
    postFile(file, overwrite)
        .then((response) => handleResponse(response, file))
        .catch(console.error);

const postFile = (file: File, overwrite = false): Promise<Response> => {
    const method = 'POST';
    const base64 = file.name.endsWith('.woff') || file.name.endsWith('.woff2') ? 'true' : 'false';
    const headers = {
        'filename': file.name,
        'Content-Length': file.size.toString(10),
        base64,
        'overwrite': overwrite.toString()
    };
    const init = { method, headers, body: file };

    return fetch(targetEndpoint, init);
};

const handleResponse = (response: Response, file: File): undefined | Promise<unknown> => {
    if (response.status === 409) {
        if (confirm('File already exists. Do you wish to overwrite?')) {
            return upload(file, true).then(console.log).catch(console.error);
        }
    } else if (response.status === 200) {
        return response.json().then(showFile);
    } else {
        return response.json().then(console.error);
    }
};

const showFile = (responseBody: { success: boolean, path: string }) => {
    const anchor: HTMLAnchorElement = document.createElement('a');
    anchor.href = responseBody.path;
    anchor.target = 'blank';
    anchor.innerText = 'Uploaded File';
    document.body.appendChild(anchor);
    const img = document.createElement('img');
    img.src = responseBody.path;
    document.body.appendChild(img);
};

firstFileInput.onchange = uploadOnChange;
secondFileInput.onchange = uploadOnChange;
