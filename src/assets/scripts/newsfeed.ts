const events: (keyof GlobalEventHandlersEventMap)[] = [
    'click', 'mouseenter', 'mouseleave', 'touchstart', 'touchend', 'touchmove', 'mousedown'
];

const addEvent = (display: HTMLDivElement, event: Event) => {
    const span = document.createElement('span');
    span.innerHTML = event.type;
    display.appendChild(span);
}

export const initNewsFeed = () => {
    const li = document.getElementById('list-item') as HTMLLIElement;
    const display = document.getElementById('event-display') as HTMLDivElement;
    const button = document.getElementById('clear-button') as HTMLInputElement;
    events.forEach((eventType) => {
        li.addEventListener(eventType, (event) => addEvent(display, event));
    });
    button.onmousedown = (event) => {
        event.preventDefault();
        display.innerHTML = '';
    };
};
