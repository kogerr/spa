import { db } from './mongo';
import { BasePrices } from '../models/base-prices.interface';
import { WithId } from 'mongodb';

const getCollection = () => db.collection<BasePrices>('basePrices');

export const getBasePrices = () => getCollection().find({}, { projection: { _id: false }}).toArray();

export const getBasePricesByFamily = (family: string) => getCollection().findOne({ family });

export const getBasePricesWithId = () => getCollection().find({}).toArray() as Promise<WithId<BasePrices>[]>;

export const removeBasePrices = (family: string) => getCollection().deleteOne({ family });

export const addBasePrices = (basePrices: BasePrices) => getCollection().insertOne(basePrices);

export const upsertBasePrices = ({ family, ...$set }: BasePrices) =>
    getCollection().findOneAndUpdate({ family }, { $set }, { upsert: true });
