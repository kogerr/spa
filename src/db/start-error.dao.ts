import { StartRequest } from '../models/barion/start-request.interface';
import { ErrorStartResponse } from '../models/barion/start-response.interface';
import { db } from './mongo';
import { StartError } from '../models/start-error.interface';

const getCollection = () => db.collection<StartError>('startErrors');

export const addError = (startRequest: StartRequest, startResponse: ErrorStartResponse) =>
    getCollection().insertOne({ startRequest, startResponse });
