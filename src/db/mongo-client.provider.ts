import { MongoClient } from 'mongodb';

const host = process.env.MONGO_HOST || 'localhost';
const port = 27017;
const uri = `mongodb://${host}:${port}`;
const options = { useNewUrlParser: true, useUnifiedTopology: true };

export const provideMongoClient = () => new MongoClient(uri, options);
