import { FontFace } from '../models/font-face.interface';
import { db } from './mongo';

const getCollection = () => db.collection<FontFace>('fontFaces');

export const getFontFaces = () => getCollection().find({}, { projection: { _id: false }}).toArray();
