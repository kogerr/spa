import { SmtpProperties } from '../models/smtp-properties.interface';
import { db } from './mongo';

const getCollection = () => db.collection<SmtpProperties>('smtpProperties');

export const getSmtpProperties = () => getCollection().findOne({});

export const updateSmtpProperties = (host: string, from: string, password: string) => {
    const credentials = createCredentials(from, password);

    return getCollection().findOneAndReplace({}, { host, from, credentials });
};

const createCredentials = (email: string, password: string) =>
    Buffer.from('\0' + email + '\0' + password)
        .toString('base64');
