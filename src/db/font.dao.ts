import { Font } from '../models/font.interface';
import { db } from './mongo';
import { ObjectId } from 'mongodb';

const getCollection = () => db.collection<Font>('fonts');

export const insertFont = (font: Font) => getCollection().insertOne(font).then(result => result.insertedId.toHexString());

export const getFontById = (id: string) => getCollection()
    .findOne({ _id: new ObjectId(id) });

export const removeFontById = (id: string) => getCollection().deleteOne({ _id: new ObjectId(id) });

export const getFontFamilies = () => getCollection()
    .find().map(font => font.family).toArray();

export const findFont = (family: string, weight: number, italic: boolean) => getCollection()
    .findOne({ family, weight, italic });

