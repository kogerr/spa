import { InsertOneWriteOpResult } from 'mongodb';
import { User } from '../models/user.interface';
import { db } from './mongo';

const getCollection = () => db.collection<User>('users');

export const checkUser = (email: string, password: string) => getCollection().findOne({ email, password })
    .then((value) => value !== null);

export const checkEmail = (email: string) => getCollection().findOne({ email })
    .then((value) => value !== null);

export const registerUser = (email: string, password: string): Promise<User> =>
    checkUser(email, password)
        .then(errorIfFound)
        .then(() => saveUser(email, password))
        .then((result) => mapInsertResult(result, { email, password }));

const errorIfFound = (isFound: boolean) => {
    if (isFound) {
        throw new Error('User already exists');
    }
};

const saveUser = (email: string, password: string) => getCollection().insertOne({ email, password } as User);

const mapInsertResult = (result: InsertOneWriteOpResult<{ _id: any }>, user: User) => {
    if (result.insertedCount === 1 && result.result.ok === 1) {
        return user;
    } else {
        throw new Error(JSON.stringify(result));
    }
};
