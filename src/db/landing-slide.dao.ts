import { db } from './mongo';
import { ObjectId, WithId } from 'mongodb';
import { LandingSlide } from '../models/landing-slide.interface';

const getCollection = () => db.collection<LandingSlide>('landingSlides');

export const getLandingSlides = () => getCollection().find({}, { projection: { _id: false }}).toArray();

export const getLandingSlidesWithId = () => getCollection().find({}).toArray() as Promise<WithId<LandingSlide>[]>;

export const removeLandingSlide = (id: string) => getCollection().deleteOne({ _id: new ObjectId(id) });

export const addLandingSlide = (slide: LandingSlide) => getCollection().insertOne(slide);

export const updateLandingSlides = (slides: WithId<LandingSlide>[]) => Promise.all(slides.map(updateSlide));

const updateSlide = ({ _id, ...$set }: WithId<LandingSlide>) => getCollection()
    .findOneAndUpdate({ _id: new ObjectId(_id) }, { $set });
