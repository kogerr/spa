import { db } from './mongo';
import { Order } from '../models/order.interface';
import { UpdateQuery } from 'mongodb';

const getCollection = () => db.collection<Order>('orders');

export const addOrder = (order: Order) => getCollection().insertOne(order);

export const getOrders = () => getCollection().find().toArray();

export const getOrderByOrderId = (orderId: string) => getCollection().findOne({ orderId });

export const updateOrder = (orderId: string, status: string, updated: number) => {
    const filter = { orderId };
    const update: UpdateQuery<Order> = { $set: { status, updated } };

    return getCollection().findOneAndUpdate(filter, update);
};
