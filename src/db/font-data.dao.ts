import { db } from './mongo';
import { ObjectId } from 'mongodb';

const getCollection = () => db.collection<{ data: string }>('fontData');

export const insertFontData = (data: string) => getCollection().insertOne({ data }).then(result => result.insertedId.toHexString());

export const getFontDataById = (id: string) => getCollection()
    .findOne({ _id: new ObjectId(id) }).then(result => result?.data);

export const removeFontDataById = (id: string) => getCollection().deleteOne({ id });
