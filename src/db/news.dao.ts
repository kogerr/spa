import { NewsPost } from '../models/news-post.interface';
import { db } from './mongo';

const getCollection = () => db.collection<NewsPost>('news-posts');

export const getNews = () => getCollection().find().toArray();
