import { Project } from '../models/project/project.interface';
import { db } from './mongo';
import { FindOneOptions } from 'mongodb';

const getCollection = () => db.collection<Project>('projects');

export const addFontToProject = (url: string, font: { index: number, id: string }) => getCollection()
    .updateOne( { url }, { $push: { fonts: font } } );

const fontsOptions = { fields: { fonts: true } } as any as FindOneOptions<Project>;
export const getProjectFontsByUrl = (url: string) => getCollection()
    .findOne({ url }, fontsOptions)
    .then(p => p?.fonts as {index: number, id: string}[]);

export const updateProjectFonts = (url: string, fonts: { index: number, id: string }[]) => getCollection()
    .findOneAndUpdate({ url }, { $set: { fonts }});

export const removeProjectFonts = (url: string, id: string) => getCollection()
    .findOneAndUpdate({ url }, { $pull: { fonts: { id } }});
