import { Db, MongoClient, MongoError } from 'mongodb';
import { provideMongoClient } from './mongo-client.provider';

const dbName = 'spa';
const retries = 3;

export let db: Db = {} as Db;

const mongoConnectWithRetry = (client: MongoClient, retryInterval: number, tries = 0) => {
    return new Promise<Db>((resolve, reject) => {
        client.connect((error: MongoError, result: MongoClient) => {
            if (error) {
                console.error(error);
                if (tries < retries) {
                    setTimeout(() => mongoConnectWithRetry(client, retryInterval, tries + 1).then(resolve).catch(reject), retryInterval);
                } else {
                    reject(error);
                }
            } else {
                console.log('Connected to MongoDB');
                resolve(result.db(dbName));
            }
        });
    });
};

export const initDb = (retryInterval = 10000) => mongoConnectWithRetry(provideMongoClient(), retryInterval)
    .then((database: Db) => db = database);
