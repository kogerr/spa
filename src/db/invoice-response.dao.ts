import { db } from './mongo';
import { InvoiceResponse } from '../models/invoice-response.interface';

type InvoiceWithId = { orderId: string } & InvoiceResponse;

const getCollection = () => db.collection<InvoiceResponse>('invoiceResponse');

export const getInvoiceResponses = () => getCollection().find({}, { projection: { _id: false }}).toArray();

export const addInvoiceResponse = (invoiceResponse: InvoiceWithId) => getCollection().insertOne(invoiceResponse);
