import { db } from './mongo';
import { Project } from '../models/project/project.interface';
import { FindOneOptions, ObjectId, WithId } from 'mongodb';
import { IndexedSlide } from '../models/project/indexed-slide.interface';

const getCollection = () => db.collection<Project>('projects');

export const getProjects = () => getCollection().find({}, { projection: { _id: false }}).toArray();

export const getProjectsWithId = () => getCollection().find({}).toArray() as Promise<WithId<Project>[]>;

const previewImageOptions = { fields: { previewImage: true, _id: false, index: true, url: true } } as any as FindOneOptions<Project>;
export const getPreviewImages = () => getCollection()
    .find({}, previewImageOptions)
    .toArray() as Promise<{ previewImage: string, index: number, url: string }[]>;

const hoverImageOptions = { fields: { previewHover: true, _id: false, index: true } } as any as FindOneOptions<Project>;
export const getHoverImages = () => getCollection()
    .find({}, hoverImageOptions)
    .toArray() as Promise<{ previewHover: string, index: number }[]>;

export const removeProject = (id: string) => getCollection().deleteOne({ _id: new ObjectId(id) });

export const addProject = (project: Project) => getCollection().insertOne(project);

export const updateProjects = (projects: WithId<Project>[]) => Promise.all(projects.map(updateProject));

const updateProject = ({ _id, ...$set }: WithId<Project>) => getCollection().findOneAndUpdate({ _id: new ObjectId(_id) }, { $set });

const slidesOptions = { fields: { slides: true } } as any as FindOneOptions<Project>;
export const getProjectSlides = (id: string) => getCollection()
    .findOne({ _id: new ObjectId(id) }, slidesOptions)
    .then(p => p?.slides);

export const getProjectSlidesByUrl = (url: string) => getCollection()
    .findOne({ url }, slidesOptions)
    .then(p => p?.slides);

export const updateProjectSlides = (id: string, slides: IndexedSlide[]) => getCollection()
    .findOneAndUpdate({ _id: new ObjectId(id) }, { $set: { slides }});
