export interface SmtpProperties {
    credentials: string;
    from: string;
    host: string;
}
