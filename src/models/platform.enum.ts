export enum Platform {
    desktop = 1,
    web = 2,
    'desktop+web' = 3,
    app = 4,
    'desktop+app' = 5,
    'web+app' = 6,
    'desktop+web+app' = 7
}
