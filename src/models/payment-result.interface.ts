export interface RawPaymentResult {
    r: number;
    t: number;
    e: string;
    m: string;
    o: string;
}

export interface PaymentResult {
    result: number;
    transactionId: number;
    event: string;
    merchant: string;
    orderId: string;
}
