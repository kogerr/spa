export interface EmailRequest {
    to: string;
    name: string;
}
