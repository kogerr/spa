import { Platform } from './platform.enum';

export interface PriceBlock {
    platform: Platform
    licenseSize: number
    singleStyle: number,
    singlePair: number,
    family: number
}
