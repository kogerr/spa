export interface FontData {
    id: string;
    family: string;
    source: string;
    descriptors: {
        weight: number;
        style: string
    }
    public: boolean;
    price?: number;
}
