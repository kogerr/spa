import { StartRequest } from './barion/start-request.interface';
import { ErrorStartResponse } from './barion/start-response.interface';

export interface StartError {
    startRequest: StartRequest,
    startResponse: ErrorStartResponse
}
