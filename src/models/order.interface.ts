import { Customer } from './customer.interface';
import { Item } from './cart-item.interface';
import { SimpleProduct } from './simple-product.interface';

export interface Order {
    orderId: string;
    paymentId: string;
    customer: Customer
    items: Item[]
    products: SimpleProduct[];
    status: string;
    updated: number;
    total: number;
}
