export interface CheckoutFormPayload {
    name: string;
    email: string;
    phone: string;
    organization?: string;
    address: string;
    city: string;
    zip: string;
    country: string;
    state?: string;
    vat?: string;
    someoneElse: 'true' | 'false';
    products: string;
    items: string;
    total: string;
    licenseOrganization?: string;
    licenseName?: string;
    domain?: string;
    subtotal: string;
    vatValue: string;
}
