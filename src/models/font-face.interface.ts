export type FontFace = PublicFontFace | PrivateFontFace;

export interface PublicFontFace {
    id: string;
    family: string;
    style: 'normal' | 'italic';
    weight: number;
    data: string;
    public: true;
    price: number;
    format: string;
}

export interface PrivateFontFace {
    id: string;
    family: string;
    style: 'normal' | 'italic';
    weight: number;
    data: string;
    public: false;
    format: string;
}
