export interface Credentials {
    key: string;
    cert: string;
    ca: string;
}
