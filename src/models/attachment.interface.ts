export interface BufferAttachment {
    filename: string;
    contentType: string;
    content: Buffer;
}

export interface Base64Attachment {
    filename: string;
    contentType: string;
    content: string;
}
