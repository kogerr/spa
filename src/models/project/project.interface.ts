import { ProjectDetails } from './project-details.interface';
import { IndexedSlide } from './indexed-slide.interface';
import { Font } from '../font.interface';

export interface Project {
    index: number;
    shortName: string;
    url: string;
    pageTitle: string;
    previewImage: string;
    previewHover: string;
    coverImage: string;
    details: ProjectDetails;
    metaImage?: string;
    slides: IndexedSlide[];
    fonts: { index: number, id: string }[];
}
