export interface IndexedSlide {
    index: number;
    src: string;
}
