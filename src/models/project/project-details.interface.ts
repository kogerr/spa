export interface ProjectDetails {
    description: string;
    languages: string;
    release: string;
    version: string;
    formats: string;
    specimenPdf: string;
}
