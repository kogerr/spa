export interface Asset {
    filename: string;
    content: string;
    contentType: string;
    lastModified: Date;
    eTag: string;
    hidden: boolean;
}
