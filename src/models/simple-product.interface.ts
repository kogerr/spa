export interface SimpleProduct {
    title: string;
    price: number;
}
