import { InvoiceItem } from './invoice-item.interface';

export interface InvoiceContext {
    settings: {
        szamlaagentkulcs: string;
        eszamla: boolean;
        szamlaLetoltes: boolean;
        valaszVerzio: number;
    };
    header: {
        date: string;
        fizmod: string;
        penznem: string;
        szamlaNyelve: 'en' | 'hu';
    };
    seller: {
        bank?: string;
        bankszamlaszam?: string;
        emailReplyto?: string;
        emailTargy?: string;
        emailSzoveg?: string;
    };
    buyer: {
        nev: string;
        irsz: string;
        telepules: string;
        cim: string;
        email?: string;
        sendEmail?: boolean;
        orszag?: string;
        adoszam?: string;
        adoszamEU?: string;
    };
    items: InvoiceItem[]
}
