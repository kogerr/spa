export interface BasePrices {
    family: string
    desktop: number
    web: number
    app: number
}
