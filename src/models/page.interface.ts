export interface Page {
    main: string;
    metadata?: string;
    path: string;
    scripts: string[];
    styles: string[];
    title: string;
}
