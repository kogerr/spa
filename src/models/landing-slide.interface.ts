export interface LandingSlide {
    index: number;
    src: string;
    link: string;
    alt?: string;
}
