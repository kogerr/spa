export interface NewsPost {
    type: 'text' | 'image' | 'video';
    date: number;
    text: string;
}
