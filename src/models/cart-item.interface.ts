export interface Item {
    family: string;
    license: number;
    platform: number;
    weight: number;
    style: string;
    price: number;
}
