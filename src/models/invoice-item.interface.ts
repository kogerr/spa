export interface InvoiceItem {
    megnevezes: string;
    mennyiseg: number;
    mennyisegiEgyseg: string;
    nettoEgysegar: number;
    afakulcs: number;
    nettoErtek: number;
    afaErtek: number;
    bruttoErtek: number;
}
