export interface DatedTemplate {
    date: number;
    template: string;
}
