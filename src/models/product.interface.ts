export interface Product {
    family: string;
    license: number;
    platform: number;
    bundle: string;
    weight?: number;
    style?: string;
    price: number;
}
