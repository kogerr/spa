export interface InvoiceResponse {
    sikeres: boolean;
    hibakod?: string;
    hibauzenet?: string;
    szamlaszam?: string;
    szamlanetto?: number;
    szamlabrutto?: number;
    kintlevoseg?: number;
    vevoifiokurl?: string;
    pdf?: string;
}
