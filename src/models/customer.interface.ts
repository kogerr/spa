export interface Customer {
    name: string;
    email: string;
    country: string;
    zip: string;
    city: string;
    state?: string;
    address: string;
    phone: string;
    vat?: string;
    domain?: string;
}
