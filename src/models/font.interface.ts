export interface Font {
    family: string;
    weight: number;
    italic: boolean;
    webfont: { filename: string, data: string };
    fullFonts: { filename: string, data: string }[];
}
