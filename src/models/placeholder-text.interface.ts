export interface PlaceholderText {
    index: number;
    text: string;
}
