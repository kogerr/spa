export type StartResponse = SuccessStartResponse | ErrorStartResponse;

export interface SuccessStartResponse {
    PaymentId: string;
    PaymentRequestId: string;
    Status: string;
    QRUrl: string;
    Transactions: [
        {
            POSTransactionId: string;
            TransactionId: string;
            Status: string;
            Currency: string;
            TransactionTime: string;
            RelatedId: null
        }
    ],
    RecurrenceResult: string;
    ThreeDSAuthClientData: null,
    GatewayUrl: string;
    RedirectUrl: string;
    CallbackUrl: string;
    TraceId?: unknown;
    Errors: StartError[];
}

export interface ErrorStartResponse {
    Errors: StartError[];
}

export interface StartError {
    Title: string;
    Description: string;
    ErrorCode: string;
    HappenedAt: string;
    AuthData: string;
    EndPoint: string;
}
