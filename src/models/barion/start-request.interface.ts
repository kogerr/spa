import { PaymentTransaction } from './payment-transaction.interface';
import { Address } from './address.interface';

export interface StartRequest {
    POSKey: string; // UUID
    PaymentType: string;
    GuestCheckOut: boolean;
    FundingSources:	FundingSource[];
    PaymentRequestId: string;
    PayerHint: string;
    CardHolderNameHint: string;
    RedirectUrl: string;
    CallbackUrl: string;
    Transactions: PaymentTransaction[];
    OrderNumber: string;
    ShippingAddress: Address;
    Locale: 'cs-CZ' | 'de-DE' | 'en-US' | 'es-ES' | 'fr-FR' | 'hu-HU' | 'sk-SK' | 'sl-SI';
    Currency: 'EUR' | 'HUF' | 'USD' | 'CZK';
    PayerPhoneNumber: string;
    PayerWorkPhoneNumber: string; // really?
    PayerHomeNumber: string; // really?
    BillingAddress: Address;
    PayerAccountInformation: {};
    PurchaseInformation: {};
    ChallengePreference: 0 | 10 | 20;
}

export type FundingSource = 'All' | 'Balance' | 'BankCard' | 'GooglePay' | 'ApplePay' | 'BankTransfer';

