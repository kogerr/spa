export interface TransactionItem {
    Name: string;
    Description: string;
    Quantity: number;
    Unit: string;
    UnitPrice: number;
    ItemTotal: number;
}
