import { TransactionItem } from './transaction-item.interface';

export interface PaymentTransaction {
    POSTransactionId: string;
    Payee: string;
    Total: number;
    Items: TransactionItem[];
}
