export interface Address {
    Country: string;
    City?: string;
    Region?: string;
    Zip?: string;
    Street?: string;
    FullName?: string;
}
