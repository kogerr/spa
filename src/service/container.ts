import { Asset } from '../models/asset.interface';

export const templates: Map<string, string> = new Map<string, string>();
export const assets: Map<string, Asset> = new Map();
export const hiddenAssets: Map<string, Asset> = new Map();
export const fullPages: Map<string, string> = new Map<string, string>();
export const api: Map<string, string> = new Map<string, any>();
