import { createServer, ServerOptions } from 'http';
import { createSecureServer, SecureServerOptions } from 'http2';
import { Credentials } from '../../models/credentials.interface';
import { RequestHandler } from '../../models/types';
import { readCredentials } from './credential.provider';
import { httpsRedirect } from './https-redirect.provider';
import { route } from './routing';
import { prePushAssets } from './prepush.handler';

const httpPort = 80;
const httpsPort = 443;

let credentials: Credentials | undefined;

const listenOnHttp2 = (requestListener: RequestHandler, credentialOptions: SecureServerOptions) => {
    const options: SecureServerOptions = { allowHTTP1: true, ...credentialOptions };
    const server = createSecureServer(options, requestListener);
    server.listen(httpsPort, () => {
        console.log(`Listening on port ${httpsPort}`);
    });
    server.on('error', console.error);
    server.on('stream', prePushAssets);
};

const listenOnHttp = (requestListener: RequestHandler, options: ServerOptions = {}) => {
    const server = createServer(options, requestListener);
    server.listen(httpPort, () => {
        console.log(`Listening on port ${httpPort}`);
    });
    server.on('error', console.error);
};

export const getServer = (loadedCredentials: Credentials | undefined) => (requestHandler: RequestHandler) => {
    let httpRequestHandler: RequestHandler;
    credentials = loadedCredentials;

    if (credentials !== undefined) {
        httpRequestHandler = httpsRedirect;
        listenOnHttp2(requestHandler, credentials);
    } else {
        httpRequestHandler = requestHandler;
    }

    listenOnHttp(httpRequestHandler);
};

export const initServer = () => readCredentials().then(getServer).then((server) => server(route));

export const getCredentials = () => credentials;
