import { ServerHttp2Stream } from 'http2';
import { assets } from '../container';
import { Asset } from '../../models/asset.interface';

const hash = process.env.CI_COMMIT_SHORT_SHA ?? '';
const pushableAssets = [ `/styles/common${hash}.css`, `/scripts/script${hash}.js` ];

const push = (stream: ServerHttp2Stream, path: string) => {
    stream.pushStream({ ':path': path }, (error: Error | null, pushStream: ServerHttp2Stream, headers) => {
        if (error) {
            console.error(error);
        }
        const asset = assets.get(path) as Asset;
        pushStream.respond({ ':status': 200, 'Content-Encoding': 'gzip', 'Content-Type': asset.contentType,
            'Last-Modified': asset.lastModified.toUTCString(), 'ETag': asset.eTag, 'Cache-Control': 'max-age=31536000' });
        pushStream.end(asset.content, 'binary');
    });
};

export const prePushAssets = (stream: ServerHttp2Stream) => {
    pushableAssets.forEach((path: string) => push(stream, path));
};
