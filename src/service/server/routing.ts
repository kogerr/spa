import { IncomingMessage, ServerResponse } from 'http';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { RequestHandler } from '../../models/types';
import { redirectIfUnauthorized } from './atuh.handler';
import { api, assets, fullPages } from '../container';
import { controllerRoutes } from '../controllers/controller-mapping';
import { mimeTypes } from '../providers/mime-types';
import { fileExists, readFileContent } from '../util/file-reader';

const staticDirectory = 'static';

export const route: RequestHandler = (req, res) =>
    redirectIfUnauthorized(req, res)(serveRequest);

const serveRequest: RequestHandler = (req, res) => {
    const path = getPathAndParameters(req.url).path;
    const method = req.method ? req.method : 'GET';

    res.statusCode = 200;

    if (fullPages.has(path) && method === 'GET') {
        sendPage(res, path);
    } else if (assets.has(path)) {
        sendAsset(req, res, path);
    } else if (api.has(path)) {
        sendJson(res, path);
    } else if (method && controllerRoutes.has(method, path)) {
        controllerRoutes.handle(method, path, req, res);
    } else if (method === 'GET' && fileExists(staticDirectory + path)) {
        return sendFile(res, path);
    } else {
        res.statusCode = 404;
        sendPage(res, '404');
    }
};

const getPathAndParameters = (url?: string): { path: string, params: (string | [string, string])[] } => {
    const params: (string | [string, string])[] = [];
    if (url) {
        const urlParts = url.split('?');
        if (urlParts[1]) {
            urlParts[1].split('&')
                .map((param) => param.split('='))
                .forEach((param) => params.push(param as string | [string, string]));
        }
        return { path: urlParts[0], params };
    } else {
        return { path: '/', params };
    }
};

const sendPage = (res: ServerResponse | Http2ServerResponse, path: string): void => {
    const content = fullPages.get(path) as string;
    res.setHeader('Content-Encoding', 'gzip');
    res.setHeader('Content-Type', 'text/html;charset=UTF-8');
    res.end(content, 'binary');
};

const sendAsset = (req: IncomingMessage | Http2ServerRequest, res: ServerResponse | Http2ServerResponse, path: string): void => {
    const asset = assets.get(path) as { content: string,  contentType: string, lastModified: Date, eTag: string, hidden: boolean };
    const ifModifiedSince = req.headers['if-modified-since'];
    const eTag = req.headers['if-none-match'];

    if (eTag && eTag === asset.eTag || ifModifiedSince && (new Date(ifModifiedSince)) > asset.lastModified) {
        res.statusCode = 304;
        res.end();
    } else {
        const tomorrow = (new Date(Date.now() + 1000 * 3600 * 24)).toUTCString();
        res.setHeader('Content-Encoding', 'gzip');
        res.setHeader('Vary', 'Accept-Encoding');
        res.setHeader('Content-Type', asset.contentType);
        res.setHeader('Cache-Control', 'max-age=31536000');
        res.setHeader('Expires', tomorrow);
        res.setHeader('Last-Modified', asset.lastModified.toUTCString());
        res.setHeader('ETag', asset.eTag);
        res.end(asset.content, 'binary');
    }
};

const sendJson = (res: ServerResponse | Http2ServerResponse, path: string): void => {
    res.writeHead(200, { 'Content-Encoding': 'gzip', 'Content-Type': 'application/json', 'Vary': 'Accept-Encoding' });
    res.end(api.get(path) as string, 'binary');
};

const sendFile = (res: ServerResponse | Http2ServerResponse, path: string): Promise<void> => {
    const type = mimeTypes.get(path.slice(path.lastIndexOf('.')));
    if (type) {
        res.setHeader('Content-Type', type);
    }

    return readFileContent(staticDirectory + path).then((content: Buffer) => {
        res.statusCode = 200;
        res.end(content, 'binary');
    }).catch((error) => {
        res.statusCode = 500;
        res.end(JSON.stringify({ error }));
    });
};
