import { IncomingMessage, ServerResponse } from 'http';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { RequestHandler } from '../../models/types';
import { validate } from '../util/jwt.factory';
import { parseParameters } from '../util/parameter.parser';

const loginPath = '/login';

export const redirectIfUnauthorized = (req: IncomingMessage | Http2ServerRequest, res: ServerResponse | Http2ServerResponse) =>
    (callback: RequestHandler) => {

    if (isProtected(req) && !hasToken(req)) {
        res.statusCode = 302;
        res.setHeader('Set-Cookie', 'target=' + req.url + '; Path=/api/login; Secure; SameSite=strict');
        res.setHeader('Location', loginPath);
        res.end();
    } else {
        callback(req, res);
    }
};

const isProtected = (req: IncomingMessage | Http2ServerRequest) => (req.url && req.url.startsWith('/admin'));

const hasToken = (req: IncomingMessage| Http2ServerRequest) => {
    const token = getTokenCookie(req);

    return token && validate(token);
};

const getTokenCookie = (req: IncomingMessage| Http2ServerRequest) => {
    let token: string | undefined;

    if (req.headers.cookie) {
        const cookies = parseParameters(req.headers.cookie, ';');
        token = cookies.get('access_token');
    }

    return token;
};
