import { RequestHandler } from '../../models/types';

const redirectStatusCode = 301;

export const httpsRedirect: RequestHandler = (req, res) => {
    res.writeHead(redirectStatusCode, { Location: 'https://' + req.headers.host + req.url });
    res.end();
};
