import { WithId } from 'mongodb';
import { Font } from '../../models/font.interface';

const formats = new Map([
    [ 'woff', 'woff' ],
    [ 'woff2', 'woff2' ],
    [ 'otf', 'opentype' ],
    [ 'ttf', 'truetype' ],
    [ 'eot', 'embedded-opentype' ],
    [ 'svg', 'svg' ]
]);

export const createFontFace = (index: number, font: Font|WithId<Font>) => {
    const { family, weight } = font;
    const match = font.webfont.filename.match(/\.([^.]+?$)/);
    const extension = (match && match.length) ? match[1] : 'woff';
    const format = formats.get(extension);
    const source = `url(data:font/${extension};charset=utf-8;base64,${font.webfont.data}) format('${format}')`;
    const style = font.italic ? 'italic' : 'normal';
    const descriptors = { weight, style };

    return  { index, family, source, descriptors };
};
