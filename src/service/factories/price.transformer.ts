import { BasePrices } from '../../models/base-prices.interface';
import { Platform } from '../../models/platform.enum';
import { PriceBlock } from '../../models/price-block.interface';

type SingleStylePrice = { platform: Platform, licenseSize: 1, singleStyle: number };

const licenseToPower: { size: number, power: number }[] = [
    { size: 1, power: 0 },
    { size: 5, power: 1 },
    { size: 10, power: 2 },
    { size: 25, power: 3 },
    { size: 50, power: 4 },
    { size: 100, power: 5 },
    { size: 250, power: 6 },
    { size: 500, power: 7 },
    { size: 750, power: 8 }
];

const calculateByLicenseSize = (price: number, power: number) => Math.round(price * Math.pow(4, power) / Math.pow(3, power));

const scaleToLicence = (priceBlock: PriceBlock, licenseSize: number, power: number) => {
    const singleStyle = calculateByLicenseSize(priceBlock.singleStyle, power);
    const singlePair = calculateByLicenseSize(priceBlock.singlePair, power);
    const family = calculateByLicenseSize(priceBlock.family, power);

    return {...priceBlock, licenseSize, singleStyle, singlePair, family };
};

const addLicences = (priceBlock: PriceBlock): PriceBlock[] =>
    licenseToPower.map(({ size, power }) => scaleToLicence(priceBlock, size, power));

const addStyleVariants = (singleStylePrice: SingleStylePrice): PriceBlock =>
    ({...singleStylePrice, singlePair: singleStylePrice.singleStyle * 1.5, family: singleStylePrice.singleStyle * 5 });

const calculateSingleStylePrices = (basePrices: BasePrices): SingleStylePrice[] => [
    { platform: Platform.desktop, licenseSize: 1, singleStyle: basePrices.desktop },
    { platform: Platform.web, licenseSize: 1, singleStyle: basePrices.web },
    { platform: Platform.app, licenseSize: 1, singleStyle: basePrices.app },

    { platform: Platform['desktop+web'], licenseSize: 1, singleStyle: (basePrices.desktop + basePrices.web) * 0.75 },
    { platform: Platform['desktop+app'], licenseSize: 1, singleStyle: (basePrices.desktop + basePrices.app) * 0.75 },
    { platform: Platform['web+app'], licenseSize: 1, singleStyle: (basePrices.web + basePrices.app) * 0.75 },
    { platform: Platform['desktop+web+app'], licenseSize: 1, singleStyle: (basePrices.desktop + basePrices.web + basePrices.app) * 0.75 }
];

export const transform = (basePrices: BasePrices): PriceBlock[] =>
    calculateSingleStylePrices(basePrices)
        .map(addStyleVariants)
        .flatMap(addLicences);
