import { Product } from '../../models/product.interface';

export const stringifyProduct = (product: Product) => {
    let title = product.family;
    if (product.weight) {
        title += (' ' + product.weight);
    }
    if (product.style) {
        title += (' ' + product.style);
    }
    switch (product.bundle) {
        case 'family':
            title += ' family';
            break;
        case 'singlePair':
            title += ' italic+regular pair';
            break;
    }
    title += (' - ' + product.license.toString() + ' person license');

    return title;
};
