import { getProjectsWithId } from '../../db/project.dao';
import { Project } from '../../models/project/project.interface';
import { createFontFace } from './font-data-api.factory';
import { db } from '../../db/mongo';
import { PlaceholderText } from '../../models/placeholder-text.interface';
import { getFontById } from '../../db/font.dao';
import { Font } from '../../models/font.interface';

export const createProjectExtrasEndpoints = () => getProjectsWithId()
    .then(projects => getTexts().then(texts => Promise.all(createEndpoints(projects, texts))));

const createEndpoints = (projects: Project[], texts: PlaceholderText[]): Promise<[string, string]>[] => projects
    .map(project => createProjectExtra(project, texts).then(endpoint => [ `/api/extras/${project.url}`, endpoint ]));

const createProjectExtra = async (project: Project, texts: PlaceholderText[]) => {
    const fonts = await Promise.all(project.fonts.map(f => getFontById(f.id).then(font => ({ index: f.index, font }))));
    const fontFaces = (fonts.filter(f => !!f.font) as { index: number, font: Font }[]).map(f => createFontFace(f.index, f.font))
    const slides = project.slides;

    return JSON.stringify({ fonts: fontFaces, texts, slides });
};

const getTexts = () => db.collection('placeholderTexts')
    .find({}, { projection: { _id: false }})
    .toArray() as Promise<PlaceholderText[]>;
