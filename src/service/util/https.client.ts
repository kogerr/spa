import { IncomingMessage } from 'http';
import { connect, SecureClientSessionOptions } from 'http2';
import { get, RequestOptions } from 'https';
import { getCredentials } from '../server/server';

export const download = (url: string) => {
    const options = { rejectUnauthorized: false } as RequestOptions;

    const credentials = getCredentials();
    if (credentials) {
        options.ca = credentials.ca;
        options.cert = credentials.cert;
        options.key = credentials.key;
    }

    return new Promise<string>((resolve, reject) => {
        get(url, options, (res: IncomingMessage) => {
            let data = '';

            if (res.statusCode !== 200) {
                reject(res.headers);
            }

            res.on('error', reject);
            res.on('data', (chunk) => data += chunk);
            res.on('end', () => resolve(data));
        });
    });
};

export const http2download = (url: string, method = 'GET') => {
    return new Promise<string>((resolve, reject) => {
        const [ authority, path ] = splitUrl(url);
        const options = { rejectUnauthorized: false } as SecureClientSessionOptions;

        const credentials = getCredentials();
        if (credentials) {
            options.ca = credentials.ca;
            options.cert = credentials.cert;
            options.key = credentials.key;
        }

        const client = connect(authority, options);
        client.on('error', (error) => {
            client.close();
            reject(error);
        });

        const request = client.request({ ':path': path,  ':method': method });
        request.setEncoding('utf8');
        request.on('response', (headers, flags) => {
            if (headers[':status'] !== 200) {
                client.close();
                reject(headers);
            }
        });

        let data = '';
        request.on('data', (chunk) => data += chunk);
        request.on('end', () => {
            resolve(data);
            client.close();
        });
        request.end();
    });
};

const splitUrl = (url: string) => {
    const matches = url.match(/https?:\/\/[^\/]+/);
    if (!matches) {
        throw new Error('Cannot parse host');
    }
    const authority = matches[0];
    const path = url.substr(authority.length);

    return [ authority, path ];
};
