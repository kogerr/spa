import { createHmac } from 'crypto';

const secret = process.env.JWT_SECRET || 'jwt secret';
const algorithm = 'sha256';
const encoding = 'base64';

const urlEncode = (raw: string) =>
    raw.replace(/=/g, '')
        .replace(/\+/g, '-')
        .replace(/\//g, '_');

const base64encode = (encodable: object) =>
    urlEncode(Buffer.from(JSON.stringify(encodable)).toString('base64'));

const encodedHeader = base64encode({ alg: 'HS256', typ: 'JWT' });

export const createToken = (sub: string) => {
    const payload = createPayload(sub);
    const signature = generateSignature(encodedHeader, payload);

    return [ encodedHeader, payload, signature ].join('.');
};

export const validate = (jwt: string) => {
    const [ header, payload, signature ] = jwt.split('.');

    return signature === generateSignature(header, payload);
};

const createPayload = (sub: string) => {
    const iat = Math.floor(Date.now() / 1000);

    return base64encode({ sub, iat });
};

const generateSignature = (header: string, payload: string) => {
    const data = [ header, payload ].join('.');

    const signature = createHmac(algorithm, secret)
        .update(data)
        .digest(encoding);

    return urlEncode(signature);
};
