import { unlink } from 'fs';

const assetsDirectory = 'src/assets';

export const unlinkFile = (path: string): Promise<void> => new Promise(((resolve, reject) => {
    unlink(assetsDirectory + path, (err: NodeJS.ErrnoException | null) => {
        if (err) {
            console.error(err);
            reject(err);
        } else {
            resolve();
        }
    })
}));
