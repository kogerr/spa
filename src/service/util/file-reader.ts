import { Dirent, readdir, readFile, statSync } from 'fs';
import { NamedFile } from '../../models/named-file.interface';

export const readFromPath = (path: string): Promise<NamedFile[]> => readDirectory(path).then(handle);

const readDirectory = (path: string) => {
    return new Promise<{ path: string, name: string, entry: Dirent }[]>((resolve, reject) => {
        readdir(path, { encoding: 'utf8', withFileTypes: true }, (err, entries) => {
            if (err) {
                reject(err);
            } else {
                resolve(entries.map((entry: Dirent) => ({ path, name: entry.name, entry })));
            }
        });
    });
};

const handle = (pathsAndEntries: { path: string, name: string, entry: Dirent }[]): Promise<NamedFile[]> => {
    const promises: Promise<NamedFile>[] = [];
    const nestedPromises: Promise<NamedFile[]>[] = [];

    pathsAndEntries.forEach((entry) => {
        if (entry.entry.isDirectory()) {
            nestedPromises.push(readDirectory(entry.path + '/' + entry.name).then(handle));
        } else if (entry.entry.isFile()) {
            promises.push(readFileContent(entry.path + '/' + entry.name)
                .then((content) => ({ path: entry.path, name: entry.name, content })));
        }
    });

    nestedPromises.push(Promise.all(promises));
    return Promise.all(nestedPromises).then(flatten);
};

export const fileExists = (path: string) => {
    try {
        return statSync(path).isFile();
    } catch (error) {
        return false;
    }
};

export const readFileContent = (path: string) => {
    return new Promise<Buffer>((resolve, reject) => {
        readFile(path, {}, (error, content: Buffer) => {
            if (error) {
                reject(error);
            } else {
                resolve(content);
            }
        });
    });
};

const flatten = <T> (templates: T[][]) => templates.reduce((a, b) => a.concat(b), []);
