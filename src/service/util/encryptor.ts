import { createHmac, Encoding } from 'crypto';

const algorithm = 'sha256';
const secret = process.env.PWSECRET || 'pwsecret';
const encoding: Encoding = 'latin1';

export const encrypt = (data: any) => createHmac(algorithm, secret)
    .update(data)
    .digest()
    .toString(encoding);
