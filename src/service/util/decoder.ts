export const base64decode = (data: string) => Buffer.from(data, 'base64').toString('ascii');
