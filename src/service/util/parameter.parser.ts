const trim = (value: string) => value.trimRight().trimLeft();

const getParameterArray = (params: string, delimiter: string) =>
    params.split(delimiter)
        .map((pair: string) => decodeURIComponent(pair))
        .map(pair => pair.replace(/\+/g, ' '))
        .map((pair: string) => pair.split('=').map(trim) as [string, string]);

export const parseParameters = (params: string, delimiter: string = '&') =>
    new Map<string, string>(getParameterArray(params, delimiter));

export const parseParameterObject =  <T>(params: string, delimiter: string = '&') =>
    // @ts-ignore
    getParameterArray(params, delimiter).reduce((a, b) => { a[b[0]] = b[1]; return a; }, {}) as T;
