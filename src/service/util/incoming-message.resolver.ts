import { IncomingMessage } from 'http';
import { Http2ServerRequest } from 'http2';
import { parseParameterObject, parseParameters } from './parameter.parser';

const getRequestData = (req: IncomingMessage | Http2ServerRequest) => {
    return new Promise<string>((resolve, reject) => {
        let data = '';

        req.on('error', reject);
        req.on('data', (chunk) => data += chunk);
        req.on('end', () => resolve(data as string));
    });
};

export const getRequestDataBuffer = (req: IncomingMessage | Http2ServerRequest) => {
    return new Promise<Buffer>((resolve, reject) => {
        const data: Uint8Array[] = [];

        req.on('error', reject);
        req.on('data', (chunk) => data.push(chunk));
        req.on('end', () => resolve(Buffer.concat(data)));
    });
};

export const getRequestDataJson = <T> (req: IncomingMessage | Http2ServerRequest) =>
    getRequestData(req)
        .then((data) => JSON.parse(data) as unknown as T);

export const getRequestParameters = (req: IncomingMessage | Http2ServerRequest): Promise<Map<string, string>> =>
    getRequestData(req).then(parseParameters);

export const getRequestFormObject = <T>(req: IncomingMessage | Http2ServerRequest): Promise<T> =>
    getRequestData(req).then((requestData) => parseParameterObject<T>(requestData));
