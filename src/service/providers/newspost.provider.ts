import { getNews } from '../../db/news.dao';
import { DatedTemplate } from '../../models/dated-template.interface';
import { NewsPost } from '../../models/news-post.interface';

export const getNewsPosts = () => getNews().then(renderPosts);

const renderPosts = (posts: NewsPost[]) => posts.map((post) => ({ date: post.date, template: post.text }) as DatedTemplate);
