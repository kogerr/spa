import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';
import { renderExampleEmail } from '../../components/example-email/example-email';
import { getSmtpProperties } from '../../db/smtp.dao';
import { EmailRequest } from '../../models/email-request.interface';
import { SmtpProperties } from '../../models/smtp-properties.interface';
import { RequestHandler } from '../../models/types';
import { sendEmail } from '../email/smtp.client';
import { getRequestDataJson } from '../util/incoming-message.resolver';

const errorMessage = 'Could not get Smtp properties';

export const handleEmailRequest: RequestHandler = (req, res) =>
    getRequestDataJson<EmailRequest>(req)
        .then(addTemplate)
        .then(addSmtpProperties)
        .then(sendTemplate)
        .then(() => sendSuccessResponse(res))
        .catch((error) => sendFailureResponse(res, error));

const addTemplate = (request: EmailRequest) => {
    const htmlContent = renderExampleEmail(request.name);
    return { to: request.to, htmlContent };
};

const addSmtpProperties = (request: { to: string, htmlContent: string }) =>
    getSmtpProperties().then((smtpProperties) => {
        if (!smtpProperties) {
            throw Error(errorMessage);
        }

        return { to: request.to, htmlContent: request.htmlContent, smtpProperties};
    });

const sendTemplate = (requestContext: { to: string, htmlContent: string, smtpProperties: SmtpProperties }) =>
    sendEmail(requestContext.to, requestContext.htmlContent, requestContext.smtpProperties);

const sendSuccessResponse = (res: ServerResponse | Http2ServerResponse) => {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ success: true }));
};

const sendFailureResponse = (res: ServerResponse | Http2ServerResponse, error: unknown) => {
    res.writeHead(400, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ success: false, error }));
};
