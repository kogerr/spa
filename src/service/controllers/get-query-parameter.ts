import { IncomingMessage } from 'http';
import { Http2ServerRequest } from 'http2';
import { parse } from 'url';

export const getQueryParameter = <T> (req: IncomingMessage | Http2ServerRequest, parameter: string) =>
    parse(req.url as string, true).query[parameter] as unknown as T;
