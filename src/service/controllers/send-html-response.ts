import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';

export const sendHtmlResponse = (content: string, res: ServerResponse | Http2ServerResponse) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html;charset=UTF-8');
    res.end(content);
};
