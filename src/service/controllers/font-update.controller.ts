import { getRequestDataJson } from '../util/incoming-message.resolver';
import { reloadPagesAndApi } from '../loader/container.service';
import { updateProjectFonts } from '../../db/project-font.dao';
import { sendErrorResponse } from './send-error-response';
import { sendJsonResponse } from './send-json-response';
import { AsyncRequestHandler } from '../../models/types';

type FontUpdateRequest = { projectUrl: string, fonts: { index: number, id: string }[] };

export const handleFontUpdate: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<FontUpdateRequest>(req)
        .then(request => updateProjectFonts(request.projectUrl, request.fonts))
        .then((result) => sendJsonResponse(result, res))
        .then(() => reloadPagesAndApi())
        .catch((error) => sendErrorResponse(res, error));
