import { getRequestDataJson } from '../util/incoming-message.resolver';
import { reloadPagesAndApi } from '../loader/container.service';
import { getFontById, insertFont } from '../../db/font.dao';
import { sendErrorResponse } from './send-error-response';
import { Font } from '../../models/font.interface';
import { AsyncRequestHandler } from '../../models/types';
import { sendJsonResponse } from './send-json-response';
import { getQueryParameter } from './get-query-parameter';
import { addFontToProject, getProjectFontsByUrl } from '../../db/project-font.dao';

type FontUpload = {
    projectUrl: string;
    index: number;
    family: string;
    weight: number;
    italic: boolean;
    webfont: { filename: string, data: string };
    fullFonts: { filename: string, data: string }[];
};

export const handleFontFaceUpload: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<FontUpload>(req)
        .then(saveFontData)
        .then(result => reloadPagesAndApi().then(() => sendJsonResponse(result, res)))
        .catch(error => sendErrorResponse(res, error));

const saveFontData = async (upload: FontUpload) => {
    const { index, family, weight, italic, webfont, fullFonts } = upload;
    const font: Font = { family, weight, italic, webfont, fullFonts };
    const id = await insertFont(font);

    return addFontToProject(upload.projectUrl, { index, id });
};

export const serveFontsByProjectUrl: AsyncRequestHandler = (req, res) =>
    Promise.resolve(req)
        .then(request => getQueryParameter<string>(request, 'projectUrl'))
        .then(getProjectFontsByUrl)
        .then(loadFonts)
        .then(result => sendJsonResponse(result, res))
        .catch(error => sendErrorResponse(res, error));

const loadFonts = (fonts: {id: string, index: number }[]) =>
    Promise.all(fonts.sort((a, b) => a.index - b.index)
        .map(({index, id }) => getFontById(id).then(font => transform(index, id, (font as Font) ))));

const transform = (index: number, id: string, font: Font) => ({
    index,
    family: font.family,
    weight: font.weight,
    italic: font.italic,
    id,
});
