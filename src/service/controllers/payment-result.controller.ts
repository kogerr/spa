import { IncomingMessage } from 'http';
import { Http2ServerRequest } from 'http2';
import { AsyncRequestHandler } from '../../models/types';
import { parseParameters } from '../util/parameter.parser';
import { queryStatus } from '../payment/back/barion/result-facade';
import { sendJsonResponse } from './send-json-response';
import { sendErrorResponse } from './send-error-response';

export const handlePaymentResult: AsyncRequestHandler = (req, res) =>
    Promise.resolve(req)
        .then(getOrderId)
        .then(queryStatus)
        .then(orderAndStatus => sendJsonResponse(orderAndStatus, res))
        .catch((error) => sendErrorResponse(res, error));

const getOrderId = (req: IncomingMessage|Http2ServerRequest) =>
    parseParameters(req.url?.split('?')[1] as string)
        .get('orderId') as string;
