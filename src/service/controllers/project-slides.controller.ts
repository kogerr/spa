import { getProjectSlides, getProjectSlidesByUrl, updateProjectSlides } from '../../db/project.dao';
import { sendErrorResponse } from './send-error-response';
import { getRequestDataJson } from '../util/incoming-message.resolver';
import { unlinkFile } from '../util/file-unlinker';
import { getQueryParameter } from './get-query-parameter';
import { sendJsonResponse } from './send-json-response';
import { AsyncRequestHandler } from '../../models/types';
import { IndexedSlide } from '../../models/project/indexed-slide.interface';

export const serveProjectSlides: AsyncRequestHandler = (req, res) =>
    Promise.resolve(req)
        .then(request => getQueryParameter<string>(request, 'id'))
        .then(id => getProjectSlides(id))
        .then((slides) => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));

export const serveProjectSlidesByUrl: AsyncRequestHandler = (req, res) =>
    Promise.resolve(req)
        .then(request => getQueryParameter<string>(request, 'url'))
        .then(getProjectSlidesByUrl)
        .then((slides) => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));

export const handleProjectSlidesUpdate: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<{ id: string, slides: IndexedSlide[], remove?: string }>(req)
        .then(payload => {
            const promises: Promise<any>[] = [ updateProjectSlides(payload.id, payload.slides) ];
            if (payload.remove) {
                promises.push(unlinkFile(payload.remove))
            }
            return Promise.all(promises).then(() => getProjectSlides(payload.id))
        })
        .then((slides) => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));
