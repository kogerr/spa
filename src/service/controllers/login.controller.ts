import { IncomingMessage, ServerResponse } from 'http';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { checkUser } from '../../db/user.dao';
import { encrypt } from '../util/encryptor';
import { getRequestFormObject } from '../util/incoming-message.resolver';
import { createToken } from '../util/jwt.factory';
import { parseParameters } from '../util/parameter.parser';
import { AsyncRequestHandler } from '../../models/types';

export const handleLogin: AsyncRequestHandler = (req, res) =>
    getRequestFormObject<{ email: string, password: string }>(req)
        .then(checkCredentials)
        .then(getToken)
        .then((token) => successRedirect(req, res, token))
        .catch((error) => errorRedirect(res, error));

const checkCredentials = (req: { email: string, password: string }) => {
    if (req.email === undefined || req.password === undefined) {
        throw new Error('Cannot parse credentials.');
    }

    return req;
};

const getToken = (credentials: { email: string, password: string }) =>
    checkUser(credentials.email, encrypt(credentials.password))
        .then((isFound) => {
            if (isFound) {
                return createToken(credentials.email);
            } else {
                throw new Error('Could not validate credentials.');
            }
        });

const successRedirect = (req: IncomingMessage | Http2ServerRequest, res: ServerResponse | Http2ServerResponse, token: string) => {
    const redirectPath = getRedirectPath(req.headers.cookie);
    res.statusCode = 302;
    res.setHeader('Set-Cookie', 'access_token=' + token + '; Path=/; Secure');
    res.setHeader('Location', redirectPath);
    res.end('Successful login');
};

const getRedirectPath = (cookieHeader?: string) => {
    let path: string | undefined;

    if (cookieHeader) {
        const cookies = parseParameters(cookieHeader);
        path = cookies.get('target');
    }

    return path || '/';
};

const errorRedirect = (res: ServerResponse | Http2ServerResponse, error: { message?: string }) => {
    const errorMessage = `${error.message}`;
    res.statusCode = 302;
    res.setHeader('error', errorMessage);
    res.setHeader('Location', `/login?error=${encodeURIComponent(errorMessage)}`);
    res.end();
};
