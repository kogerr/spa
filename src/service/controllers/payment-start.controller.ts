import { getRequestFormObject } from '../util/incoming-message.resolver';
import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';
import { initiateStart } from '../payment/start/barion/start-facade';
import { validate } from '../payment/validator';
import { AsyncRequestHandler } from '../../models/types';
import { PaymentRequest } from '../payment/start/payment-request.interface';
import { StartResponse, SuccessStartResponse } from '../../models/barion/start-response.interface';
import { CheckoutFormPayload } from '../../models/checkout-form-payload.interface';
import { Product } from '../../models/product.interface';
import { Customer } from '../../models/customer.interface';
import { Item } from '../../models/cart-item.interface';
import { stringifyProduct } from '../factories/product.stingifier';
import { SimpleProduct } from '../../models/simple-product.interface';

export const startPayment: AsyncRequestHandler = (req, res) =>
    getRequestFormObject<CheckoutFormPayload>(req)
        .then(extractPaymentRequest)
        .then(initiateStart)
        .then(startResponse => handleResponse(startResponse, res))
        .catch(error => sendErrorResponse(error, res));

const extractPaymentRequest = async (payload: CheckoutFormPayload): Promise<PaymentRequest> => {
    if (!(payload.email && payload.total)) {
        throw new Error('Cannot parse request.' + payload); // TODO: error page
    }

    const { name, email, country, city, address, zip, vat, domain, phone, state } = payload;
    const customer: Customer = { name, email, country, city, address, state, zip, vat, domain, phone: phone.substring(1) };
    const products = (JSON.parse(payload.products) as Product[]);
    const items = (JSON.parse(payload.items) as Item[]);
    const total = parseInt(payload.total, 10);
    await validate(products, items, total);

    const simpleProducts: SimpleProduct[] = products.map(p => ({ title: stringifyProduct(p), price: p.price }));

    return { customer, products: simpleProducts, items, total };
};

const handleResponse = (startResponse: StartResponse, serverResponse: ServerResponse | Http2ServerResponse) => {
    if (startResponse.Errors.length) {
        sendErrorResponse(startResponse.Errors, serverResponse);
    } else {
        serverResponse.statusCode = 302;
        serverResponse.setHeader('Location', (startResponse as SuccessStartResponse).GatewayUrl);
        serverResponse.end();
    }
};

const sendErrorResponse = (error: any, serverResponse: ServerResponse | Http2ServerResponse) => {
    serverResponse.statusCode = 500;
    serverResponse.end(JSON.stringify(error.message ? { error: error.message } : error));
};
