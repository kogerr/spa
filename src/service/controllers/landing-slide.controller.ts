import { addLandingSlide, getLandingSlides, getLandingSlidesWithId, removeLandingSlide, updateLandingSlides } from '../../db/landing-slide.dao';
import { sendErrorResponse } from './send-error-response';
import { getRequestDataJson } from '../util/incoming-message.resolver';
import { unlinkFile } from '../util/file-unlinker';
import { reloadPagesAndApi } from '../loader/container.service';
import { sendJsonResponse } from './send-json-response';
import { AsyncRequestHandler } from '../../models/types';
import { LandingSlide } from '../../models/landing-slide.interface';
import { WithId } from 'mongodb';

export const handleLandingSlidesQuery: AsyncRequestHandler = (req, res) =>
    getLandingSlides()
        .then(slides => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));

export const handleLandingSlidesAdminQuery: AsyncRequestHandler = (req, res) =>
    getLandingSlidesWithId()
        .then(slides => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));

export const handleLandingSlidesUpload: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<LandingSlide>(req)
        .then(addLandingSlide)
        .then(reloadPagesAndApi)
        .then(getLandingSlidesWithId)
        .then(slides => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));

export const handleLandingSlideRemoval: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<{ id: string, src: string }>(req)
        .then((request) => Promise.all([ unlinkFile(request.src), removeLandingSlide(request.id) ]))
        .then(reloadPagesAndApi)
        .then(getLandingSlidesWithId)
        .then(slides => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));

export const handleLandingSlideUpdate: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<WithId<LandingSlide>[]>(req)
        .then(updateLandingSlides)
        .then(reloadPagesAndApi)
        .then(getLandingSlidesWithId)
        .then(slides => sendJsonResponse(slides, res))
        .catch(error => sendErrorResponse(res, error));
