import { IncomingMessage, ServerResponse } from 'http';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { checkEmail, registerUser } from '../../db/user.dao';
import { RequestHandler } from '../../models/types';
import { encrypt } from '../util/encryptor';
import { getRequestParameters } from '../util/incoming-message.resolver';
import { createToken } from '../util/jwt.factory';

export const handleRegister: RequestHandler = (req, res) => {
    return getRequestParameters(req)
        .then(extractCredentials)
        .then(checkIfUserExists)
        .then(saveUser)
        .then((user) => createToken(user.email))
        .then((token) => successRedirect(req, res, token))
        .catch((error) => errorRedirect(res, error));
};

const extractCredentials = (fields: Map<string, string>) => {
    const email = fields.get('email');
    const password = fields.get('password');

    if (email === undefined || password === undefined) {
        throw new Error('Cannot parse credentials.');
    }

    return { email, password };
};

const checkIfUserExists = (credentials: { email: string, password: string }) =>
    checkEmail(credentials.email).then((exists: boolean) => {
        if (exists) {
            throw new Error('User already exists.');
        }

        return credentials;
    });

const saveUser = (credentials: { email: string, password: string }) => {
    if (process.env.REGISTRATION === 'true') {
        return registerUser(credentials.email, encrypt(credentials.password));
    } else {
        throw new Error('Not open for registration.');
    }
};

const successRedirect = (req: IncomingMessage | Http2ServerRequest, res: ServerResponse | Http2ServerResponse, token: string) => {
    res.statusCode = 302;
    res.setHeader('Set-Cookie', 'access_token=' + token + '; Path=/; Secure');
    res.setHeader('Location', '/');
    res.end('Successful registration');
};

const errorRedirect = (res: ServerResponse | Http2ServerResponse, error: { message?: string }) => {
    const errorMessage = `${error.message}`;
    res.statusCode = 302;
    res.setHeader('error', errorMessage);
    res.setHeader('Location', `/registration?error=${encodeURIComponent(errorMessage)}`);
    res.end();
};
