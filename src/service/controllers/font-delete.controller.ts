import { reloadPagesAndApi } from '../loader/container.service';
import { getRequestDataJson } from '../util/incoming-message.resolver';
import { sendJsonResponse } from './send-json-response';
import { sendErrorResponse } from './send-error-response';
import { AsyncRequestHandler } from '../../models/types';
import { removeFontById } from '../../db/font.dao';
import { removeProjectFonts } from '../../db/project-font.dao';

type FontDeleteRequest = { projectUrl: string, id: string };

export const handleFontDelete: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<FontDeleteRequest>(req)
        .then(deleteAndUpdate)
        .then((result) => sendJsonResponse(result, res))
        .then(() => reloadPagesAndApi())
        .catch((error) => sendErrorResponse(res, error));

const deleteAndUpdate = (request: FontDeleteRequest) =>
    removeFontById(request.id)
        .then(() => removeProjectFonts(request.projectUrl, request.id));
