import { stat, writeFile } from 'fs';
import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';
import { reloadPagesAndApi, renderAssets } from '../loader/container.service';
import { getRequestDataBuffer } from '../util/incoming-message.resolver';
import { AsyncRequestHandler } from '../../models/types';

const targetDirectory = 'src/assets/media/';

export const handleUpload: AsyncRequestHandler = (req, res) => {
    const base64 = req.headers.base64 && req.headers.base64 === 'true';
    const filename = encodeURI(base64 ? req.headers.filename + '.b64' : req.headers.filename as string);
    const targetPath = targetDirectory + filename;
    const overwrite = req.headers.overwrite && req.headers.overwrite === 'true';
    const encoding: BufferEncoding = base64 ? 'base64' : 'binary';

    return checkIfExists(targetPath).then((exists) => {
        if (exists && !overwrite) {
            return sendExistsResponse(res, filename);
        } else {
            return getRequestDataBuffer(req)
                .then((content) => saveFile(targetPath, content, encoding))
                .then(renderAssets)
                .then(reloadPagesAndApi)
                .then(() => sendSuccessResponse(res, targetPath))
                .catch((error) => sendErrorResponse(res, error));
        }
    });
};

const checkIfExists = (targetPath: string) => new Promise<boolean>((resolve) => {
    stat(targetPath, (err, stats) => {
        const exists = !err && stats.isFile();
        resolve(exists);
    });
});

const sendExistsResponse = (res: ServerResponse | Http2ServerResponse, filename: string) => {
    res.statusCode = 409;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({ error: 'A file with that name already exists.', filename }));
};

const saveFile = (path: string, data: Buffer, encoding: BufferEncoding): Promise<void> => new Promise((resolve, reject) => {
    writeFile(path, data.toString(encoding), {encoding: 'binary'}, (err: NodeJS.ErrnoException | null) => {
        if (err) {
            reject(err);
        } else {
            resolve();
        }
    });
});

const sendSuccessResponse = (res: ServerResponse | Http2ServerResponse, targetPath: string) => {
    const path = targetPath.replace('src/assets', '');
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({ success: true, path }));
};

const sendErrorResponse = (res: ServerResponse | Http2ServerResponse, error: { message?: string }) => {
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 500;
    res.end(JSON.stringify({ error: error.message }));
};
