import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';

export const sendJsonResponse = (content: any, res: ServerResponse | Http2ServerResponse) => {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(content));
};
