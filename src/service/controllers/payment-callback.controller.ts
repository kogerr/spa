import { AsyncRequestHandler } from '../../models/types';

export const answerPaymentCallback: AsyncRequestHandler = (req, res) => {
    res.writeHead(200);
    return Promise.resolve(res.end());
};
