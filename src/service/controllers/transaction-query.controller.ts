import { queryTransactionDetails } from '../payment/query/transaction-query.facade';
import { getQueryParameter } from './get-query-parameter';
import { AsyncRequestHandler } from '../../models/types';

export const getTransactionDetails: AsyncRequestHandler = (req, res) =>
    Promise.resolve(req)
        .then(request => getQueryParameter<string>(request, 'id'))
        .then(queryTransactionDetails)
        .then(details => res.end(details))
        .catch(error => res.end(error));
