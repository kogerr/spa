import { RequestHandler } from '../../models/types';
import { assets } from '../container';

export const servePreloadPaths: RequestHandler = (req, res) => {
    const preloadPaths = JSON.stringify(Array.from(assets.keys()));
    const contentLength = Buffer.byteLength(preloadPaths);
    const headers = { 'Content-Type': 'application/json', 'Content-Length': contentLength };

    res.writeHead(200, headers);
    res.end(preloadPaths);
};
