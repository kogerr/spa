import { getBasePrices, getBasePricesByFamily, getBasePricesWithId, removeBasePrices, upsertBasePrices } from '../../db/base-prices.dao';
import { sendErrorResponse } from './send-error-response';
import { getRequestDataJson } from '../util/incoming-message.resolver';
import { reloadPagesAndApi } from '../loader/container.service';
import { sendJsonResponse } from './send-json-response';
import { AsyncRequestHandler } from '../../models/types';
import { BasePrices } from '../../models/base-prices.interface';
import { getFontFamilies } from '../../db/font.dao';
import { getQueryParameter } from './get-query-parameter';
import { transform } from '../factories/price.transformer';

export const handleBasePricesQuery: AsyncRequestHandler = (req, res) =>
    getBasePrices()
        .then(basePrices => getFontFamilies().then(fontFamilies => mergeFontsAndPrices(basePrices, fontFamilies)))
        .then(basePrices => sendJsonResponse(basePrices, res))
        .catch(error => sendErrorResponse(res, error));

const mergeFontsAndPrices = (basePrices: BasePrices[], fontFamilies: string[]) =>
    fontFamilies.map((family) =>  basePrices.find(bp => bp.family === family) ?? { family });

/**
 * Not used anywhere at the moment, the operations for the admin page identify prices by the font family.
 *
 * @param req the request
 * @param res the response
 */
export const handleBasePricesAdminQuery: AsyncRequestHandler = (req, res) =>
    getBasePricesWithId()
        .then(basePrices => sendJsonResponse(basePrices, res))
        .catch(error => sendErrorResponse(res, error));

export const handleBasePricesRemoval: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<{ family: string }>(req)
        .then(payload => removeBasePrices(payload.family))
        .then(reloadPagesAndApi)
        .then(() => handleBasePricesQuery(req, res))
        .catch(error => sendErrorResponse(res, error));

export const handleBasePricesUpdate: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<BasePrices>(req)
        .then(upsertBasePrices)
        .then(reloadPagesAndApi)
        .then(() => handleBasePricesQuery(req, res))
        .catch(error => sendErrorResponse(res, error));

export const handlePricesQuery: AsyncRequestHandler = (req, res) =>
    Promise.resolve(req)
        .then(request => getQueryParameter<string>(request, 'family'))
        .then(getBasePricesByFamily)
        .then(basePrices => basePrices ? transform(basePrices) : [])
        .then(basePrices => sendJsonResponse(basePrices, res))
        .catch(error => sendErrorResponse(res, error));
