import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';

export const sendErrorResponse = (res: ServerResponse | Http2ServerResponse, error: any) => {
    res.statusCode = 500;
    res.end(JSON.stringify(error.stack ? error.stack : error));
};
