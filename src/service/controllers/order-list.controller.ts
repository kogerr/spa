import { AsyncRequestHandler } from '../../models/types';
import { getOrders } from '../../db/order.dao';
import { renderOrderList } from '../../components/admin/order-list/order-list';
import { renderMainframe } from '../../components/mainframe/mainframe';
import { sendHtmlResponse } from './send-html-response';

export const getOrderListPage: AsyncRequestHandler = (req, res) =>
    getOrders()
        .then(renderOrderList)
        .then(renderMainframe)
        .then((page) => sendHtmlResponse(page.content, res))
        .catch(console.error);
