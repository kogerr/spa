import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';
import { updateSmtpProperties } from '../../db/smtp.dao';
import { RequestHandler } from '../../models/types';
import { getRequestDataJson } from '../util/incoming-message.resolver';

export const handleSmtpUpdate: RequestHandler = (req, res) =>
    getRequestDataJson<{ host: string, from: string, password: string }>(req)
        .then(updateDatabase)
        .then(() => sendSuccessResponse(res))
        .catch((error) => sendFailureResponse(res, error));

const updateDatabase = (request: { host: string, from: string, password: string }) =>
    updateSmtpProperties(request.host, request.from, request.password).then((result) => {
        if (result.ok !== 1) {
            throw new Error('Could not update smtp properties');
        }
    });

const sendSuccessResponse = (res: ServerResponse | Http2ServerResponse) => {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ success: true }));
};

const sendFailureResponse = (res: ServerResponse | Http2ServerResponse, error: unknown) => {
    res.writeHead(400, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ success: false, error }));
};
