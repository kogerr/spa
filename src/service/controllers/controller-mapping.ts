import { IncomingMessage, ServerResponse } from 'http';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { handleEmailRequest } from './email.controller';
import { handleFontFaceUpload, serveFontsByProjectUrl } from './font-face.controller';
import { handleLogin } from './login.controller';
import { servePreloadPaths } from './preload.controller';
import { handleRegister } from './register.controller';
import { handleSmtpUpdate } from './smtp-update.controller';
import { handleUpload } from './upload.controller';
import { startPayment } from './payment-start.controller';
import { handlePaymentResult } from './payment-result.controller';
import { getOrderListPage } from './order-list.controller';
import { getTransactionDetails } from './transaction-query.controller';
import { handleFontDelete } from './font-delete.controller';
import { handleFontUpdate } from './font-update.controller';
import { handleProjectsQuery, handleProjectsUpload, handleProjectRemoval,
    handleProjectUpdate, handleProjectsAdminQuery, serveHoverImages } from './project.controller';
import { handleAdminQuery, handlePublicQuery, handleAddition, handleRemoval, handleUpdate } from './generic-db-connector';
import { handleProjectSlidesUpdate, serveProjectSlides, serveProjectSlidesByUrl } from './project-slides.controller';
import { AsyncRequestHandler, RequestHandler } from '../../models/types';
import { LandingSlide } from '../../models/landing-slide.interface';
import { PlaceholderText } from '../../models/placeholder-text.interface';
import { handleBasePricesQuery, handleBasePricesRemoval, handleBasePricesUpdate, handlePricesQuery } from './base-prices.controller';
import { answerPaymentCallback } from './payment-callback.controller';

type Route = { method: string, path: string, listener: RequestHandler|AsyncRequestHandler };

const routes: Route[] = [
    { method: 'GET', path: '/api/landing-slides', listener: handlePublicQuery<LandingSlide>('landingSlides') },
    { method: 'GET', path: '/api/projects', listener: handleProjectsQuery },
    { method: 'GET', path: '/api/hover-images', listener: serveHoverImages },
    { method: 'POST', path: '/api/login', listener: handleLogin },
    { method: 'POST', path: '/api/register', listener: handleRegister },
    { method: 'GET', path: '/api/preload', listener: servePreloadPaths },
    { method: 'POST', path: '/api/payment-start', listener: startPayment },
    { method: 'POST', path: '/api/payment-callback', listener: answerPaymentCallback },
    { method: 'GET', path: '/payment-result', listener: handlePaymentResult },
    { method: 'GET', path: '/api/project-slides', listener: serveProjectSlidesByUrl },
    { method: 'GET', path: '/api/texts', listener: handlePublicQuery<PlaceholderText>('placeholderTexts') },
    { method: 'GET', path: '/api/prices', listener: handlePricesQuery },
    { method: 'GET', path: '/admin/order-list', listener: getOrderListPage },
    { method: 'GET', path: '/admin/api/fonts', listener: serveFontsByProjectUrl },
    { method: 'POST', path: '/admin/api/fonts', listener: handleFontFaceUpload },
    { method: 'PATCH', path: '/admin/api/fonts', listener: handleFontUpdate },
    { method: 'DELETE', path: '/admin/api/fonts', listener: handleFontDelete },
    { method: 'POST', path: '/admin/api/upload', listener: handleUpload },
    { method: 'GET', path: '/admin/api/transaction', listener: getTransactionDetails },
    { method: 'POST', path: '/admin/api/smtp', listener: handleSmtpUpdate },
    { method: 'POST', path: '/admin/api/email', listener: handleEmailRequest },
    { method: 'POST', path: '/admin/api/landing-slides', listener: handleAddition<LandingSlide>('landingSlides') },
    { method: 'DELETE', path: '/admin/api/landing-slides', listener: handleRemoval<LandingSlide>('landingSlides') },
    { method: 'PATCH', path: '/admin/api/landing-slides', listener: handleUpdate<LandingSlide>('landingSlides') },
    { method: 'GET', path: '/admin/api/landing-slides', listener: handleAdminQuery<LandingSlide>('landingSlides') },
    { method: 'POST', path: '/admin/api/projects', listener: handleProjectsUpload },
    { method: 'DELETE', path: '/admin/api/projects', listener: handleProjectRemoval },
    { method: 'PATCH', path: '/admin/api/projects', listener: handleProjectUpdate },
    { method: 'GET', path: '/admin/api/projects', listener: handleProjectsAdminQuery },
    { method: 'GET', path: '/admin/api/project-slides', listener: serveProjectSlides },
    { method: 'PATCH', path: '/admin/api/project-slides', listener: handleProjectSlidesUpdate },
    { method: 'GET', path: '/admin/api/texts', listener: handleAdminQuery<PlaceholderText>('placeholderTexts') },
    { method: 'POST', path: '/admin/api/texts', listener: handleAddition<PlaceholderText>('placeholderTexts') },
    { method: 'DELETE', path: '/admin/api/texts', listener: handleRemoval<PlaceholderText>('placeholderTexts') },
    { method: 'PATCH', path: '/admin/api/texts', listener: handleUpdate<PlaceholderText>('placeholderTexts') },
    { method: 'GET', path: '/admin/api/prices', listener: handleBasePricesQuery },
    { method: 'DELETE', path: '/admin/api/prices', listener: handleBasePricesRemoval },
    { method: 'PATCH', path: '/admin/api/prices', listener: handleBasePricesUpdate },
];

const has = (method: string, path: string) => routes.some(matches(method, path));

const handle = (method: string, path: string, req: IncomingMessage | Http2ServerRequest, res: ServerResponse | Http2ServerResponse) => {
    const route = routes.find(matches(method, path));

    if (route) {
        route.listener(req, res);
    } else {
        throw new Error('No controller found for the request.');
    }
};

const matches = (method: string, path: string) => (route: Route) => route.method === method && route.path === path;

export const controllerRoutes = { has, handle };
