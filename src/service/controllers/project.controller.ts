import { addProject, getHoverImages, getProjects, getProjectsWithId, removeProject, updateProjects } from '../../db/project.dao';
import { sendErrorResponse } from './send-error-response';
import { getRequestDataJson } from '../util/incoming-message.resolver';
import { unlinkFile } from '../util/file-unlinker';
import { reloadPagesAndApi } from '../loader/container.service';
import { sendJsonResponse } from './send-json-response';
import { AsyncRequestHandler } from '../../models/types';
import { Project } from '../../models/project/project.interface';
import { WithId } from 'mongodb';

export const handleProjectsQuery: AsyncRequestHandler = (req, res) =>
    getProjects()
        .then(projects => sendJsonResponse(projects, res))
        .catch(error => sendErrorResponse(res, error));

export const handleProjectsAdminQuery: AsyncRequestHandler = (req, res) =>
    getProjectsWithId()
        .then(projects => sendJsonResponse(projects, res))
        .catch(error => sendErrorResponse(res, error));

export const handleProjectsUpload: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<Project>(req)
        .then(addProject)
        .then(reloadPagesAndApi)
        .then(getProjectsWithId)
        .then(projects => sendJsonResponse(projects, res))
        .catch(error => sendErrorResponse(res, error));

export const handleProjectRemoval: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<{ id: string, src: string[] }>(req)
        .then((request) => Promise.all<any>([ ...request.src.map(src => unlinkFile(src)), removeProject(request.id) ]))
        .then(reloadPagesAndApi)
        .then(getProjectsWithId)
        .then(projects => sendJsonResponse(projects, res))
        .catch(error => sendErrorResponse(res, error));

export const handleProjectUpdate: AsyncRequestHandler = (req, res) =>
    getRequestDataJson<WithId<Project>[]>(req)
        .then(updateProjects)
        .then(reloadPagesAndApi)
        .then(getProjectsWithId)
        .then(projects => sendJsonResponse(projects, res))
        .catch(error => sendErrorResponse(res, error));

export const serveHoverImages: AsyncRequestHandler = (req, res) =>
    getHoverImages()
        .then(projects => sendJsonResponse(projects, res))
        .catch(error => sendErrorResponse(res, error));
