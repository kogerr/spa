import { Collection, FilterQuery, ObjectId, UpdateQuery, WithId } from 'mongodb';
import { db } from '../../db/mongo';
import { sendErrorResponse } from './send-error-response';
import { getRequestDataJson } from '../util/incoming-message.resolver';
import { reloadPagesAndApi } from '../loader/container.service';
import { unlinkFile } from '../util/file-unlinker';
import { sendJsonResponse } from './send-json-response';
import { GenericRequestHandler } from '../../models/types';

export const handlePublicQuery: GenericRequestHandler = <T extends { index: number}>(collection: string) => (req, res) =>
    getAllFromDb<T>(collection)
        .then(items => sendJsonResponse(items, res))
        .catch(error => sendErrorResponse(res, error));

export const handleAdminQuery: GenericRequestHandler = <T extends { index: number}>(collection: string) => (req, res) =>
    getAllFromDbWithId<T>(collection)
        .then(items => sendJsonResponse(items, res))
        .catch(error => sendErrorResponse(res, error));

export const handleAddition: GenericRequestHandler = <T extends { index: number}>(collection: string) => (req, res) =>
    getRequestDataJson<T>(req)
        .then((item) => addItem<T>(collection, item))
        .then(reloadPagesAndApi)
        .then(() => getAllFromDbWithId<T>(collection))
        .then(items => sendJsonResponse(items, res))
        .catch(error => sendErrorResponse(res, error));

export const handleRemoval: GenericRequestHandler = <T extends { index: number }>(collection: string) => (req, res) =>
    getRequestDataJson<{ id: string, src: string[] }>(req)
        .then((request) => Promise.all<any>([ ...request.src.map(src => unlinkFile(src)), removeItem(collection, request.id) ]))
        .then(reloadPagesAndApi)
        .then(() => getAllFromDbWithId<T>(collection))
        .then(items => sendJsonResponse(items, res))
        .catch(error => sendErrorResponse(res, error));

export const handleUpdate: GenericRequestHandler = <T extends { index: number, _id: string }>(collection: string) => (req, res) =>
    getRequestDataJson<T[]>(req)
        .then((items) => updateMany(collection, items))
        .then(reloadPagesAndApi)
        .then(() => getAllFromDbWithId<T>(collection))
        .then(items => sendJsonResponse(items, res))
        .catch(error => sendErrorResponse(res, error));

const getAllFromDb = <T>(collectionName: string) =>
    db.collection<T>(collectionName).find({}, { projection: { _id: false }}).toArray() as Promise<T[]>;

const getAllFromDbWithId = <T>(collectionName: string) =>
    db.collection<T>(collectionName).find({}).toArray() as Promise<WithId<T>[]>;

const addItem = <T>(collectionName: string, item: T) =>
    // @ts-ignore
    db.collection<T>(collectionName).insertOne(item);

const removeItem = <T>(collectionName: string, id: string) =>
    db.collection<T>(collectionName).deleteOne({ _id: new ObjectId(id) } as FilterQuery<T>);

const updateMany = <T extends { _id: string }>(collectionName: string, items: T[]) => {
    const collection = db.collection<T>(collectionName);
    return Promise.all([ ...items.map(item => updateOne(item, collection)) ]);
};

const updateOne = <T extends { _id: string }>({ _id, ...$set }: T, collection: Collection<T>) => collection
    .findOneAndUpdate({ _id: new ObjectId(_id) } as FilterQuery<T>, { $set } as UpdateQuery<T>);
