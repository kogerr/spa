import { Product } from '../../models/product.interface';
import { Item } from '../../models/cart-item.interface';
import { getBasePricesByFamily } from '../../db/base-prices.dao';
import { transform } from '../factories/price.transformer';
import { BasePrices } from '../../models/base-prices.interface';
import { PriceBlock } from '../../models/price-block.interface';

export const validate = (products: Product[], items: Item[], total: number): Promise<void> => {
    matchTotalToProducts(products, total);
    matchItemsToProducts(products, items);
    return matchProductsToPrices(products);
};

const matchTotalToProducts = (products: Product[], total: number) => {
    if (products.reduce((a, b) => a + b.price, 0) !== total) {
        throw new Error('product prices and total do not match');
    }
};

const matchItemsToProducts = (products: Product[], items: Item[]) => {
    items.forEach(item => {
        if (!products.some(product => matches(item, product))) {
            throw new Error('products and items do not match');
        }
    });
};

const matches = (item: Item, product: Product) =>
    product.family === item.family && product.bundle === 'family' ||
    product.family === item.family && product.weight === item.weight && product.bundle === 'singlePair' ||
    product.family === item.family && product.weight === item.weight && product.style === item.style && product.bundle === 'singleStyle';

const matchProductsToPrices = (products: Product[]) => {
    const families = products.map(p => p.family).reduce<string[]>(filterUnique, []);
    return Promise.all(families.map(family => getPriceBlock(family)))
        .then(priceBlocks => new Map<string, PriceBlock[]>(priceBlocks))
        .then((priceBlocksMap) => verifyPrices(priceBlocksMap, products));
};

const getPriceBlock = (family: string) => getBasePricesByFamily(family)
        .then(bp => [ family, transform(bp as BasePrices) ] as [string, PriceBlock[]]);

const filterUnique = (previous: string[], current: string) => previous.includes(current) ? previous : [ ...previous, current ];

const verifyPrices = (priceBlocksMap: Map<string, PriceBlock[]>, products: Product[]) => {
    products.forEach(product => {
        const priceBlocks = priceBlocksMap.get(product.family) as PriceBlock[];
        if (!priceBlocks.some(priceBlock => priceBlockMatches(priceBlock, product))) {
            throw new Error('products and prices do not match');
        }
    });
};

const priceBlockMatches = (priceBlock: PriceBlock, product: Product) =>
    priceBlock.licenseSize === product.license &&
    priceBlock.platform === product.platform &&
    priceBlock[product.bundle as keyof PriceBlock] === product.price;
