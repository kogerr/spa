const oneHour = 3600000;

export const createTimeout = () => (new Date(Date.now() + oneHour))
    .toISOString()
    .split('.')[0] + '+00:00';
