const dashPositions = [ 8, 12, 16, 20 ];

export const generateUUID = () => {
    const randomCharacters = getRandomString() + getRandomString() + getRandomString();
    const rawId = randomCharacters.substring(randomCharacters.length - 32);
    return rawId.split('')
        .map((character, i) => dashPositions.includes(i) ? '-' + character : character)
        .join('');
};

const getRandomString = () => Math.random().toString(16).substring(2);
