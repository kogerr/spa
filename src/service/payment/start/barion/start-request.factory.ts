import { PaymentRequest } from '../payment-request.interface';
import { FundingSource, StartRequest } from '../../../../models/barion/start-request.interface';
import { Customer } from '../../../../models/customer.interface';
import { Address } from '../../../../models/barion/address.interface';
import { SimpleProduct } from '../../../../models/simple-product.interface';
import { TransactionItem } from '../../../../models/barion/transaction-item.interface';
import { PaymentTransaction } from '../../../../models/barion/payment-transaction.interface';

const POSKey = process.env.POSKEY || '41a8c935220e42d096b923911b0f685f';
const PaymentType = 'Immediate';
const GuestCheckOut = true;
const FundingSources: FundingSource[] = [ 'BankCard', 'GooglePay' ];
const redirectUrlBase = 'https://koger.io/payment-result?orderId=';
const CallbackUrl = 'https://koger.io/api/payment-callback';
const Locale = 'en-US';
const Currency = 'EUR';
const PayerAccountInformation = {};
const PurchaseInformation = {};
const ChallengePreference = 0;
const Payee = process.env.PAYEE || 'kogerr@hotmail.com';
const Quantity = 1;
const Unit = 'instance';

const createShippingAddress = (customer: Customer): Address =>
    ({ Country : customer.country,  City : customer.city,
        Zip : customer.zip,  Street : customer.address, FullName: customer.name });

const createItem = (product: SimpleProduct): TransactionItem => {
    const Name = product.title;
    const Description = product.title;
    const UnitPrice = product.price;
    const ItemTotal = product.price;

    return { Name, Description, Quantity, Unit, UnitPrice, ItemTotal };
};

const createTransaction = (paymentRequest: PaymentRequest, uuid: string): PaymentTransaction => {
    const POSTransactionId = uuid;
    const Total = paymentRequest.total;
    const Items = paymentRequest.products.map(createItem);

    return { POSTransactionId, Payee, Total, Items };
};

export const createBody = (paymentRequest: PaymentRequest, orderId: string, uuid: string): StartRequest => {
    const PaymentRequestId = uuid;
    const PayerHint = paymentRequest.customer.email;
    const CardHolderNameHint = paymentRequest.customer.name;
    const RedirectUrl = redirectUrlBase + orderId;
    const Transactions = [ createTransaction(paymentRequest, uuid) ];
    const OrderNumber = orderId;
    const ShippingAddress = createShippingAddress(paymentRequest.customer);
    const PayerPhoneNumber = paymentRequest.customer.phone;
    const PayerWorkPhoneNumber = PayerPhoneNumber;
    const PayerHomeNumber = PayerPhoneNumber;
    const BillingAddress = ShippingAddress;

    return { POSKey, PaymentType, GuestCheckOut, FundingSources, PaymentRequestId, PayerHint, CardHolderNameHint, RedirectUrl, CallbackUrl,
        Transactions, OrderNumber, ShippingAddress, Locale, Currency, PayerPhoneNumber, PayerWorkPhoneNumber, PayerHomeNumber,
        BillingAddress, PayerAccountInformation, PurchaseInformation, ChallengePreference,
    };
};
