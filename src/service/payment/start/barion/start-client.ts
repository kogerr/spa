import { request, RequestOptions } from 'https';
import { IncomingMessage } from 'http';
import { Credentials } from '../../../../models/credentials.interface';
import { StartRequest } from '../../../../models/barion/start-request.interface';
import { StartResponse } from '../../../../models/barion/start-response.interface';

const method = 'post';
const headers = { 'Content-Type': 'application/json' };
const host = process.env.BARION_HOST || 'api.test.barion.com';
const path = '/v2/Payment/Start';
const rejectUnauthorized = false;
const port = 443;

export const callStart = (startRequest: StartRequest, credentials: Credentials|undefined): Promise<StartResponse> => {
    const options: RequestOptions = { method, headers, host, path, rejectUnauthorized, port };
    const body = JSON.stringify(startRequest);

    if (credentials) {
        options.ca = credentials.ca;
        options.cert = credentials.cert;
        options.key = credentials.key;
    }

    return new Promise<StartResponse>((resolve, reject) => {
        const req = request(options, (res: IncomingMessage) => {
            let data = '';

            res.on('error', reject);
            res.on('data', (chunk) => data += chunk);
            res.on('end', () => res.statusCode === 200 ? resolve(JSON.parse(data)) : reject(JSON.parse(data)));
        });
        req.write(body);
        req.end();
    });
};
