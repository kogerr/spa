import { PaymentRequest } from '../payment-request.interface';
import { StartResponse } from '../../../../models/barion/start-response.interface';
import { createBody } from './start-request.factory';
import { getCredentials } from '../../../server/server';
import { callStart } from './start-client';
import { createOrderRef } from '../order-ref.provider';
import { generateUUID } from '../uuid-generator';
import { saveResult } from '../order-save';

export const initiateStart = (paymentRequest: PaymentRequest): Promise<StartResponse> => {
    const orderId = createOrderRef();
    const uuid = generateUUID();
    const startRequest = createBody(paymentRequest, orderId, uuid);
    const credentials = getCredentials();

    const $response = callStart(startRequest, credentials);
    $response.then((response) => setTimeout(() => saveResult(startRequest, response, paymentRequest), 0));

    return $response;
};
