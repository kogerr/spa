import { createSalt } from '../random-salt.provider';
import { createOrderRef } from './order-ref.provider';
import { createTimeout } from './timeout.provider';
import { Invoice, StartRequest } from './start-request.interface';
import { PaymentRequest } from './payment-request.interface';
import { SimpleProduct } from '../../../models/simple-product.interface';

const merchant = process.env.MERCHANT || 'PUBLICTESTHUF';
const sdkVersion = 'SimplePayV2.1_Payment_PHP_SDK_2.0.7_190701:dd236896400d7463677a82a47f53e36e';
const methods = [ 'CARD' ];
const url = 'https://koger.io/payment-result';
const threeDSReqAuthMethod = '01';
const language = 'EN';
const currency = 'HUF';
const amount = '1';

const transformProduct = (product: SimpleProduct) => {
    return { title: product.title, description: product.title, amount, price: product.price.toString() };
};

export const createBody = (paymentRequest: PaymentRequest): StartRequest => {
    const salt = createSalt();
    const orderRef = createOrderRef();
    const timeout = createTimeout();
    const customerEmail = paymentRequest.customer.email;
    const total = paymentRequest.total.toString();
    const { name, country, city, address, zip, phone } = paymentRequest.customer;
    const invoice: Invoice = { name, country, city, address, zip, phone, threeDSReqAuthMethod };
    const items = paymentRequest.products.map(transformProduct);

    return { salt, merchant, orderRef, sdkVersion, methods, timeout, url, language,
        currency, customerEmail, total, invoice, items };
};
