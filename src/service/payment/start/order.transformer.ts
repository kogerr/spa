import { Order } from '../../../models/order.interface';
import { PaymentRequest } from './payment-request.interface';

export const createOrder = (orderId: string, paymentId: string, paymentRequest: PaymentRequest): Order => ({
    orderId,
    paymentId,
    customer: paymentRequest.customer,
    items: paymentRequest.items,
    products: paymentRequest.products,
    total: paymentRequest.total,
    status: 'pending',
    updated: Date.now()
});
