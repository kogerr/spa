import { OutgoingHttpHeaders } from 'http';
import { create } from '../signature.factory';
import { StartRequest } from './start-request.interface';

const method = 'POST';

export type RequestStub = { method: string, body: string, headers: OutgoingHttpHeaders };

export const createRequest = (startRequest: StartRequest): RequestStub => {
    const body = JSON.stringify(startRequest);
    const signature = create(body);
    const headers = { 'Content-Type': 'application/json', 'Signature': signature };

    return { method, headers, body };
};
