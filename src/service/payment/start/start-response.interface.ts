export type StartResponse = SuccessStartResponse | ErrorStartResponse;

export interface SuccessStartResponse {
    salt: string;
    transactionId: number;
    merchant: string;
    orderRef: string;
    currency: string;
    timeout: string;
    total: number;
    paymentUrl: string;
}

export interface ErrorStartResponse {
    salt: string;
    errorCodes: number[];
    merchant: string;
    orderRef: string;
    currency: string;
    total: number;
}
