import { PaymentRequest } from './payment-request.interface';
import { StartResponse } from './start-response.interface';
import { createBody } from './start-request.factory';
import { getCredentials } from '../../server/server';
import { callStart } from './start-client';
import { createRequest } from './request-stub.factory';

export const initiateStart = (paymentRequest: PaymentRequest): Promise<StartResponse> => {
    const startRequest = createBody(paymentRequest);
    const requestStub = createRequest(startRequest);
    const credentials = getCredentials();

    return callStart(requestStub, credentials);
};
