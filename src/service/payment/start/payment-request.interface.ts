import { Item } from '../../../models/cart-item.interface';
import { Customer } from '../../../models/customer.interface';
import { SimpleProduct } from '../../../models/simple-product.interface';

export interface PaymentRequest {
    customer: Customer;
    total: number;
    products: SimpleProduct[];
    items: Item[];
}
