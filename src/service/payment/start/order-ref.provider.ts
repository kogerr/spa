const base = 1640991600000;
const radix = 32;

export const createOrderRef = () => (Date.now() - base).toString(radix);
