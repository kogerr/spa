export interface StartRequest {
    salt: string;
    merchant: string;
    orderRef: string;
    currency: string;
    customerEmail: string;
    language: string;
    sdkVersion: string;
    methods: string[];
    total: string;
    timeout: string;
    url: string;
    invoice?: Invoice;
    items?: Item[]
}

export interface Invoice {
    name: string;
    company?: string;
    country: string;
    city: string;
    zip: string;
    address: string;
    address2?: string;
    phone?: string;
    threeDSReqAuthMethod?: '01'|'02'|'05';
}

export interface Item {
    ref?: string;
    title: string;
    description?: string;
    amount: string;
    price: string;
    tax?: string;
}
