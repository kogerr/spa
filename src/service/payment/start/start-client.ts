import { request, RequestOptions } from 'https';
import { IncomingMessage } from 'http';
import { RequestStub } from './request-stub.factory';
import { StartResponse } from './start-response.interface';
import { Credentials } from '../../../models/credentials.interface';

const method = 'post';
const host = 'sandbox.simplepay.hu';
const path = '/payment/v2/start';
const rejectUnauthorized = false;
const port = 443;

export const callStart = (startRequest: RequestStub, credentials: Credentials|undefined): Promise<StartResponse> => {
    const headers = startRequest.headers;
    const options: RequestOptions = { method, headers, host, path, rejectUnauthorized, port };

    if (credentials) {
        options.ca = credentials.ca;
        options.cert = credentials.cert;
        options.key = credentials.key;
    }

    return new Promise<StartResponse>((resolve, reject) => {
        const req = request(options, (res: IncomingMessage) => {
            let data = '';

            if (res.statusCode !== 200) {
                reject(res.headers);
            }

            res.on('error', reject);
            res.on('data', (chunk) => data += chunk);
            res.on('end', () => resolve(JSON.parse(data)));
        });
        req.write(startRequest.body);
        req.end();
    });
};
