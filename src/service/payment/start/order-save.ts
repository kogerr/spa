import { StartRequest } from '../../../models/barion/start-request.interface';
import { StartResponse } from '../../../models/barion/start-response.interface';
import { createOrder } from './order.transformer';
import { addOrder } from '../../../db/order.dao';
import { InsertOneWriteOpResult, WithId } from 'mongodb';
import { addError } from '../../../db/start-error.dao';
import { Order } from '../../../models/order.interface';
import { StartError } from '../../../models/start-error.interface';
import { PaymentRequest } from './payment-request.interface';

export const saveResult = (startRequest: StartRequest, startResponse: StartResponse, paymentRequest: PaymentRequest) => {
    let $saveResult: Promise<InsertOneWriteOpResult<WithId<Order|StartError>>>;

    if ('PaymentId' in startResponse) {
        $saveResult = addOrder(createOrder(startRequest.OrderNumber, startResponse.PaymentId, paymentRequest));
    } else {
        $saveResult = addError(startRequest, startResponse);
    }

    return $saveResult.catch(console.error);
};
