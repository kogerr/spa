import { createHmac } from 'crypto';

const key = 'FxDa5w314kLlNseq2sKuVwaqZshZT5d6';
const encoding = 'base64';

export const create = (data: string) => createHmac('sha384', key)
    .update(data)
    .digest(encoding);
