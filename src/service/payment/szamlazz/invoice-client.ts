import { getCredentials } from '../../server/server';
import { request, RequestOptions } from 'https';
import { IncomingMessage } from 'http';

const method = 'POST';
const host = 'www.szamlazz.hu';
const path = '/szamla/';
const rejectUnauthorized = false;
const port = 443;

export const requestInvoice = (payload: string) => {
    const body = 'action-xmlagentxmlfile=' + payload;
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'User-Agent': 'curl/7.79.1',
        'Content-Length': Buffer.byteLength(body)
    };
    const options: RequestOptions = { method, headers, rejectUnauthorized, host, path, port };
    const credentials = getCredentials();
    if (credentials) {
        options.ca = credentials.ca;
        options.cert = credentials.cert;
        options.key = credentials.key;
    }

    return new Promise<string>((resolve, reject) => {
        const req = request(options, (res: IncomingMessage) => {
            let data: Buffer = Buffer.from([]);

            res.on('error', (error) => { console.error(error); reject(error); });
            res.on('data', (chunk: Buffer) => { data = Buffer.concat([ data, chunk ]) });
            res.on('end', () => {
                res.statusCode === 200 ? resolve(data.toString('utf8')) : reject(data);
            });
        });
        req.write(body);
        req.end();
    });
};
