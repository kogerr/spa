import { InvoiceContext } from '../../../models/invoice-context.interface';
import { InvoiceItem } from '../../../models/invoice-item.interface';
import { SimpleProduct } from '../../../models/simple-product.interface';
import { Order } from '../../../models/order.interface';

const settings = {
    szamlaagentkulcs: 'tudqpu2aiv4zgudqpu2sx75t3udqpu2umhhynudqpu',
    eszamla: true,
    szamlaLetoltes: true,
    valaszVerzio: 2,
    szamlaNyelve: 'en'
};
const seller = {};
const fizmod = 'Átutalás';
const szamlaNyelve = 'en';
const mennyiseg = 1;
const mennyisegiEgyseg = 'license';
const afakulcs = 27;

export const createInvoiceContext = (order: Order): InvoiceContext => ({
    settings,
    header: {
        date: new Date().toISOString().substring(0, 10),
        fizmod,
        penznem: 'EUR',
        szamlaNyelve
    },
    seller,
    buyer: {
        nev: order.customer.name,
        orszag: order.customer.country,
        irsz: order.customer.zip,
        telepules: order.customer.city,
        cim: order.customer.address,
        email: order.customer.email,
        sendEmail: true,
        adoszam: order.customer.vat
    },
    items: order.products.map(product => convertProduct(product))
});

// TODO: revise all numbers
const convertProduct = (product: SimpleProduct): InvoiceItem => ({
    megnevezes: product.title,
    mennyiseg,
    mennyisegiEgyseg,
    nettoEgysegar: product.price,
    afakulcs,
    nettoErtek: product.price,
    afaErtek: product.price * 0.27,
    bruttoErtek: product.price * 1.27
});
