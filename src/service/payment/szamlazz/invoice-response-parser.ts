import { InvoiceResponse } from '../../../models/invoice-response.interface';

export const parseInvoiceResponse = (response: string): InvoiceResponse => {
    const sikeres = extractField(response, 'sikeres') === 'true';
    const hibakod = extractField(response, 'hibakod');
    const hibauzenet = extractField(response, 'hibauzenet');
    const szamlaszam = extractField(response, 'szamlaszam');
    const szamlanetto = extractNumericField(response, 'szamlanetto');
    const szamlabrutto = extractNumericField(response, 'szamlabrutto');
    const kintlevoseg = extractNumericField(response, 'kintlevoseg');
    const vevoifiokurl = extractField(response, 'vevoifiokurl');
    const pdf = extractField(response, 'pdf');

    const transformedResponse = { sikeres, hibakod, hibauzenet, szamlaszam, szamlanetto, szamlabrutto, kintlevoseg, vevoifiokurl, pdf };

    Object.keys(transformedResponse).forEach(key => {
        // @ts-ignore
        if (transformedResponse[key] === undefined) {
            // @ts-ignore
            delete transformedResponse[key];
        }
    });

    return transformedResponse;
}

const extractField = (response: string, field: string) => {
    const match = findField(response, field);
    return match && match[1] ? match[1] : undefined;
};

const extractNumericField = (response: string, field: string) => {
    const match = findField(response, field);
    return match && match[1] ? parseFloat(match[1]) : undefined;
};

const findField = (response: string, field: string) => response.match(new RegExp(`<${field}>(.+?)<\/${field}>`, 's'));
