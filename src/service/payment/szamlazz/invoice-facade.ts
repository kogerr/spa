import { createInvoiceContext}  from './invoice-context.factory';
import { renderInvoiceRequest } from '../../../components/invoice-request/invoice-request';
import { requestInvoice } from './invoice-client';
import { parseInvoiceResponse } from './invoice-response-parser';
import { Order } from '../../../models/order.interface';
import { addInvoiceResponse } from '../../../db/invoice-response.dao';

export const initiateInvoice = (order: Order) => {
    const context = createInvoiceContext(order);
    const requestPayload = renderInvoiceRequest(context);

    return requestInvoice(requestPayload)
        .then(parseInvoiceResponse)
        .then(response => {
            const { pdf, ...rest } = response;
            addInvoiceResponse({ orderId: order.orderId, ...rest }).catch(console.error);
            return pdf;
        });
};
