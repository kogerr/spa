import { PaymentResult, RawPaymentResult } from '../../../models/payment-result.interface';

export const transformResult = (raw: RawPaymentResult): PaymentResult => ({
    result: raw.r,
    transactionId: raw.t,
    event: raw.e,
    merchant: raw.m,
    orderId: raw.o
});
