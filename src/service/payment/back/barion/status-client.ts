import { request, RequestOptions } from 'https';
import { IncomingMessage } from 'http';
import { Credentials } from '../../../../models/credentials.interface';
import { StatusResponse } from '../../../../models/barion/status-response.interface';

const method = 'get';
const headers = { 'Content-Type': 'application/json' };
const host = process.env.BARION_HOST || 'api.test.barion.com';
const pathBase = '/v2/Payment/GetPaymentState?';
const rejectUnauthorized = false;
const port = 443;
const POSKey = process.env.POSKEY || '41a8c935220e42d096b923911b0f685f';

export const getStatus = (paymentId: string, credentials?: Credentials): Promise<StatusResponse> => {
    const path = pathBase + 'POSKey=' + POSKey + '&PaymentId=' + paymentId;
    const options: RequestOptions = { method, headers, host, path, rejectUnauthorized, port };

    if (credentials) {
        options.ca = credentials.ca;
        options.cert = credentials.cert;
        options.key = credentials.key;
    }

    return new Promise<StatusResponse>((resolve, reject) => {
        const req = request(options, (res: IncomingMessage) => {
            let data = '';

            res.on('error', reject);
            res.on('data', (chunk) => data += chunk);
            res.on('end', () => res.statusCode === 200 ? resolve(JSON.parse(data)) : reject(JSON.parse(data)));
        });
        req.end();
    });
};
