import { getOrderByOrderId, updateOrder } from '../../../../db/order.dao';
import { getCredentials } from '../../../server/server';
import { getStatus } from './status-client';
import { initiateInvoice } from '../../szamlazz/invoice-facade';
import { Order } from '../../../../models/order.interface';
import { serveOrder } from '../order-serve.service';

export const queryStatus = async (orderId: string) => {
    const order = await getOrderByOrderId(orderId);
    if (order === null) {
        throw Error(`Order ${orderId} not found`);
    }

    const status = await getStatus(order.paymentId, getCredentials());
    updateOrder(orderId, status.Status.toString(), toEpoch(status.CompletedAt))
        .then(savedOrder => {
            if (status.Status.toString() === 'Succeeded') {
                initiateInvoice(savedOrder.value as Order).then(pdf => {
                    serveOrder(savedOrder.value as Order, pdf);
                });
            }
        });

    return { order, status };
};

const toEpoch = (date: string) => new Date(date).valueOf();
