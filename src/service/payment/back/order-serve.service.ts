import { Order } from '../../../models/order.interface';
import { getSmtpProperties } from '../../../db/smtp.dao';
import { sendEmail } from '../../email/smtp.client';
import { renderConfirmationMail } from '../../../components/confirmation-mail/confirmation-mail';
import { Base64Attachment } from '../../../models/attachment.interface';
import { Item } from '../../../models/cart-item.interface';
import { findFont } from '../../../db/font.dao';
import { Font } from '../../../models/font.interface';

const errorMessage = 'Could not get Smtp properties';
type FontNameAndData = { filename: string, data: string };

const formats = new Map([
    [ 'woff', 'woff' ],
    [ 'woff2', 'woff2' ],
    [ 'otf', 'opentype' ],
    [ 'ttf', 'truetype' ],
    [ 'eot', 'embedded-opentype' ],
    [ 'svg', 'svg' ]
]);

export const serveOrder = async (order: Order, pdf?: string) => {
    const content = renderConfirmationMail(order);
    const to = order.customer.email;
    const attachments = order.items ? await createAttachments(order.items) : [];
    if (pdf) {
        attachments.push(createInvoiceAttachment(pdf));
    }
    const smtpProperties = await getSmtpProperties();

    return smtpProperties ? sendEmail(to, content, smtpProperties, ...attachments) : console.error(errorMessage, JSON.stringify(order))
};

const createAttachments = (items: Item[]): Promise<Base64Attachment[]> =>
    Promise.all(items.map(item => findFont(item.family, item.weight, item.style === 'italic')))
        .then(fonts => reduceFonts(fonts))
        .then((fonts: FontNameAndData[]) => fonts.map(font => convertFontFace(font)));

const reduceFonts = (fonts: (Font|null)[]) =>
    (fonts.filter(f => !!f) as Font[])
      .reduce((previousValue: FontNameAndData[], currentValue: Font) =>
          [ ...previousValue, currentValue.webfont, ...currentValue.fullFonts ], [] as FontNameAndData[]);

const convertFontFace = ({ filename, data }: FontNameAndData): Base64Attachment => ({
    content: data,
    contentType: 'font/' + findFormat(filename),
    filename,
});

const findFormat = (filename: string) => {
    const match = filename.match(/\.([^.]+?$)/);
    const extension = (match && match.length) ? match[1] : 'woff';
    return formats.get(extension);
}

const createInvoiceAttachment = (pdf: string): Base64Attachment => ({
    content: pdf,
    contentType: 'application/pdf',
    filename: 'invoice.pdf'
});
