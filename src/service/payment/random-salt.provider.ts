const tenOnTheEighteenth = Math.pow(10, 18);
const targetLength = 32;
const fillString = '0';

const createRandomString = () => (tenOnTheEighteenth * Math.random()).toString();

const createEpochString = () => (Date.now()).toString();

export const createSalt = () => (createRandomString() + createEpochString()).padStart(targetLength, fillString);
