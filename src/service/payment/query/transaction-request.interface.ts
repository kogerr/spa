export interface TransactionRequest {
    merchant: string;
    detailed?: boolean;
    transactionIds: string[];
    salt: string;
    sdkVersion: string;
}
