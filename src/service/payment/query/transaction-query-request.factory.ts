import { createSalt } from '../random-salt.provider';
import { TransactionRequest } from './transaction-request.interface';

const merchant = process.env.MERCHANT || 'PUBLICTESTHUF';
const detailed = true;
const sdkVersion = 'SimplePayV2.1_Payment_PHP_SDK_2.0.7_190701:dd236896400d7463677a82a47f53e36e';

export const createDetailsRequest = (transactionIds: string[]): TransactionRequest => {
    const salt = createSalt();

    return { merchant, detailed, transactionIds, salt, sdkVersion };
};
