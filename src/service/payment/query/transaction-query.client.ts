import { request, RequestOptions } from 'https';
import { IncomingMessage, OutgoingHttpHeaders } from 'http';
import { Credentials } from '../../../models/credentials.interface';

const method = 'post';
const host = 'sandbox.simplepay.hu';
const path = '/payment/v2/query';
const rejectUnauthorized = false;
const port = 443;

export const callTransactionQuery = (body: string, headers: OutgoingHttpHeaders, credentials: Credentials|undefined): Promise<string> => {
    const options: RequestOptions = { method, headers, host, path, rejectUnauthorized, port };

    if (credentials) {
        options.ca = credentials.ca;
        options.cert = credentials.cert;
        options.key = credentials.key;
    }

    return new Promise<string>((resolve, reject) => {
        const req = request(options, (res: IncomingMessage) => {
            let data = '';

            if (res.statusCode !== 200) {
                reject(res.headers);
            }

            res.on('error', reject);
            res.on('data', (chunk) => data += chunk);
            res.on('end', () => resolve(data));
        });
        req.write(body);
        req.end();
    });
};
