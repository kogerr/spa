import { createDetailsRequest } from './transaction-query-request.factory';
import { create } from '../signature.factory';
import { getCredentials } from '../../server/server';
import { OutgoingHttpHeaders } from 'http';
import { callTransactionQuery } from './transaction-query.client';

export const queryTransactionDetails = (...transactionIds: string[]) => {
    const body = JSON.stringify(createDetailsRequest(transactionIds));
    const signature = create(body);
    const headers: OutgoingHttpHeaders = { 'Content-Type': 'application/json', 'Signature': signature };
    const credentials = getCredentials();

    return callTransactionQuery(body, headers, credentials);
};
