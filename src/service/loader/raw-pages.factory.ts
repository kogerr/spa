import { renderEmailTestPage } from '../../components/admin/email-edge/email-edge';
import { renderFontFaceUploadPage } from '../../components/admin/font-face-upload/font-face-upload';
import { renderUploadPage } from '../../components/admin/upload-page/upload-page';
import { renderLanding } from '../../components/landing/landing';
import { renderLogin } from '../../components/login/login';
import { renderNewsFeed } from '../../components/newsfeed/newsfeed';
import { renderPhilosophy } from '../../components/philosophy/philosophy';
import { renderRegistration } from '../../components/registration/registration';
import { renderCheckout } from '../../components/checkout/checkout';
import { renderAdminMenu } from '../../components/admin/admin-menu/admin-menu';
import { render404 } from '../../components/404/404';
import { renderLandingSlideManager } from '../../components/admin/landing-slide-manager/landing-slide-manager';
import { renderProjectEditor } from '../../components/admin/project-editor/project-editor';
import { renderProjectManager } from '../../components/admin/project-manager/project-manager';
import { getProjects } from '../../db/project.dao';
import { renderProjectPage } from '../../components/project/project-page';
import { renderProjectSlidesPage } from '../../components/admin/project-slides-page/project-slides-page';
import { renderFontManagerPage } from '../../components/admin/font-manager/font-manager';
import { renderTextManagerPage } from '../../components/admin/text-manager/text-manager';
import { renderSandboxPage } from '../../components/admin/sandbox/sandbox';
import { renderPriceDetails } from '../../components/admin/price-details/price-details';
import { renderPricesComponent } from '../../components/admin/prices/prices.component';
import { renderCart } from '../../components/cart/cart';

export const createRawPages = async () => {
    const rawPages = await renderPublicPages();
    const rawAdminPages = renderAdminPages();

    return { rawPages, rawAdminPages };
};

const renderPublicPages = async () =>
    [ await renderLanding(), renderPhilosophy(), await renderNewsFeed(), renderLogin(), renderRegistration(),
        renderCheckout(), renderCart(), renderAdminMenu(), render404(), ...(await getProjectPages()) ];

const getProjectPages = () => getProjects().then(projects => projects.map(project => renderProjectPage(project)));

const renderAdminPages = () => [ renderFontFaceUploadPage(), renderUploadPage(), renderEmailTestPage(), renderLandingSlideManager(),
    renderProjectEditor(), renderProjectManager(), renderProjectSlidesPage(), renderFontManagerPage(), renderTextManagerPage(),
    renderSandboxPage(), renderPriceDetails(), renderPricesComponent() ];
