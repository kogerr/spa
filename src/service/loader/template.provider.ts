import { NamedFile } from '../../models/named-file.interface';
import { readFromPath } from '../util/file-reader';

const templatesDirectory = './src/components';

export const readTemplates = () =>
    readFromPath(templatesDirectory)
        .then(filterHtml)
        .then(addBackticks);

const filterHtml = (files: NamedFile[]) => files.filter((file) => file.name.endsWith('.html'));

const addBackticks = (files: NamedFile[]) => files.map((file) => ({ name: file.name, path: file.path, content: '`' + file.content + '`' }));
