import { renderMainframe } from '../../components/mainframe/mainframe';
import { Page } from '../../models/page.interface';
import { api, assets, fullPages, hiddenAssets, templates } from '../container';
import { createRawPages } from './raw-pages.factory';
import { createStaticApi } from './static-api.factory';
import { readAssets } from './asset.provider';
import { readTemplates } from './template.provider';
import { compress } from './compressor';

export const initContainer = () =>
    saveTemplates()
        .then(renderRawPages)
        .then(refreshStaticApi)
        .then(renderFullPages)
        .then(renderAssets);

const saveTemplates = () => readTemplates()
    .then((entries) => entries.forEach((template) => templates.set(template.name, template.content)));

const renderRawPages = () => createRawPages();

const refreshStaticApi = (pages: { rawPages: Page[], rawAdminPages: Page[] }) => {
    createStaticApi(pages.rawPages).then((routes) => routes.forEach(([route, data]) => {
        compress(data).then((content) => api.set(route, content));
    }));

    return pages;
};

const renderFullPages = (pages: { rawPages: Page[], rawAdminPages: Page[] }) => {
    [...pages.rawPages, ...pages.rawAdminPages].forEach((rawPage: Page) => {
        const fullPage = renderMainframe(rawPage);
        compress(fullPage.content)
            .then((compressedContent) => fullPages.set(fullPage.path, compressedContent));
    });
};

export const renderAssets = () => {
    return readAssets()
        .then((values) => values.forEach((asset) => {
            if (asset.asset.hidden) {
                hiddenAssets.set(asset.urlPath, asset.asset);
            } else {
                assets.set(asset.urlPath, asset.asset);
            }
        }));
};

export const reloadPagesAndApi = () =>
    renderRawPages()
    .then(refreshStaticApi)
    .then(renderFullPages)
    .catch(console.error);
