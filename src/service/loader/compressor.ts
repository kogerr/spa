import { gzip, InputType } from 'zlib';

export const compress = (data: InputType, encoding: BufferEncoding = 'binary'): Promise<string> => new Promise((resolve, reject) => {
    gzip(data, (error: Error | null, result: Buffer) => {
        if (!error) {
            resolve(result.toString(encoding));
        } else {
            reject(error);
        }
    });
});
