import { Page } from '../../models/page.interface';
import { createProjectExtrasEndpoints } from '../factories/project-extras-factory';

export const createStaticApi = async (rawPages: Page[]): Promise<[string, string][]> => {
    const staticApi: [string, string][] = [
        ['/api/pages', JSON.stringify(rawPages)],
    ];

    const projectExtrasEndpoints = await createProjectExtrasEndpoints();
    staticApi.push(...projectExtrasEndpoints);

    return staticApi;
};
