import { createHash } from 'crypto';
import { stat } from 'fs';
import { Asset } from '../../models/asset.interface';
import { NamedFile } from '../../models/named-file.interface';
import { compress } from './compressor';
import { readFromPath } from '../util/file-reader';
import { mimeTypes } from '../providers/mime-types';

const assetsDirectory = './src/assets';

export const readAssets = (): Promise<{ urlPath: string, asset: Asset }[]> =>
    readFromPath(assetsDirectory).then((files) => Promise.all(files.map(createAsset)));

const createAsset = (file: NamedFile) => new Promise<{ urlPath: string, asset: Asset }>((resolve, reject) => {
    const filename = file.name;
    console.log('Loaded asset ' + file.path + '/' + filename);
    const urlPath = file.path.replace(assetsDirectory, '') + '/' + filename;
    const contentType = getContentType(urlPath);
    const eTag = createHash('md5').update(file.content).digest('hex');
    const hidden = isHidden(filename);

    stat(file.path + '/' + filename, (error, stats) => {
        if (error) {
            reject(error);
        } else {
            (filename.endsWith('.b64') ? Promise.resolve(file.content.toString()) : compress(file.content))
                .then((content) => resolve({urlPath, asset: { filename, content, contentType, lastModified: stats.mtime, eTag, hidden}}));
        }
    });
});

const getContentType = (path: string): string => {
    const extension = path.slice(path.lastIndexOf('.'));
    const contentType = mimeTypes.get(extension);

    return contentType === undefined ? '' : contentType;
};

const isHidden = (path: string) => path.endsWith('.ttf');
