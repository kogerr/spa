import { randomBytes } from 'crypto';
import { connect } from 'net';
import { Base64Attachment, BufferAttachment } from '../../models/attachment.interface';
import { SmtpProperties } from '../../models/smtp-properties.interface';

const port = 465;

export const sendEmailBufferAttachment = (to: string, htmlContent: string, properties: SmtpProperties, ...files: BufferAttachment[]) => {
    const base64Files = files.map(a => ({ contentType: a.contentType, filename: a.filename, content: a.content.toString('base64') }));

    return sendEmail(to, htmlContent, properties, ...base64Files);
};

export const sendEmail = (to: string, htmlContent: string, properties: SmtpProperties, ...files: Base64Attachment[]): Promise<void> => {
    const commands = createCommands(to, htmlContent, properties, files);
    return createSocket(commands, properties.host);
};

const createCommands = (to: string, htmlContent: string, properties: SmtpProperties, files: Base64Attachment[]) => [
    'EHLO ' + properties.host + '\r\n',
    'AUTH PLAIN ' + properties.credentials + '\r\n',
    'MAIL FROM:<' + properties.from + '>\r\n',
    'RCPT TO:<' + to + '>\r\n',
    'DATA\r\n',
    createData(properties.from, to, htmlContent, files),
    'QUIT\r\n',
];

const createData = (from: string, to: string, htmlContent: string, files: Base64Attachment[]) =>
    `FROM: <${from}>
TO: <${to}>
SUBJECT: Order Confirmation
Date: ${getDate()}
Content-Type: multipart/alternative; boundary=hatarvonal
MIME-Version: 1.0
Message-Id: ${getMessageId()}

--hatarvonal
Content-Type: text/plain; charset="UTF-8";

Hi, this is the plain text version of this email
--hatarvonal
Content-Type: text/html; charset="UTF-8";

${htmlContent}
${files.map(createAttachment).join('\r\n')}
--hatarvonal--
.
`;

const createAttachment = (attachment: Base64Attachment) =>
    `--hatarvonal
Content-Type: ${attachment.contentType}
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="${attachment.filename}"

${split(attachment.content)}
`;

const split = (content: string) => content.match(/.{1,70}/g)?.join('\r\n') || '';

const getDate = () => {
    const dateParts = (new Date()).toString().split(' ');

    return [(dateParts[0] + ','), dateParts[2], dateParts[1], dateParts[3], dateParts[4], '+0000'].join(' ');
};

const getMessageId = () => '<' + randomBytes(16).toString('hex') + '@mail.koger.io>';

const createSocket = (commands: (string | Buffer)[], host: string) => new Promise<void>((resolve, reject) => {
    const socket = connect({ host, port });

    socket.setEncoding('utf8');
    socket.on('error', (error: Error) => { console.error(error); reject(error); });
    socket.on('connect', () => console.log('connected'));
    socket.on('lookup', () => console.log('lookup'));
    socket.on('close', () => console.log('close'));
    socket.on('end', () => resolve());

    socket.on('data', (data: Buffer | string) => {
        const response = data.toString('binary');
        console.log(response);
        if (response.includes('queued')) {
            resolve();
        } else if (response.includes('Error') || response.includes('break') || response.includes('reject')) {
            console.error(response);
            reject(response);
        }
        const command = commands.splice(0, 1)[0];
        if (command) {
            socket.write(command);
        }
    });

    return socket;
});
