import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';
import { getLandingSlides } from '../../db/landing-slide.dao';
import { LandingSlide } from '../../models/landing-slide.interface';
import { getPreviewImages } from '../../db/project.dao';
import { renderLandingProjectPreview } from './project-preview/project-preview';

const path = '/';
const title = 'koger.io';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'landing.main.html';
const metadataTemplateKey = 'landing.metadata.html';

const getImages = (slides: LandingSlide[]) => slides.map((slide, i) =>
    `<a href="${slide.link}" style="left: ${i * 100}%"><img src="${slide.src}" alt="${slide.alt}" /></a>`).join('');

export const renderLanding = async (): Promise<Page> => {
    const slides = await getLandingSlides();
    const previews = await getPreviewImages();
    const previewWrappers = previews.sort((a, b) => a.index - b.index).map(preview => renderLandingProjectPreview(preview)).join('');
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
