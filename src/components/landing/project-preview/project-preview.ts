import { templates } from '../../../service/container';

const mainTemplateKey = 'project-preview.main.html';

export const renderLandingProjectPreview = (preview: { url: string, previewImage: string }) => {
    return eval(templates.get(mainTemplateKey) as string) as string;
};
