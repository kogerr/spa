import { templates } from '../../../service/container';
import { ProjectDetails } from '../../../models/project/project-details.interface';

const templateKey = 'project-about.html';

export const renderProjectAbout = (shortName: string, projectDetails: ProjectDetails): string => {
    return eval(templates.get(templateKey) as string) as string;
};
