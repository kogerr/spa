import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';
import { Project } from '../../models/project/project.interface';
import { renderProjectAbout } from './project-about/project-about';
import { IndexedSlide } from '../../models/project/indexed-slide.interface';

const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'project.main.html';
const metadataTemplateKey = 'project.metadata.html';

const getImage = (slide: IndexedSlide) =>
    `<img style="left: 0" src="${slide.src}" alt="alt" />`;

export const renderProjectPage = (project: Project): Page => {
    const path = '/fonts/' + project.url;
    const title = project.pageTitle;
    const url = 'https://koger.io/' + project.url;
    const about = renderProjectAbout(project.shortName, project.details);
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
