import { templates } from '../../service/container';
import { Order } from '../../models/order.interface';
import { SimpleProduct } from '../../models/simple-product.interface';

const mainTemplateKey = 'confirmation-mail.full.html';

const getDate = (updated: number) => new Date(updated).toDateString();

const getProducts = (products: SimpleProduct[]) => products
    .map(product => `<tr><td style="padding: 16px">${product.title}</td><td style="text-align: right; padding: 16px">${product.price} €</td></tr>`)
    .join('\n');

export const renderConfirmationMail = (order: Order): string => {
    return eval(templates.get(mainTemplateKey) as string) as string;
};
