import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/project-slides';
const title = 'Project Slides';
const scripts: string[] = [ '/scripts/admin/project-slides-manager.js' ];
const styles: string[] = [ '/styles/landing-slide-manager.css' ];
const metadata = '';
const mainTemplateKey = 'project-slides-page.main.html';

export const renderProjectSlidesPage = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
