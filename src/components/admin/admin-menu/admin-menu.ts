import { Page } from '../../../models/page.interface';
import { templates } from '../../../service/container';

const path = '/admin';
const title = 'Admin Platform';
const scripts: string[] = [];
const styles: string[] = [ '/styles/admin-menu.css' ];
const mainTemplateKey = 'admin-menu.main.html';
const metadata = '';

export const renderAdminMenu = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
