import { Order } from '../../../models/order.interface';
import { templates } from '../../../service/container';

const mainTemplateKey = 'order.template.html';

export const renderOrder = (order: Order): string => {
    const date = order.updated ?
        (new Date(order.updated)).toISOString().replace('T', ' ').substring(0, 16) : '2020-12-xx yy:zz';
    const statusClass = order.status.toLowerCase();

    return eval(templates.get(mainTemplateKey) as string) as string;
};
