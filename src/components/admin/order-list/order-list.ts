import { Page } from '../../../models/page.interface';
import { templates } from '../../../service/container';
import { Order } from '../../../models/order.interface';
import { renderOrder } from './order';

const path = '/admin/order-list';
const title = 'Order List';
const scripts: string[] = [ '/scripts/order-list.js' ];
const styles: string[] = [ '/styles/order-list.css' ];
const metadata = '';
const mainTemplateKey = 'order-list.main.html';

export const renderOrderList = (orders: Order[]): Page => {
    const orderList = orders.map(renderOrder).join('');
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
