import { Item } from '../../../service/payment/start/start-request.interface';
import { templates } from '../../../service/container';

const mainTemplateKey = 'items.template.html';

export const renderItems = (items: Item[]) => {
    const itemLines = items.map(item => `<tr><td>${item.ref}</td><td>${item.title}</td><td>${item.price}</td></tr>`).join('');
    const total = items.map(item => parseInt(item.price, 10)).reduce((a, b) => a + b);

    return eval(templates.get(mainTemplateKey) as string) as string;
};
