import { templates } from '../../../service/container';
import { Invoice } from '../../../service/payment/start/start-request.interface';

const mainTemplateKey = 'invoice.template.html';

export const renderInvoice = (invoice: Invoice): string => {
    const address = [ invoice.country, invoice.zip, invoice.city, invoice.address ].join(' ');
    return  eval(templates.get(mainTemplateKey) as string) as string;
};
