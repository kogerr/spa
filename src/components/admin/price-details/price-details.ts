import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/price-details';
const title = 'Price Details';
const scripts: string[] = [ '/scripts/admin/price-details.js' ];
const styles: string[] = [ '/styles/price-details.css' ];
const metadata = '';
const mainTemplateKey = 'price-details.main.html';

export const renderPriceDetails = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
