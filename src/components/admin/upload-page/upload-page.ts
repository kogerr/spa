import { Page } from '../../../models/page.interface';
import { templates } from '../../../service/container';

const path = '/admin/upload';
const title = 'Upload Page';
const scripts: string[] = [ '/scripts/upload.js' ];
const styles: string[] = [];
const mainTemplateKey = 'upload-page.main.html';
const metadataTemplateKey = 'upload-page.metadata.html';

export const renderUploadPage = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
