import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/text-manager';
const title = 'Placeholder Texts';
const scripts: string[] = [ '/scripts/admin/text-manager.js' ];
const styles: string[] = [ '/styles/text-manager.css' ];
const metadata = '';
const mainTemplateKey = 'text-manager.main.html';

export const renderTextManagerPage = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
