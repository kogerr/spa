import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/main-slides';
const title = 'Main Page Slides';
const scripts: string[] = [ '/scripts/admin/landing-slide-manager.js' ];
const styles: string[] = [ '/styles/landing-slide-manager.css' ];
const metadata = '';
const mainTemplateKey = 'landing-slide-manager.main.html';

export const renderLandingSlideManager = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
