import { Page } from '../../../models/page.interface';
import { templates } from '../../../service/container';

const path = '/admin/email';
const title = 'email testing page';
const scripts: string[] = [ '/scripts/email-edge.js' ];
const styles: string[] = [ '/styles/email-edge.css' ];
const metadata = '';
const mainTemplateKey = 'email-edge.main.html';

export const renderEmailTestPage = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
