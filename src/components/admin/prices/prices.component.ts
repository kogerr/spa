import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/prices';
const title = 'Prices';
const scripts: string[] = [ '/scripts/admin/prices.js' ];
const styles: string[] = [ '/styles/prices.css' ];
const metadata = '';
const mainTemplateKey = 'prices.component.main.html';

export const renderPricesComponent = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
