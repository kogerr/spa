import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/sandbox';
const title = 'Sandbox';
const scripts: string[] = [ '/scripts/admin/sandbox.js' ];
const styles: string[] = [ '/styles/sandbox.css' ];
const metadata = '';
const mainTemplateKey = 'sandbox.main.html';

export const renderSandboxPage = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
