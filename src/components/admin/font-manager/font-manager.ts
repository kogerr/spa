import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/font-manager';
const title = 'Font Manager';
const scripts: string[] = [ '/scripts/admin/font-manager.js' ];
const styles: string[] = [ '/styles/font-manager.css' ];
const metadata = '';
const mainTemplateKey = 'font-manager.main.html';

export const renderFontManagerPage = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
