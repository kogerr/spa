import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/projects';
const title = 'Projects';
const scripts: string[] = [ '/scripts/admin/project-manager.js' ];
const styles: string[] = [ '/styles/project-manager.css' ];
const metadata = '';
const mainTemplateKey = 'project-manager.main.html';

export const renderProjectManager = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
