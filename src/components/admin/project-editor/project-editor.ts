import { templates } from '../../../service/container';
import { Page } from '../../../models/page.interface';

const path = '/admin/project-editor';
const title = 'Project Editor';
const scripts: string[] = [ '/scripts/admin/project-editor.js' ];
const styles: string[] = [ '/styles/project-editor.css' ];
const metadata = '';
const mainTemplateKey = 'project-editor.main.html';

export const renderProjectEditor = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
