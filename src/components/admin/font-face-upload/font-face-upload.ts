import { Page } from '../../../models/page.interface';
import { templates } from '../../../service/container';

const path = '/admin/font-face-upload';
const title = 'font face upload page';
const scripts: string[] = [ '/scripts/admin/font-face-upload.js' ];
const styles: string[] = [ '/styles/font-face-upload.css' ];
const metadata = '';
const mainTemplateKey = 'font-face-upload.main.html';

export const renderFontFaceUploadPage = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
