import { templates } from '../../service/container';
import { renderInvoiceItem } from './invoice-request-item/invoice-request-item';
import { InvoiceContext } from '../../models/invoice-context.interface';

const mainTemplateKey = 'invoice-request.html';

export const renderInvoiceRequest = (context: InvoiceContext) => {
    const items = context.items.map(item => renderInvoiceItem(item)).join('');
    return eval(templates.get(mainTemplateKey) as string) as string;
};
