import { InvoiceItem } from '../../../models/invoice-item.interface';
import { templates } from '../../../service/container';

const mainTemplateKey = 'invoice-request-item.html';

export const renderInvoiceItem = (item: InvoiceItem) => eval(templates.get(mainTemplateKey) as string) as string;
