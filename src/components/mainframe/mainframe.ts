import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';

const templateKey = 'mainframe.html';
const hash = process.env.CI_COMMIT_SHORT_SHA ?? '';

const getStyleTags = (styles: string[]) => styles.map((path) => `<link href="${path}" rel="stylesheet">`).join('\n');

export const renderMainframe = (page: Page) => {
    const [ main, metadata, title, scripts, path, styles ] = [ page.main, page.metadata, page.title, page.scripts, page.path, page.styles ];
    const content = eval(templates.get(templateKey) as string) as string;

    return { path: page.path, content};
};
