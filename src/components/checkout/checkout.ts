import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';

const path = '/checkout';
const title = 'Checkout';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'checkout.main.html';
const metadataTemplateKey = 'checkout.metadata.html';

export const renderCheckout = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
