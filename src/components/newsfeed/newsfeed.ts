import { DatedTemplate } from '../../models/dated-template.interface';
import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';
import { getNewsPosts } from '../../service/providers/newspost.provider';

const path = '/newsfeed';
const title = 'newsfeed';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'newsfeed.main.html';
const metadataTemplateKey = 'newsfeed.metadata.html';

const sortPosts = (newsPosts: DatedTemplate[]) =>
    newsPosts
        .sort((a, b) => b.date - a.date)
        .map((post: DatedTemplate) => post.template)
        .join('');

export const renderNewsFeed = async (): Promise<Page> => {
    const newsPosts = await getNewsPosts();
    const posts = sortPosts(newsPosts);

    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
