import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';
import { PaymentResult } from '../../models/payment-result.interface';

const title = 'Invalid Order';
const path = 'invalid';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'invalid-order.main.html';
const metadata = '';

export const renderInvalidOrder = (paymentResult: PaymentResult): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
