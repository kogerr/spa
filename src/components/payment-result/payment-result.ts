import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';
import { Order } from '../../models/order.interface';

const path = 'invalid';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'payment-result.main.html';
const metadata = '';

export const renderPaymentResult = (order: Order, status: string): Page => {
    const title = 'Payment ' + status;
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
