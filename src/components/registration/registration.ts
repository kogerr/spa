import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';

const path = '/registration';
const title = 'Registration';
const scripts: string[] = [ '/scripts/login.js' ];
const styles: string[] = [];
const mainTemplateKey = 'registration.main.html';
const metadataTemplateKey = 'registration.metadata.html';

export const renderRegistration = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
