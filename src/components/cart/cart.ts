import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';

const path = '/cart';
const title = 'Cart';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'cart.main.html';
const metadataTemplateKey = 'cart.metadata.html';

export const renderCart = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
