import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';

const path = '/philosophy';
const title = 'philosophy';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'philosophy.main.html';
const metadataTemplateKey = 'philosophy.metadata.html';

export const renderPhilosophy = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
