import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';

const title = '404 Page Not Found';
const path = '404';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = '404.main.html';
const metadata = '';

export const render404 = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
