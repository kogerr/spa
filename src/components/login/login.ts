import { Page } from '../../models/page.interface';
import { templates } from '../../service/container';

const path = '/login';
const title = 'Login';
const scripts: string[] = [];
const styles: string[] = [];
const mainTemplateKey = 'login.main.html';
const metadataTemplateKey = 'login.metadata.html';

export const renderLogin = (): Page => {
    const main = eval(templates.get(mainTemplateKey) as string) as string;
    const metadata = eval(templates.get(metadataTemplateKey) as string) as string;

    return { main, metadata, path, title, scripts, styles };
};
