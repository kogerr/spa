import { templates } from '../../service/container';

const templateKey = 'example-email.html';

export const renderExampleEmail = (name: string): string =>
    eval(templates.get(templateKey) as string) as string;
