import { initDb } from './src/db/mongo';
import { initContainer } from './src/service/loader/container.service';
import { initServer } from './src/service/server/server';

initDb()
    .then(initContainer)
    .then(initServer)
    .catch(console.error);

process.on('uncaughtException', console.error);
