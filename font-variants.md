# font-variant-alternates

`"aalt"` - All Alternates

* normal (none)
* historical - `"hist"`
* stylistic(): for individual chars - `"salt" 1`, `"salt" 2`
* styleset: alternates for sets of chars. font-specific - `"ss01"`, `"ss02"`
* character-variant(): like styleset but not coherent - `"cv02"`
* swash(): pretty serif-like things - `swsh 2`, `cswh 2`
* ornaments(): like glyphs - `ornm 2`

# font-variant-ligatures
* none
* normal
* common-ligatures - `"liga"`, `"clig"`
* discretionary-ligatures - `"dlig"`
* historical-ligatures - `"hlig"`
* contextual - `"calt"`

Probably just switch between none and common.

# font-variant-numeric
* normal (none)
* ordinal (eg. 1st, 2nd, 1a - primera) - `"ordn"`
* slashed zero - `"zero"`
* numeric-figure-values:
    * lining-nums: all on baseline - `"lnum"`
    * oldstyle-nums - `"onum"`
* numeric-spacing-values:
    * proportional-nums - `"pnum"`
    * tabular-nums: like monospace - `"tnum"`
* numeric-fraction-values:
    * diagonal-fractions - `"frac"`
    * stacked-fractions - `"afrc"`

## font-feature-settings
