const path = require('path');

module.exports = {
    entry: './build/src/assets/scripts/init.js',
    module: {},
    resolve: {
        extensions: [ '.js' ],
    },
    output: {
        filename: `script${process.env.CI_COMMIT_SHORT_SHA ?? ''}.js`,
        path: path.resolve(__dirname, './build/src/assets/scripts'),
        environment: {
            // The environment supports arrow functions ('() => { ... }').
            arrowFunction: true,
            // The environment supports BigInt as literal (123n).
            bigIntLiteral: true,
            // The environment supports const and let for variable declarations.
            const: true,
            // The environment supports destructuring ('{ a, b } = obj').
            destructuring: true,
            // The environment supports an async import() function to import EcmaScript modules.
            dynamicImport: false,
            // The environment supports 'for of' iteration ('for (const x of array) { ... }').
            forOf: true,
            // The environment supports ECMAScript Module syntax to import ECMAScript modules (import ... from '...').
            module: true,
        }
    },
};
